package filetests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import bonuses.Bonus;
import bonuses.ChooseCityBonus;
import bonuses.DrawPoliticsCard;
import bonuses.GiveCoins;
import bonuses.GivePoints;
import file.Lettore;
import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.Type;

public class LettoreTest {

	@Test
	public void testLettore() {
		Lettore lettore=new Lettore();
		assertEquals(false,lettore.equals(null));
	}

	@Test
	public void testReadNobilityScale() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		ArrayList<Bonus> nobilityScale=new ArrayList<>();
		GiveCoins b1=new GiveCoins(2);
		GivePoints b2=new GivePoints(2);
		nobilityScale.add(b1);
		nobilityScale.add(b2);
		
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		try {
			lettore.readNobilityScale(br);
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertEquals(GiveCoins.class,lettore.getNobilityScale().get(2).get(0).getClass());
		assertEquals(GivePoints.class,lettore.getNobilityScale().get(2).get(1).getClass());
	}

	@Test
	public void testReadCityName() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.readNobilityScale(br);
			ArrayList<String> names=lettore.readCityName(br);
			assertEquals("Burgen",names.get(1));
			assertEquals("Juvelar",names.get(9));
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testGetTokenPool() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.readNobilityScale(br);
			lettore.readTokenPool(br);
			assertEquals(GiveCoins.class,lettore.getTokenPool().get(2).getBonuses().get(0).getClass());
		
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testReadCityNeighbours() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.setMapConfiguration("AAA");
			lettore.readNobilityScale(br);
			ArrayList<String> names=lettore.readCityName(br);
			lettore.readTokenPool(br);
			ArrayList<City> cities=new ArrayList<>();
			for(String n:names){
				
				City c=new City(null,null,n);
				cities.add(c);
			}
			lettore.setPoolCities(cities);
			
			lettore.readCityNeighbours(br);
			assertEquals("Dorful",lettore.getPoolCities().get(3).getName());
			assertEquals("Burgen",lettore.getPoolCities().get(3).getNeighborhood().get(0).getName());
			assertEquals(Type.SEASIDE,lettore.getPoolCities().get(3).getType());
			
			
		} catch (NumberFormatException | IOException e) {
			
			e.printStackTrace();
		}
	}

	@Test
	public void testReadPlayerColors() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.readNobilityScale(br);
			ArrayList<String> names=lettore.readCityName(br);
			lettore.readTokenPool(br);
			ArrayList<City> cities=new ArrayList<>();
			for(String n:names){
				
				City c=new City(null,null,n);
				cities.add(c);
			}
			lettore.setPoolCities(cities);
			lettore.setMapConfiguration("AAA");
			lettore.readCityNeighbours(br);
			lettore.readPlayerColors(br);
			
			assertEquals(Color.WHITE,lettore.getPlayersColors().get(3));
			
		} catch (NumberFormatException | IOException e) {
			
			e.printStackTrace();
		}
	}

	@Test
	public void testReadCityKing() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.readNobilityScale(br);
			ArrayList<String> names=lettore.readCityName(br);
			lettore.readTokenPool(br);
			ArrayList<City> cities=new ArrayList<>();
			for(String n:names){
				
				City c=new City(null,null,n);
				cities.add(c);
			}
			lettore.setPoolCities(cities);
			lettore.setMapConfiguration("AAA");
			lettore.readCityNeighbours(br);
			lettore.readPlayerColors(br);
			lettore.readCityKing(br);
			
			assertEquals("Juvelar",lettore.getCityKing().getName());
			
		} catch (NumberFormatException | IOException e) {
			
			e.printStackTrace();
		}
	}

	@Test
	public void testReadNumberOfEmporium() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.readNobilityScale(br);
			ArrayList<String> names=lettore.readCityName(br);
			lettore.readTokenPool(br);
			ArrayList<City> cities=new ArrayList<>();
			for(String n:names){
				
				City c=new City(null,null,n);
				cities.add(c);
			}
			lettore.setPoolCities(cities);
			lettore.setMapConfiguration("AAA");
			lettore.readCityNeighbours(br);
			lettore.readPlayerColors(br);
			lettore.readCityKing(br);
			lettore.readNumberOfEmporium(br);
			
			
			assertEquals(10,lettore.getNumberOfRequiredEmporiums());
			
		} catch (NumberFormatException | IOException e) {
			
			e.printStackTrace();
		}
	}

	@Test
	public void testReadPermits() throws FileNotFoundException {
		Lettore lettore=new Lettore();
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
		try {
			lettore.readNobilityScale(br);
			ArrayList<String> names=lettore.readCityName(br);
			lettore.readTokenPool(br);
			
			ArrayList<City> cities=new ArrayList<>();
			for(String n:names){
				
				City c=new City(null,null,n);
				cities.add(c);
			}
			lettore.setPoolCities(cities);
			lettore.setMapConfiguration("AAA");
			lettore.readCityNeighbours(br);
			lettore.readPlayerColors(br);
			lettore.readCityKing(br);
			lettore.readNumberOfEmporium(br);
			lettore.readPermits(br);
			assertEquals("Esti",lettore.getDecks().get(1).get(0).getBuildPermissions().get(2).getName());
			assertEquals(DrawPoliticsCard.class,lettore.getDecks().get(1).get(0).getBonus().get(1).getClass());
			
			
			assertEquals("Kultos",lettore.getDecks().get(3).get(0).getBuildPermissions().get(0).getName());
			assertEquals(DrawPoliticsCard.class,lettore.getDecks().get(3).get(0).getBonus().get(3).getClass());
			
			
		} catch (NumberFormatException | IOException e) {
			
			e.printStackTrace();
		}
	}

	@Test
	public void testLoad() throws FileNotFoundException {
		
		Lettore lettore=new Lettore();;
		lettore.setMapConfiguration("AAA");
		lettore.load();
		
		assertEquals(ChooseCityBonus.class,lettore.getNobilityScale().get(4).get(0).getClass());
		
		assertEquals("Hellar",lettore.getPoolCities().get(7).getName());
		assertEquals("Merkatim",lettore.getPoolCities().get(7).getNeighborhood().get(2).getName());
		
	}

	@Test
	public void testGetPoolCities() {
		Lettore lettore=new Lettore();
		ArrayList<City> poolCities=new ArrayList<>();
		City c1=new City(poolCities, null, null);
		poolCities.add(c1);
		lettore.setPoolCities(poolCities);
		assertEquals(poolCities,lettore.getPoolCities());
	}

	@Test
	public void testSetPoolCities() {
		Lettore lettore=new Lettore();
		ArrayList<City> poolCities=new ArrayList<>();
		City c1=new City(poolCities, null, null);
		poolCities.add(c1);
		lettore.setPoolCities(poolCities);
		assertEquals(poolCities,lettore.getPoolCities());
	}

	@Test
	public void testToTypeShouldBeSeaside() {
		Lettore lettore=new Lettore();
		assertEquals(Type.SEASIDE,lettore.toType(1));
	}
	
	@Test
	public void testToTypeShouldBePlain() {
		Lettore lettore=new Lettore();
		assertEquals(Type.PLAIN,lettore.toType(2));
	}
	
	@Test
	public void testToTypeShouldBeMountain() {
		Lettore lettore=new Lettore();
		assertEquals(Type.MOUNTAIN,lettore.toType(3));
	}

	@Test
	public void testToColor() {
		Lettore lettore=new Lettore();
		assertEquals(Color.YELLOW, lettore.toColor("y"));
	}


	@Test
	public void testGetPlayersColors() {
		Lettore lettore=new Lettore();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		lettore.setPlayersColors(colors);
		assertEquals(Color.BLACK, lettore.getPlayersColors().get(0));
	}

	@Test
	public void testGetCityKing() {
		Lettore lettore=new Lettore();
		City city=new City(null, null, null);
		lettore.setCityKing(city);
		assertEquals(city,lettore.getCityKing());
	}
	
	@Test
	public void testSetCityKing() {
		Lettore lettore=new Lettore();
		City city=new City(null, null, null);
		lettore.setCityKing(city);
		assertEquals(city,lettore.getCityKing());
	}

	@Test
	public void testSetPlayersColors() {
		Lettore lettore=new Lettore();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		lettore.setPlayersColors(colors);
		assertEquals(Color.BLACK, lettore.getPlayersColors().get(0));
	}


	@Test
	public void testGetNobilityScale() {
		Lettore lettore=new Lettore();
		Map<Integer,ArrayList<Bonus>> scale=new HashMap();
		lettore.setNobilityScale(scale);
		assertEquals(scale,lettore.getNobilityScale());
	}

	@Test
	public void testGetDecks() {
		Lettore lettore=new Lettore();
		Map<Integer,ArrayList<PermissionCard>> scale=new HashMap();
		PermissionCard card=new PermissionCard(null, null, null);
		scale.put(0, new ArrayList<PermissionCard>());
		scale.get(0).add(card);
		lettore.setDecks(scale);
		assertEquals(scale,lettore.getDecks());
	}

	@Test
	public void testSetDecks() {
		Lettore lettore=new Lettore();
		Map<Integer,ArrayList<PermissionCard>> scale=new HashMap();
		PermissionCard card=new PermissionCard(null, null, null);
		scale.put(0, new ArrayList<PermissionCard>());
		scale.get(0).add(card);
		lettore.setDecks(scale);
		assertEquals(scale,lettore.getDecks());
	}

}
