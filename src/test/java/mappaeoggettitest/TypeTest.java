package mappaeoggettitest;

import static org.junit.Assert.*;

import org.junit.Test;

import mappaeoggetti.Type;

public class TypeTest {
	
	@Test
	public void testtoString(){
		String string = "MOUNTAIN";
		String string2 = "PLAIN";
		String string3 = "SEASIDE";

		
		assertEquals(string, Type.MOUNTAIN.toString());
		assertEquals(string2, Type.PLAIN.toString());
		assertEquals(string3, Type.SEASIDE.toString());
	}

}
