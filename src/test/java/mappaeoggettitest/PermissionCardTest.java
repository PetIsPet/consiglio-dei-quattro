package mappaeoggettitest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import bonuses.Bonus;
import mappaeoggetti.Assistant;
import mappaeoggetti.Card;
import mappaeoggetti.City;
import mappaeoggetti.PermissionCard;

public class PermissionCardTest {
	
	@Test
	public void testPermissionCard(){
		PermissionCard permissioncard = new PermissionCard(null, null, null);
		ArrayList<Bonus> bonus=new ArrayList<>();
		ArrayList<City> buildPermissions=new ArrayList<>();
		permissioncard.setBonus(bonus);
		assertEquals(bonus, permissioncard.getBonus());
		permissioncard.setBuildPermissions(buildPermissions);
		assertEquals(buildPermissions, permissioncard.getBuildPermissions());
	}
	
	@Test
	public void testgetId(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		permissionCard.setId(0);
		assertEquals(0, permissionCard.getId());
	}
	
	@Test
	public void testsetId(){
		PermissionCard permissioncard = new PermissionCard(null, null, null);
		permissioncard.setId(2);
		assertEquals(2, permissioncard.getId());
	}
	
	@Test
	public void testgetBuildPermissions(){
		ArrayList<City> buildPermissions=new ArrayList<>();
		PermissionCard permissioncard = new PermissionCard(null, null, buildPermissions);
		permissioncard.getBuildPermissions();
		assertEquals(buildPermissions, permissioncard.getBuildPermissions());
	}
	
	@Test
	public void testsetBuildPermissions(){
		ArrayList<City> buildPermissions=new ArrayList<>();
		ArrayList<City> buildPermissions2=new ArrayList<>();
		PermissionCard permissioncard = new PermissionCard(null, null, buildPermissions);
		permissioncard.setBuildPermissions(buildPermissions2);
		assertEquals(buildPermissions2, permissioncard.getBuildPermissions());
	}
	
	@Test //????
	public void testisAssistantShouldBeFalse(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		assertEquals(false, permissionCard.isAssistant());
	}
	
	@Test //????
	public void testisAssistantShouldBeTrue(){
		Assistant assistant = new Assistant();
		assertEquals(true, assistant.isAssistant());
	}
	
	@Test
	public void testisPermissionCardShouldBeFalse(){
		Assistant assistant = new Assistant();
		assertEquals(false, assistant.isPermissionCard());
	}
	
	@Test
	public void testisPermissionCardShouldBeTrue(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		assertEquals(true, permissionCard.isPermissionCard());

	}
	
	@Test
	public void testisCardShouldBeTrue(){
		Card card = new Card(null);
		
		assertEquals(true, card.isCard());
	}
	
	@Test
	public void testisCardShouldBeFalse(){
		Assistant assistant = new Assistant();
		
		assertEquals(false, assistant.isCard());
	}
	
	@Test
	public void testgetBonus(){
		ArrayList<Bonus> bonus=new ArrayList<>();
		PermissionCard permissioncard = new PermissionCard(bonus, null, null);
		permissioncard.getBonus();
		assertEquals(bonus, permissioncard.getBonus());
	}
	
	@Test
	public void testsetBonus(){
		ArrayList<Bonus> bonus=new ArrayList<>();
		ArrayList<Bonus> bonus2=new ArrayList<>();
		PermissionCard permissioncard = new PermissionCard(bonus, null, null);
		permissioncard.setBonus(bonus2);
		assertEquals(bonus2, permissioncard.getBonus());
	}

}
