package mappaeoggettitest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import org.junit.Test;

import gamemanagement.Player;
import mappaeoggetti.Card;

public class CardDeckTest {
	
	@Test
	public void testCardDeck(){
		 ArrayList<Color> colorPool = new ArrayList<>();
		 ArrayList<Color> colors = new ArrayList<>();
		 	colorPool.add(Color.BLACK);
			colorPool.add(Color.BLUE);
			colorPool.add(Color.YELLOW);
			colorPool.add(Color.MAGENTA);
			colors.add(Color.BLACK);
			colors.add(Color.BLUE);
			boolean flag = false;
			for(Color c: colors){
				for(int index=0;index<colorPool.size();index++)
					if(c.equals(colorPool.get(index)));
						flag = true;
			}
			
			assertEquals(true, flag);
	}
	
	@Test
	public void testdrawCard(){
		ArrayList<Color> colorPool = new ArrayList<>();
		colorPool.add(Color.BLACK);
		colorPool.add(Color.BLUE);
		colorPool.add(Color.YELLOW);
		colorPool.add(Color.MAGENTA);
		Player player = new Player(0, null, null);
		Random random = new Random();
		int num = random.nextInt(colorPool.size()-1);
		Card card = new Card(colorPool.get(num));
		player.getCards().add(card);
		ArrayList<Card> playerCards = player.getCards();
		int n = player.getCards().size()-1;
		assertEquals(card, playerCards.get(n));
	}

}
