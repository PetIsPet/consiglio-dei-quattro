package mappaeoggettitest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.Emporium;
import mappaeoggetti.Type;

public class CityTest {
	
	 @Test
	 public void testCty(){
		 List<City> neighborhood=new ArrayList<City>();
		 City city = new City(null, null, null);
		 City city2 = new City(null, null, null);
		 neighborhood.add(city2);
		 city.setNeighborhood(neighborhood);
		 assertEquals(neighborhood, city.getNeighborhood());
		 CityToken cityToken = null;
		 city.setCityToken(cityToken);
		 assertEquals(cityToken,city.getCityToken());
		 city.setName("Alba");
		 assertEquals("Alba", city.getName());
	 }
	 
	 @Test
	 public void testCity2(){
		 List<City> neighborhood=new ArrayList<City>();
		 City city = new City(null, null, null, 0);
		 City city2 = new City(null, null, null, 0);
		 neighborhood.add(city2);
		 city.setNeighborhood(neighborhood);
		 assertEquals(neighborhood, city.getNeighborhood());
		 CityToken cityToken = new CityToken(null);
		 city.setCityToken(cityToken);
		 assertEquals(cityToken,city.getCityToken());
		 city.setName("Alba");
		 assertEquals("Alba", city.getName());
	 }
	 
	 @Test
	 public void testgetEmporiums(){
		 List<Emporium> emporium=new ArrayList<Emporium>();
		 Emporium emp1 = new Emporium(null);
		 Emporium emp2 = new Emporium(null);
		 Emporium emp3 = new Emporium(null);
		 emporium.add(emp1);
		 emporium.add(emp2);
		 emporium.add(emp3);
		 assertEquals(emp1, emporium.get(0));
	 }
	 
	 @Test
	 public void testsetEmporium(){
		 List<Emporium> emporium=new ArrayList<Emporium>();
		 Emporium emp1 = new Emporium(null);
		 Emporium emp2 = new Emporium(null);
		 Emporium emp3 = new Emporium(null);
		 emporium.add(emp1);
		 emporium.add(emp2);
		 emporium.add(emp3);
		 emporium.set(0, emp3);
		 assertEquals(emp3, emporium.get(0));
	 }
	 
	 @Test
	 public void testgetName(){
		 City city = new City(null, null, "Alba");
		 assertEquals("Alba", city.getName());
	 }
	 
	 @Test
	 public void testsetName(){
		 City city = new City(null, null, "Fesso");
		 city.setName("Alba");
		 assertEquals("Alba", city.getName());
	 }
	 
	 @Test
	 public void testgetNeighborhood(){
		 List<City> neighborhood=new ArrayList<City>();
		 City city = new City(neighborhood, null, null);
		 assertEquals(neighborhood, city.getNeighborhood());
	 }
	 
	 @Test
	 public void testsetNeighborhood(){
		 List<City> neighborhood=new ArrayList<City>();
		 City city = new City(neighborhood, null, null);
		 List<City> neighborhood2 = new ArrayList<City>();
		 city.setNeighborhood(neighborhood2);
		 assertEquals(neighborhood2, city.getNeighborhood());
	 }
	 
	 @Test
	 public void testgetcityToken(){
		 CityToken cityToken = null;
		 City city = new City(null, cityToken, null);
		 assertEquals(cityToken, city.getCityToken());
	 }
	 
	 @Test
	 public void testsetcityToken(){
		 CityToken cityToken = null;
		 CityToken cityToken2 = null;
		 City city = new City(null, cityToken, null);
		 city.setCityToken(cityToken2);
		 assertEquals(cityToken2, city.getCityToken());
	 }
	 
	 @Test 
	 public void testgetType(){
		 City city = new City(null, null, null);
		 Type type = Type.MOUNTAIN;
		 city.setType(type);
			assertEquals(type, city.getType());
	 }
	 
	 @Test 
	 public void testsetType(){
		 City city = new City(null, null, null);
		 Type type = Type.MOUNTAIN;
		 Type type1 = Type.PLAIN;
		 city.setType(type);
			assertEquals(type, city.getType());
			city.setType(type1);
			assertEquals(type1, city.getType());
	 }
	 
	 @Test 
	 public void testisControlled(){
		 City city = new City(null, null, null);
		 Boolean controlled = false;
		 city.setControlled(controlled);
		 assertEquals(false, city.isControlled());
	 }
	 
	 @Test 
	 public void testsetControlled(){
		 City city = new City(null, null, null);
		 Boolean controlled = false;
		 Boolean controlled1 = true;
		 city.setControlled(controlled);
		 assertEquals(false, city.isControlled());
		 city.setControlled(controlled1);
		 assertEquals(true, city.isControlled());
		 
	 }

}