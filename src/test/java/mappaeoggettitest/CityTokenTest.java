package mappaeoggettitest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import bonuses.Bonus;
import bonuses.GiveCoins;
import bonuses.GivePoints;
import mappaeoggetti.CityToken;

public class CityTokenTest {
	
	@Test
	public void testCityToken(){
		String string = new String();
		CityToken cityToken = new CityToken(string);
		assertEquals(string, cityToken.getPath());
	}
	
	@Test
	public void testgetBonuses(){
		ArrayList<Bonus> bonuses = new ArrayList<>();
		ArrayList<Bonus> bonuses2 = new ArrayList<>();
		Bonus bonus = new GiveCoins(0);
		Bonus bonus1 = new GivePoints(0);
		bonuses.add(bonus);
		bonuses.add(bonus1);
		bonuses2.add(bonus);
		bonuses2.add(bonus1);
		CityToken cityToken = new CityToken(null);
		cityToken.setBonuses(bonuses2);
		assertEquals(bonuses, cityToken.getBonuses());
	}
	
	@Test
	public void testsetBonuses(){
		ArrayList<Bonus> bonuses = new ArrayList<>();
		ArrayList<Bonus> bonuses2 = new ArrayList<>();
		ArrayList<Bonus> bonuses3 = new ArrayList<>();
		Bonus bonus = new GiveCoins(0);
		Bonus bonus1 = new GivePoints(0);
		bonuses.add(bonus);
		bonuses.add(bonus1);
		bonuses2.add(bonus);
		bonuses2.add(bonus1);
		bonuses3.add(bonus);
		bonuses3.add(bonus1);
		CityToken cityToken = new CityToken(null);
		cityToken.setBonuses(bonuses2);
		assertEquals(bonuses, cityToken.getBonuses());
		cityToken.setBonuses(bonuses3);
		assertEquals(bonuses, cityToken.getBonuses());
	}
	
	@Test
	public void testgetPath(){
		String string = new String();
		CityToken cityToken = new CityToken(string);
		assertEquals(string, cityToken.getPath());	
	}
	
	@Test
	public void testsetPath(){
		String string = new String();
		String string2 = new String();
		CityToken cityToken = new CityToken(string);
		cityToken.setPath(string2);
		assertEquals(string2, cityToken.getPath());
	}

}
