package mappaeoggettitest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

import mappaeoggetti.Balcony;
import mappaeoggetti.Councelor;
import mappaeoggetti.Type;
import mvc.Game;


public class BalconyTest {
	
	@Test 
	public void testBalcony(){
		ArrayList<Councelor> pool=new ArrayList<>();
		Game game = new Game(null, null);
		Councelor co1 = new Councelor(Color.BLACK);
		Councelor co2 = new Councelor(Color.WHITE);
		Councelor co3 = new Councelor(Color.PINK);
		Councelor co4 = new Councelor(Color.GREEN);	
		pool.add(co1);
		pool.add(co2);
		pool.add(co3);
		pool.add(co4);
		game.setCouncelorsPool(pool);
		Balcony balcony = new Balcony(game,Type.MOUNTAIN);
		assertEquals(co1, balcony.getCouncelors()[0]);	
		assertEquals(game, balcony.getGame());
		assertEquals(Type.MOUNTAIN, balcony.getType());
		
	}
	
	@Test
	public void testBalconyKing(){
		ArrayList<Councelor> pool=new ArrayList<>();
		Game game = new Game(null, null);
		Councelor co1 = new Councelor(Color.BLACK);
		Councelor co2 = new Councelor(Color.WHITE);
		Councelor co3 = new Councelor(Color.PINK);
		Councelor co4 = new Councelor(Color.GREEN);	
		pool.add(co1);
		pool.add(co2);
		pool.add(co3);
		pool.add(co4);
		game.setCouncelorsPool(pool);
		Balcony balconyKing = new Balcony(game);
		assertEquals(co1, balconyKing.getCouncelors()[0]);
		assertEquals(co2, balconyKing.getCouncelors()[1]);
		assertEquals(game, balconyKing.getGame());
		
	}
	
	@Test
	public void testappend(){
		ArrayList<Councelor> pool=new ArrayList<>();
		Game game = new Game(null, null);
		Councelor co1 = new Councelor(Color.BLACK);
		Councelor co2 = new Councelor(Color.WHITE);
		Councelor co3 = new Councelor(Color.PINK);
		Councelor co4 = new Councelor(Color.GREEN);	
		pool.add(co1);
		pool.add(co2);
		pool.add(co3);
		pool.add(co4);
		game.setCouncelorsPool(pool);
		Balcony balcony = new Balcony(game);
		balcony.append(co1);
		assertEquals(co1, balcony.getCouncelors()[3]);
		assertEquals(false, game.getCouncelorsPool().contains(co1));
	}
	
	@Test
	public void testgetCouncelor(){
		ArrayList<Councelor> councelors=new ArrayList<>();
		Councelor co1 = new Councelor(null);
		Councelor co2 = new Councelor(null);
		Councelor co3 = new Councelor(null);
		councelors.add(co1);
		councelors.add(co2);
		councelors.add(co3);
		assertEquals(co1, councelors.get(0));	
	}
	
	@Test
	public void testsetCouncelor(){
		ArrayList<Councelor> councelors=new ArrayList<>();
		Councelor co1 = new Councelor(null);
		Councelor co2 = new Councelor(null);
		Councelor co3 = new Councelor(null);
		councelors.add(co1);
		councelors.add(co2);
		councelors.add(co3);
		councelors.set(0, co3);
		assertEquals(co3, councelors.get(0));
	}
	

	 @Test
	 public void testgetType(){
		 ArrayList<Councelor> councelors = new ArrayList<>();
		 Game game = new Game(null, null);
		 Councelor co1 = new Councelor(null);
		 Councelor co2 = new Councelor(null);
		 Councelor co3 = new Councelor(null);
		 Councelor co4 = new Councelor(null);
		 councelors.add(co1);
		 councelors.add(co2);
		 councelors.add(co3);
		 councelors.add(co4);
		 game.setCouncelorsPool(councelors);
		 Type type = Type.PLAIN;
		 Balcony balcony = new Balcony(game, type);
		 assertEquals(type, balcony.getType());
	 }
	 
	 @Test
	 public void testsetType(){
		 ArrayList<Councelor> councelors = new ArrayList<>();
		 Game game = new Game(null, null);
		 Councelor co1 = new Councelor(null);
		 Councelor co2 = new Councelor(null);
		 Councelor co3 = new Councelor(null);
		 Councelor co4 = new Councelor(null);
		 councelors.add(co1);
		 councelors.add(co2);
		 councelors.add(co3);
		 councelors.add(co4);
		 game.setCouncelorsPool(councelors);
		 Type type = Type.PLAIN;
		 Balcony balcony = new Balcony(game, type);
		 balcony.setType(Type.MOUNTAIN);
		 assertEquals(Type.MOUNTAIN, balcony.getType());
	 }
	 
	 @Test
	 public void testgetGame(){
		 ArrayList<Councelor> councelors = new ArrayList<>();
		 Game game = new Game(null, null);
		 Councelor co1 = new Councelor(null);
		 Councelor co2 = new Councelor(null);
		 Councelor co3 = new Councelor(null);
		 Councelor co4 = new Councelor(null);
		 councelors.add(co1);
		 councelors.add(co2);
		 councelors.add(co3);
		 councelors.add(co4);
		 game.setCouncelorsPool(councelors);
		 Balcony balcony = new Balcony(game);
		 assertEquals(game, balcony.getGame()); 
	 }
	 
	 @Test
	 public void testsetGame(){
		 ArrayList<Councelor> councelors = new ArrayList<>();
		 Game game = new Game(null, null);
		 Game game1 = new Game(null, null);
		 Councelor co1 = new Councelor(null);
		 Councelor co2 = new Councelor(null);
		 Councelor co3 = new Councelor(null);
		 Councelor co4 = new Councelor(null);
		 councelors.add(co1);
		 councelors.add(co2);
		 councelors.add(co3);
		 councelors.add(co4);
		 game.setCouncelorsPool(councelors);
		 Balcony balcony = new Balcony(game);
		 balcony.setGame(game1);
		 assertEquals(game1, balcony.getGame());
	 }

}
