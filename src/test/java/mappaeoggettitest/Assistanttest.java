package mappaeoggettitest;

import static org.junit.Assert.*;

import org.junit.Test;

import mappaeoggetti.Assistant;
import mappaeoggetti.Card;
import mappaeoggetti.PermissionCard;

public class Assistanttest {

	@Test //????
	public void testisAssistantShouldBeFalse(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		assertEquals(false, permissionCard.isAssistant());
	}
	
	@Test //????
	public void testisAssistantShouldBeTrue(){
		Assistant assistant = new Assistant();
		assertEquals(true, assistant.isAssistant());
	}
	
	@Test
	public void testisPermissionCardShouldBeFalse(){
		Assistant assistant = new Assistant();
		assertEquals(false, assistant.isPermissionCard());
	}
	
	@Test
	public void testisPermissionCardShouldBeTrue(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		assertEquals(true, permissionCard.isPermissionCard());

	}
	
	@Test
	public void testisCardShouldBeTrue(){
		Card card = new Card(null);
		
		assertEquals(true, card.isCard());
	}
	
	@Test
	public void testisCardShouldBeFalse(){
		Assistant assistant = new Assistant();
		
		assertEquals(false, assistant.isCard());
	}

}
