package mappaeoggettitest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

import mappaeoggetti.Councelor;
import mappaeoggetti.CouncelorsColor;

public class CouncelorColorsTest {
	
	@Test
	public void testCouncelorColor(){
		ArrayList<Color> colors = new ArrayList<>();
		CouncelorsColor councelorsColor = new CouncelorsColor(colors, 0);
		assertEquals(colors, councelorsColor.getColorPool());
	}
	
	@Test
	public void testcreateCouncelors(){
		ArrayList<Color> colors = new ArrayList<>();
		Color co1 = Color.BLACK;
		Color co2 = Color.BLUE;
		colors.add(co1);
		colors.add(co2);
		ArrayList<Councelor> councelorPool = new ArrayList<>();
		ArrayList<Councelor> councelorPool1 = new ArrayList<>();
		Councelor councelor = new Councelor(Color.BLUE);
		Councelor councelor1 = new Councelor(Color.BLACK);
		councelorPool.add(councelor);
		councelorPool.add(councelor1);
		CouncelorsColor councelorsColor = new CouncelorsColor(colors, 0);
		councelorsColor.setCouncelorsPool(councelorPool1);
		assertEquals(councelorPool1 , councelorsColor.createCouncelors());
	}
	
	@Test
	public void testgetColorPool(){
		ArrayList<Color> colors = new ArrayList<>();
		CouncelorsColor councelorsColor = new CouncelorsColor(colors, 0);
		assertEquals(colors, councelorsColor.getColorPool());
	}
	
	@Test
	public void testsetColorPool(){
		ArrayList<Color> colors = new ArrayList<>();
		ArrayList<Color> colors2 = new ArrayList<>();
		CouncelorsColor councelorsColor = new CouncelorsColor(colors, 0);
		councelorsColor.setColorPool(colors2);
		assertEquals(colors2, councelorsColor.getColorPool());
	}
	
	@Test
	public void testgetCouncelorPool(){
		ArrayList<Councelor> councelorsPool1 = new ArrayList<>();
		ArrayList<Councelor> councelorsPool2 = new ArrayList<>();
		Councelor councelor1 = new Councelor(null);
		Councelor councelor2 = new Councelor(null);
		councelorsPool1.add(councelor1);
		councelorsPool1.add(councelor2);
		councelorsPool2.add(councelor1);
		councelorsPool2.add(councelor2);
		CouncelorsColor councelorsColor = new CouncelorsColor(null, 0);
		councelorsColor.setCouncelorsPool(councelorsPool2);
		assertEquals(councelorsPool1, councelorsColor.getCouncelorsPool());
	}
	
	@Test
	public void testsetCouncelorPool(){
		ArrayList<Councelor> councelorsPool1 = new ArrayList<>();
		ArrayList<Councelor> councelorsPool2 = new ArrayList<>();
		ArrayList<Councelor> councelorsPool3 = new ArrayList<>();
		Councelor councelor1 = new Councelor(null);
		Councelor councelor2 = new Councelor(null);
		councelorsPool1.add(councelor1);
		councelorsPool1.add(councelor2);
		councelorsPool2.add(councelor1);
		councelorsPool2.add(councelor2);
		councelorsPool3.add(councelor1);
		councelorsPool3.add(councelor2);
		CouncelorsColor councelorsColor = new CouncelorsColor(null, 0);
		councelorsColor.setCouncelorsPool(councelorsPool2);
		assertEquals(councelorsPool1, councelorsColor.getCouncelorsPool());
		councelorsColor.setCouncelorsPool(councelorsPool3);
		assertEquals(councelorsPool1, councelorsColor.getCouncelorsPool());	
	}
	
	@Test
	public void testgetColorsNumber(){
		int colorsNumber = 0;
		CouncelorsColor councelorsColor = new CouncelorsColor(null, colorsNumber);
		assertEquals(0, councelorsColor.getColorsNumber());
	}
	
	@Test
	public void testsetColorsNumber(){
		int colorsNumber = 0;
		CouncelorsColor councelorsColor = new CouncelorsColor(null, colorsNumber);
		councelorsColor.setColorsNumber(3);
		assertEquals(3, councelorsColor.getColorsNumber());
	}

}
