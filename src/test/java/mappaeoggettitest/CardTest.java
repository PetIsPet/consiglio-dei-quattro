package mappaeoggettitest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

import mappaeoggetti.Assistant;
import mappaeoggetti.Card;
import mappaeoggetti.PermissionCard;

public class CardTest {

	@Test
	public void testCard(){
		Card card = new Card(Color.BLACK);
		card.setColor(Color.BLUE);
		assertEquals(Color.BLUE, card.getColor());
	}
	
	@Test
	public void testgetColor(){
		 ArrayList<Color> colors=new ArrayList<>();
			colors.add(Color.BLACK);
			colors.add(Color.BLUE);
			colors.add(Color.YELLOW);
			colors.add(Color.MAGENTA);
			Card card = new Card(Color.BLACK);
		 assertEquals(Color.BLACK, card.getColor());
	}
	
	@Test
	public void testsetColor(){
		 ArrayList<Color> colors=new ArrayList<>();
			colors.add(Color.BLACK);
			colors.add(Color.BLUE);
			colors.add(Color.YELLOW);
			colors.add(Color.MAGENTA);
			Card card = new Card(Color.BLACK);
			card.setColor(Color.CYAN);
		 assertEquals(Color.CYAN, card.getColor());
	}
	
	@Test //????
	public void testisAssistantShouldBeFalse(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		assertEquals(false, permissionCard.isAssistant());
	}
	
	@Test //????
	public void testisAssistantShouldBeTrue(){
		Assistant assistant = new Assistant();
		assertEquals(true, assistant.isAssistant());
	}
	
	@Test
	public void testisPermissionCardShouldBeFalse(){
		Assistant assistant = new Assistant();
		assertEquals(false, assistant.isPermissionCard());
	}
	
	@Test
	public void testisPermissionCardShouldBeTrue(){
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		assertEquals(true, permissionCard.isPermissionCard());

	}
	
	@Test
	public void testisCardShouldBeTrue(){
		Card card = new Card(null);
		
		assertEquals(true, card.isCard());
	}
	
	@Test
	public void testisCardShouldBeFalse(){
		Assistant assistant = new Assistant();
		
		assertEquals(false, assistant.isCard());
	}
}
