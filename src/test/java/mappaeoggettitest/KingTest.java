package mappaeoggettitest;

import static org.junit.Assert.*;

import org.junit.Test;

import mappaeoggetti.City;
import mappaeoggetti.King;

public class KingTest {
	
	@Test
	public void testKing(){
		City city = new City(null, null, null);
		King king = new King(city);
		assertEquals(city, king.getCurrentCity());
	}
	
	@Test
	public void testgetCurrentCity(){
		City city = new City(null, null, null);
		King king = new King(city);
		assertEquals(city, king.getCurrentCity());
	}
	
	@Test
	public void testsetCurrentCity(){
		City city = new City(null, null, null);
		City city2 = new City(null, null, null);
		King king = new King(city);
		king.setCurrentCity(city2);
		assertEquals(city2, king.getCurrentCity());
	}

}
