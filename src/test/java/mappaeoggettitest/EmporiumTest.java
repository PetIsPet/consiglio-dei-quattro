package mappaeoggettitest;

import static org.junit.Assert.*;

import org.junit.Test;

import gamemanagement.Player;
import mappaeoggetti.Emporium;

public class EmporiumTest {
	
	@Test
	public void testEmporium(){
		Player player = new Player(0, null, null);
		Player player2 = new Player(0, null, null);
		Emporium emporium = new Emporium(player);
		emporium.setPlayer(player2);
		assertEquals(player2, emporium.getPlayer());	
	}
	
	@Test
	public void testgetPlayer(){
		Player player = new Player(0, null, null);
		Emporium emporium = new Emporium(player);
		assertEquals(player, emporium.getPlayer());
	}
	
	@Test
	public void testsetPlayer(){
		Player player = new Player(0, null, null);
		Player player2 = new Player(0, null, null);
		Emporium emporium = new Emporium(player);
		emporium.setPlayer(player2);
		assertEquals(player2, emporium.getPlayer());
	}

}
