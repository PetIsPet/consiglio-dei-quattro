package mappaeoggettitest;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import org.junit.Test;

import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mappaeoggetti.Type;

public class PermissionDeckTest {
	
	@Test
	public void testPermissionDeck(){
		Type type = Type.MOUNTAIN;
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c1);
		cards.add(c2);
		PermissionDeck deck = new PermissionDeck(type, cards);
		assertEquals(type, deck.getType());
		assertEquals(cards, deck.getCards());
	}
	
	@Test
	public void testdiscover(){
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		PermissionCard c3=new PermissionCard(null, null, null);
		PermissionCard c4=new PermissionCard(null, null, null);
		cards.add(c1);
		cards.add(c2);
		cards.add(c3);
		cards.add(c4);
		PermissionDeck deck = new PermissionDeck(null, cards);
		ArrayList<PermissionCard> cards1 = new ArrayList<>();
		cards1.add(c1);
		cards1.add(c2);
		cards1.add(c3);
		cards1.add(c4);
		deck.setCards(cards1);
		deck.discover();
		assertEquals(c3, deck.getCards().get(0));
		assertEquals(c4, deck.getCards().get(1));
		
	}
	
	@Test
	public void testreplaceCard(){
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c1);
		cards.add(c2);
		PermissionDeck deck = new PermissionDeck(null, cards);
		ArrayList<PermissionCard> cards1 = new ArrayList<>();
		PermissionCard c3=new PermissionCard(null, null, null);
		PermissionCard c4=new PermissionCard(null, null, null);
		cards1.add(c3);
		cards1.add(c4);
		deck.setCards(cards1);
		assertEquals(c3, deck.replaceCard());
		assertEquals(c4, cards1.get(0));
	}
	
	@Test
	public void testGetDiscoveredCard() {
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		PermissionDeck deck = new PermissionDeck(null, cards);
		PermissionCard[] discoveredCards=new PermissionCard[2];
		discoveredCards[0]=new PermissionCard(null,null,null);
		discoveredCards[1]=new PermissionCard(null,null,null);
		deck.setDiscoveredCards(discoveredCards);
		assertArrayEquals(discoveredCards, deck.getDiscoveredCards());
	}
	
	@Test
	public void testSetDiscoveredCard() {
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		PermissionDeck deck = new PermissionDeck(null, cards);
		PermissionCard[] discoveredCards=new PermissionCard[2];
		discoveredCards[0]=new PermissionCard(null,null,null);
		discoveredCards[1]=new PermissionCard(null,null,null);
		deck.setDiscoveredCards(discoveredCards);
		assertArrayEquals(discoveredCards, deck.getDiscoveredCards());
		PermissionCard[] discoveredCards1=new PermissionCard[2];
		discoveredCards1[0]=new PermissionCard(null,null,null);
		discoveredCards1[1]=new PermissionCard(null,null,null);
		deck.setDiscoveredCards(discoveredCards1);
		assertArrayEquals(discoveredCards1, deck.getDiscoveredCards());
		
	}
		
	@Test
	public void testGetCards() {
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		PermissionDeck deck = new PermissionDeck(null, cards);
		assertEquals(cards, deck.getCards());
	}

	@Test
	public void testSetCards() {
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		PermissionDeck deck = new PermissionDeck(null, cards);
		assertEquals(cards, deck.getCards());
		ArrayList<PermissionCard> cards1 = new ArrayList<>();
		PermissionCard c3=new PermissionCard(null, null, null);
		PermissionCard c4=new PermissionCard(null, null, null);
		cards1.add(c3);
		cards1.add(c4);
		deck.setCards(cards1);
		assertEquals(cards1, deck.getCards());
		
	}
	
	@Test
	public void testgetType(){
		Type type = Type.MOUNTAIN;
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		PermissionDeck deck = new PermissionDeck(type, cards);
		assertEquals(type, deck.getType());
	}

}
