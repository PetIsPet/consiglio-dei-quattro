package mappaeoggettitest;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import mappaeoggetti.Councelor;

public class CouncelorTest {
	
	@Test
	public void testCouncelor(){
		Councelor councelor = new Councelor(null);
		councelor.setColor(Color.BLACK);
		assertEquals(Color.BLACK, councelor.getColor());
	}
	
	@Test
	public void testgetColor(){
		Councelor councelor = new Councelor(Color.GRAY);
		assertEquals(Color.GRAY, councelor.getColor());
	}
	
	@Test
	public void testsetColor(){
		Councelor councelor = new Councelor(Color.BLACK);
		councelor.setColor(Color.YELLOW);
		assertEquals(Color.YELLOW, councelor.getColor());
	}

}
