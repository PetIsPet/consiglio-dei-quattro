package gamemanagementandmvctest;

import static org.junit.Assert.*;

import org.junit.Test;

import gamemanagement.MarketTurn;
import gamemanagement.Player;

public class MarketTurnTest {
	
	@Test
	public void testMarketTurn(){
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		MarketTurn turn = new MarketTurn(player);
		assertEquals(player, turn.getPlayer());
		turn.setPlayer(player1);
		assertEquals(player1, turn.getPlayer());
	}
	
	@Test
	public void testgetPlayer(){
		Player player = new Player(0, null, null);
		MarketTurn turn = new MarketTurn(player);
		assertEquals(player, turn.getPlayer());
	}
	
	@Test
	public void testsetPlayer(){
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		MarketTurn turn = new MarketTurn(player);
		assertEquals(player, turn.getPlayer());
		turn.setPlayer(player1);
		assertEquals(player1, turn.getPlayer());
	}

}
