package gamemanagementandmvctest;

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

import actions.BuyPermissionCard;
import actions.EmployAssistant;
import actions.EmporiumConstruction;
import actions.PrimaryAction;
import bonuses.Bonus;
import bonuses.GiveAssistants;
import bonuses.GivePoints;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import gamemanagement.Turn;
import gamemanagement.TurnManager;
import mappaeoggetti.Balcony;
import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.Councelor;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mvc.Game;

public class TurnTest {
	
	@Test
	public void testTurn(){
		Player player = new Player(0, null, null);
		ArrayList<City> neighborhood = new ArrayList<>();
		City neighbour = new City(null, null, null);
		neighborhood.add(neighbour); 
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		cityToken.setBonuses(bonuses);
		City city = new City(neighborhood, cityToken, null);
		ArrayList<PrimaryAction> primaryActionPool = new ArrayList<>();
		EmporiumConstruction construction = new EmporiumConstruction(player, city, null, null, null);
		primaryActionPool.add(construction);
		Turn turn = new Turn(player);
		turn.setPrimaryActionPool(primaryActionPool);
		assertEquals(player, turn.getPlayer());
		assertEquals(construction, turn.getPrimaryActionPool().get(0));
	}
	
	@Test
	public void testprimaryAction(){
		Game game = new Game(null, null);
		TurnManager controller = new TurnManager(null, game);
		Player player = new Player(0, null, null);
		ArrayList<City> neighborhood = new ArrayList<>();
		City neighbour = new City(null, null, null);
		neighborhood.add(neighbour); 
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		cityToken.setBonuses(bonuses);
		City city = new City(neighborhood, cityToken, null);
		ArrayList<PrimaryAction> primaryActionPool = new ArrayList<>();
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard card = new PermissionCard(bonuses, null, null);
		PermissionCard card1 = new PermissionCard(bonuses, null, null);
		PermissionCard card2 = new PermissionCard(bonuses, null, null);
		cards.add(card);
		cards.add(card1);
		cards.add(card2);
		PermissionDeck deck = new PermissionDeck(null, cards);
		Councelor councelor = new Councelor(null);
		Councelor councelor1 = new Councelor(null);
		
	
		BuyPermissionCard buy = new BuyPermissionCard(player, deck, 0, null, null, null, controller);
		primaryActionPool.add(buy);
		Turn turn = new Turn(player);
		turn.setPrimaryActionPool(primaryActionPool);
		try {
			turn.primaryAction();
		} catch (NotEnoughResourcesException e) {
			
			e.printStackTrace();
		} catch (InvalidInputException e) {
			
			e.printStackTrace();
		}
		assertEquals(1, player.getPermits().size());
	}
	
	@Test
	public void testsecondaryAction(){
		Player player = new Player(0, null, null);
		player.setAssistants(0);
		player.setCoins(3);
		EmployAssistant employ = new EmployAssistant(player);
		Turn turn = new Turn(player);
		try {
			turn.secondaryAction(employ);
		} catch (NotEnoughResourcesException | InvalidDoAnotherPrimaryActionChoiceException e) {
			
			e.printStackTrace();
		}
		assertEquals(1, player.getAssistants());
		assertEquals(0, player.getCoins());
	}
	
	@Test
	public void testgetPlayer(){
		Player player = new Player(0, null, null);
		Turn turn = new Turn(player);
		assertEquals(player, turn.getPlayer());
	}
	
	@Test
	public void testsetPlayer(){
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		Turn turn = new Turn(player);
		assertEquals(player, turn.getPlayer());
		turn.setPlayer(player1);
		assertEquals(player1, turn.getPlayer());
	}
	
	@Test
	public void testgetPrimaryActionPool(){
		Player player = new Player(0, null, null);
		ArrayList<City> neighborhood = new ArrayList<>();
		City neighbour = new City(null, null, null);
		neighborhood.add(neighbour); 
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		cityToken.setBonuses(bonuses);
		City city = new City(neighborhood, cityToken, null);
		ArrayList<PrimaryAction> primaryActionPool = new ArrayList<>();
		EmporiumConstruction construction = new EmporiumConstruction(player, city, null, null, null);
		primaryActionPool.add(construction);
		Turn turn = new Turn(player);
		turn.setPrimaryActionPool(primaryActionPool);
		assertEquals(primaryActionPool, turn.getPrimaryActionPool());
	}
	
	@Test
	public void testsetPrimaryActionPool(){
		Player player = new Player(0, null, null);
		ArrayList<City> neighborhood = new ArrayList<>();
		City neighbour = new City(null, null, null);
		neighborhood.add(neighbour); 
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		cityToken.setBonuses(bonuses);
		City city = new City(neighborhood, cityToken, null);
		ArrayList<PrimaryAction> primaryActionPool = new ArrayList<>();
		EmporiumConstruction construction = new EmporiumConstruction(player, city, null, null, null);
		primaryActionPool.add(construction);
		Turn turn = new Turn(player);
		turn.setPrimaryActionPool(primaryActionPool);
		
		Player player1 = new Player(0, null, null);
		ArrayList<City> neighborhood1 = new ArrayList<>();
		City neighbour1 = new City(null, null, null);
		neighborhood1.add(neighbour1); 
		CityToken cityToken1 = new CityToken(null);
		ArrayList<Bonus> bonuses1 = new ArrayList<>();
		GiveAssistants giveAssistants1 = new GiveAssistants(0);
		GivePoints givePoints1 = new GivePoints(0);
		bonuses1.add(giveAssistants1);
		bonuses1.add(givePoints1);
		cityToken1.setBonuses(bonuses1);
		City city1 = new City(neighborhood1, cityToken1, null);
		ArrayList<PrimaryAction> primaryActionPool1 = new ArrayList<>();
		EmporiumConstruction construction1 = new EmporiumConstruction(player1, city1, null, null, null);
		primaryActionPool1.add(construction1);
		turn.setPrimaryActionPool(primaryActionPool1);
		assertEquals(primaryActionPool1, turn.getPrimaryActionPool());
	}

}
