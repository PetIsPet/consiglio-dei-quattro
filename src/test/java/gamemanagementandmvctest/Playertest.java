package gamemanagementandmvctest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

import bonuses.GiveAssistants;
import bonuses.GiveCoins;
import bonuses.GiveNobilityPoints;
import bonuses.GivePoints;
import gamemanagement.Player;
import mappaeoggetti.Card;
import mappaeoggetti.Emporium;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.Type;
import mvc.CLI;

public class Playertest {

	@Test 
	public void testgetBuildEmporiums(){
		Player player = new Player(0, null, null);
		Emporium emporium = new Emporium(player);
		assertEquals(player, emporium.getPlayer());
	}
	
	@Test 
	public void testsetBuildEmporiums(){
		Player player = new Player(0, null, null);
		Emporium emporium = new Emporium(player);
		emporium.setPlayer(player);
		assertEquals(player, emporium.getPlayer());
	}
	
	@Test
	public void testhasFinishedIsTrue(){
		Player player = new Player(0, null, null);
		player.setBuiltEmporiums(10);
		assertEquals(true, player.hasFinished());
	}
	
	@Test
	public void testhasFinishedIsFalse(){
		Player player = new Player(0, null, null);
		player.setBuiltEmporiums(7);
		assertEquals(false, player.hasFinished());
	}
	
	@Test
	public void testgetCards(){
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.YELLOW);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		assertEquals(ca3, cards.get(2));
	}
	
	@Test
	public void testsetCards(){
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.YELLOW);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		cards.set(0, ca3);
		assertEquals(ca3, cards.get(0));
	}
	
	@Test 
	public void testgetPermits(){
		ArrayList<PermissionCard> permits=new ArrayList<>();
		PermissionCard ca1 = new PermissionCard(null, Type.MOUNTAIN, null);
		PermissionCard ca2 = new PermissionCard(null, Type.MOUNTAIN, null);
		PermissionCard ca3 = new PermissionCard(null, Type.MOUNTAIN, null);
		permits.add(ca1);
		permits.add(ca2);
		permits.add(ca3);
		assertEquals(ca2, permits.get(1));
	}
	
	@Test 
	public void testsetPermits(){
		ArrayList<PermissionCard> permits=new ArrayList<>();
		PermissionCard ca1 = new PermissionCard(null, Type.MOUNTAIN, null);
		PermissionCard ca2 = new PermissionCard(null, Type.MOUNTAIN, null);
		PermissionCard ca3 = new PermissionCard(null, Type.MOUNTAIN, null);
		permits.add(ca1);
		permits.add(ca2);
		permits.add(ca3);
		permits.set(0, ca3);
		assertEquals(ca3, permits.get(0));
	}
		
	@Test
	public void testgetAssistants() {
		GiveAssistants bonus=new GiveAssistants(3);
		assertEquals(3,bonus.getNumberOfAssistants());
	}

	@Test
	public void testsetAssistants() {
		GiveAssistants bonus=new GiveAssistants(3);
		bonus.setNumberOfAssistants(2);
		assertEquals(2,bonus.getNumberOfAssistants());
	}
	
	@Test
	public void testgetId(){
		Player player = new Player(0, null, null);
		int id = 0;
		player.setId(id);
		assertEquals(0, player.getId());
	}
	
	@Test
	public void testsetId(){
		Player player = new Player(0, null, null);
		int id = 0;
		player.setId(id);
		assertEquals(0, player.getId());
		int id1=1;
		player.setId(id1);
		assertEquals(1, player.getId());
	}
	
	@Test
	public void testgetCli(){
		CLI cli= new CLI();
		Player player = new Player(0, null, cli);
		assertEquals(cli, player.getCli());
	}
	
	@Test
	public void testsetCli(){
		CLI cli = new CLI();
		CLI cli2 = new CLI();
		Player player = new Player(0, null, cli);
		player.setCli(cli2);
		assertEquals(cli2, player.getCli());
	}
	
	@Test
	public void testgetCoins(){
		GiveCoins coins = new GiveCoins(3);
		assertEquals(3, coins.getNumberOfCoins());
	}
	
	@Test
	public void testsetCoins(){
		GiveCoins coins = new GiveCoins(3);
		coins.setNumberOfCoins(2);
		assertEquals(2, coins.getNumberOfCoins());
	}

	@Test
	public void testgetNobilityPoints(){
		GiveNobilityPoints points = new GiveNobilityPoints(3, null);
		assertEquals(3, points.getNumberOfPoints());
	}
	
	@Test
	public void testsetNobilityPoints(){
		GiveNobilityPoints points = new GiveNobilityPoints(3, null);
		points.setNumberOfPoints(2);
		assertEquals(2, points.getNumberOfPoints());
	}
	
	@Test
	public void testgetPoints(){
		GivePoints point = new GivePoints(3);
		assertEquals(3, point.getNumberOfPoints());
	}
	
	@Test
	public void testsetPoints(){
		GivePoints point = new GivePoints(3);
		point.setNumberOfPoints(2);
		assertEquals(2, point.getNumberOfPoints());
	}
	
	 @Test
	 public void testgetColor(){
		 ArrayList<Color> colors=new ArrayList<>();
			colors.add(Color.BLACK);
			colors.add(Color.BLUE);
			colors.add(Color.YELLOW);
			colors.add(Color.MAGENTA);
		 Player player = new Player(0, Color.BLACK, null);
		 assertEquals(Color.BLACK, player.getColor());
	 }
	 
	 @Test
	 public void testsetColor(){
		 ArrayList<Color> colors=new ArrayList<>();
			colors.add(Color.BLACK);
			colors.add(Color.BLUE);
			colors.add(Color.YELLOW);
			colors.add(Color.MAGENTA);
		 Player player = new Player(0, Color.BLACK, null);
		 player.setColor(Color.BLUE);
		 assertEquals(Color.BLUE, player.getColor());
	 }
}

