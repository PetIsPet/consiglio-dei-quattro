package gamemanagementandmvctest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import bonuses.Bonus;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.Balcony;
import mappaeoggetti.CardDeck;
import mappaeoggetti.City;
import mappaeoggetti.Councelor;
import mappaeoggetti.King;
import mappaeoggetti.PermissionDeck;
import mvc.CLI;
import mvc.Game;

public class GameTest {

	@Test
	public void testgetCli(){
		Game game = new Game(null, null);
		CLI cli = new CLI();
		game.setCli(cli);
		assertEquals(cli, game.getCli());
	}
	
	@Test
	public void testsetCli(){
		Game game = new Game(null, null);
		CLI cli = new CLI();
		game.setCli(cli);
		assertEquals(cli, game.getCli());
		CLI cli1 = new CLI();
		game.setCli(cli1);
		assertEquals(cli1, game.getCli());
	}
	
	@Test
	public void testgetMapConfiguration(){
		Game game = new Game(null, null);
		String mapConfig = new String();
		game.setMapConfiguration(mapConfig);
		assertEquals(mapConfig, game.getMapConfiguration());
	}
	
	@Test
	public void testsetMapConfiguration(){
		Game game = new Game(null, null);
		String mapConfig = new String();
		game.setMapConfiguration(mapConfig);
		assertEquals(mapConfig, game.getMapConfiguration());
		String mapConfig1 = new String();
		game.setMapConfiguration(mapConfig1);
		assertEquals(mapConfig1, game.getMapConfiguration());
	}
	
	@Test
	public void testgetDecks(){
		Game game = new Game(null, null);
		ArrayList<PermissionDeck> deck = new ArrayList<>();
		game.setDecks(deck);
		assertEquals(deck, game.getDecks());
	}
	
	@Test
	public void testsetDecks(){
		Game game = new Game(null, null);
		ArrayList<PermissionDeck> deck = new ArrayList<>();
		game.setDecks(deck);
		assertEquals(deck, game.getDecks());
		ArrayList<PermissionDeck> deck1 = new ArrayList<>();
		game.setDecks(deck1);
		assertEquals(deck1, game.getDecks());
	}
	
	@Test
	public void testgetBalconies(){
		Game game = new Game(null, null);
		ArrayList<Balcony> balconies = new ArrayList<>();
		game.setBalconies(balconies);
		assertEquals(balconies, game.getBalconies());
	}
	
	@Test
	public void testsetBalconies(){
		Game game = new Game(null, null);
		ArrayList<Balcony> balconies = new ArrayList<>();
		game.setBalconies(balconies);
		assertEquals(balconies, game.getBalconies());
		ArrayList<Balcony> balconies1 = new ArrayList<>();
		game.setBalconies(balconies1);
		assertEquals(balconies1, game.getBalconies());
	}
	
	@Test
	public void testgetCardColors(){
		Game game = new Game(null, null);
		ArrayList<Color> colors = new ArrayList<>();
		game.setCardColors(colors);
		assertEquals(colors, game.getCardColors());
	}
	
	@Test
	public void testsetCardColors(){
		Game game = new Game(null, null);
		ArrayList<Color> colors = new ArrayList<>();
		game.setCardColors(colors);
		assertEquals(colors, game.getCardColors());
		ArrayList<Color> colors1 = new ArrayList<>();
		game.setCardColors(colors1);
		assertEquals(colors1, game.getCardColors());
	}
	
	@Test
	public void testgetCouncelorPool(){
		Game game = new Game(null, null);
		ArrayList<Councelor> councelors = new ArrayList<>();
		game.setCouncelorsPool(councelors);
		assertEquals(councelors, game.getCouncelorsPool());
	}
	
	@Test
	public void testsetCouncelorPool(){
		Game game = new Game(null, null);
		ArrayList<Councelor> councelors = new ArrayList<>();
		game.setCouncelorsPool(councelors);
		assertEquals(councelors, game.getCouncelorsPool());
		ArrayList<Councelor> councelors1 = new ArrayList<>();
		game.setCouncelorsPool(councelors1);
		assertEquals(councelors1, game.getCouncelorsPool());
	}
	
	@Test
	public void testgetKing(){
		Game game = new Game(null, null);
		King king = new King(null);
		game.setKing(king);
		assertEquals(king, game.getKing());
	}
	
	@Test
	public void testsetKing(){
		Game game = new Game(null, null);
		King king = new King(null);
		game.setKing(king);
		assertEquals(king, game.getKing());
		King king1 = new King(null);
		game.setKing(king1);
		assertEquals(king1, game.getKing());
	}
	
	@Test
	public void testgetPlayer(){
		Game game = new Game(null, null);
		ArrayList<Player> players = new ArrayList<>();
		game.setPlayers(players);
		assertEquals(players, game.getPlayers());
	}
	
	@Test
	public void testsetPlayer(){
		Game game = new Game(null, null);
		ArrayList<Player> players = new ArrayList<>();
		game.setPlayers(players);
		assertEquals(players, game.getPlayers());
		ArrayList<Player> players1 = new ArrayList<>();
		game.setPlayers(players1);
		assertEquals(players1, game.getPlayers());
	}
	
	
	@Test
	public void testgetMap(){
		Game game = new Game(null, null);
		Lettore le = new Lettore();
		GraphMap map = new GraphMap(le);
		game.setMap(map);
		assertEquals(map, game.getMap());
	}
	
	@Test
	public void testsetMap(){
		Game game = new Game(null, null);
		Lettore le = new Lettore();
		GraphMap map = new GraphMap(le);
		game.setMap(map);
		assertEquals(map, game.getMap());
		GraphMap map1 = new GraphMap(le);
		game.setMap(map1);
		assertEquals(map1, game.getMap());
	}
	
	@Test
	public void testgetController(){
		Game game = new Game(null, null);
		TurnManager controller = new TurnManager(null, game);
		game.setController(controller);
		assertEquals(controller, game.getController());
	}
	
	@Test
	public void testsetController(){
		Game game = new Game(null, null);
		TurnManager controller = new TurnManager(null, game);
		game.setController(controller);
		assertEquals(controller, game.getController());
		TurnManager controller1 = new TurnManager(null, game);
		game.setController(controller1);
		assertEquals(controller1, game.getController());
	}
	
	@Test
	public void testgetCardDeck(){
		Game game = new Game(null, null);
		CardDeck deck = new CardDeck(null);
		game.setCardDeck(deck);
		assertEquals(deck, game.getCardDeck());	
	}
	
	@Test
	public void testsetCardDeck(){
		Game game = new Game(null, null);
		CardDeck deck = new CardDeck(null);
		game.setCardDeck(deck);
		assertEquals(deck, game.getCardDeck());
		CardDeck deck1 = new CardDeck(null);
		game.setCardDeck(deck1);
		assertEquals(deck1, game.getCardDeck());
	}
	
	@Test
	public void testgetCities(){
		Game game = new Game(null, null);
		ArrayList<City> cities = new ArrayList<>();
		game.setCities(cities);
		assertEquals(cities, game.getCities());
	}
	
	@Test
	public void testsetCities(){
		Game game = new Game(null, null);
		ArrayList<City> cities = new ArrayList<>();
		game.setCities(cities);
		assertEquals(cities, game.getCities());
		ArrayList<City> cities1 = new ArrayList<>();
		game.setCities(cities1);
		assertEquals(cities1, game.getCities());
	}
	
	@Test
	public void testgetNobilityScale(){
		Game game = new Game(null, null);
		Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<>();
		game.setNobilityScale(nobilityScale);
		assertEquals(nobilityScale, game.getNobilityScale());
	}
	
	@Test
	public void testgetName(){
		Game game = new Game(null, null);
		String name = new String();
		game.setName(name);
		assertEquals(name, game.getName());
	}
	
	@Test
	public void testsetName(){
		Game game = new Game(null, null);
		String name = new String();
		game.setName(name);
		assertEquals(name, game.getName());
		String name1 = new String();
		game.setName(name1);
		assertEquals(name1, game.getName());
	}
	
	@Test
	public void testgetPlayerID(){
		Game game = new Game(null, null);
		int id = 1;
		game.setPlayersID(id);
		assertEquals(id, game.getPlayersID());
	}
	
	@Test
	public void testsetPlayerID(){
		Game game = new Game(null, null);
		int id = 1;
		game.setPlayersID(id);
		assertEquals(id, game.getPlayersID());
		int id1 = 3;
		game.setPlayersID(id1);
		assertEquals(id1, game.getPlayersID());
	}
	
	@Test
	public void testincrementPlayerID(){
		Game game = new Game(null, null);
		int id = 1;
		game.setPlayersID(id);
		game.incrementPlayersID();
		assertEquals(2, game.getPlayersID());
	}
	
	@Test
	public void testgetPermitsImagesPaths(){
		Game game = new Game(null, null);
		ArrayList<String> permitsImagePath = new ArrayList<>();
		game.setPermitsImagesPaths(permitsImagePath);
		assertEquals(permitsImagePath, game.getPermitsImagesPaths());
	}
	
	@Test
	public void testsetPermitsImagesPaths(){
		Game game = new Game(null, null);
		ArrayList<String> permitsImagePath = new ArrayList<>();
		game.setPermitsImagesPaths(permitsImagePath);
		assertEquals(permitsImagePath, game.getPermitsImagesPaths());
		ArrayList<String> permitsImagePath1 = new ArrayList<>();
		game.setPermitsImagesPaths(permitsImagePath1);
		assertEquals(permitsImagePath1, game.getPermitsImagesPaths());
	}
	
	@Test
	public void testhasFinischedIsTrue(){
		Game game = new Game(null, null);
		ArrayList<Player> players = new ArrayList<>();
		Player player = new Player(0, null, null);
		players.add(player);
		game.setPlayers(players);
		game.getPlayers().get(0);
		player.setBuiltEmporiums(10);
		assertEquals(true, game.hasFinished());
	}
	
	@Test
	public void testhasFinischedIsFalse(){
		Game game = new Game(null, null);
		ArrayList<Player> players = new ArrayList<>();
		Player player = new Player(0, null, null);
		players.add(player);
		game.setPlayers(players);
		game.getPlayers().get(0);
		player.setBuiltEmporiums(4);
		assertEquals(false, game.hasFinished());
	}
}
