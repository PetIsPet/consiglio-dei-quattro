package gamemanagementandmvctest;

import static org.junit.Assert.*;

import org.junit.Test;

import gamemanagement.Player;
import gamemanagement.RegularTurn;

public class RegularTurnTest {
	
	@Test
	public void testRegularTurn(){
		Player player = new Player(0, null, null);
		RegularTurn regularTurn = new RegularTurn(player);
		assertEquals(player, regularTurn.getPlayer());	
	}

}
