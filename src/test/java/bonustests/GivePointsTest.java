package bonustests;

import static org.junit.Assert.*;

import org.junit.Test;

import bonuses.GivePoints;
import gamemanagement.Player;

public class GivePointsTest {
	
	@Test
	public void testGivePoints() {
		

		GivePoints bonus=new GivePoints(1);
		assertEquals(1,bonus.getNumberOfPoints());
	}

	

	@Test
	public void testAct() {
		Player player=new Player(0, null, null);
		player.setPoints(2);
		GivePoints bonus=new GivePoints(1);
		bonus.act(player);
		assertEquals(3,player.getPoints());
	}

	@Test
	public void testGetName() {
		GivePoints bonus=new GivePoints(1);
		bonus.setName("nome");
		assertEquals(true,bonus.getName().equals("nome"));
	}

	@Test
	public void testSetName() {
		GivePoints bonus=new GivePoints(1);
		String nome="nome";
		bonus.setName(nome);
		assertEquals(true,bonus.getName().equals("nome"));
		
	}

	@Test
	public void testGetNumberOfPoints() {
		GivePoints bonus=new GivePoints(1);
		assertEquals(1,bonus.getNumberOfPoints());
	}

	@Test
	public void testSetNumberOfPoints() {
		GivePoints bonus=new GivePoints(1);
		bonus.setNumberOfPoints(2);
		assertEquals(2,bonus.getNumberOfPoints());
	}

}
