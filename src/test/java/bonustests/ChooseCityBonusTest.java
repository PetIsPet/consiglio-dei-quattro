package bonustests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;

import bonuses.Bonus;
import bonuses.ChooseCityBonus;
import bonuses.GiveAssistants;
import bonuses.GiveNobilityPoints;
import bonuses.GivePoints;
import gamemanagement.Player;
import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.Emporium;
import mvc.CLI;

public class ChooseCityBonusTest {
	
	@Test
	public void testChooseCityBonus(){
		boolean flag = false;
		boolean flag1 = true;
		ChooseCityBonus bonus = new ChooseCityBonus(flag);
		bonus.setFlag(flag1);
		assertEquals(true, bonus.isFlag());
	}
	
	@Test
	public void testisControlCityTrue(){
		boolean flag = true;
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		cityToken.setBonuses(bonuses);
		Player player = new Player(0, null, null);
		City city = new City(null, cityToken, null);
		Emporium emporium = new Emporium(player);
		city.getEmporiums().add(emporium);
		player.hasBuilt(city);
		ChooseCityBonus bonus = new ChooseCityBonus(flag);
		bonus.isNobilityPoints(city.getCityToken().getBonuses());
		assertEquals(true, bonus.controlCity(player, city));
	}
	
	@Test
	public void testisControlCityFalse(){
		boolean flag = true;
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		Map<Integer, ArrayList<Bonus>> nobilityscale = null;
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		GiveNobilityPoints nobilityPoints = new GiveNobilityPoints(0, nobilityscale);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		bonuses.add(nobilityPoints);
		cityToken.setBonuses(bonuses);
		Player player = new Player(0, null, null);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, cityToken, null);
		Emporium emporium = new Emporium(player);
		city.getEmporiums().add(emporium);
		cities.add(city);
		ChooseCityBonus bonus = new ChooseCityBonus(flag);
		bonus.setCities(cities);
		assertEquals(false, bonus.controlCity(player, city));
	}
	
	@Test
	public void testisControlCityFalse2(){
		boolean flag = true;
		CityToken cityToken = new CityToken(null);
		Player player = new Player(0, null, null);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, cityToken, null);
		ChooseCityBonus bonus = new ChooseCityBonus(flag);
		cities.add(city);
		bonus.setCities(cities);
		assertEquals(false, bonus.controlCity(player, city));
	}
	
	@Test
	public void testact(){
		boolean flag= true;
		CLI cli = new CLI();
		CityToken cityToken = new CityToken(null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GivePoints bonus1=new GivePoints(1);
		bonuses.add(bonus1);
		cityToken.setBonuses(bonuses);
		Player player=new Player(0, Color.BLACK, cli);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, cityToken, null);
		cities.add(city);
		ChooseCityBonus chooseCityBonus = new ChooseCityBonus(flag);
		chooseCityBonus.setCities(cities);
		chooseCityBonus.act(player);
		assertEquals(1, player.getPoints());
	}
	
	@Test
	public void testisFlagTrue(){
		boolean flag = true;
		ChooseCityBonus chooseCityBonus = new ChooseCityBonus(flag);
		assertEquals(true, chooseCityBonus.isFlag());
	}
	
	@Test
	public void testisFlagFalse(){
		boolean flag = false;
		ChooseCityBonus chooseCityBonus = new ChooseCityBonus(flag);
		assertEquals(false, chooseCityBonus.isFlag());
	}
	
	@Test
	public void testsetFlag(){
		boolean flag = true;
		boolean flag1 = false;
		ChooseCityBonus chooseCityBonus = new ChooseCityBonus(flag);
		chooseCityBonus.setFlag(flag1);
		assertEquals(false, chooseCityBonus.isFlag());
	}
	
	@Test
	public void testgetCities(){
		boolean flag = true;
		CityToken cityToken = new CityToken(null);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, cityToken, null);
		City city1 = new City(null, cityToken, null);
		cities.add(city);
		cities.add(city1);
		ChooseCityBonus chooseCityBonus = new ChooseCityBonus(flag);
		chooseCityBonus.setCities(cities);
		assertEquals(cities, chooseCityBonus.getCities());
	}
	
	@Test
	public void testsetCities(){
		boolean flag = true;
		CityToken cityToken = new CityToken(null);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, cityToken, null);
		City city1 = new City(null, cityToken, null);
		cities.add(city);
		cities.add(city1);
		ChooseCityBonus chooseCityBonus = new ChooseCityBonus(flag);
		chooseCityBonus.setCities(cities);
		assertEquals(cities, chooseCityBonus.getCities());	
	}
		
}
