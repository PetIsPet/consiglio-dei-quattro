package bonustests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import bonuses.Bonus;
import bonuses.GiveAssistants;
import bonuses.GiveCoins;
import bonuses.GiveNobilityPoints;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import mappaeoggetti.City;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import servercompleto.ServerView;

public class GiveNobilityPointsTest {
	
	@Test
	public void testGiveNobilityPoints() {
		GiveNobilityPoints bonus=new GiveNobilityPoints(2,null);
		assertEquals(2,bonus.getNumberOfPoints());
	}

	@Test
	public void testAct() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(2);
		GiveAssistants assistant = new GiveAssistants(2);
		bonuses.add(coins);
		bonuses.add(assistant);
		nobilityScale.put(3, bonuses);
		int numberOfPoints = 3;
		Player player = new Player(0, null, null);
		GiveNobilityPoints bonus=new GiveNobilityPoints(2,nobilityScale);
		bonus.setNumberOfPoints(numberOfPoints);
		bonus.act(player);
		assertEquals(3,player.getNobilityPoints());
		assertEquals(2, player.getCoins());
		assertEquals(2, player.getAssistants());	
	}
	
	@Test
	public void testdiscoveredCardsToString(){
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(1);
		bonuses.add(coins);
		ArrayList<City> buildPermission = new ArrayList<>();
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1 = new PermissionCard(bonuses, null, buildPermission);
		PermissionCard c2 = new PermissionCard(bonuses, null, buildPermission);
		PermissionCard c3 = new PermissionCard(bonuses, null, buildPermission);
		PermissionCard c4 = new PermissionCard(bonuses, null, buildPermission);
		cards.add(c1);
		cards.add(c2);
		cards.add(c3);
		cards.add(c4);
		PermissionDeck deck = new PermissionDeck(null, cards);
		GiveNobilityPoints points = new GiveNobilityPoints(0, null);
		points.setDeck(deck);
		points.discoveredCardsToString(deck);
		assertEquals(2, points.discoveredCardsToString(deck).size());
	}

	@Test
	public void testGetName() {
		GiveNobilityPoints bonus=new GiveNobilityPoints(2,null);
		bonus.setName("nome");
		assertEquals("nome",bonus.getName());
	}

	@Test
	public void testSetName() {
		GiveNobilityPoints bonus=new GiveNobilityPoints(2,null);
		String nome="nome";
		bonus.setName(nome);
		assertEquals("nome",bonus.getName());
		String nome1 = "nome1";
		bonus.setName(nome1);
		assertEquals("nome1", bonus.getName());
	}
	
	@Test
	public void testsetNobilityStuff(){
		Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(2);
		GiveAssistants assistant = new GiveAssistants(2);
		bonuses.add(coins);
		bonuses.add(assistant);
		nobilityScale.put(3, bonuses);
		GiveNobilityPoints points = new GiveNobilityPoints(0, nobilityScale);
		ServerView server = null;
		ArrayList<PermissionDeck> decks = new ArrayList<>();
		points.setNobilityStuff(decks, server, null);
		assertEquals(decks, points.getDecks());
		
	}

	@Test
	public void testGetNobilityBonus() {
		int finalPoint = 3;
		Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(2);
		GiveAssistants assistant = new GiveAssistants(2);
		bonuses.add(coins);
		bonuses.add(assistant);
		nobilityScale.put(3, bonuses);
		GiveNobilityPoints giveNobilityPoints = new GiveNobilityPoints(0, nobilityScale);
		assertEquals(bonuses, giveNobilityPoints.getNobilityBonus(nobilityScale, finalPoint));   
	}

	@Test
	public void testGetNumberOfPoints() {
		GiveNobilityPoints bonus=new GiveNobilityPoints(2,null);
		assertEquals(2,bonus.getNumberOfPoints());
	}

	@Test
	public void testSetNumberOfPoints() {
		GiveNobilityPoints bonus=new GiveNobilityPoints(2,null);
		bonus.setNumberOfPoints(3);
		assertEquals(3,bonus.getNumberOfPoints());
	}
	
	@Test
	public void testgetDecks(){
		Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(2);
		GiveAssistants assistant = new GiveAssistants(2);
		bonuses.add(coins);
		bonuses.add(assistant);
		nobilityScale.put(3, bonuses);
		GiveNobilityPoints points = new GiveNobilityPoints(0, nobilityScale);
		ServerView server = null;
		ArrayList<PermissionDeck> decks = new ArrayList<>();
		points.setNobilityStuff(decks, server, null);
		assertEquals(decks, points.getDecks());
	}
	
	@Test
	public void testsetDecks(){
		Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(2);
		GiveAssistants assistant = new GiveAssistants(2);
		bonuses.add(coins);
		bonuses.add(assistant);
		nobilityScale.put(3, bonuses);
		GiveNobilityPoints points = new GiveNobilityPoints(0, nobilityScale);
		ServerView server = null;
		ArrayList<PermissionDeck> decks = new ArrayList<>();
		points.setNobilityStuff(decks, server, null);
		assertEquals(decks, points.getDecks());
		ArrayList<PermissionDeck> decks1 = new ArrayList<>();
		points.setNobilityStuff(decks1, server, null);
		assertEquals(decks1, points.getDecks());
	}
}