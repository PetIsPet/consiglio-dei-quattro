package bonustests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

import bonuses.DrawPoliticsCard;
import gamemanagement.Player;
import mappaeoggetti.Card;
import mappaeoggetti.CardDeck;;

public class DrawPoliticsCardTest {
	
	
	@Test
	public void testact(){
		Player player = new Player(0, null, null);
		Card card = new Card(null);
		Card card1 = new Card(null);
		ArrayList<Color> colors = new ArrayList<>();
		Color color = Color.BLACK;
		Color color1 = Color.BLUE;
		colors.add(color);
		colors.add(color1);
		ArrayList<Card> cards = new ArrayList<>();
		cards.add(card);
		cards.add(card1);
		player.setCards(cards);
		CardDeck deck = new CardDeck(colors);
		DrawPoliticsCard drawPoliticsCard = new DrawPoliticsCard();
		drawPoliticsCard.setPoliticDeck(deck);
		int num = player.getCards().size();
		drawPoliticsCard.act(player);
		assertEquals(cards.size(), num+1);
	}
	
	@Test
	public void testgetDeck(){
		DrawPoliticsCard drawPoliticsCard = new DrawPoliticsCard();
		CardDeck deck = new CardDeck(null);
		drawPoliticsCard.setPoliticDeck(deck);
		assertEquals(deck, drawPoliticsCard.getDeck());
	}
	
	@Test
	public void testsetDeck(){
		DrawPoliticsCard drawPoliticsCard = new DrawPoliticsCard();
		CardDeck deck = new CardDeck(null);
		CardDeck deck1 = new CardDeck(null);
		drawPoliticsCard.setPoliticDeck(deck);
		assertEquals(deck, drawPoliticsCard.getDeck());
		drawPoliticsCard.setPoliticDeck(deck1);
		assertEquals(deck1, drawPoliticsCard.getDeck());
	}
}
