package bonustests;

import static org.junit.Assert.*;

import org.junit.Test;

import bonuses.GiveAssistants;
import gamemanagement.Player;

public class GiveAssistantsTest {
	
	
	@Test
	public void testGiveAssistants() {
		GiveAssistants bonus=new GiveAssistants(3);
		assertEquals(3,bonus.getNumberOfAssistants());
		
	}

	@Test
	public void testAct() {
		Player player=new Player(0,null,null);
		player.setAssistants(3);
		GiveAssistants bonus=new GiveAssistants(3);
		bonus.act(player);
		assertEquals(6,player.getAssistants());
	}

	@Test
	public void testGetName() {
		GiveAssistants bonus=new GiveAssistants(3);
		bonus.setName("nome");
		assertEquals("nome",bonus.getName());
	}

	@Test
	public void testSetName() {
		GiveAssistants bonus=new GiveAssistants(3);
		String nome="bonus";
		bonus.setName(nome);
		assertEquals(nome,bonus.getName());
	}


	@Test
	public void testGetNumberOfAssistants() {
		GiveAssistants bonus=new GiveAssistants(3);
		assertEquals(3,bonus.getNumberOfAssistants());
	}

	@Test
	public void testSetNumberOfAssistants() {
		GiveAssistants bonus=new GiveAssistants(3);
		bonus.setNumberOfAssistants(2);
		assertEquals(2,bonus.getNumberOfAssistants());
	}

}
