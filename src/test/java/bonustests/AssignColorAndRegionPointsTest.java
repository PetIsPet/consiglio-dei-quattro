package bonustests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import bonuses.AssignColorAndRegionPoints;
import gamemanagement.Player;
import mappaeoggetti.City;
import mappaeoggetti.Emporium;
import mappaeoggetti.Type;
import mvc.Game;

public class AssignColorAndRegionPointsTest {
	
	@Test
	public void testAssignColorAndRegionPoints(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		Game game1 = new Game(null, null);
		points.setGame(game1);
		assertEquals(game1, points.getGame());
	}
	
	@Test
	public void testAssignColorPointsIsTrue(){
		boolean flag = false;
		ArrayList<Player> players = new ArrayList<>();
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		players.add(player);
		players.add(player1);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, null, null);
		City city1 = new City(null, null, null);
		cities.add(city);
		cities.add(city1);
		city.setColor(Color.BLACK);
		city1.setColor(Color.BLACK);
		ArrayList<Emporium> emporiums = new ArrayList<>();
		ArrayList<Emporium> emporiums1 = new ArrayList<>();
		Emporium emporium = new Emporium(player);
		Emporium emporium1 = new Emporium(player1);
		emporiums.add(emporium);
		emporiums.add(emporium1);
		emporiums1.add(emporium1);
		city.setEmporiums(emporiums);
		city1.setEmporiums(emporiums1);
		Game game = new Game(null, null);
		game.setCities(cities);
		game.setPlayers(players);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		points.setFlag(flag);
		points.AssignColorPoints(player, city);
		assertEquals(true, points.isFlag());
	}
	
	@Test
	public void testAssignColorPointsIsFalse(){
		boolean flag = false;
		ArrayList<Player> players = new ArrayList<>();
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		players.add(player);
		players.add(player1);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, null, null);
		City city1 = new City(null, null, null);
		cities.add(city);
		cities.add(city1);
		city.setColor(Color.BLACK);
		city1.setColor(Color.BLACK);
		ArrayList<Emporium> emporiums = new ArrayList<>();
		ArrayList<Emporium> emporiums1 = new ArrayList<>();
		Emporium emporium = new Emporium(player);
		Emporium emporium1 = new Emporium(player1);
		emporiums.add(emporium);
		emporiums.add(emporium1);
		emporiums1.add(emporium1);
		city.setEmporiums(emporiums);
		city1.setEmporiums(emporiums1);
		Game game = new Game(null, null);
		game.setCities(cities);
		game.setPlayers(players);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		points.setFlag(flag);
		ArrayList<Object> colorType = new ArrayList<>();
		points.setColorType(colorType);
		ArrayList<Player> constructionOrder = new ArrayList<>();
		points.setConstructionOrder(constructionOrder);
		Map<Player, Object> map = new HashMap<>();
		points.setMap(map);
		points.AssignColorPoints(player1, city);
		assertEquals(false, points.isFlag());
		
		assertEquals(Color.BLACK, points.getColorType().get(0));
		assertEquals(player1, points.getConstructionOrder().get(0));
		assertEquals(true, points.getMap().containsKey(player1));
	}
	
	@Test
	public void testAssignTypePointsIsTrue(){
		boolean flag = false;
		ArrayList<Player> players = new ArrayList<>();
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		players.add(player);
		players.add(player1);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, null, null);
		City city1 = new City(null, null, null);
		cities.add(city);
		cities.add(city1);
		city.setType(Type.MOUNTAIN);
		city1.setType(Type.MOUNTAIN);
		ArrayList<Emporium> emporiums = new ArrayList<>();
		ArrayList<Emporium> emporiums1 = new ArrayList<>();
		Emporium emporium = new Emporium(player);
		Emporium emporium1 = new Emporium(player1);
		emporiums.add(emporium);
		emporiums.add(emporium1);
		emporiums1.add(emporium1);
		city.setEmporiums(emporiums);
		city1.setEmporiums(emporiums1);
		Game game = new Game(null, null);
		game.setCities(cities);
		game.setPlayers(players);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		points.setFlag(flag);
		points.AssignTypePoints(player, city);
		assertEquals(true, points.isFlag());
	}
	
	@Test
	public void testAssignTypePointsIsFalse(){
		boolean flag = false;
		ArrayList<Player> players = new ArrayList<>();
		Player player = new Player(0, null, null);
		Player player1 = new Player(0, null, null);
		players.add(player);
		players.add(player1);
		ArrayList<City> cities = new ArrayList<>();
		City city = new City(null, null, null);
		City city1 = new City(null, null, null);
		cities.add(city);
		cities.add(city1);
		city.setType(Type.MOUNTAIN);
		city1.setType(Type.MOUNTAIN);
		ArrayList<Emporium> emporiums = new ArrayList<>();
		ArrayList<Emporium> emporiums1 = new ArrayList<>();
		Emporium emporium = new Emporium(player);
		Emporium emporium1 = new Emporium(player1);
		emporiums.add(emporium);
		emporiums.add(emporium1);
		emporiums1.add(emporium1);
		city.setEmporiums(emporiums);
		city1.setEmporiums(emporiums1);
		Game game = new Game(null, null);
		game.setCities(cities);
		game.setPlayers(players);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		points.setFlag(flag);
		ArrayList<Object> colorType = new ArrayList<>();
		points.setColorType(colorType);
		ArrayList<Player> constructionOrder = new ArrayList<>();
		points.setConstructionOrder(constructionOrder);
		Map<Player, Object> map = new HashMap<>();
		points.setMap(map);
		points.AssignTypePoints(player1, city);
		assertEquals(false, points.isFlag());
		
		assertEquals(Type.MOUNTAIN, points.getColorType().get(0));
		assertEquals(player1, points.getConstructionOrder().get(0));
		assertEquals(true, points.getMap().containsKey(player1));
	}
	
	@Test
	public void testgetGame(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		assertEquals(game, points.getGame());
	}
	
	@Test
	public void testsetGame(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		Game game1 = new Game(null, null);
		assertEquals(game, points.getGame());
		points.setGame(game1);
		assertEquals(game1, points.getGame());
	}
	
	@Test
	public void testisFlag(){
		boolean flag = false;
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		points.setFlag(flag);
		assertEquals(false, points.isFlag());
	}
	
	@Test
	public void testsetFlag(){
		boolean flag = false;
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		points.setFlag(flag);
		assertEquals(false, points.isFlag());
		boolean flag1 = true;
		points.setFlag(flag1);
		assertEquals(true, points.isFlag());
	}
	
	@Test
	public void testgetColorType(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		ArrayList<Object> colorType = new ArrayList<>();
		points.setColorType(colorType);
		assertEquals(colorType, points.getColorType());
	}
	
	@Test
	public void testsetColorType(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		ArrayList<Object> colorType = new ArrayList<>();
		points.setColorType(colorType);
		assertEquals(colorType, points.getColorType());
		ArrayList<Object> colorType1 = new ArrayList<>();
		points.setColorType(colorType1);
		assertEquals(colorType1, points.getColorType());
	}
	
	@Test
	public void testgetConstructionOrder(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		ArrayList<Player> constructionOrder = new ArrayList<>();
		points.setConstructionOrder(constructionOrder);
		assertEquals(constructionOrder, points.getConstructionOrder());
	}
	
	@Test
	public void testsetConstructionOrder(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		ArrayList<Player> constructionOrder = new ArrayList<>();
		points.setConstructionOrder(constructionOrder);
		assertEquals(constructionOrder, points.getConstructionOrder());
		ArrayList<Player> constructionOrder1 = new ArrayList<>();
		points.setConstructionOrder(constructionOrder1);
		assertEquals(constructionOrder1, points.getConstructionOrder());
	}
	
	@Test
	public void testgetMap(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		Map<Player, Object> map = new HashMap<>();
		points.setMap(map);
		assertEquals(map, points.getMap());
	}
	
	@Test
	public void testsetMap(){
		Game game = new Game(null, null);
		AssignColorAndRegionPoints points = new AssignColorAndRegionPoints(game);
		Map<Player, Object> map = new HashMap<>();
		points.setMap(map);
		assertEquals(map, points.getMap());
		Map<Player, Object> map1 = new HashMap<>();
		points.setMap(map1);
		assertEquals(map1, points.getMap());
		
	}
	

}
