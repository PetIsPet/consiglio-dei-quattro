package bonustests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import bonuses.BonusCityKing;
import gamemanagement.Player;
import mappaeoggetti.Card;

public class BonusCityKingTest {
	
	@Test
	public void testact(){
		BonusCityKing bonusCityKing = new BonusCityKing();
		ArrayList<Card> cards = new ArrayList<>();
		Card card = new Card(null);
		cards.add(card);
		Player player = new Player(0, null, null);
		player.setAssistants(0);
		bonusCityKing.act(player);
		assertEquals(0, player.getAssistants());
		player.setCoins(0);
		bonusCityKing.act(player);
		assertEquals(0, player.getCoins());
		player.setPoints(0);
		bonusCityKing.act(player);
		assertEquals(0, player.getPoints());
		player.setNobilityPoints(0);
		bonusCityKing.act(player);
		assertEquals(0, player.getNobilityPoints());
		player.setCards(cards);
		bonusCityKing.act(player);
		assertEquals(cards, player.getCards());
	}
	}
