package bonustests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import bonuses.Bonus;
import bonuses.DrawPermissionCard;
import bonuses.GiveAssistants;
import gamemanagement.Player;
import mappaeoggetti.City;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mappaeoggetti.Type;

public class DrawPermissionCardTest {
	
	@Test
	public void testact(){
		Player player = new Player(0, null, null);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		Bonus bonus = new GiveAssistants(0);
		bonuses.add(bonus);
		ArrayList<City> cities = new ArrayList<>();
		PermissionCard card = new PermissionCard(bonuses, null, cities);
		PermissionCard card1 = new PermissionCard(bonuses, null, cities);
		PermissionCard card2 = new PermissionCard(bonuses, null, cities);
		PermissionCard card3 = new PermissionCard(bonuses, null, cities);
		ArrayList<PermissionCard> permits = new ArrayList<>();
		permits.add(card);
		permits.add(card1);
		permits.add(card2);
		permits.add(card3);
		player.setPermits(permits);
		PermissionDeck deck = new PermissionDeck(Type.MOUNTAIN, permits);
		DrawPermissionCard drawPermissionCard = new DrawPermissionCard();
		drawPermissionCard.setDeck(deck);
		int choice = 0;
		drawPermissionCard.setChoice(choice);
		drawPermissionCard.getChoice();
		drawPermissionCard.act(player);
		assertEquals(true, player.getPermits().add(deck.getDiscoveredCards()[choice]));
	}

	@Test
	public void testgetDeck(){
		DrawPermissionCard drawPermissionCard = new DrawPermissionCard();
		ArrayList<PermissionCard> permissionCards = new ArrayList<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		Bonus bonus = new GiveAssistants(0);
		bonuses.add(bonus);
		ArrayList<City> cities = new ArrayList<>();
		PermissionCard permissionCard = new PermissionCard(bonuses, null, cities);
		PermissionCard permissionCard2 = new PermissionCard(bonuses, null, cities);
		permissionCards.add(permissionCard);
		permissionCards.add(permissionCard2);
		PermissionDeck deck = new PermissionDeck(Type.MOUNTAIN, permissionCards);
		drawPermissionCard.setDeck(deck);
		assertEquals(deck, drawPermissionCard.getDeck());
	}
	
	@Test
	public void testsetDeck(){
		DrawPermissionCard drawPermissionCard = new DrawPermissionCard();
		ArrayList<PermissionCard> permissionCards = new ArrayList<>();
		ArrayList<PermissionCard> permissionCards1 = new ArrayList<>();
		ArrayList<City> cities = new ArrayList<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		PermissionCard permissionCard = new PermissionCard(bonuses, null, cities);
		PermissionCard permissionCard2 = new PermissionCard(bonuses, null, cities);
		permissionCards.add(permissionCard);
		permissionCards.add(permissionCard2);
		permissionCards1.add(permissionCard);
		permissionCards1.add(permissionCard2);
		PermissionDeck deck = new PermissionDeck(null, permissionCards);
		drawPermissionCard.setDeck(deck);
		assertEquals(deck, drawPermissionCard.getDeck());
		PermissionDeck deck1 = new PermissionDeck(null, permissionCards1);
		drawPermissionCard.setDeck(deck1);
		assertEquals(deck1, drawPermissionCard.getDeck());	
	}
	
	@Test
	public void testgetChoice(){
		int choice = 2;
		DrawPermissionCard drawPermissionCard = new DrawPermissionCard();
		drawPermissionCard.setChoice(choice);
		assertEquals(2, drawPermissionCard.getChoice());
	}
	
	@Test
	public void tessetChoice(){
		int choice = 2;
		DrawPermissionCard drawPermissionCard = new DrawPermissionCard();
		drawPermissionCard.setChoice(choice);
		assertEquals(2, drawPermissionCard.getChoice());
		drawPermissionCard.setChoice(1);
		assertEquals(1, drawPermissionCard.getChoice());
	}
}