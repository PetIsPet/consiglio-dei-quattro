package bonustests;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

import bonuses.Bonus;
import bonuses.GetUsedBonus;
import bonuses.GivePoints;
import gamemanagement.Player;
import mappaeoggetti.PermissionCard;
import mvc.CLI;

public class GetUsedBonusTest {
	
	@Test
	public void testAct() {
		CLI cli = new CLI();
		Player player=new Player(0, Color.BLACK, cli);
		player.setPoints(3);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GetUsedBonus bonus=new GetUsedBonus();
		GivePoints bonus1=new GivePoints(1);
		bonuses.add(bonus1);
		PermissionCard permissionCard  = new PermissionCard(bonuses, null, null);
		bonus.setPermissionCard(permissionCard);
		bonus.act(player);
		assertEquals(4, player.getPoints());
	}
	
	@Test
	public void testgetPermissionCard(){
		ArrayList<PermissionCard> permits = new ArrayList<>();
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		permits.add(permissionCard);
		GetUsedBonus bonus = new GetUsedBonus();
		bonus.setPermissionCard(permissionCard);
		assertEquals(permissionCard, bonus.getPermissionCard());
	}
	
	@Test
	public void testsetPermissionCard(){
		ArrayList<PermissionCard> permits = new ArrayList<>();
		PermissionCard permissionCard = new PermissionCard(null, null, null);
		permits.add(permissionCard);
		GetUsedBonus bonus = new GetUsedBonus();
		bonus.setPermissionCard(permissionCard);
		assertEquals(permissionCard, bonus.getPermissionCard());
	}

}
