package bonustests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import actions.BuyPermissionCard;
import actions.CouncelorElection;
import actions.EmporiumConstruction;
import actions.PrimaryAction;
import bonuses.Bonus;
import bonuses.DoAnotherPrimaryActionBonus;
import bonuses.GiveCoins;
import gamemanagement.Player;
import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.Emporium;

public class DoAnotherPrimaryActionBonusTest {
	

	
	@Test
	public void testgetAction(){
		Player player = new Player(0, null, null);
		PrimaryAction action = new CouncelorElection(player, null, null);
		DoAnotherPrimaryActionBonus doAnotherPrimaryActionBonus = new DoAnotherPrimaryActionBonus();
		doAnotherPrimaryActionBonus.setAction(action);
		assertEquals(action, doAnotherPrimaryActionBonus.getAction());
	}
	
	@Test
	public void testsetAction(){
		Player player = new Player(0, null, null);
		PrimaryAction action = new CouncelorElection(player, null, null);
		PrimaryAction action1 = new BuyPermissionCard(player, null, 0, null, null, null, null);
		DoAnotherPrimaryActionBonus doAnotherPrimaryActionBonus = new DoAnotherPrimaryActionBonus();
		doAnotherPrimaryActionBonus.setAction(action);
		assertEquals(action, doAnotherPrimaryActionBonus.getAction());
		doAnotherPrimaryActionBonus.setAction(action1);
		assertEquals(action1, doAnotherPrimaryActionBonus.getAction());
	}

}
