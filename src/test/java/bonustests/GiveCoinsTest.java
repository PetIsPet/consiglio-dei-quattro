package bonustests;

import static org.junit.Assert.*;

import org.junit.Test;

import bonuses.GiveCoins;
import gamemanagement.Player;

public class GiveCoinsTest {
	
	@Test
	public void testGiveCoins() {
		GiveCoins bonus=new GiveCoins(3);
		assertEquals(3,bonus.getNumberOfCoins());
	}

	@Test
	public void testAct() {
		GiveCoins bonus=new GiveCoins(3);
		Player player=new Player(0,null,null);
		player.setCoins(2);
		bonus.act(player);
		assertEquals(5,player.getCoins());
		
		
	}

	@Test
	public void testGetName() {
		GiveCoins bonus=new GiveCoins(3);
		bonus.setName("nome");
		assertEquals("nome",bonus.getName());
	}

	@Test
	public void testSetName() {
		GiveCoins bonus=new GiveCoins(3);
		String nome="nome";
		bonus.setName(nome);
		assertEquals("nome",bonus.getName());
	}

	@Test
	public void testGetNumberOfCoins() {
		GiveCoins bonus=new GiveCoins(3);
		assertEquals(3,bonus.getNumberOfCoins());
	}

	@Test
	public void testSetNumberOfCoins() {
		GiveCoins bonus=new GiveCoins(3);
		bonus.setNumberOfCoins(2);
		assertEquals(2,bonus.getNumberOfCoins());
	}

}
