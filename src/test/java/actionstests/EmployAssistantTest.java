package actionstests;

import static org.junit.Assert.*;

import org.junit.Test;

import actions.EmployAssistant;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;

public class EmployAssistantTest {

	@Test
	public void testEmployAssistant() {
		Player player=new Player(0,null,null);
		EmployAssistant action=new EmployAssistant(player);
		assertEquals(false,action.equals(null));
	}

	@Test
	public void testIsEnoughMoneyShouldBeFalse() {
		Player player=new Player(0,null,null);
		player.setCoins(2);
		EmployAssistant action=new EmployAssistant(player);
		assertEquals(false,action.isEnoughMoney());
	}
	
	@Test
	public void testIsEnoughMoneyShouldBeTrue() {
		Player player=new Player(0,null,null);
		player.setCoins(4);
		EmployAssistant action=new EmployAssistant(player);
		assertEquals(true,action.isEnoughMoney());
	}

	@Test
	public void testDoSecondaryAction() {
		Player player=new Player(0,null,null);
		player.setCoins(4);
		player.setAssistants(4);
		EmployAssistant action=new EmployAssistant(player);
		try {
			action.doSecondaryAction();
		} catch (NotEnoughResourcesException e) {
			
			e.printStackTrace();
		}
		assertEquals(5,player.getAssistants());
		
	}

}
