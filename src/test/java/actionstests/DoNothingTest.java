package actionstests;

import static org.junit.Assert.*;

import org.junit.Test;

import actions.DoNothing;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;

public class DoNothingTest {

	@Test
	public void testDoNothing() throws NotEnoughResourcesException, InvalidDoAnotherPrimaryActionChoiceException{
		Player player = new Player(0, null, null);
		player.setCoins(3);
		DoNothing nothing = new DoNothing();
		nothing.doSecondaryAction();
		assertEquals(3, player.getCoins());
	}
}
