package actionstests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import actions.CouncelorElection;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import mappaeoggetti.Balcony;
import mappaeoggetti.PermissionDeck;
import mappaeoggetti.Type;
import mvc.Game;

public class CouncelorElectionTest {

	@Test
	public void testCouncelorElection() {
		Player player=new Player(0,null,null);
		CouncelorElection action=new CouncelorElection(player,Color.YELLOW,null);
		
		assertEquals(false,action.getPlayer().equals(null));
	}

	@Test
	public void testDoPrimaryActionCouncelorIsCorrect() throws FileNotFoundException {
		
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		Player p=new Player(0, null, null);
		game.getPlayers().add(p);
		game.initialize();
		
		
		Player player=game.getPlayers().get(0);
		player.setCli(game.getCli());
		
		CouncelorElection action=new CouncelorElection(player,Color.MAGENTA,game.getBalconies().get(2));
		
		action.doPrimaryAction();//scelgo balcone di montagna e colore magenta da cli
		Balcony balcony=game.getBalconies().get(2);
		assertEquals(Color.MAGENTA,balcony.getCouncelors()[3].getColor());
	}
	
	@Test
	public void testGetPlayer() {
		Player player=new Player(0,null,null);
		CouncelorElection action=new CouncelorElection(player,null,null);
		
		assertEquals(player,action.getPlayer());
	}

}
