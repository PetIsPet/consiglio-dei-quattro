package actionstests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import actions.BuyPermissionCard;
import bonuses.Bonus;
import bonuses.GiveCoins;
import exceptions.NotEnoughResourcesException;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import mappaeoggetti.Balcony;
import mappaeoggetti.Card;
import mappaeoggetti.Councelor;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mappaeoggetti.Type;
import mvc.Game;

public class BuyPermissionCardTest {
	
	@Test
	public void testBuyPermissionCard() throws FileNotFoundException{
		Lettore loader=new Lettore();
		GraphMap map=new GraphMap(loader);
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		ArrayList<Councelor> pool=new ArrayList<>();
		Councelor co1 = new Councelor(Color.BLACK);
		Councelor co2 = new Councelor(Color.WHITE);
		Councelor co3 = new Councelor(Color.PINK);
		Councelor co4 = new Councelor(Color.GREEN);	
		pool.add(co1);
		pool.add(co2);
		pool.add(co3);
		pool.add(co4);
		game.setCouncelorsPool(pool);
		Balcony balcony = new Balcony(game,Type.MOUNTAIN);
		Player player = new Player(0, null, null);
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		PermissionDeck deck = new PermissionDeck(null, cards);
		ArrayList<PermissionDeck> decks = new ArrayList<>();
		BuyPermissionCard buyPermissionCard = new BuyPermissionCard(player, deck, 0, balcony, decks, null, null);
		game.initialize();
		buyPermissionCard.setBalcony(balcony);
		assertEquals(player, buyPermissionCard.getPlayer());
		assertEquals(deck, buyPermissionCard.getDeck());
		assertEquals(balcony, buyPermissionCard.getBalcony());
	}
	
	@Test
	public void testGetPlayer() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		assertEquals(player,action.getPlayer());
		
	}

	@Test
	public void testSetPlayer() {
		Player player1=new Player(0,null,null);
		Player player2=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player1,null, 0, null, null, null, null);
		action.setPlayer(player2);
		assertEquals(player2,action.getPlayer());
		
	}

	@Test
	public void testSetDiscoveredCard() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		PermissionCard[] discoveredCards=new PermissionCard[2];
		discoveredCards[0]=new PermissionCard(null,null,null);
		discoveredCards[1]=new PermissionCard(null,null,null);
		action.setDiscoveredCard(discoveredCards);
		assertArrayEquals(discoveredCards,action.getDiscoveredCard());
		
	}
	
	
	@Test
	public void testGetDiscoveredCard() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		PermissionCard[] discoveredCards=new PermissionCard[2];
		discoveredCards[0]=new PermissionCard(null,null,null);
		discoveredCards[1]=new PermissionCard(null,null,null);
		action.setDiscoveredCard(discoveredCards);
		assertArrayEquals(action.getDiscoveredCard(),discoveredCards);
	}


	@Test
	public void testGetDeck() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		 ArrayList<PermissionCard> cards=new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		 PermissionDeck deck=new PermissionDeck(Type.MOUNTAIN,cards);
		action.setDeck(deck);
		assertEquals(action.getDeck(),deck);
	}

	@Test
	public void testSetDeck() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		 ArrayList<PermissionCard> cards=new ArrayList<>();
		PermissionCard c1=new PermissionCard(null, null, null);
		PermissionCard c2=new PermissionCard(null, null, null);
		cards.add(c2);
		cards.add(c1);
		 PermissionDeck deck=new PermissionDeck(Type.MOUNTAIN,cards);
		 action.setDeck(deck);
		 assertEquals(deck,action.getDeck());
	}

	@Test
	public void testGetCounterPoliticsCard() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		assertEquals(0,action.getCounterPoliticsCard());
	}

	@Test
	public void testSetCounterPoliticsCard() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		action.setCounterPoliticsCard(3);
		assertEquals(3,action.getCounterPoliticsCard());
	}

	@Test
	public void testGetCards() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		ArrayList<Card> cards=new ArrayList<>();
		Card card1=new Card(Color.BLACK);
		cards.add(card1);
		action.setCards(cards);
		assertEquals(cards,action.getCards());
	}

	@Test
	public void testSetCards() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null, 0, null, null, null, null);
		ArrayList<Card> cards=new ArrayList<>();
		Card card1=new Card(Color.BLACK);
		cards.add(card1);
		action.setCards(cards);
		assertEquals(cards,action.getCards());
	}

	@Test
	public void testGetBalcony() throws FileNotFoundException {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		Lettore loader=new Lettore();
		GraphMap map=new GraphMap(loader);
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		game.initialize();
		Balcony balcony=new Balcony(game);
		action.setBalcony(balcony);
		assertEquals(balcony,action.getBalcony());
	}

	@Test
	public void testSetBalcony() throws FileNotFoundException {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		Lettore loader=new Lettore();
		GraphMap map=new GraphMap(loader);
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		game.initialize();
		Balcony balcony=new Balcony(game);
		action.setBalcony(balcony);
		assertEquals(balcony,action.getBalcony());
	}

	@Test
	public void testGetMatches() {
		Player player=new Player(0,null,null);
		ArrayList<Card> matches=new ArrayList<>();
		Card card1=new Card(Color.YELLOW);
		matches.add(card1);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		action.setMatches(matches);
		assertEquals(matches,action.getMatches());

	}

	@Test
	public void testSetMatches() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> matches=new ArrayList<>();
		Card card1=new Card(Color.YELLOW);
		matches.add(card1);
		action.setMatches(matches);
		assertEquals(matches,action.getMatches());
	}



	@Test
	public void testGetUnmatchedCouncelors() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Councelor> councelors=new ArrayList<>();
		Councelor c=new Councelor(Color.BLACK);
		councelors.add(c);
		action.setUnmatchedCouncelors(councelors);
		assertEquals(councelors,action.getUnmatchedCouncelors());
	}

	@Test
	public void testSetUnmatchedCouncelors() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Councelor> councelors=new ArrayList<>();
		Councelor c=new Councelor(Color.BLACK);
		councelors.add(c);
		action.setUnmatchedCouncelors(councelors);
		assertEquals(councelors,action.getUnmatchedCouncelors());
	}

	@Test
	public void testIsEnoughCardIsTrue() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> cards = new ArrayList<>();
		Card card = new Card(null);
		Card card1 = new Card(null);
		cards.add(card);
		cards.add(card1);
		player.setCards(cards);
		assertEquals(true,action.isEnoughCard());
	}
	
	@Test
	public void testIsEnoughCardIsFalse() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> cards = new ArrayList<>();
		player.setCards(cards);
		assertEquals(false,action.isEnoughCard());
	}

	@Test
	public void testCreateMatchPool() throws FileNotFoundException {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.YELLOW);
		Councelor c3=new Councelor(Color.BLACK);
		Councelor c4=new Councelor(Color.BLUE);
		ArrayList<Councelor> councelors=new ArrayList<>();
		councelors.add(c1);
		councelors.add(c2);
		councelors.add(c3);
		councelors.add(c4);
		action.setUnmatchedCouncelors(councelors);
		int i=0;
		Lettore loader=new Lettore();
		GraphMap map=new GraphMap(loader);
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		game.initialize();
		Councelor[] counc=new Councelor[4];
		Balcony balcony=new Balcony(game);
		for(Councelor councelor:councelors){
			
			counc[i]=councelor;
			i++;
		}
		balcony.setCouncelors(counc);
		action.setBalcony(balcony);
		
		Card card1=new Card(Color.BLACK);
		Card card2=new Card(Color.YELLOW);
		Card card3=new Card(Color.WHITE);
		ArrayList<Card> cards=new ArrayList<>();
		ArrayList<Card> matches=new ArrayList<>();
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		matches.add(card1);
		matches.add(card2);
		action.setCards(cards);
		action.createMatchPool();
		action.setMatches(matches);
		
		assertEquals(matches,action.getMatches());
		
		
	}

	@Test
	public void testCalculatePriceShouldBe10() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> matches=new ArrayList<>();
		ArrayList<Councelor> councelors=new ArrayList<>();
		
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.WHITE);
		Councelor c3=new Councelor(Color.BLACK);
		Councelor c4=new Councelor(Color.MAGENTA);
		
		councelors.add(c1);
		councelors.add(c2);
		councelors.add(c3);
		councelors.add(c4);
		
		Card ca1=new Card(Color.BLACK);
		matches.add(ca1);
		
		assertEquals(10,action.calculatePrice(matches));

		
	}
	
	@Test
	public void testCalculatePriceShouldBe7() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> matches=new ArrayList<>();
		ArrayList<Councelor> councelors=new ArrayList<>();
		
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.WHITE);
		Councelor c3=new Councelor(Color.BLACK);
		Councelor c4=new Councelor(Color.MAGENTA);
		
		councelors.add(c1);
		councelors.add(c2);
		councelors.add(c3);
		councelors.add(c4);
		
		
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.MAGENTA);
		matches.add(ca1);
		matches.add(ca2);
		
		assertEquals(7,action.calculatePrice(matches));

		
	}
	
	@Test
	public void testCalculatePriceShouldBe4() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> matches=new ArrayList<>();
		ArrayList<Councelor> councelors=new ArrayList<>();
		
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.WHITE);
		Councelor c3=new Councelor(Color.BLACK);
		Councelor c4=new Councelor(Color.MAGENTA);
		
		councelors.add(c1);
		councelors.add(c2);
		councelors.add(c3);
		councelors.add(c4);
		
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.MAGENTA);
		Card ca3=new Card(Color.BLACK);
		matches.add(ca1);
		matches.add(ca2);
		matches.add(ca3);
		
		assertEquals(4,action.calculatePrice(matches));

		
	}
	
	@Test
	public void testCalculatePriceShouldBe0() {
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> matches=new ArrayList<>();
		ArrayList<Councelor> councelors=new ArrayList<>();
		
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.WHITE);
		Councelor c3=new Councelor(Color.BLACK);
		Councelor c4=new Councelor(Color.MAGENTA);
		
		councelors.add(c1);
		councelors.add(c2);
		councelors.add(c3);
		councelors.add(c4);
		
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.MAGENTA);
		Card ca3=new Card(Color.BLACK);
		Card ca4=new Card(Color.WHITE);
		matches.add(ca1);
		matches.add(ca2);
		matches.add(ca3);
		matches.add(ca4);
		
		assertEquals(0,action.calculatePrice(matches));

		
	}

	@Test
	public void testDoPrimaryAction() {
		
		Player player=new Player(0,null,null);
		BuyPermissionCard action=new BuyPermissionCard(player,null,0,null, null, null, null);
		ArrayList<Card> matches=new ArrayList<>();
		ArrayList<Councelor> councelors=new ArrayList<>();
		
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.WHITE);
		Councelor c3=new Councelor(Color.BLACK);
		Councelor c4=new Councelor(Color.MAGENTA);
		
		councelors.add(c1);
		councelors.add(c2);
		councelors.add(c3);
		councelors.add(c4);
		
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.MAGENTA);
		Card ca3=new Card(Color.BLACK);
		Card ca4=new Card(Color.WHITE);
		matches.add(ca1);
		matches.add(ca2);
		matches.add(ca3);
		matches.add(ca4);
		
		player.setCards(matches);
		
		ArrayList<PermissionCard> cards=new ArrayList<>();
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins bonus = new GiveCoins(3);
		bonuses.add(bonus);
		PermissionCard pc1=new PermissionCard(bonuses, null, null);
		PermissionCard pc2=new PermissionCard(bonuses, null, null);
		PermissionCard pc3=new PermissionCard(bonuses, null, null);
		PermissionCard pc4=new PermissionCard(bonuses, null, null);
		cards.add(pc1);
		cards.add(pc2);
		cards.add(pc3);
		cards.add(pc4);
		PermissionDeck deck=new PermissionDeck(Type.MOUNTAIN,cards);
		action.setDeck(deck);
		
		ArrayList<Card> matches1 = new ArrayList<>();
		action.setMatches(matches1);
		matches1.addAll(matches);
		
		try {
			action.doPrimaryAction();
		} catch (NotEnoughResourcesException e) {
			e.printStackTrace();
		}

		
		assertEquals(0, action.calculatePrice(matches));
		assertEquals(true, player.getCards().isEmpty());
		assertEquals(3, player.getCoins());
		assertEquals(1, player.getPermits().size());
		
	}

}