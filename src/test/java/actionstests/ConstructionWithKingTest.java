package actionstests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import actions.ConstructionWithKing;
import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import mappaeoggetti.Balcony;
import mappaeoggetti.Card;
import mappaeoggetti.City;
import mappaeoggetti.Councelor;
import mappaeoggetti.Emporium;
import mappaeoggetti.King;
import mvc.Game;

public class ConstructionWithKingTest {

	@Test
	public void testConstructionWithKing() {
		Lettore loader=new Lettore();
		GraphMap map=new GraphMap(loader);
		King king=new King(loader.getCityKing());
		Player player=new Player(0,Color.YELLOW,null);
		ConstructionWithKing action=new ConstructionWithKing(king,player,map,null,null, null);
		assertEquals(false,action.equals(null));
	}

	@Test
	public void testDoPrimaryAction() throws FileNotFoundException {
		Game game = new Game(null, null);
		Lettore reader = new Lettore();
		City previousCity = new City(null, null, null);
		King king = new King(previousCity);
		Player player = new Player(0, null, null);
		GraphMap graph = new GraphMap(reader);
		City nextCity = null;
		ArrayList<Councelor> councelorsPool = new ArrayList<>();
		councelorsPool.add(new Councelor(Color.WHITE));
		councelorsPool.add(new Councelor(Color.BLACK));
		councelorsPool.add(new Councelor(Color.BLACK));
		councelorsPool.add(new Councelor(Color.ORANGE));
		game.setCouncelorsPool(councelorsPool);
		Balcony balcony = new Balcony(game);
		boolean flag = false;
		ConstructionWithKing cwk = new ConstructionWithKing(king, player,graph,balcony, nextCity, null);
		try {
			cwk.doPrimaryAction();
		} catch (NotEnoughResourcesException e) {
			flag=true;
		} catch (InvalidInputException e) {
		}
		assertEquals(true,flag);
		}

	
	@Test
	public void testHasBuiltShouldBeFalse() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		King king=new King(loader.getCityKing());
		Player player=new Player(0,Color.YELLOW,null);
		Player player2=new Player(0,Color.BLACK,null);
		ConstructionWithKing action=new ConstructionWithKing(king,player,map,null,null, null);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		Emporium e=new Emporium(player2);
		emporiums.add(e);
		king.getCurrentCity().setEmporiums(emporiums);
		assertEquals(false,action.hasBuilt(king.getCurrentCity()));
	}
	
	@Test
	public void testHasBuiltShouldBeTrue() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		King king=new King(loader.getCityKing());
		Player player=new Player(0,Color.YELLOW,null);
		ConstructionWithKing action=new ConstructionWithKing(king,player,map,null,null, null);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		Emporium e=new Emporium(player);
		emporiums.add(e);
		king.getCurrentCity().setEmporiums(emporiums);
		assertEquals(true,action.hasBuilt(king.getCurrentCity()));
	}

	@Test
	public void testPriceToCorruptShouldBe0() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Player p=new Player(0, null, null);
		Game game=new Game(colors,map);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		Card ca4=new Card(Color.WHITE);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		cards.add(ca4);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),map,game.getKingBalcony(),game.getCities().get(3), null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors=new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		action.getKingBalcony().setCouncelors(councelors);
		assertEquals(0,action.priceToCorruptBalcony());
	}
	
	@Test
	public void testPriceToCorruptShouldBe4() throws FileNotFoundException {
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		action.setMatches(cards);
		assertEquals(4, action.priceToCorruptBalcony());	
	}
	
	@Test
	public void testPriceToCorruptShouldBe3() throws FileNotFoundException {
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.YELLOW);
		Card ca3=new Card(Color.YELLOW);
		Card ca4=new Card(Color.YELLOW);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		cards.add(ca4);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		action.setMatches(cards);
		assertEquals(3, action.priceToCorruptBalcony());
	}
	
	@Test
	public void testPriceToCorruptShouldBe7() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Player p=new Player(0, null, null);
		Game game=new Game(colors,map);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),map,game.getKingBalcony(),game.getCities().get(3), null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors=new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		action.getKingBalcony().setCouncelors(councelors);
		action.setMatches(cards);
		assertEquals(7,action.priceToCorruptBalcony());
	}
	
	@Test
	public void testPriceToCorruptShouldBe10() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Player p=new Player(0, null, null);
		Game game=new Game(colors,map);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),map,game.getKingBalcony(),game.getCities().get(3), null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors=new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		action.getKingBalcony().setCouncelors(councelors);
		action.setMatches(cards);
		assertEquals(10,action.priceToCorruptBalcony());
	}
	@Test
	public void testPriceToCorruptShouldBe11() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Player p=new Player(0, null, null);
		Game game=new Game(colors,map);
		game.initialize();
		Card ca1=new Card(Color.YELLOW);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),map,game.getKingBalcony(),game.getCities().get(3), null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors=new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		action.getKingBalcony().setCouncelors(councelors);
		action.setMatches(cards);
		assertEquals(11,action.priceToCorruptBalcony());
	}
	
	@Test
	public void testPriceToCorruptShouldBe7WithYellow() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Player p=new Player(0, null, null);
		Game game=new Game(colors,map);
		game.initialize();
		Card ca1=new Card(Color.YELLOW);
		Card ca2=new Card(Color.YELLOW);
		Card ca3=new Card(Color.YELLOW);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),map,game.getKingBalcony(),game.getCities().get(3), null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors=new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		action.getKingBalcony().setCouncelors(councelors);
		action.setMatches(cards);
		assertEquals(7,action.priceToCorruptBalcony());
	}
	
	@Test
	public void testgetKingBalcony() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		assertEquals(balcony, action.getKingBalcony());
	}
	
	@Test
	public void testsetKingBalcony() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		assertEquals(balcony, action.getKingBalcony());
		Balcony balcony1 = new Balcony(game);
		balcony1.setCouncelors(councelors);
		action.setKingBalcony(balcony1);
		assertEquals(balcony1, action.getKingBalcony());
	}
	
	@Test
	public void testgetKing() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		King king = new King(game.getCities().get(0));
		ConstructionWithKing action=new ConstructionWithKing(king,game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		assertEquals(king, action.getKing());
	}
	
	@Test
	public void testgetMatches() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		ArrayList<Card> matches = new ArrayList<>();
		action.setMatches(matches);
		assertEquals(matches, action.getMatches());
	}
	
	@Test
	public void testsetMatches() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		ArrayList<Card> matches = new ArrayList<>();
		action.setMatches(matches);
		assertEquals(matches, action.getMatches());
		ArrayList<Card> matches1 = new ArrayList<>();
		action.setMatches(matches1);
		assertEquals(matches1, action.getMatches());
	}
	
	@Test
	public void testgetPrice() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		int price = 1;
		action.setPrice(price);
		assertEquals(1, action.getPrice());
	}
	
	@Test
	public void testsetPrice() throws FileNotFoundException{
		Player p=new Player(0, null, null);
		Game game=new Game(null,null);
		game.initialize();
		Card ca1=new Card(Color.BLACK);
		Card ca2=new Card(Color.BLUE);
		Card ca3=new Card(Color.MAGENTA);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		cards.add(ca3);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors = new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		Balcony balcony = new Balcony(game);
		balcony.setCouncelors(councelors);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),null,balcony,game.getCities().get(3), null);
		int price = 0;
		action.setPrice(price);
		assertEquals(0, action.getPrice());
		int price1=4;
		action.setPrice(price1);
		assertEquals(4, action.getPrice());
	}
	
	@Test
	public void testcreateMatchPool() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Player p=new Player(0, null, null);
		Game game=new Game(colors,map);
		game.initialize();
		Card ca1=new Card(Color.YELLOW);
		Card ca2=new Card(Color.BLACK);
		ArrayList<Card> cards=new ArrayList<>();
		cards.add(ca1);
		cards.add(ca2);
		game.getPlayers().add(p);
		game.getPlayers().get(0).setCards(cards);
		ConstructionWithKing action=new ConstructionWithKing(game.getKing(),game.getPlayers().get(0),map,game.getKingBalcony(),game.getCities().get(3), null);
		Councelor c1=new Councelor(Color.BLACK);
		Councelor c2=new Councelor(Color.BLUE);
		Councelor c3=new Councelor(Color.MAGENTA);
		Councelor c4=new Councelor(Color.WHITE);
		Councelor[] councelors=new Councelor[4];
		councelors[0]=c1;
		councelors[1]=c2;
		councelors[2]=c3;
		councelors[3]=c4;
		action.getKingBalcony().setCouncelors(councelors);
		
		action.createMatchPool();
		assertEquals(2 ,action.getMatches().size());
	}
	
}
