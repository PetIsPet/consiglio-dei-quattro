package actionstests;

import static org.junit.Assert.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.junit.Test;

import actions.BuyPermissionCard;
import actions.EmporiumConstruction;
import bonuses.Bonus;
import bonuses.GiveAssistants;
import bonuses.GiveCoins;
import bonuses.GivePoints;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.Emporium;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mvc.Game;
import servercompleto.ServerView;

public class EmporiumConstructionTest {

	@Test
	public void testEmporiumConstruction() {
		Player player=new Player(0,null,null);
		City city=new City(null,null,null);
		ArrayList<PermissionDeck> deck = new ArrayList<>();
		EmporiumConstruction action=new EmporiumConstruction(player,city, deck, null, null);
		assertEquals(false,action.equals(null));
		assertEquals(deck, action.getDecks());
	}

	@Test
	public void testDoPrimaryAction() {
		Game game = new Game(null, null);
		TurnManager controller = new TurnManager(null, game);
		Player player1=new Player(0,null,null);
		Player player2=new Player(0,null,null);
		Player player3=new Player(0,null,null);
		Player player4=new Player(0,null,null);
		City city=new City(null,null,null);
		CityToken cityToken = new CityToken(null);
		Bonus bonus = new GiveAssistants(3);
		cityToken.getBonuses().add(bonus);
		city.setCityToken(cityToken);
		City c2=new City(null,null,null);
		City c3=new City(null,null,null);
		ArrayList<City> n1=new ArrayList<>();
		n1.add(c2);
		n1.add(c3);
		city.setNeighborhood(n1);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		emporiums.add(new Emporium(player2));
		emporiums.add(new Emporium(player3));
		emporiums.add(new Emporium(player4));
		city.setEmporiums(emporiums);
		player1.setAssistants(10);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveAssistants giveAssistants = new GiveAssistants(0);
		GivePoints givePoints = new GivePoints(0);
		bonuses.add(giveAssistants);
		bonuses.add(givePoints);
		cityToken.setBonuses(bonuses);
		ArrayList<PermissionCard> cards = new ArrayList<>();
		PermissionCard card = new PermissionCard(bonuses, null, null);
		PermissionCard card1 = new PermissionCard(bonuses, null, null);
		PermissionCard card2 = new PermissionCard(bonuses, null, null);
		cards.add(card);
		cards.add(card1);
		cards.add(card2);
		PermissionDeck deck = new PermissionDeck(null, cards);
		BuyPermissionCard buy = new BuyPermissionCard(player1, deck, 0, null, null, null, controller);

		try {
			buy.doPrimaryAction();
		} catch (NotEnoughResourcesException e) {
			
			e.printStackTrace();
		}
		assertEquals(1, player1.getPermits().size());
	}


	
	@Test
	public void testisEnoughAssistantShouldBeTrue(){
		Player player = new Player(0, null, null);
		player.setBuiltEmporiums(2);
		player.setAssistants(3);
		EmporiumConstruction construction = new EmporiumConstruction(player, null, null, null, null);
		assertEquals(true, construction.isEnoughAssistant(player.getBuiltEmporiums()));
	}
	
	@Test
	public void testisEnoughAssistantShouldBeFalse(){
		Player player = new Player(0, null, null);
		player.setBuiltEmporiums(2);
		player.setAssistants(1);
		EmporiumConstruction construction = new EmporiumConstruction(player, null, null, null, null);
		assertEquals(false, construction.isEnoughAssistant(player.getBuiltEmporiums()));
	}

	@Test
	public void testHasBuiltShouldBeFalse() {
		Player player1=new Player(0,null,null);
		Player player2=new Player(0,null,null);
		Player player3=new Player(0,null,null);
		Player player4=new Player(0,null,null);
		City city=new City(null,null,null);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		emporiums.add(new Emporium(player2));
		emporiums.add(new Emporium(player3));
		emporiums.add(new Emporium(player4));
		city.setEmporiums(emporiums);
		EmporiumConstruction action=new EmporiumConstruction(player1,city, null, null, null);
		assertEquals(false,action.hasBuilt(city));
	}
	
	@Test
	public void testHasBuiltShouldBeTrue() {
		Player player1=new Player(0,null,null);
		Player player2=new Player(0,null,null);
		Player player3=new Player(0,null,null);
		City city=new City(null,null,null);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		emporiums.add(new Emporium(player2));
		emporiums.add(new Emporium(player3));
		emporiums.add(new Emporium(player1));
		city.setEmporiums(emporiums);
		EmporiumConstruction action=new EmporiumConstruction(player1,city, null, null, null);
		assertEquals(true,action.hasBuilt(city));
	}
	
	@Test
	public void testanotherHasBuiltShouldBeTrue(){
		Player player1=new Player(0,null,null);
		Player player2=new Player(0,null,null);
		Player player3=new Player(0,null,null);
		City city=new City(null,null,null);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		emporiums.add(new Emporium(player2));
		emporiums.add(new Emporium(player3));
		emporiums.add(new Emporium(player1));
		city.setEmporiums(emporiums);
		EmporiumConstruction action=new EmporiumConstruction(player1,city, null, null, null);
		assertEquals(true,action.anotherHasBuilt(city));
	}
	
	@Test
	public void testanotherHasBuiltShouldBeFalse(){
		Player player1=new Player(0,null,null);
		City city=new City(null,null,null);
		ArrayList<Emporium> emporiums=new ArrayList<>();
		emporiums.add(new Emporium(player1));
		city.setEmporiums(emporiums);
		EmporiumConstruction action=new EmporiumConstruction(player1,city, null, null, null);
		assertEquals(false,action.anotherHasBuilt(city));
	}
	
	@Test
	public void testgetNeighboursBonus(){
		ArrayList<City> n1=new ArrayList<>();
		ArrayList<City> n2=new ArrayList<>();
		ArrayList<City> n3=new ArrayList<>();
		ArrayList<City> n4=new ArrayList<>();
		GiveAssistants bonus=new GiveAssistants(2);
		CityToken cityToken = new CityToken(null);
		CityToken cityToken1 = new CityToken(null);
		CityToken cityToken2 = new CityToken(null);
		Bonus bonus1 = new GiveCoins(2);
		Bonus bonus2 = new GivePoints(2);
		Bonus bonus3 = new GivePoints(3);
		cityToken.getBonuses().add(bonus1);
		cityToken1.getBonuses().add(bonus2);
		cityToken2.getBonuses().add(bonus3);
		City c1=new City(n1,cityToken,null);
		City c2=new City(n2,cityToken1,null);
		City c3=new City(n3,cityToken2,null);
	    n1.add(c2);
		n1.add(c3);
		n2.add(c3);
		n2.add(c1);
		n3.add(c2);
		n3.add(c1);
		c1.setNeighborhood(n1);
		c2.setNeighborhood(n2);
		c3.setNeighborhood(n3);
		Player player1=new Player(0,null,null);
		c1.getEmporiums().add(new Emporium(player1));
		c3.getEmporiums().add(new Emporium(player1));
		c2.getEmporiums().add(new Emporium(player1));
		EmporiumConstruction action=new EmporiumConstruction(player1,c1, null, null, null);
		ArrayList<Bonus> bonuses=EmporiumConstruction.getNeighboursBonus(c1, player1);
		action.getBonus().addAll(bonuses);
		assertEquals(true,bonuses.contains(bonus1));
		assertEquals(true,bonuses.contains(bonus2));
		assertEquals(true,bonuses.contains(bonus3));
	}
	
	@Test
	public void testNeighbourHaveMyEmporiumShouldBeFalse(){
		
		Player player=new Player(0,null,null);
		Player player2=new Player(0,null,null);
		CityToken cityToken = new CityToken(null);
		Bonus bonus = new GiveAssistants(3);
		cityToken.getBonuses().add(bonus);
		City c1=new City(null,cityToken,null);
		c1.getEmporiums().add(new Emporium(player2));
		EmporiumConstruction action=new EmporiumConstruction(player,c1, null, null, null);
		assertEquals(false, action.neighbourHaveMyEmporium(c1,player));	
	}
	
	@Test
	public void testNeighbourHaveMyEmporiumShouldBeTrue(){
		
		Player player=new Player(0,null,null);
		CityToken cityToken = new CityToken(null);
		Bonus bonus = new GiveAssistants(3);
		cityToken.getBonuses().add(bonus);
		City c1=new City(null,cityToken,null);
		c1.getEmporiums().add(new Emporium(player));
		EmporiumConstruction action=new EmporiumConstruction(player,c1, null, null, null);
		assertEquals(true,EmporiumConstruction.neighbourHaveMyEmporium(c1,player));	
	}
	
	@Test
	public void testGetBonus() {
	
		
		
	}
	
	@Test
	public void testgetDeck(){
		ArrayList<PermissionDeck> deck = new ArrayList<>();
		EmporiumConstruction action=new EmporiumConstruction(null,null, deck, null, null);
		assertEquals(deck, action.getDecks());
	}

}
