package actionstests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import actions.ChangePermissionCard;
import bonuses.Bonus;
import bonuses.GiveAssistants;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import mappaeoggetti.City;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;

public class ChangePermissionCardTest {

	@Test
	public void testChangePermissionCard() {
		
		Player player=new Player(0, null, null);
		ChangePermissionCard action=new ChangePermissionCard(player,null);
		assertEquals(false,action.equals(null));
	}

	@Test
	public void testIsEnoughAssistantShouldBeTrue() {
		Player player=new Player(0, null, null);
		player.setAssistants(3);
		ChangePermissionCard action=new ChangePermissionCard(player,null);
		assertEquals(true,action.isEnoughAssistant());
	}

	@Test
	public void testIsEnoughAssistantShouldBeFalse() {
		Player player=new Player(0, null, null);
		player.setAssistants(0);
		ChangePermissionCard action=new ChangePermissionCard(player,null);
		assertEquals(false,action.isEnoughAssistant());
	}
	
	@Test
	public void testDoSecondaryAction() throws NotEnoughResourcesException {
		Player player = new Player(0, null, null);
		player.setAssistants(3);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		Bonus bonus = new GiveAssistants(0);
		bonuses.add(bonus);
		ArrayList<City> cities = new ArrayList<>();
		PermissionCard card = new PermissionCard(bonuses, null, cities);
		PermissionCard card1 = new PermissionCard(bonuses, null, cities);
		PermissionCard card2 = new PermissionCard(bonuses, null, cities);
		PermissionCard card3 = new PermissionCard(bonuses, null, cities);
		ArrayList<PermissionCard> cards = new ArrayList<>();
		cards.add(card);
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		player.setPermits(cards);
		PermissionDeck deck = new PermissionDeck(null, cards);
		PermissionCard[] cards1=deck.getDiscoveredCards();
		ChangePermissionCard action=new ChangePermissionCard(player,deck);
		action.getDeck().setCards(cards);
		action.doSecondaryAction();
		assertEquals(2, player.getAssistants());
		assertEquals(cards, deck.getCards());
		assertEquals(false, action.getDeck().equals(cards1));
	}
}
