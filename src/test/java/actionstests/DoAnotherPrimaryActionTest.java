package actionstests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import actions.BuyPermissionCard;
import actions.DoAnotherPrimaryAction;
import actions.PrimaryAction;
import bonuses.Bonus;
import bonuses.GiveCoins;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.NotEnoughResourcesException;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import gamemanagement.Turn;
import mappaeoggetti.Balcony;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mvc.Game;

public class DoAnotherPrimaryActionTest {

	@Test
	public void testDoAnotherPrimaryAction() {
		Player player=new Player(0,null,null);
		DoAnotherPrimaryAction action=new DoAnotherPrimaryAction(player,null,null);
		assertEquals(false,action.equals(null));
	}

	@Test
	public void testIsEnoughAssistantShouldBeTrue() {
		
		Player player=new Player(0,null,null);
		player.setAssistants(4);
		DoAnotherPrimaryAction action=new DoAnotherPrimaryAction(player,null,null);
		assertEquals(true,action.isEnoughAssistant());
	}
	
	@Test
	public void testIsEnoughAssistantShouldBeFalse() {
		
		Player player=new Player(0,null,null);
		player.setAssistants(2);
		DoAnotherPrimaryAction action=new DoAnotherPrimaryAction(player,null,null);
		assertEquals(false,action.isEnoughAssistant());
	}

	@Test
	public void testBonusAnotherPrimaryAction() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		Player p=new Player(0, null, null);
		game.getPlayers().add(p);
		game.initialize();
		
		Player player=game.getPlayers().get(0);
		player.setCli(game.getCli());
		
		BuyPermissionCard action2=new BuyPermissionCard(player, null,0,null, null, null, null);
		player.getCli().setTurn(new Turn(player));
		DoAnotherPrimaryAction action=new DoAnotherPrimaryAction(player,action2,player.getCli().getTurn());
 		action.bonusAnotherPrimaryAction();
		assertEquals(true,player.getCli().getTurn().getPrimaryActionPool().contains(action2));
	}

	@Test
	public void testDoSecondaryAction() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		Player p=new Player(0, null, null);
		game.getPlayers().add(p);
		game.initialize();
		
		Player player=game.getPlayers().get(0);
		player.setCli(game.getCli());
		player.setCoins(7);
		player.setAssistants(4);
		ArrayList<Bonus> bonuses = new ArrayList<>();
		GiveCoins coins = new GiveCoins(2);
		bonuses.add(coins);
		PermissionCard card = new PermissionCard(bonuses, null, null);
		PermissionCard card1 = new PermissionCard(bonuses, null, null);
		PermissionCard card2 = new PermissionCard(bonuses, null, null);
		PermissionCard card3 = new PermissionCard(bonuses, null, null);
		ArrayList<PermissionCard> cards = new ArrayList<>();
		cards.add(card);
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		PermissionDeck deck = new PermissionDeck(null, cards);
		Balcony balcony = new Balcony(game);
		ArrayList<PermissionDeck> decks = new ArrayList<>();
		BuyPermissionCard action2=new BuyPermissionCard(player, deck,0,balcony, decks, null, null);
		Turn turn = new Turn(player);
		ArrayList<PrimaryAction> actions = new ArrayList<>();
		actions.add(action2);
		turn.setPrimaryActionPool(actions);
		player.getCli().setTurn(turn);
		DoAnotherPrimaryAction action=new DoAnotherPrimaryAction(player,action2,player.getCli().getTurn());
		try {
			action.doSecondaryAction();
		} catch (NotEnoughResourcesException | InvalidDoAnotherPrimaryActionChoiceException e) {
			
			e.printStackTrace();
		}
		assertEquals(true,player.getCli().getTurn().getPrimaryActionPool().contains(action2));
	}

}
