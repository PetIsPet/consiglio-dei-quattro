package actionstests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import actions.SendAssistantToCouncil;
import exceptions.NotEnoughResourcesException;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import mappaeoggetti.Balcony;
import mvc.Game;

public class SendAssistantToCouncilTest {

	@Test
	public void testSendAssistantToCouncil() {
		Player player=new Player(0,null,null);
		SendAssistantToCouncil action=new SendAssistantToCouncil(player,null,null);
		assertEquals(false,action.equals(null));
	}

	@Test
	public void testIsEnoughAssistantShouldBeTrue() {
		Player player=new Player(0,null,null);
		player.setAssistants(1);
		SendAssistantToCouncil action=new SendAssistantToCouncil(player,null,null);
		assertEquals(true,action.isEnoughAssistant());
	}
	
	@Test
	public void testIsEnoughAssistantShouldBeFalse() {
		Player player=new Player(0,null,null);
		player.setAssistants(0);
		SendAssistantToCouncil action=new SendAssistantToCouncil(player,null,null);
		assertEquals(false,action.isEnoughAssistant());
	}

	@Test
	public void testDoSecondaryAction() throws FileNotFoundException {
		Lettore loader=new Lettore();
		loader.load();
		GraphMap map=new GraphMap(loader);
		map.run();
		ArrayList<Color> colors=new ArrayList<>();
		colors.add(Color.BLACK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		Game game=new Game(colors,map);
		Player p=new Player(0, null, null);
		game.getPlayers().add(p);
		game.initialize();
		
		Player player=game.getPlayers().get(0);
		player.setCli(game.getCli());
		player.setAssistants(2);
		SendAssistantToCouncil action=new SendAssistantToCouncil(player,game.getBalconies().get(2),Color.MAGENTA);
		try {
			action.doSecondaryAction();
		} catch (NotEnoughResourcesException e) {
			
			e.printStackTrace();
		} //scelgo mazzo montagna e consigliere magenta
		Balcony balcony=game.getBalconies().get(2);
		assertEquals(Color.MAGENTA,balcony.getCouncelors()[3].getColor());
	}

}
