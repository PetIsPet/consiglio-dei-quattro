package markettest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import market.Market;
import market.MarketStuff;

public class MarketTest {
	
	@Test
	public void testMarket(){
		Market market = new Market();
		ArrayList<MarketStuff> showCase = new ArrayList<>();
		market.setShowcase(showCase);
		assertEquals(showCase, market.getShowcase());
	}
	
	@Test
	public void testgetShowCase(){
		Market market = new Market();
		ArrayList<MarketStuff> showCase = new ArrayList<>();
		market.setShowcase(showCase);
		assertEquals(showCase, market.getShowcase());
	}
	
	@Test
	public void testsetShowCase(){
		Market market = new Market();
		ArrayList<MarketStuff> showCase = new ArrayList<>();
		market.setShowcase(showCase);
		assertEquals(showCase, market.getShowcase());
		ArrayList<MarketStuff> showCase1 = new ArrayList<>();
		market.setShowcase(showCase1);
		assertEquals(showCase1, market.getShowcase());
		
	}

}
