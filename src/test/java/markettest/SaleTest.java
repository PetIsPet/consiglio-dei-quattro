package markettest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import gamemanagement.Player;
import mappaeoggetti.Assistant;
import market.Market;
import market.MarketStuff;
import market.Sale;

public class SaleTest {

	@Test
	public void testact(){
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		int price = 3;
		Market market = new Market();
		MarketStuff stuff = new MarketStuff(price, owner, product);
		Sale sale = new Sale(owner);
		ArrayList<MarketStuff> showCase = new ArrayList<>();
		showCase.add(stuff);
		market.setShowcase(showCase);
		sale.act();
		assertEquals(stuff, market.getShowcase().get(0));
	}
}
