package markettest;

import static org.junit.Assert.*;

import org.junit.Test;

import gamemanagement.Player;
import mappaeoggetti.Assistant;
import mappaeoggetti.Card;
import market.MarketStuff;

public class MarketStuffTest {
	
	@Test
	public void testMarketStuff(){
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(0, owner, product);
		assertEquals(owner, marketStuff.getOwner());
		assertEquals(product, marketStuff.getProduct());
	}
	
	@Test
	public void testgetProduct(){
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(0, owner, product);
		assertEquals(product, marketStuff.getProduct());
	}
	
	@Test
	public void testsetProduct(){
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(0, owner, product);
		assertEquals(product, marketStuff.getProduct());
		Card product1 = new Card(null);
		marketStuff.setProduct(product1);
		assertEquals(product1, marketStuff.getProduct());
	}
	
	@Test
	public void testgetOwner(){
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(0, owner, product);
		assertEquals(owner, marketStuff.getOwner());
	}
	
	@Test
	public void testsetOwner(){
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(0, owner, product);
		assertEquals(owner, marketStuff.getOwner());
		Player owner1 = new Player(0, null, null);
		marketStuff.setOwner(owner1);
		assertEquals(owner1, marketStuff.getOwner());
	}
	
	@Test
	public void testgetPrice(){
		int price = 3;
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(price, owner, product);
		assertEquals(3, marketStuff.getPrice());
	}
	
	@Test
	public void testsetPrice(){
		int price = 3;
		Player owner = new Player(0, null, null);
		Assistant product = new Assistant();
		MarketStuff marketStuff = new MarketStuff(price, owner, product);
		assertEquals(3, marketStuff.getPrice());
		int price1 = 8;
		marketStuff.setPrice(price1);
		assertEquals(8, marketStuff.getPrice());
	}

}
