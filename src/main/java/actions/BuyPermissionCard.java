package actions;



import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import exceptions.NotEnoughResourcesException;
import actions.PrimaryAction;
import bonuses.Bonus;
import bonuses.DoAnotherPrimaryActionBonus;
import bonuses.GiveNobilityPoints;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.Balcony;
import mappaeoggetti.Card;
import mappaeoggetti.Councelor;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import servercompleto.ServerView;

	public class BuyPermissionCard implements PrimaryAction {
	
		 private Player player;
		 private PermissionCard[] discoveredCard;
		 private PermissionDeck deck;
		 private int counterPoliticsCard;
		 private List<Card> cards=new ArrayList<Card>();
		 private Balcony balcony;
		 private ArrayList<Card> matches = new ArrayList<>();
		 private List<Councelor> unmatchedCouncelors=new ArrayList<Councelor>();
		 private int idCard;
		 private ArrayList<PermissionDeck> decks=new ArrayList<>();
		 private ServerView client;
		 private TurnManager controller;
		 
		 /**
		  *do a primary action that allows to buy a PermissionCard
		  * @param player
		 * @param deck
		 * @param idCard
		 * @param balcony
		 * @param controller TODO
		  */
		 public BuyPermissionCard(Player player, PermissionDeck deck, int idCard, Balcony balcony, ArrayList<PermissionDeck> decks, ServerView s, TurnManager controller){
		  
		  this.cards=player.getCards();
		  this.deck=deck;
		  this.player=player;
		  this.counterPoliticsCard=0;
		  this.controller=controller;
		  this.matches=new ArrayList<>();
		  this.decks=decks;
		  this.client=s;
		  this.idCard=idCard;
		  this.balcony=balcony;
		  
		  
		 }
		 
		 /**
		  * 
		  * Execute the bonus, giving a permission card to the player
		  */
		
		 @Override
		 public void doPrimaryAction() throws NotEnoughResourcesException{
		  
		  int price;
		  
	
		  price=calculatePrice(this.matches);

		  
		  if(this.player.getCoins()-price>=0){
			  this.player.setCoins(this.player.getCoins()-price);
			    
			  for(Card c:this.matches){
			   
			   player.getCards().remove(c);
			  }
			   
			  PermissionCard permit=deck.getDiscoveredCards()[idCard];
			  
			  for(Bonus b:permit.getBonus()){
			   if(b.getClass()==GiveNobilityPoints.class){
			    b.setNobilityStuff(decks,client,controller);
			   }
			   if(b.getClass()==DoAnotherPrimaryActionBonus.class){
				   try {
						PrimaryAction actionBonus=controller.parsePrimaryAction(client.askPrimaryAction(), this.player, client);
						
						b.setAction(actionBonus);
					} catch (RemoteException e) {
						
					System.out.println("Connection lost");
			    }
			   }
			  
			   b.act(player);
			  }
			  
			  player.getPermits().add(permit);
			  
			  deck.getDiscoveredCards()[idCard]=deck.replaceCard(); //i indice dell'array dato dalla CLI
			  
			  }
		  else{
			  NotEnoughResourcesException e1=new NotEnoughResourcesException();
			  throw e1;
		  }
		  
		 
		 }
		 
		 
		 /**
		  * @return the player
		  */
		 public Player getPlayer() {
		  return player;
		 }
		
		 /**
		  * @param player the player to set
		  */
		 public void setPlayer(Player player) {
		  this.player = player;
		 }
		
		 /**
		  * @return the discoveredCard
		  */
		 public PermissionCard[] getDiscoveredCard() {
		  return discoveredCard;
		 }
		
		 /**
		  * @param discoveredCard the discoveredCard to set
		  */
		 public void setDiscoveredCard(PermissionCard[] discoveredCard) {
		  this.discoveredCard = discoveredCard;
		 }
		
		
		
		 /**
		  * @return the deck
		  */
		 public PermissionDeck getDeck() {
		  return deck;
		 }
		
		 /**
		  * @param deck the deck to set
		  */
		 public void setDeck(PermissionDeck deck) {
		  this.deck = deck;
		 }
		
		 /**
		  * @return the counterPoliticsCard
		  */
		 public int getCounterPoliticsCard() {
		  return counterPoliticsCard;
		 }
		
		 /**
		  * @param counterPoliticsCard the counterPoliticsCard to set
		  */
		 public void setCounterPoliticsCard(int counterPoliticsCard) {
		  this.counterPoliticsCard = counterPoliticsCard;
		 }
		
		 /**
		  * @return the cards
		  */
		 public List<Card> getCards() {
		  return cards;
		 }
		
		 /**
		  * @param cards the cards to set
		  */
		 public void setCards(List<Card> cards) {
		  this.cards = cards;
		 }
		
		 /**
		  * @return the balcony
		  */
		 public Balcony getBalcony() {
		  return balcony;
		 }
		
		 /**
		  * @param balcony the balcony to set
		  */
		 public void setBalcony(Balcony balcony) {
		  this.balcony = balcony;
		 }
		
		 /**
		  * @return the matches
		  */
		 public ArrayList<Card> getMatches() {
		  return matches;
		 }
		
		 /**
		  * @param matches the matches to set
		  */
		 public void setMatches(ArrayList<Card> matches) {
		  this.matches = matches;
		 }
		
		
		
		 /**
		  * @return the unmatchedCouncelors
		  */
		 public List<Councelor> getUnmatchedCouncelors() {
		  return unmatchedCouncelors;
		 }
		
		 /**
		  * @param unmatchedCouncelors the unmatchedCouncelors to set
		*/
		 public void setUnmatchedCouncelors(List<Councelor> unmatchedCouncelors) {
		  this.unmatchedCouncelors = unmatchedCouncelors;
		 }
		 
		 /**
		  * 
		  * @return if the player has enough cards
		  */
		 public boolean isEnoughCard(){
		
		  if(player.getCards().isEmpty()){
		   return false;
		   }
		  return true;
		  }
		 
		 /**
		  * 
		  * creates a pool with the cards matching with the councelors of the balcony
		  */
		 
		 public  void createMatchPool(){
		  
			 
		  for(int i=0; i<4; i++){
		   
		   unmatchedCouncelors.add(balcony.getCouncelors()[i]);
		  }
		  
		  
		ArrayList<Councelor> councelors=new ArrayList<>();
		ArrayList<Card> cards=new ArrayList<>();
		Map<Councelor,Boolean> set=new HashMap<Councelor,Boolean>();
		
		councelors.addAll(unmatchedCouncelors);
		cards.addAll(this.player.getCards());
		for(Card ca:cards){
			
		}
		
		for(Councelor c:councelors){
			
			set.put(c, false);
		}
		
		mainLoop:
		for(Card c:cards){
			secondaryLoop:
			for(Councelor cr:councelors){
				if(c.getColor().equals(cr.getColor())){
					if(!set.get(cr)){
						
						set.put(cr, true);
						
						this.matches.add(c);
						break secondaryLoop;
					}
				}
			}
		}
		
		for(Card c:player.getCards()){
			if (c.getColor()==Color.YELLOW) this.matches.add(c);
		}
		  
		
		  
		  }
		 

		 
		 
		 
		 /**
		  * 
		  * @param matches2 the pool of the matching cards
		  * @param unmatchedCouncelors2 all the councelors of the balcony
		  * @return the price to corrupt the balcony
		  */
		 public int calculatePrice(List<Card> matches2){
		  
		  int multicolorCounter=0;
		  for(Card c:matches2){
			  if(c.getColor()==Color.YELLOW) multicolorCounter++;
		  }
		
		  if(multicolorCounter==0){
			  int counter=4-matches2.size();
			  
			  switch(counter){
			  
			  case 1:
			   return 4;
			   
			  case 2:
			   return 7;
			   
			  case 3:
			   return 10;
			   
			   
			   
			  case 0:
			   return 0;
			  
			  default:return 0;
			  }
		  }
		  else{
			  int counter=4-matches2.size();
			  
			  switch(counter){
			  
			  case 1:
			   return 4+multicolorCounter;
			   
			  case 2:
			   return 7+multicolorCounter;
			   
			  case 3:
			   return 10+multicolorCounter;
			   
			   
			   
			  case 0:
			   return 0+multicolorCounter;
			  
			  default:return 0;
			  }
		  }
		  
		  
		 }
	 
	 
	 
	 
	}