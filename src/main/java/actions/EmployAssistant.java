package actions;

import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;

public class EmployAssistant implements SecondaryAction {

	private final Player player;

	/**
	 * 
	 * @param player the player that execute the action
	 */
	public EmployAssistant (Player player){
		
		this.player=player;
	}
	
	/**
	 *  Execute the secondary action that allow the player to employ one assistant
	 */
	@Override
	public void doSecondaryAction() throws NotEnoughResourcesException{

		if(isEnoughMoney()){
			
			player.setCoins(player.getCoins()-3);
			player.setAssistants(player.getAssistants()+1);
		}
		
		else {
			NotEnoughResourcesException e1=new NotEnoughResourcesException();
			throw e1;
		}
			
	}
	
	/**
	 * @return if the player has enough money
	 */
	public boolean isEnoughMoney(){

		if(player.getCoins()>=3){
			return true;
		}
		return false;
		}
	


}
