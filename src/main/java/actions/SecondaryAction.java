package actions;

import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.NotEnoughResourcesException;

@FunctionalInterface
public interface SecondaryAction {
	
	public void doSecondaryAction() throws NotEnoughResourcesException, InvalidDoAnotherPrimaryActionChoiceException;

}
