package actions;

import java.awt.Color;


import gamemanagement.Player;
import mappaeoggetti.Balcony;
import mappaeoggetti.Councelor;

public class CouncelorElection implements PrimaryAction{

	
	private Color color;
	private Balcony balcony;
	private final Player player;

	/**
	 * 
	 * @param player is the current player
	 * @param color is the color of the councelor that the player wants to put on the balcony
	 * @param balcony is the chosen balcony
	 */
	public CouncelorElection (Player player,Color color,Balcony balcony) {
		
		this.player=player;
		this.color=color;
		this.balcony=balcony;

		}
	
	/**
	 *  Execute the primary action that allow the player to elect a Councelor
	 */
	@Override
	public void doPrimaryAction() {
		
		balcony.append(new Councelor(color));
		
		
		player.setCoins(player.getCoins()+4);
	}
	
	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

}

