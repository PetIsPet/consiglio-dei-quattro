package actions;

import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import mappaeoggetti.PermissionDeck;


public class ChangePermissionCard implements SecondaryAction {

	private final Player player;
    private PermissionDeck deck;

    /**
     *	do a secondary action that allow to replace the permission cards on the table
     *	with different ones
     */
			@Override
			public void doSecondaryAction() throws NotEnoughResourcesException{
				
				if(isEnoughAssistant()){
					
					player.setAssistants(player.getAssistants()-1);
					
					deck.discover();
							
				}
				else {
					NotEnoughResourcesException e1=new NotEnoughResourcesException();
					throw e1;
				}
			
			}

	/**
	 * 
	 * @param player is the current player
	 * @param deck is the chosen deck
	 */
    public ChangePermissionCard(Player player, PermissionDeck deck) {
		
		this.player=player;
		this.deck=deck;

		}
    
	/**
	 * @return the deck
	 */
	public PermissionDeck getDeck() {
		return deck;
	}

/**
 * @return if the player has enough assistants
 * 
 */
        public boolean isEnoughAssistant(){

		if(player.getAssistants()>0){
			
			return true;
		}
		
			return false;
		}


}
