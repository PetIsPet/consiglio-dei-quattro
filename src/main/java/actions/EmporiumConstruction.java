package actions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bonuses.Bonus;
import bonuses.GiveNobilityPoints;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import gui.TheWinnerIs;
import mappaeoggetti.City;
import mappaeoggetti.Emporium;
import mappaeoggetti.PermissionDeck;
import servercompleto.ServerView;

public class EmporiumConstruction implements PrimaryAction { 
	
	private static final int NUMBEROFPOINTS = 3;
	private City city;
	private Player player;
	private List<Bonus> bonus=new ArrayList<Bonus>(); //pool di bonus guadagnati
	private ArrayList<PermissionDeck> decks=new ArrayList<>();
	private ServerView client;
	private TurnManager controller;
	
	/**
	 * 
	 * @param player the player that execute the action
	 * @param city the city where the player wants to execute the action
	 * @param controller 
	 */
	public EmporiumConstruction(Player player, City city,ArrayList<PermissionDeck> decks,ServerView s, TurnManager controller){	
			
		this.city=city;
		this.player=player;
		this.bonus=new ArrayList<>();
		this.decks=decks;
		this.client=s;
		this.controller=controller;
		
	}
	
	/**
	 * 
	 * Execute the action of building an emporium
	 * @throws NotEnoughResourcesException 
	 */
	
	@Override
	public void doPrimaryAction() throws NotEnoughResourcesException {	
		
		this.buildEmporium();
		
	}

	
	/**
	 * 
	 * the act of building an emporium on this city
	 */
	public void buildEmporium() throws NotEnoughResourcesException{	
		
		if(!(hasBuilt(city))){
			int numberofemporium = 0;		
				if(anotherHasBuilt(city)){
					for(Emporium e: this.city.getEmporiums()){
					
						numberofemporium++;
				}
			}
			
				if(isEnoughAssistant(numberofemporium)){
					
					this.player.setAssistants(player.getAssistants()-numberofemporium);
					this.city.getEmporiums().add(new Emporium(player));
					player.setBuiltEmporiums(player.getBuiltEmporiums()+1);
					ArrayList<Bonus> bonuses=EmporiumConstruction.getNeighboursBonus(this.city,this.player);
					
					for(Bonus b:bonuses){
						
						if(b.getClass()==GiveNobilityPoints.class){
							b.setNobilityStuff(decks, client,controller);
						}
						b.act(this.player);
						
					if(player.hasFinished()) 
						player.setPoints(player.getPoints()+NUMBEROFPOINTS);
				}
		
			}
				else{
					NotEnoughResourcesException e1=new NotEnoughResourcesException();
					throw e1;
				}
				
		}
	}
	
	/**
	 *  A method that check if a player has enough assistant to built an emporium
	 * @param numberofemporium the number of the emporiums that could be built in a specific city
	 * @return if the player has enough assistants
	 */
		public boolean isEnoughAssistant(int numberofemporium){

			if(player.getAssistants()>=numberofemporium){
				return true;
			}
			return false;
			}
	


	/**
	 * A method that check if a player has built an emporium in the city yet
	 * @param city the city that must be checked
	 * @return if this player has already built in this city
	 */
 
	public boolean hasBuilt(City city){
		if(this.city.getEmporiums()!=null){
			for(Emporium e:this.city.getEmporiums()){
				
				if(e.getPlayer()==this.player) 
					return true;
			}
			
		}
		return false;
	}
	/**
	 * A method that check if another player has built an emporium in the city yet
	 * @param city the city that must be checked
	 * @return if another player has already built in this city
	 */
 
	public boolean anotherHasBuilt(City city){
		
		for(Emporium e:this.city.getEmporiums()){
			
			if(e.getPlayer()!=this.player) 
				return true;
		}
		return false;
	}
	
	
	/**
	 * A methot that search an emporium of the current player in all city near the one we consider, and for each city with an emporium return
	 * and assign the bonus of that city to the current player
	 * @param city the city that starts the chain of bonus given to the player
	 * 
	 * 
	 */
	public static ArrayList<Bonus> getNeighboursBonus(City c,Player p){
		Set<City> myCities=new HashSet<>();
		Set<City>city=getEmporiumsMyNeighbours(c,p,myCities);
		ArrayList<Bonus> bonuses=new ArrayList<>();
		for(City s:myCities){
			bonuses.addAll(s.getCityToken().getBonuses());
		}
		return bonuses;
		
		
	}
	
	
	public static boolean myNeighboursHaveEmporiums(City c,Player p){

		
		for(City n:c.getNeighborhood()){
			for(Emporium e:n.getEmporiums()){
				if(e.getPlayer()==p) return true;
			}
		}
		
		return false;
		
	}
	
	public static Set<City> getEmporiumsMyNeighbours(City c,Player p,Set<City> myCities){
		if(myNeighboursHaveEmporiums(c, p)) myCities.add(c);
		for(City n:c.getNeighborhood()){
				if(p.hasBuilt(n)) {
					if(!n.isControlled()){
						n.setControlled(true);
						myCities.add(n);
						getEmporiumsMyNeighbours(n,p,myCities);
					}
				}
				
			}
		
		return myCities;
		}
	
	
	
	
			
	
	
	/**
	 * A method that search the emporium of the current player in the neighbour city
	 * @param c the neighbour
	 * @return if the neighbour has an emporium of this player
	 */
	
	public static boolean neighbourHaveMyEmporium(City c,Player player){
		
		for(Emporium d:c.getEmporiums()){
			
			if(d.getPlayer()==player) 
				return true;
		}
		return false;
	}
	
	

	/**
	 * @return the bonus
	 */
	public List<Bonus> getBonus() {
		return bonus;
	}
	
	public ArrayList<PermissionDeck> getDecks() {
		return decks;
	}
	
	

}
