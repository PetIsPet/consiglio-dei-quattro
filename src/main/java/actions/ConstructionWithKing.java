package actions;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bonuses.Bonus;
import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.Balcony;
import mappaeoggetti.Card;
import mappaeoggetti.City;
import mappaeoggetti.Councelor;
import mappaeoggetti.Emporium;
import mappaeoggetti.King;

public class ConstructionWithKing implements PrimaryAction {

	
	private King king;
	private Player player;
	private GraphMap graph;
	private Balcony kingBalcony;
	private City target;
	private ArrayList<Card> matches=new ArrayList<>();
	private int price;
	private TurnManager controller;
	
	/**
	 * 
	 * @param king sets the king
	 * @param player is the current player
	 * @param graph is the current map
	 * @param kingBalcony is the current king's balcony
	 * @param target is the location where the kings is supposed to go
	 * @param controller TODO
	 */
	
	public ConstructionWithKing(King king, Player player, GraphMap graph, Balcony kingBalcony, City target, TurnManager controller){
		
		this.king=king;
		this.player=player;
		this.setGraph(graph);
		this.kingBalcony=kingBalcony;
		this.target=target;
		
		
	}

	/**
	 *  Execute the primary action that allow the player to built with the king
	 * @throws NotEnoughResourcesException 
	 * @throws InvalidInputException 
	 */
	@Override
	public void doPrimaryAction() throws NotEnoughResourcesException, InvalidInputException {			
		
		if(!this.player.getCards().isEmpty()){
		
		if((this.player.getCoins()-this.priceToCorruptBalcony())>=0){
			this.player.setCoins(this.player.getCoins()-this.priceToCorruptBalcony());
			
				if(!(hasBuilt(target))){
					int i=this.price;
					if((player.getCoins()-i)>=0){
					player.setCoins(player.getCoins()-i);
					target.getEmporiums().add(new Emporium(player));
					player.setBuiltEmporiums(player.getBuiltEmporiums()+1);
					king.setCurrentCity(target);
					ArrayList<Bonus> bonuses=EmporiumConstruction.getNeighboursBonus(target, player);
					
					for(Bonus b:bonuses){
						b.act(player);
					}
					player.getCards().removeAll(matches);
					}
					else{
						NotEnoughResourcesException e0=new NotEnoughResourcesException();
						throw e0;
					}
				}
				else {
					InvalidInputException e1=new InvalidInputException();
					throw e1;
				}
			}
			else{
				
				NotEnoughResourcesException e2=new NotEnoughResourcesException();
				throw e2;
			
		}
		}
		
		else {
			
			NotEnoughResourcesException e3=new NotEnoughResourcesException();
			throw e3;
		}
		
	}
	
	
	/**
	 * 
	 * @param city to control
	 * @return if the player has already built in this city
	 */
	public boolean hasBuilt(City city) {			
		
		for(Emporium e:city.getEmporiums()){
			if(e.getPlayer()==player) 
				return true;
		}
		
		return false;
		
	}
	
	
	/**
	 * @return the price to corrupt the balcony
	 */
	public int priceToCorruptBalcony(){
		
		  int multicolorCounter=0;
		  for(Card c:player.getCards()){
			  if(c.getColor()==Color.YELLOW) multicolorCounter++;
		  }
		  
		  
		
		  if(multicolorCounter==0){
			  int counter=4-matches.size();
			  
			  switch(counter){
			  
			  case 1:
			   return 4;
			   
			  case 2:
			   return 7;
			   
			  case 3:
			   return 10;
			    
			   
			  case 0:
			   return 0;
			  
			  default:return 0;
			  }
		  }
		  else{
			  
			  int counter=4-matches.size();
			  
			  switch(counter){
			  
			  case 1:
			   return 4+multicolorCounter;
			   
			  case 2:
			   return 7+multicolorCounter;
			   
			  case 3:
			   return 10+multicolorCounter;
			   
			   
			   
			  case 0:
			   return 0+multicolorCounter;
			  
			  default:return 0;
			  }
		  }	
	}
	
	
	/**
	 * @param kingBalcony the kingBalcony to set
	 */
	public void setKingBalcony(Balcony kingBalcony) {
		this.kingBalcony = kingBalcony;
	}
	

	/**
	 * @return the kingBalcony
	 */
	public Balcony getKingBalcony() {
		return kingBalcony;
	}

	/**
	 * @return the king
	 */
	public King getKing() {
		return king;
	}

	/**
	 * @param graph the graph to set
	 */
	public void setGraph(GraphMap graph) {
		this.graph = graph;
	}
	
	 /**
	 * @return the matches
	 */
	public ArrayList<Card> getMatches() {
		return matches;
	}


	/**
	 * @param matches the matches to set
	 */
	public void setMatches(ArrayList<Card> matches) {
		this.matches = matches;
	}
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}


	/**
	 * A method that create a pool where are set the player's card that matches 
	 * the color with the councelors in a balcony
	 */
	public  void createMatchPool(){
		  
		 ArrayList<Councelor>  unmatchedCouncelors=new ArrayList<>();
		 
		 
		  for(int i=0; i<4; i++){
		   
		   unmatchedCouncelors.add(kingBalcony.getCouncelors()[i]);
		   
		  }
		  
		  
		ArrayList<Councelor> councelors=new ArrayList<>();
		ArrayList<Card> cards=new ArrayList<>();
		Map<Councelor,Boolean> set=new HashMap<Councelor,Boolean>();
		
		councelors.addAll(unmatchedCouncelors);
		cards.addAll(player.getCards());
		
		for(Councelor c:councelors){
			set.put(c, false);
		}
		
		
		for(Card c:cards){
			
			for(Councelor cr:councelors){
				if(c.getColor()==cr.getColor()){
					if(!set.get(cr)){
						set.put(cr, true);
						this.matches.add(c);
						
						break;
					}
				}
			}
		}
		
		for(Card c:player.getCards()){
			if (c.getColor()==Color.YELLOW) this.matches.add(c);
		}
		  
		  
		  }
		 

		 
	

}
