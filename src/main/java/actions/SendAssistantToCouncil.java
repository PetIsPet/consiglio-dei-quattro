package actions;

import java.awt.Color;

import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import mappaeoggetti.Balcony;
import mappaeoggetti.Councelor;

public class SendAssistantToCouncil implements SecondaryAction {

	private final Player player;
	
	private Color color;
	private Balcony balcony;
	
	/**
	 * 
	 * @param player the player that execute the action
	 * @param balcony the balcony that the player wants to modify
	 * @param color the color of the councelor that the player want to append
	 */
	public SendAssistantToCouncil (Player player, Balcony balcony, Color color){
		
			this.player=player;
			this.color=color;
			this.balcony=balcony;
			
			
		}

/**
 * 
 * @return if the player has enough assistants
 */
	public boolean isEnoughAssistant(){

		if(player.getAssistants()>0){
			return true;
		}
		return false;
		}
	
	/**
	 * secondary action checks with IsEnough if the current player has at least one assistant.
	 * If he has, he remove an assistant and then with Appeend puts a councelor in the balcony
	 * and remove another one 
	 */
	
	@Override
	public void doSecondaryAction() throws NotEnoughResourcesException{
		
		if(isEnoughAssistant()){
			
			Councelor councelor=new Councelor(color);
			
			player.setAssistants(player.getAssistants()-1);
			balcony.append(councelor);
			
		}
		else{
			NotEnoughResourcesException e1=new NotEnoughResourcesException();
			throw e1;
		}
	}
}

