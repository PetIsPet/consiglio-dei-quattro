package actions;

import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.NotEnoughResourcesException;

public class DoNothing implements SecondaryAction {

	@Override
	public void doSecondaryAction() throws NotEnoughResourcesException, InvalidDoAnotherPrimaryActionChoiceException {
		
	}

}
