package actions;

import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;

@FunctionalInterface
public interface PrimaryAction {
	
	
	public void doPrimaryAction() throws NotEnoughResourcesException, InvalidInputException;

}
