package actions;

import actions.PrimaryAction;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import gamemanagement.Turn;

public class DoAnotherPrimaryAction implements SecondaryAction {
private final Player player;
private PrimaryAction action;
private Turn turn;
	
	/**
	 * @param player the player that execute the action
	 * @param action the next primary action to execute
	 * @param turn the current turn of the current player
	 */

	public DoAnotherPrimaryAction(Player player, PrimaryAction action, Turn turn){
		
		this.player=player;
		this.action=action;
		this.turn=turn;
		
		
	}

	/**
	 * Execute the secondary action that allow the player to take one another primary action
	 * @throws InvalidDoAnotherPrimaryActionChoiceException 
	 */

	@Override
	public void doSecondaryAction() throws NotEnoughResourcesException, InvalidDoAnotherPrimaryActionChoiceException {
		
		
		 int coins=player.getCoins();
			
			if(isEnoughAssistant()){
			
			player.setAssistants(player.getAssistants()-3);
			
			try {
				action.doPrimaryAction();
			} catch (InvalidInputException|NotEnoughResourcesException e) {
				player.setAssistants(player.getAssistants()+3);
				player.setCoins(coins);
				throw new InvalidDoAnotherPrimaryActionChoiceException();
			}
			
			
			}
			else{
				InvalidDoAnotherPrimaryActionChoiceException e1=new InvalidDoAnotherPrimaryActionChoiceException();
				throw e1;
			}
		
	}
	/**
	 * 
	 * @return if the player has enough assistants
	 */
	public boolean isEnoughAssistant(){

		if(player.getAssistants()>=3){
			return true;
		}
		return false;
		}
	
	
	/**
	 * the method invoked by the bonus
	 */
	public void bonusAnotherPrimaryAction(){
		
		turn.getPrimaryActionPool().add(this.action); //aggiunge azione primaria al pool del turno
		
		
		}
	
	
	/**
	 * @return the action
	 */
	public PrimaryAction getAction() {
		return action;
	}

}
