package servercompleto;

import java.awt.Color;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import exceptions.VoidMarketException;
import mappaeoggetti.City;

public interface SubscriberRMIInterface extends Remote {
	
	public boolean primaryActionFirst() throws RemoteException;
	public String askPrimaryAction() throws RemoteException;
	public String askSecondaryAction() throws RemoteException;
	public int getChosenDeck() throws RemoteException;
	public int getChosenBalcony() throws RemoteException;
	public int getPermissionCard(ArrayList<String> discoveredCards) throws RemoteException;
	public int askYourPermissionCard(ArrayList<String> permits) throws RemoteException;
	public String askCityPermit(ArrayList<String> cities) throws RemoteException;
	public String getKingMovement(ArrayList<String> cities, String currentCity) throws RemoteException;
	public String getChosenColor(ArrayList<String> colors) throws RemoteException;
	public int getProductToPurchase(ArrayList<String> market) throws RemoteException,VoidMarketException;
	public int getPrice() throws RemoteException;
	public ArrayList<String> getProductToSell(int assistants, ArrayList<String> permits, ArrayList<String> cards) throws RemoteException;
    public void notifyActions (String msg) throws RemoteException;
    public void printShowcase (ArrayList<String> showcase)throws RemoteException;
    public ArrayList<String> usePoliticCards(ArrayList<String> matches) throws RemoteException;
    public void setCityTokenPaths(ArrayList<String> paths)  throws RemoteException;
    public void setPermitsImagesPaths(ArrayList<String> paths)  throws RemoteException;
    public void wakeUpOnStartLock() throws RemoteException;
    public void setDiscoveredCards(ArrayList<String> cards)  throws RemoteException;
    public void setBalconies(ArrayList<String> balconies)  throws RemoteException;
    public void setYourCards(String cards) throws RemoteException;
    public void setPlayersPermits(ArrayList<String> permits) throws RemoteException;
    public void setMapForCLI(ArrayList<String> map) throws RemoteException;
    public void setStats (ArrayList<String> stats) throws RemoteException;
    public void setMapConfiguration (String configuration) throws RemoteException;
    public void setWinner(String winner) throws RemoteException;
    public void setBuiltEmporiums(ArrayList<String> emporiums) throws RemoteException;
    public void notifyException(String exception) throws RemoteException;
	public void setDrawnCard(String card)throws RemoteException;
	public void refresh()throws RemoteException;
	public int chooseBalconyPlusKing() throws RemoteException;
	public String chooseCity(ArrayList<String> cities) throws RemoteException;
    
}
