package servercompleto;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Scanner;

import exceptions.VoidMarketException;

public class ServerSocketView implements ServerView,Runnable{
	
	private Broker broker;
	private Socket socket;
	private Scanner socketIn;
	private PrintWriter socketOut;
	protected String response;
	private int id;
	private boolean isActive;
	
	
	public ServerSocketView(Broker b,Socket s) throws IOException{
		
		this.broker=b;
		this.socket=s;
		socketIn = new Scanner(socket.getInputStream());
		socketOut = new PrintWriter(socket.getOutputStream());
		this.isActive=true;
		
	}

	public void sendCommand(String name){
		
		
		socketOut.print(name);
		socketOut.flush();
	}

	@Override
	public void run() {
		
		
		
		String chosenGame=this.sendGameNames();
		
		while("refresh".equals(chosenGame)){
			
			chosenGame=this.sendGameNames();
		}
		
		try {
			id=subscribe(this,chosenGame);
			this.sendPlayerId(id);
		} catch (RemoteException e1) {
			
			e1.printStackTrace();
		}

			    
		while(true){
			
			
		}
		
	}

	@Override
	public ArrayList<String> getGamesNames() {
		return broker.getGamesNames();
	}

	@Override
	public int subscribe(ServerView s, String gameName) throws RemoteException {
		return this.broker.subscribe(s, gameName);
		
	}
	
	public String sendGameNames(){
		socketOut.println("GAMES");
		socketOut.flush();
		for(String s:broker.getGamesNames()){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String response=socketIn.nextLine();
		
		return response;
		
	}
	
	public void sendPlayerId(int id){
		socketOut.println("PLAYERID");
		socketOut.flush();
		socketOut.println(Integer.toString(id));
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
		
		}
	
	
	
	

	@Override
	public boolean primaryActionFirst() throws RemoteException {
		socketOut.println("WHICHACTION");
		socketOut.flush();
		String answer=socketIn.nextLine();
		
		if(answer.equals("TRUE")) return true;
		return false;
		
	}

	@Override
	public String askPrimaryAction() throws RemoteException {
		socketOut.println("PRIMARYACTION");
		socketOut.flush();
		String answer=socketIn.nextLine();
		
		return answer;
	}

	@Override
	public String askSecondaryAction() throws RemoteException {
		socketOut.println("SECONDARYACTION");
		socketOut.flush();
		String answer=socketIn.nextLine();
		return answer;
	}

	@Override
	public int getChosenDeck() throws RemoteException {
		socketOut.println("DECK");
		socketOut.flush();
		String answer=socketIn.nextLine();
		
		return Integer.parseInt(answer);
	}

	@Override
	public int getChosenBalcony() throws RemoteException {
		socketOut.println("BALCONY");
		socketOut.flush();
		
		String answer=socketIn.nextLine();
		
		return Integer.parseInt(answer);
	}
	

	@Override
	public int getChosenBalconyPlusKing() throws RemoteException {
		socketOut.println("BALCONYWITHKING");
		socketOut.flush();
		
		String answer=socketIn.nextLine();
		
		return Integer.parseInt(answer);
	}

	@Override
	public int getPermissionCard(ArrayList<String> discoveredCards) throws RemoteException {
		socketOut.println("DISCOVERED CARDS");
		socketOut.flush();
		for(String s:discoveredCards){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String answer=socketIn.nextLine();
		return Integer.parseInt(answer);
	}

	@Override
	public int askYourPermissionCard(ArrayList<String> permits) throws RemoteException {
		socketOut.println("YOUR PERMIT");
		socketOut.flush();
		for(String s:permits){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String answer=socketIn.nextLine();
		return Integer.parseInt(answer);
	}

	@Override
	public String askCityPermit(ArrayList<String> cities) throws RemoteException {
		socketOut.println("PERMITCITY");
		socketOut.flush();
		for(String s:cities){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String answer=socketIn.nextLine();
		return answer;
	}

	@Override
	public String getKingMovement(ArrayList<String> cities, String currentCity) throws RemoteException {
		cities.add(currentCity); //aagiungo in ultima posizione la città corrente
		socketOut.println("KINGCITIES");
		socketOut.flush();
		for(String s:cities){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String answer=socketIn.nextLine();
		return answer;
	}

	@Override
	public Color getChosenColor(ArrayList<Color> colors) throws RemoteException {
		ArrayList<String> stringColors=new ArrayList<>();
		for(Color c:colors){ //trasformo colori in stringhe
			
			
			stringColors.add(this.colorToString(c));
			
		}
		socketOut.println("COLOR");
		socketOut.flush();
		for(String s:stringColors){
			
			socketOut.println(s);
			socketOut.flush();
			
		}
		socketOut.println("END");
		socketOut.flush();
		String answer=socketIn.nextLine();
		
		Color color=this.stringToColor(answer); 
		return color;
	}


	@Override
	public int getPrice() throws RemoteException {
		socketOut.println("PRICE");
		socketOut.flush();
		String answer=socketIn.nextLine();
		return Integer.parseInt(answer);
	}

	@Override
	public ArrayList<String> getProductToSell(int assistants, ArrayList<String> permits,
			ArrayList<String> cards) {
		
		ArrayList<String> answer=new ArrayList<>();

		
		cards.add("ENDCARDS");
		for(String s:permits){
			cards.add(s);
		}
		cards.add("ENDPERMITS");
		cards.add(Integer.toString(assistants));
		socketOut.println("SELL");
		socketOut.flush();
		for(String s:cards){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String received=null;
		while(socketIn.hasNextLine()){
			received=socketIn.nextLine();
			if("END".equals(received)) break;
			answer.add(received);
		}
		
		
		return answer;
		
	}

	@Override
	public void setClientInt() throws RemoteException {
		
		
	}

	@Override
	public void notifyActions(String msg) throws RemoteException {
		socketOut.println("NOTIFYACTION");
		socketOut.flush();
		socketOut.println(msg);
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
		
	}

	
	@Override
	public int getProductToPurchase(ArrayList<String> showcase) throws RemoteException, VoidMarketException {
		
		socketOut.println("PURCHASE");
		socketOut.flush();
		for(String s:showcase){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String answer=socketIn.nextLine();
		if(!"VOID".equals(answer)){
			int response=Integer.parseInt(answer);
			return response;
		}
		else{
			throw new VoidMarketException();
		}
	}
	
	@Override
	public void sendCityTokenPath(ArrayList<String> paths) throws RemoteException {
		socketOut.println("PATHS");
		socketOut.flush();
		for(String s:paths){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
	}

	
	@Override
	public ArrayList<String> matchToUse(ArrayList<String> matches) throws RemoteException {
		
		ArrayList<String> answer=new ArrayList<>();
		socketOut.println("MATCH");
		socketOut.flush();
		for(String s:matches){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		String received=null;
		while(socketIn.hasNextLine()){
			received=socketIn.nextLine();
			if("END".equals(received)) break;
			answer.add(received);
		}
		return answer;
	}
	
	@Override
	public void notifyGameStarted() throws RemoteException {
		socketOut.println("GAMESTARTED");
		socketOut.flush();
	
		
	}
	
	@Override
	public void sendBalconies(ArrayList<String> balconies) throws RemoteException {
		socketOut.println("BALCONIES");
		socketOut.flush();
		for(String s:balconies){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		
	}
	
	@Override
	public void sendPlayersPermits(ArrayList<String> permits) throws RemoteException {
		
		socketOut.println("PLAYERSPERMITS");
		socketOut.flush();
		for(String s:permits){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		
	}

	@Override
	public void sendMapForCLI(ArrayList<String> map) throws RemoteException {
		socketOut.println("MAPFORCLI");
		socketOut.flush();
		for(String s:map){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		
		
	}
	
	@Override
	public void sendStats(ArrayList<String> stats) throws RemoteException {
		
		socketOut.println("GAMESTATS");
		socketOut.flush();
		for(String s:stats){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		
		
	}


	
	@Override
	public void sendDiscoveredCards(ArrayList<String> cards) throws RemoteException {
		socketOut.println("DISCOVEREDCARDSFORMAP");
		socketOut.flush();
		for(String s:cards){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		
	}
	
	@Override
	public void sendYourPoliticCard(String card) throws RemoteException {
		socketOut.println("YOURCARDS");
		socketOut.flush();
		socketOut.println(card);
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
	}
	
	
	@Override
	public void sendMapConfiguration(String configuration) throws RemoteException {
		socketOut.println("MAPCONFIGURATION");
		socketOut.flush();
		socketOut.println(configuration);
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
		
	}
	
	@Override
	public void sendWinner(String s) throws RemoteException {
		socketOut.println("WINNER");
		socketOut.flush();
		socketOut.println(s);
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
		
	}
	

	@Override
	public void sendBuiltEmporiums(ArrayList<String> emporiums) throws RemoteException {
		
		socketOut.println("EMPORIUMS");
		socketOut.flush();
		for(String s:emporiums){
			socketOut.println(s);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		
	}
	
	@Override
	public void sendException(String exception) throws RemoteException {

		socketOut.println("EXCEPTION");
		socketOut.flush();
		socketOut.println(exception);
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
		
	}
	
	@Override
	public void sendDrawnCard(String card) throws RemoteException {
		socketOut.println("DRAWNCARD");
		socketOut.flush();
		socketOut.println(card);
		socketOut.flush();
		socketOut.println("END");
		socketOut.flush();
		
	}

	
	public String colorToString(Color c){
		
		if (c==Color.YELLOW) return "giallo";
		if (c==Color.MAGENTA) return "magenta";
		if (c==Color.BLUE) return "blu";
		if (c==Color.BLACK) return "nero";
		if (c==Color.WHITE) return "bianco";
		if (c==Color.PINK) return "rosa";
		if (c==Color.ORANGE) return "arancione";
		if (c==Color.GREEN) return "verde";
		return null;
	}
	
	
	public Color stringToColor(String s){
		if ("giallo".equals(s)) return Color.YELLOW;
		if ("magenta".equals(s)) return Color.MAGENTA;
		if ("blu".equals(s)) return Color.BLUE;
		if ("nero".equals(s)) return Color.BLACK;
		if ("bianco".equals(s)) return Color.WHITE;
		if ("rosa".equals(s)) return Color.PINK;
		if ("arancione".equals(s)) return Color.ORANGE;
		if ("verde".equals(s)) return Color.GREEN;
		return null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void setInactive() {
		
		this.isActive=false;
	}
	
	public boolean isActive(){
		return this.isActive;
	}

	@Override
	public void notifyRefresh() throws RemoteException {
		socketOut.println("REFRESH");
		socketOut.flush();
		
	}

	@Override
	public String chooseCity(ArrayList<String> cities) throws RemoteException {
		socketOut.println("CHOOSECITY");
		socketOut.flush();
		for(String c:cities){
			socketOut.println(c);
			socketOut.flush();
		}
		socketOut.println("END");
		socketOut.flush();
		return socketIn.nextLine();
		
	}


	





	

	

	

	



	

	

	

	

	

	
}
