package servercompleto;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ClientInHandler implements Runnable {
	
	
	private Scanner socketIn;
	private ArrayList<String> msg;
	private SubscriberSocket client;
	private String chosenGame;
	   
	 public ClientInHandler(Scanner socketIn, SubscriberSocket s) {
	 
		 this.socketIn=socketIn;
		 this.client=s;
	}
	
	

	@Override
	public void run() {
	
			
		while(true){
			
			String msg=socketIn.nextLine(); //legge il comando inviato dal server

			
			
		   
		
			if(isInputable(msg)){
				String card = "initializing"; 
				while(!"END".equals(card)){
					card=socketIn.nextLine();
					
					if("END".equals(card)) break;
					client.input.add(card);
					
					
					
				}
				
			}
			
			
			client.setCommand(msg); //sveglia il ClientOutHandler 
			client.commandSync();
			
		}
	}
	



	public ArrayList<String> getMsg() {
		return msg;
	}
	
	public boolean isInputable(String msg){
		
		switch(msg){
		case "CHOOSECITY":
			return true;
		case "DRAWNCARD":
			return true;
		case "WINNER":
			return true;
		case "EXCEPTION":
			return true;
		case "EMPORIUMS":
			return true;
		case "MAPCONFIGURATION":
			return true;
		case "GAMESTATS":
			return true;
		case "MAPFORCLI":
			return true;
		case "PLAYERSPERMITS":
			return true;
		case "YOURCARDS":
			return true;
		case "BALCONIES":
			return true;
		case "DISCOVEREDCARDSFORMAP":
			return true;
		case "PATHS":
			return true;
		case "GAMES":
			return true;
		case "PLAYERID":
			return true;
		case "DISCOVERED CARDS":
			return true;
		case "YOUR PERMIT":
			return true;
		case "PERMITCITY":
			return true;
		case "KINGCITIES":
			return true;
		case "COLOR":
			return true;
		case "SELL":
			return true;
		case "NOTIFYACTION":
			return true;
		case "PURCHASE":
			return true;
		case "MATCH":
			return true;
		default: 
			return false;
		}
	}


	public void goToSleep() throws InterruptedException{
		synchronized (this){
			wait();
		}
	}
	
	public  void wakeUp()  {
		synchronized (this){
			notifyAll();
			
		}
	}



	public String getChosenGame() {
		return chosenGame;
	}



	public void setChosenGame(String chosenGame) {
		this.chosenGame = chosenGame;
	}
}
