package servercompleto;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.Iterator;

import exceptions.InvalidInputException;
import exceptions.VoidMarketException;
import gui.GUI;
import gui.LobbyHandler;
import gui.LobbyJFrame;
import mappaeoggetti.Councelor;
import mvc.CLI;
import mvc.View;

public class ClientOutHandler implements Runnable{
	
	private View view;
	private PrintWriter socketOut;
	private SubscriberSocket client;
	private String chosenGame;
	
	
	ClientOutHandler(PrintWriter socketOut, SubscriberSocket s) throws IOException{
		
		
		this.socketOut=socketOut;
		this.client=s;
		if(s.isGUI()) this.view=new GUI();
		if(!s.isGUI()) this.view=new CLI();
		
	}


	public void parseCommand(String name) throws InvalidInputException{
		
		String answer=null;
		
		switch(name){
		
		case "GAMES":
			String game=null;
			if(client.isGUI()){
				
			         client.launchLobby(client.input, this);
					game=chosenGame;
				
				
				
				
			}
			if(!client.isGUI()){
				System.out.println("***AVAILABLE GAMES***");
				for(String  s:client.input){
					
					System.out.println(s);
				}
				Scanner sc=new Scanner(System.in);
				game=sc.nextLine();
				
			}
			
			socketOut.println(game);
			socketOut.flush();
			client.input.clear();
			break;
		case "WINNER":
			client.setWinner(client.input.get(0));
			client.input.clear();
			if(!client.isGUI()) System.out.println("The winner is Player "+Integer.parseInt(client.getWinner()));
			if(client.isGUI()) view.theWinnerIs(Integer.parseInt(client.getWinner()));
			break;
		case "DRAWNCARD":
			client.setYourCards(client.getYourCards()+client.input.get(0));
			String card=client.input.get(0);
			client.input.clear();
			if(!client.tableIsNull()){
				if(client.isGUI())
					try {
						client.getTable().printPoliticCard(client.getYourCards());
					} catch (IOException e) {
						
					}
				}
			if(!client.isGUI()) System.out.println("You have drawn a "+card+" card");
			break;
			
		case "EXCEPTION":
			if(!client.isGUI()){
				System.out.println(client.input.get(0));
				
			}
			if(client.isGUI()){
				client.showException(client.input.get(0));
			}
			client.input.clear();
			break;
		case "MAPCONFIGURATION":
			client.setMapConfiguration(client.input.get(0));
			client.input.clear();
			break;
		case "PLAYERID":
			int id=Integer.parseInt(client.input.get(0));
			client.playerId=id;
			System.out.println("You have been registered as Player "+id);
			System.out.println("Waiting for game to start...");
			client.input.clear();
			break;
		case "GAMESTARTED":
			try {
				client.launchGameTable();
			} catch (IOException | InterruptedException e1) {
				
				e1.printStackTrace();
			}
			break;
		case "WHICHACTION":
			try{
			
				boolean choice=view.primaryActionFirst(client.playerId);
				if(choice){
					
					
					socketOut.println("TRUE");
					socketOut.flush();	
					
				}
				else {
					socketOut.println("FALSE");
					socketOut.flush();
				}
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				this.parseCommand(name);
			}
			break;
		case "REFRESH":
			try {
				client.getTable().printPermissionCards(client.getPlayersPermits());
				client.getTable().printPlayerStats(client.getStats());
				client.getTable().printPoliticCard(client.getYourCards());
			} catch (IOException e2) {
			
			}
			break;
		case "DISCOVEREDCARDSFORMAP":
			client.getDiscoveredCards().clear();
			client.getDiscoveredCards().addAll(client.input);
			client.input.clear();
			break;
		case "EMPORIUMS":
			client.getEmporiums().clear();
			client.getEmporiums().addAll(client.input);
			client.input.clear();
			break;
		case "GAMESTATS":
			client.getStats().clear();
			client.getStats().addAll(client.input);
			client.input.clear();
			break;
		case "MAPFORCLI":
			client.getMapForCLI().clear();
			client.getMapForCLI().addAll(client.input);
			client.input.clear();
			break;
		case "YOURCARDS":
			client.setYourCards(client.input.get(0));
			client.input.clear();
			
			break;
		case "BALCONIES":
			client.getBalconies().clear();
			client.getBalconies().addAll(client.input);
			client.input.clear();
			break;
		case "PATHS":
			client.setCityTokenPaths(client.input);
			client.input.clear();
			break;
		case "PLAYERSPERMITS":
			client.getPlayersPermits().clear();
			client.getPlayersPermits().addAll(client.input);
			client.input.clear();
			break;
		case "PRIMARYACTION":
			try{
				String action=view.askPrimaryAction();
				socketOut.println(action);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			client.input.clear();
			break;
		case "SECONDARYACTION":
			try{
				String action=view.askSecondaryAction();
				socketOut.println(action);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			break;
		case "DECK":
			try{
				answer=Integer.toString(view.chooseDeck());
				socketOut.println(answer);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			
			break;
		case "BALCONY":
			try{
			answer=Integer.toString(view.chooseBalcony());
			socketOut.println(answer);
			socketOut.flush();
		}
		catch(InvalidInputException e){
			if(!client.isGUI()) System.out.println("The submitted input is invalid");
			if(client.isGUI()) client.showException("The submitted input is invalid");
			this.parseCommand(name);
		}
			break;
		case "BALCONYWITHKING":
			try{
			int risp=this.view.chooseBalconyPlusKing();
			this.socketOut.println(Integer.toString(risp));
			this.socketOut.flush();
			}catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			break;
		case "DISCOVERED CARDS":
			try{
				answer=Integer.toString(view.choosePermissionCard(client.input));
				socketOut.println(answer);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			client.input.clear();
			break;
		case "YOUR PERMIT":
			try{
				answer=Integer.toString(view.askYourPermissionCard(client.input));
				socketOut.println(answer);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			client.input.clear();
			break;
		case  "CHOOSECITY":
			String temp=this.view.chooseCityNobilityBonus(client.input);
			client.input.clear();
			socketOut.println(temp);
			socketOut.flush();
			break;
		case "PERMITCITY":
			try{
				String response=view.askCityPermit(client.input);
				socketOut.println(response);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			
			client.input.clear();
			break;
		case "KINGCITIES":
			try{
				String currentCity=client.input.get(client.input.size()-1);
				client.input.remove(client.input.size()-1);
				answer=view.moveKing(client.input, currentCity);
				socketOut.println(answer);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
		
			client.input.clear();
			break;
		case "COLOR":
			try{
				ArrayList<String> colors=new ArrayList<>();
				for(String s:client.input){
					
					colors.add(s);
				}
				String color=view.chooseColor(colors);
				socketOut.println(color);
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			client.input.clear();
			break;
		case "MATCH":
			try{
				ArrayList<String> matches=view.usePoliticCard(client.input);
				for(String s:matches){
					socketOut.println(s);
					socketOut.flush();
				}
				socketOut.println("END");
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			client.input.clear();
			break;
			
		case "PURCHASE":
			
			
			try {
				answer=Integer.toString(view.productToPurchase(client.playerId,client.input));
			} catch (VoidMarketException e1) {
				answer="VOID";
			}
			socketOut.println(answer);
			socketOut.flush();
			client.input.clear();
			break;
		case "PRICE":
			answer=Integer.toString(view.askPrice());
			socketOut.println(answer);
			socketOut.flush();
			break;
		case "NOTIFYACTION":
			client.setNotifiedActions(client.input.get(0));
			client.input.clear();
			if(client.isGUI()){
				try {
					client.getTable().printMap(client.getCityTokenPaths(), client.getMapConfiguration(),client.getEmporiums(),client.getMapForCLI().get(client.getMapForCLI().size()-1));
					client.getTable().printBalconies(client.getBalconies());
					client.getTable().printPoliticCard(client.getYourCards());
					client.getTable().printPermissionCards(client.getPlayersPermits());
					client.getTable().printPermissionDecks(client.getDiscoveredCards());
					client.getTable().printPlayerStats(client.getStats());
					
					client.getTable().printPublisher(client.getNotifiedActions());
					
				} catch (IOException e) {
					System.out.println("Unable to find file");
				}
			}
			else{
				CLI cli=(CLI) this.view;
				System.out.println(client.getNotifiedActions());
				for(String s:client.getMapForCLI()){
					System.out.println(s);
				}
				cli.printStats(client.getYourCards(), client.getStats(), client.getPlayersPermits(),client.getBalconies(),client.getEmporiums());
			}
			
			
			break;
			case "SELL":
			try{
				ArrayList<String> permits=new ArrayList<>();
				ArrayList<String> cards=new ArrayList<>();
				ArrayList<String> result=new ArrayList<>();
				int assistants=0;
				int indexPermits=client.input.indexOf("ENDCARDS");
				int indexAssistants=client.input.indexOf("ENDPERMITS");
				for(int i=0; i<indexPermits; i++){
					cards.add(client.input.get(i));
				}
				for(int i=indexPermits+1; i<indexAssistants; i++){
					permits.add(client.input.get(i));
				}
				assistants=Integer.parseInt(client.input.get(indexAssistants+1));
				
				result=view.productToSell(client.playerId,assistants, permits, cards);
				for(String s:result){
					socketOut.println(s);
					socketOut.flush();
				}
				socketOut.println("END");
				socketOut.flush();
			}
			catch(InvalidInputException e){
				if(!client.isGUI()) System.out.println("The submitted input is invalid");
				if(client.isGUI()) client.showException("The submitted input is invalid");
				this.parseCommand(name);
			}
			client.input.clear();
			break;
		}
		
		
		
	}
		

	

	@Override
	public void run() {
		
	
				while(true){
					try {
						
						client.commandSync();
						try {
							parseCommand(client.getCommand());
						} catch (InvalidInputException e) {
							System.out.println("The input is invalid");
							
							
						}
					
					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
				}
				
			
		}
		

	public Color stringToColor(String s){
		if ("giallo".equals(s)) return Color.YELLOW;
		if ("magenta".equals(s)) return Color.MAGENTA;
		if ("blu".equals(s)) return Color.BLUE;
		if ("nero".equals(s)) return Color.BLACK;
		if ("bianco".equals(s)) return Color.WHITE;
		if ("rosa".equals(s)) return Color.PINK;
		if ("arancione".equals(s)) return Color.ORANGE;
		if ("verde".equals(s)) return Color.GREEN;
		return null;
	}
	
	public String colorToString(Color c){
		
		if (c==Color.YELLOW) return "giallo";
		if (c==Color.MAGENTA) return "magenta";
		if (c==Color.BLUE) return "blu";
		if (c==Color.BLACK) return "nero";
		if (c==Color.WHITE) return "bianco";
		if (c==Color.PINK) return "rosa";
		if (c==Color.ORANGE) return "arancione";
		if (c==Color.GREEN) return "verde";
		return null;
	}
	

	public void goToSleep() throws InterruptedException{
		synchronized (this){
			wait();
		}
	}
	
	public  void wakeUp()  {
		synchronized (this){
			notifyAll();
			
		}
	}


	public String getChosenGame() {
		return chosenGame;
	}


	public void setChosenGame(String chosenGame) {
		this.chosenGame = chosenGame;
	}
	

}
