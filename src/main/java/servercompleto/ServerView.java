package servercompleto;



import java.awt.Color;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import exceptions.VoidMarketException;

public interface ServerView extends Remote{

	public ArrayList<String> getGamesNames() throws RemoteException;
	public int subscribe(ServerView s, String gameName) throws RemoteException;
	public boolean primaryActionFirst() throws RemoteException;
	public String askPrimaryAction() throws RemoteException;
	public String askSecondaryAction() throws RemoteException;
	public int getChosenDeck() throws RemoteException;
	public int getChosenBalcony() throws RemoteException;
	public int getPermissionCard(ArrayList<String> discoveredCards) throws RemoteException;
	public int askYourPermissionCard(ArrayList<String> permits) throws RemoteException;
	public String askCityPermit(ArrayList<String> cities) throws RemoteException;
	public String getKingMovement(ArrayList<String> cities, String currentCity) throws RemoteException;
	public Color getChosenColor(ArrayList<Color> colors) throws RemoteException;
	public int getProductToPurchase(ArrayList<String> showcase) throws RemoteException, VoidMarketException;
	public int getPrice() throws RemoteException;
	public ArrayList<String> getProductToSell(int assistants,ArrayList<String> permits,ArrayList<String> cards) throws RemoteException;
    public void setClientInt() throws RemoteException;
    public void notifyActions(String msg) throws RemoteException;
    public ArrayList<String> matchToUse(ArrayList<String> matches) throws RemoteException;
    public void setId(int id) throws RemoteException;
    public int getId() throws RemoteException;
    public void sendCityTokenPath(ArrayList<String> paths) throws RemoteException;
    public void notifyGameStarted() throws RemoteException;
    public void sendDiscoveredCards(ArrayList<String> cards) throws RemoteException;
    public void sendBalconies(ArrayList<String> balconies) throws RemoteException;
    public void sendYourPoliticCard(String card) throws RemoteException;
    public void sendPlayersPermits(ArrayList<String> permits) throws RemoteException;
    public void sendMapForCLI (ArrayList<String> map) throws RemoteException;
    public void sendStats(ArrayList<String> stats) throws RemoteException;
    public void sendMapConfiguration(String configuration) throws RemoteException;
    public void sendWinner(String s) throws RemoteException;
    public void sendBuiltEmporiums(ArrayList<String> emporiums) throws RemoteException;
    public void sendException(String exception) throws RemoteException;
    public void setInactive()throws RemoteException;
    public boolean isActive()throws RemoteException;
	public void sendDrawnCard(String card)throws RemoteException;
	public void notifyRefresh()throws RemoteException;
	public int getChosenBalconyPlusKing() throws RemoteException;
	public String chooseCity(ArrayList<String> cities) throws RemoteException;
    
}
