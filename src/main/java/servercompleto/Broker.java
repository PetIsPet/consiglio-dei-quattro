package servercompleto;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import mvc.Game;


public class Broker {

	protected Map<String, Set<ServerView>> subscriptions=new HashMap<String,Set<ServerView>>();

	

	private ArrayList<GameRunner> games=new ArrayList<>();
	private ArrayList<String> gamesNames=new ArrayList<>();
	
	public static void main(String[] args){
		
		Broker broker=new Broker();
		RMIListener rmiListener=new RMIListener(broker);//creo e lancio listener RMI
		Thread t1=new Thread(rmiListener);
		t1.start();
		SocketListener socketListener=new SocketListener(broker); //creo e lancio listener Socket
		Thread t2=new Thread(socketListener);
		t2.start();
		
		Scanner sc=new Scanner(System.in); //creazione giochi
		String command="inizializzo";
		
		while(!sc.equals("exit")){
			command=sc.nextLine();
			if("exit".equals(command)) break;
			
			Game game=broker.createGame(command);
			
			broker.getGamesNames().add(command);
			GameRunner gameRunner=new GameRunner(game,broker,broker.askForMapConfiguration());
			
			broker.games.add(gameRunner);
			Thread t=new Thread(gameRunner);
			
			System.out.println("Created game "+gameRunner.getGame().getName());
			t.start();
		}
		
		
	}
	
	public int subscribe(ServerView s, String name) throws RemoteException {
		
		int id=0;
		synchronized(subscriptions){
			if(!subscriptions.containsKey(name)){
		
			subscriptions.put(name, new HashSet<ServerView>());
			
		}
	    
		subscriptions.get(name).add(s);
		
		Player player=new Player(0, null, null);
		for(GameRunner g:this.games){ //ogni volta che aggiungo un client creo un giocatore nel gioco
			
			if(g.getGame().getName().equals(name)) {
				
				synchronized(this){
				id=g.getGame().getPlayersID();
				player.setId(g.getGame().getPlayersID());
				g.getGame().incrementPlayersID();
				}
				g.getGame().getPlayers().add(player);
				System.out.println("Added player in " + name);
			}
		}
		if(this.gameExists(name)){
			System.out.println("Added a subscriber to "+ name);
		}
		
		
	}
	return id;	
	}
	
	public boolean gameExists(String name){
		for(String n:this.gamesNames){
			if(n.equals(name)) return true;
		}
		return false;
			
	}
	
	public Game createGame(String name){
		
		
		Lettore lettore=new Lettore();
		GraphMap map=new GraphMap(lettore);
		ArrayList<Color> colors=new ArrayList<Color>();
		colors.add(Color.BLACK);
		colors.add(Color.WHITE);
		colors.add(Color.MAGENTA);
		colors.add(Color.GREEN);
		Game game=new Game(colors,map);
		game.setName(name);
		
		return game;
	}

	public ArrayList<String> getGamesNames() {
		return gamesNames;
	}

	public void setGamesNames(ArrayList<String> gamesNames) {
		this.gamesNames = gamesNames;
	}
	
	/**
	 * @return the subscriptions
	 */
	public Map<String, Set<ServerView>> getSubscriptions() {
		return subscriptions;
	}
	
	public String askForMapConfiguration(){
		System.out.println("Choose a map configuration");
		System.out.println("1-AAA\n2-AAB\n3-ABA\n4-ABB\n5-BAA\n6-BAB\n7-BBA\n8-BBB");
		
		Scanner scanner = new Scanner(System.in);
		String choice= scanner.nextLine();
		switch(choice){
		case "AAA":
			return choice;
			
		case "AAB":
			return choice;
		
		case "ABA":
			return choice;
		
		case "ABB":
			return choice;
		
		case "BAA":
			return choice;
			
		case "BAB":
			return choice;
			
		case "BBA":
			return choice;
			
		case "BBB":
			return choice;
		
		default :
			System.out.println("Your choice is not permitted");
			return askForMapConfiguration();
		}
		
		
	}
	
}
