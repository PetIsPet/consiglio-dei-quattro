package servercompleto;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import gui.GUI;
import gui.GUIHandler;
import gui.GameTable;
import gui.LobbyHandler;
import gui.LobbyJFrame;
import gui.ShowMessage;
import mvc.CLI;

public class SubscriberSocket  {
	
	public static final int PORT=9999;
	public static final String IP="127.0.0.1";
	private String command;
	private String mapConfiguration;
	protected int playerId;
	protected ArrayList<String> input;
	private boolean GUI;
	private boolean gameStarted=false;
	private Object startingLock=new Object();
	private Object commandLock=new Object();
	private ArrayList<String> cityTokenPaths=new ArrayList<>();
	private ArrayList<String> discoveredCards=new ArrayList<>();
	private ArrayList<String> balconies=new ArrayList<>();
	private String yourCards="";
	private String notifiedActions;
	private ArrayList<String> playersPermits=new ArrayList<>();
	private ArrayList<String> mapForCLI=new ArrayList<>();
	private ArrayList<String> stats=new ArrayList<>();
	private ArrayList<String> emporiums=new ArrayList<>();
	private GameTable table;
	private String winner;
	
	public SubscriberSocket(){
		chooseGUI();
		this.input=new ArrayList<>();
		command=null;
	}
	
	

	public static void main(String[] args) throws UnknownHostException, IOException {
		
	
		SubscriberSocket client=new SubscriberSocket();
		client.startClient();
		
		
		

	}
	
	public void startClient() throws IOException{
		
		Socket socket = new Socket(IP, PORT);
		

		
		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		
		executor.submit(new ClientOutHandler(new PrintWriter(socket.getOutputStream()),this));
		executor.submit(new ClientInHandler(new Scanner(socket.getInputStream()),this));
		
			
	}

	public  String getCommand() throws InterruptedException {
	
		return command;
	}

	
	public void launchLobby(ArrayList<String> games, ClientOutHandler client){
		LobbyJFrame lobby=new LobbyJFrame(games,client);
		LobbyHandler handler=new LobbyHandler(client);
		Thread t1=new Thread(lobby);
		Thread t2=new Thread(handler);
		t1.start();
		t2.start();
		try {
			t2.join();
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	
	public void showException(String exception){
		GUI gui=new GUI();
		ShowMessage s=new ShowMessage(gui,exception);
		GUIHandler handler=new GUIHandler(gui);
		Thread t1=new Thread(s);
		Thread t2=new Thread(handler);
		t1.start();
		t2.start();
		try {
			t2.join();
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	

	
	public  void wakeUp()  {
		synchronized (this){
			notifyAll();
			
		}
	}


	public void setCommand(String command) {

		this.command=command;
		
	}
	
	public void commandSync(){
		synchronized(this.commandLock){
			commandLock.notifyAll();
			
			try {
				
				commandLock.wait();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
		}
	}
	
	
	public void chooseGUI(){
		System.out.println("Choose between GUI or CLI");
		Scanner sc=new Scanner(System.in);
		
		String choice=sc.nextLine();
		Thread.yield();
		
		switch(choice){
		case "GUI":
			this.GUI=true;
			break;
		case "CLI":
			this.GUI=false;
			break;
		case "gui":
			this.GUI=true;
			break;
		case "cli":
			this.GUI=false;
			break;
		default:
			System.out.println("Your choice is not permitted");
			chooseGUI();
			break;
		}
		
		
	}
	
	
	
	public void goToSleep() throws InterruptedException{
		synchronized (this){
			wait();
		}
	}
	
	
	public boolean isGUI() {
		return GUI;
	}


	public void setGUI(boolean gUI) {
		GUI = gUI;
	}



	public ArrayList<String> getCityTokenPaths() {
		return cityTokenPaths;
	}


	public void setCityTokenPaths(ArrayList<String> cityTokenPaths) {
		this.cityTokenPaths.addAll(cityTokenPaths);
	}

	public void setGameStarted(boolean flag){
		this.gameStarted=flag;
		
	}
	
	public boolean getGameStarted(){
		return this.gameStarted;
	}

	
	
	
	public void launchGameTable() throws IOException, InterruptedException{
		
		if(this.GUI){
			GameTable table=new GameTable();
			this.table=table;
			table.printMap(cityTokenPaths, mapConfiguration,emporiums,mapForCLI.get(mapForCLI.size()-1));
			table.printPermissionDecks(discoveredCards);
			table.printBalconies(balconies);
			table.printPoliticCard(yourCards);
			table.printNobility();
			table.printBonus();
			table.printPlayerStats(stats);
			
		}
		else{
			for(String s:this.mapForCLI){
				
				System.out.println(s);
			}
			CLI cli=new CLI();
			System.out.println(this.getNotifiedActions());
			for(String s:this.getMapForCLI()){
				System.out.println(s);
			}
			cli.printStats(this.getYourCards(), this.getStats(), this.getPlayersPermits(),this.getBalconies(),this.emporiums);
		}
		
	 
	}



	public ArrayList<String> getDiscoveredCards() {
		return discoveredCards;
	}



	public void setDiscoveredCards(ArrayList<String> discoveredCards) {
		this.discoveredCards = discoveredCards;
	}



	public ArrayList<String> getBalconies() {
		return balconies;
	}



	public void setBalconies(ArrayList<String> balconies) {
		this.balconies = balconies;
	}



	public String getYourCards() {
		return yourCards;
	}



	public void setYourCards(String yourCards) {
		this.yourCards = yourCards;
	}



	public String getNotifiedActions() {
		return notifiedActions;
	}



	public void setNotifiedActions(String notifiedActions) {
		this.notifiedActions = notifiedActions;
	}



	public ArrayList<String> getPlayersPermits() {
		return playersPermits;
	}



	public void setPlayersPermits(ArrayList<String> playersPermits) {
		this.playersPermits = playersPermits;
	}



	public ArrayList<String> getMapForCLI() {
		return mapForCLI;
	}



	public void setMapForCLI(ArrayList<String> mapForCLI) {
		this.mapForCLI = mapForCLI;
	}





	public ArrayList<String> getStats() {
		return stats;
	}



	public void setStats(ArrayList<String> stats) {
		this.stats = stats;
	}



	public String getMapConfiguration() {
		return mapConfiguration;
	}



	public void setMapConfiguration(String mapConfiguration) {
		this.mapConfiguration = mapConfiguration;
	}



	public GameTable getTable() {
		return table;
	}



	public void setTable(GameTable table) {
		this.table = table;
	}



	public String getWinner() {
		return winner;
	}



	public void setWinner(String winner) {
		this.winner = winner;
	}



	public ArrayList<String> getEmporiums() {
		return emporiums;
	}



	public void setEmporiums(ArrayList<String> emporiums) {
		this.emporiums = emporiums;
	}
	
	public boolean tableIsNull(){
		if(this.table==null) return true;
		return false;
	}
	

	
}
