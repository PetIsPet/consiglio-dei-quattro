package servercompleto;

import java.awt.Color;
import java.io.IOException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;

import exceptions.InvalidInputException;
import exceptions.VoidMarketException;
import mvc.CLI;
import mvc.View;
import gui.GUI;
import gui.GUIHandler;
import gui.GameTable;
import gui.LobbyHandler;
import gui.LobbyJFrame;
import gui.ShowMessage;



public class SubscriberRMI implements Remote,SubscriberRMIInterface {
	
	private View view;
	private Object printLock=new Object();
	private Object startLock=new Object();
	private int playerId;
	private boolean GUI;
	private boolean gameStarted=false;
	private String chosenGame;
	private String mapConfiguration;
	private ArrayList<String> cityTokenPaths=new ArrayList<>();
	private ArrayList<String> permitsImagesPaths=new ArrayList<>();
	private ArrayList<String> discoveredCards=new ArrayList<>();
	private ArrayList<String> balconies=new ArrayList<>();
	private String yourCards="";
	private String notifiedActions;
	private ArrayList<String> playersPermits=new ArrayList<>();
	private ArrayList<String> mapForCLI=new ArrayList<>();
	private ArrayList<String> stats=new ArrayList<>();
	private ArrayList<String> emporiums=new ArrayList<>();
	private GameTable table;
	private String winner;
	
	
	public SubscriberRMI(){
		chooseGUI();
		if(this.GUI) this.view=new GUI();
		if(!this.GUI) this.view=new CLI();
		
	}
	
	
	

	public static void main(String[] args) throws AccessException, RemoteException, NotBoundException {
		
		SubscriberRMI client=new SubscriberRMI();
		
		
		Registry reg=LocateRegistry.getRegistry(8888); //inizializzo connessione
		ServerView rmiInt=(ServerView) reg.lookup("BROKER");
		
		
		SubscriberRMIInterface view= (SubscriberRMIInterface) UnicastRemoteObject.exportObject(client,0);
		reg.rebind("CLIENT",view);
		
		
		rmiInt.setClientInt();
		
		
		ArrayList<String> games=new ArrayList<>();
		for(String s:rmiInt.getGamesNames()){
			
			games.add(s);
		}
		
		if(client.GUI){
			LobbyJFrame lobby=new LobbyJFrame(games,client);
			LobbyHandler handler=new LobbyHandler(client);
			Thread t1=new Thread(lobby);
			Thread t2=new Thread(handler);
			t1.start();
			t2.start();
			try {
				t2.join();
			} catch (InterruptedException e) {
				System.out.println("Unable to reach thread");
			}
		}
		if(!client.GUI){
			System.out.println("***AVAILABLE GAMES***");
			for(String s:games){
				System.out.println(s);
			}
			Scanner sc=new Scanner(System.in);
			System.out.println("Choose your game");
			client.chosenGame=sc.nextLine();
			
			
		}
		String choice=client.chosenGame;
		while(choice.equals("refresh")){
			if(!client.GUI){
				System.out.println("***AVAILABLE GAMES***");
				for(String s:rmiInt.getGamesNames()){
					System.out.println(s);
				}
				Scanner sc=new Scanner(System.in);
				System.out.println("Choose your game");
				client.chosenGame=sc.nextLine();
				choice=client.chosenGame;
				
			}
			if(client.GUI){
				ArrayList<String> gamesRefresh=new ArrayList<>();
				for(String s:rmiInt.getGamesNames()){
					
					gamesRefresh.add(s);
				}
				LobbyJFrame lobbyRefresh=new LobbyJFrame(gamesRefresh,client);
				LobbyHandler handlerRefresh=new LobbyHandler(client);
				Thread t1Refresh=new Thread(lobbyRefresh);
				Thread t2Refresh=new Thread(handlerRefresh);
				t1Refresh.start();
				t2Refresh.start();
				try {
					t2Refresh.join();
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				choice=client.chosenGame;
			
			}
		}
		
		client.playerId=rmiInt.subscribe(rmiInt, choice);
		rmiInt.setId(client.playerId);
		System.out.println("You have been registered as Player "+client.playerId);
		System.out.println("Waiting for game to start...");
		
		
		client.sleepOnStartLock();
		if(client.GUI){
			
			try {
				client.launchGameTable();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		
		else{
			for(String s:client.mapForCLI){
				
				System.out.println(s);
			}
			CLI cli=(CLI) client.view;
			cli.printStats(client.yourCards, client.stats, client.playersPermits,client.balconies,client.emporiums);
			
		}
		
		
		while(true){
		
			
		}

	}
	
	public boolean primaryActionFirst(){
	
		try {
			return this.view.primaryActionFirst(this.playerId);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return primaryActionFirst();
		}
	}
	
	public String askPrimaryAction(){
		
		try {
			return this.view.askPrimaryAction();
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return askPrimaryAction();
		}
	}
	
	public String askSecondaryAction(){
		
		try {
			return this.view.askSecondaryAction();
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return askSecondaryAction();
		}
	}
	
	public void showException(String exception){
		GUI gui=new GUI();
		ShowMessage s=new ShowMessage(gui,exception);
		GUIHandler handler=new GUIHandler(gui);
		Thread t1=new Thread(s);
		Thread t2=new Thread(handler);
		t1.start();
		t2.start();
		try {
			t2.join();
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	public int getChosenDeck(){
		
		try {
			return this.view.chooseDeck();
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return getChosenDeck();
		}
	}
	
	public int getChosenBalcony(){
		
		try {
			return this.view.chooseBalcony();
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return getChosenBalcony();
		}
	}
	
	public int getPermissionCard(ArrayList<String> discoveredCards){
		
		try {
			return this.view.choosePermissionCard(discoveredCards);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return getPermissionCard(discoveredCards);
		}
	}
	
	public int askYourPermissionCard(ArrayList<String> permits){
		
		try {
			return this.view.askYourPermissionCard(permits);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return askYourPermissionCard(permits);
		}
	}
	
	public String askCityPermit(ArrayList<String> cities){
		
		try {
			return this.view.askCityPermit(cities);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return askCityPermit(cities);
		}
	}
	
	public String getKingMovement(ArrayList<String> cities, String currentCity){
		
		try {
			return this.view.moveKing(cities, currentCity);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return getKingMovement(cities,currentCity);
		}
	}
	
	public String getChosenColor(ArrayList<String> colors){
		
		
		
		
		try {
			return this.view.chooseColor(colors);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return getChosenColor(colors);
		}
		
	}
	
	public int getProductToPurchase(ArrayList<String> market) throws VoidMarketException{
		
		return this.view.productToPurchase(playerId,market);
	}
	
	public int getPrice(){
		
		return this.view.askPrice();
	}
	
	public ArrayList<String> getProductToSell(int assistants, ArrayList<String> permits, ArrayList<String> cards){
		
		try {
			return this.view.productToSell(this.playerId, assistants, permits, cards);
		} catch (InvalidInputException e) {
			if(!this.GUI) System.out.println("The submitted input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return getProductToSell(assistants, permits, cards);
		}
	}
	
	public void notifyActions (String msg){
		
		this.notifiedActions=msg;
		
		if(this.GUI){
			table.printPublisher(this.notifiedActions);
			try {
				table.printMap(cityTokenPaths, mapConfiguration, emporiums, mapForCLI.get(mapForCLI.size()-1));
				table.printBalconies(balconies);
				table.printPermissionDecks(discoveredCards);
				
				table.printPoliticCard(yourCards);
				table.printPermissionCards(playersPermits);
				
				table.printPlayerStats(stats);
				
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
		}
		else{
			System.out.println(msg);
			CLI cli=(CLI) this.view;
			for(String s:this.mapForCLI){
				System.out.println(s);
			}
			cli.printStats(this.yourCards, stats, playersPermits,balconies,emporiums);
			
			
		}
	}


	@Override
	public ArrayList<String> usePoliticCards(ArrayList<String> matches) throws RemoteException {
		
		try {
			return this.view.usePoliticCard(matches);
		} catch (InvalidInputException e) {
			if(!this.isGUI()) System.out.println("The input is invalid");
			if(this.isGUI()) this.showException("The submitted input is invalid");
			return usePoliticCards(matches);
		}
	}



	@Override
	public void printShowcase(ArrayList<String> showcase) throws RemoteException {
		for(String s:showcase){
			System.out.println(s);
		}
		
		
	}
	
	public String colorToString(Color c){
		
		if (c==Color.YELLOW) return "giallo";
		if (c==Color.MAGENTA) return "magenta";
		if (c==Color.BLUE) return "blu";
		if (c==Color.BLACK) return "nero";
		if (c==Color.WHITE) return "bianco";
		if (c==Color.PINK) return "rosa";
		if (c==Color.ORANGE) return "arancione";
		if (c==Color.GREEN) return "verde";
		return null;
	}
	
	
	public Color stringToColor(String s){
		if ("giallo".equals(s)) return Color.YELLOW;
		if ("magenta".equals(s)) return Color.MAGENTA;
		if ("blu".equals(s)) return Color.BLUE;
		if ("nero".equals(s)) return Color.BLACK;
		if ("bianco".equals(s)) return Color.WHITE;
		if ("rosa".equals(s)) return Color.PINK;
		if ("arancione".equals(s)) return Color.ORANGE;
		if ("verde".equals(s)) return Color.GREEN;
		return null;
	}


	public void chooseGUI(){
		System.out.println("Choose between GUI or CLI");
		Scanner sc=new Scanner(System.in);
		String choice=sc.nextLine();
		
		switch(choice){
		case "GUI":
			this.GUI=true;
			break;
		case "CLI":
			this.GUI=false;
			break;
		case "gui":
			this.GUI=true;
			break;
		case "cli":
			this.GUI=false;
			break;
		default:
			System.out.println("Your choice is not permitted");
			chooseGUI();
			break;
		}
		
	}
	
	
	
	public void wakeUp(){
		synchronized (this){
			notifyAll();
				}
			}
	
	public void goToSleep() throws InterruptedException{
		synchronized (this){
			wait();
		}
	}
	
	public boolean isGUI() {
		return GUI;
	}


	public void setGUI(boolean gUI) {
		GUI = gUI;
	}




	public String getChosenGame() {
		return chosenGame;
	}




	public void setChosenGame(String chosenGame) {
		this.chosenGame = chosenGame;
	}




	@Override
	public void setCityTokenPaths(ArrayList<String> paths) throws RemoteException {
		this.cityTokenPaths=paths;
		
	}
	
	public void launchGameTable() throws IOException{
		
		GameTable table=new GameTable();
		this.table=table;
		table.run();
		table.printMap(cityTokenPaths,this.mapConfiguration,emporiums,mapForCLI.get(mapForCLI.size()-1));
		table.printPermissionDecks(discoveredCards);
		table.printBalconies(balconies);
		table.printPoliticCard(yourCards);
		table.printPlayerStats(stats);
		table.printNobility();
		table.printBonus();		
		
		
		
	}
	
	





	@Override
	public void setPermitsImagesPaths(ArrayList<String> paths) throws RemoteException {
		this.permitsImagesPaths.addAll(paths);
		
	}




	@Override
	public void setDiscoveredCards(ArrayList<String> cards) throws RemoteException {
		this.discoveredCards.clear();
		this.discoveredCards.addAll(cards);
		
	}


	@Override
	public void setYourCards(String cards) throws RemoteException {
		this.yourCards=cards;
		
	}


	public ArrayList<String> getBalconies() {
		return balconies;
	}




	public void setBalconies(ArrayList<String> balconies) {
		this.balconies = balconies;
	}




	@Override
	public void setPlayersPermits(ArrayList<String> permits) throws RemoteException {
		this.playersPermits.clear();
		this.playersPermits.addAll(permits);
		
	}




	@Override
	public void setMapForCLI(ArrayList<String> map) throws RemoteException {
		
		this.mapForCLI.clear();
		
		this.mapForCLI.addAll(map);
		
	}




	@Override
	public void setStats(ArrayList<String> stats) throws RemoteException {
		this.stats.clear();
		this.stats.addAll(stats);
		
	}




	@Override
	public void setMapConfiguration(String configuration) throws RemoteException {
		this.mapConfiguration=configuration;
		
	}




	@Override
	public void setWinner(String winner) throws RemoteException {
		this.winner=winner;
		if(this.GUI)  view.theWinnerIs(Integer.parseInt(winner));
		if(!this.GUI) System.out.println("The winner is Player "+Integer.parseInt(winner));
		
	}




	/**
	 * a lock to wait for the start of the game
	 */
	public void sleepOnStartLock(){
		synchronized(this.startLock){
			
			try {
				this.startLock.wait();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}


/**
 * a method to wake up the waiters on startLock
 *
 */
	public void wakeUpOnStartLock(){
		synchronized(this.startLock){
			
			this.startLock.notify();
		}
	}




@Override
public void setBuiltEmporiums(ArrayList<String> emporiums) throws RemoteException {
	this.emporiums.clear();
	this.emporiums.addAll(emporiums);
	
}




@Override
public void notifyException(String exception) throws RemoteException {
	if(!this.GUI){
		System.out.println(exception);
	}
	if(this.isGUI()) this.showException(exception);
	
}




@Override
public void setDrawnCard(String card) {

	this.yourCards=yourCards+card;
	if(this.table!=null){
		if(this.GUI)
			try {
				table.printPoliticCard(yourCards);
			} catch (IOException e) {
				
			}
		if(!this.GUI) System.out.println("You have drawn a "+card+" card");
		
	}
	
	
}




@Override
public void refresh() throws RemoteException {
	try {
		if(this.GUI){
			this.table.printPlayerStats(stats);
			this.table.printPoliticCard(yourCards);
			this.table.printPermissionCards(playersPermits);
			}
			else{
				CLI cli=(CLI) view;
				cli.printStats(this.yourCards, stats, playersPermits,balconies,emporiums);
			}
	} catch (IOException e) {
		
	}
		
	
}




@Override
public int chooseBalconyPlusKing() throws RemoteException {
	try {
		return view.chooseBalconyPlusKing();
	} catch (InvalidInputException e) {
		return chooseBalconyPlusKing();
	}
	
}




@Override
public String chooseCity(ArrayList<String> cities) throws RemoteException {
	
	return view.chooseCityNobilityBonus(cities);
}







	
	

}
