package servercompleto;

import java.io.FileNotFoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import exceptions.NoPlayersLeftException;
import gamemanagement.TurnManager;
import mvc.Game;

public class GameRunner implements Runnable {
	
	
	private Game game;
	private Broker broker;
	private String mapConfiguration;
	
	public GameRunner(Game game, Broker broker, String mapConfiguration){
		
		this.game=game;
		this.broker=broker;
		this.mapConfiguration=mapConfiguration;
	}

	@Override
	public void run() {
		try {
			
		
		
		while(!this.enoughPlayers()){
		
			Thread.yield();
			if(this.enoughPlayers())
				break;
			
			
		}
		
		System.out.println("STARTING GAME "+ game.getName()+" IN 20 SEC...");
		Thread.sleep(20000); 
		System.out.println("GAME "+game.getName()+" STARTED");
		
		game.setMapConfiguration(this.mapConfiguration);
		
		TurnManager turnManager=new TurnManager(broker.subscriptions.get(game.getName()),game);
		
		game.initialize();
		
		
	
		
		try {
			turnManager.assignRegularTurn();
		} catch (NoPlayersLeftException e) {
			System.out.println("No players left. Nobody wins the game!");
			
		}
		
		
		
		} catch (  InterruptedException e) {
			
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("Unable to find file");
		}

	}

	/**
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}
	
	public boolean enoughPlayers(){
		
		if(game.getPlayers().size()>1) {
			return true;
		}
		return false;
	}
	

	

}
