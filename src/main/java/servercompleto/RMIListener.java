package servercompleto;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import servercompleto.ServerRMIView;




public class RMIListener implements Runnable {
	
	private Broker broker;
	
	public RMIListener(Broker b){
		
		this.broker=b;
	}

	@Override
	public void run() {
		Registry reg = null;
		try {
			reg = LocateRegistry.createRegistry(8888);
			
		
		} catch (RemoteException e) {
			System.out.println("Unable to connect");
		}
		
		
	
		System.out.println("SERVER RMI ready on PORT: 8888");
		System.out.println("(type exit to finish)");
		
		while(true){
			ServerRMIView rmiInt = null;
			try {
				rmiInt = new ServerRMIView(broker);
			} catch (RemoteException | NotBoundException e1) {
				System.out.println("Unable to connect");
			}
			ServerView view;
			try {
				view = (ServerView) UnicastRemoteObject.exportObject(rmiInt,0);
				reg.rebind("BROKER",view);
			} catch (RemoteException e) {
				System.out.println("Unable to connect");
			}
			
		}

	}
	

	

}
