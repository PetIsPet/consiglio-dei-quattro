package servercompleto;


import java.awt.Color;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import exceptions.VoidMarketException;

public class ServerRMIView implements ServerView, Remote {
	
	
	

	private Broker broker;
	private SubscriberRMIInterface clientInt;
	private int id;
	private boolean isActive;
	
	public ServerRMIView(Broker b) throws RemoteException, NotBoundException{
		
		this.broker=b;
		this.isActive=true;
		
		
	}


	@Override
	public ArrayList<String> getGamesNames() {
		return broker.getGamesNames();
	}



	@Override
	public int subscribe(ServerView s, String gameName) throws RemoteException {
		
		int id= broker.subscribe(s, gameName);
		this.id=id;
		return id;
	}


	@Override
	public boolean primaryActionFirst() throws RemoteException {
		return clientInt.primaryActionFirst();
	}


	@Override
	public String askPrimaryAction() throws RemoteException {
		return clientInt.askPrimaryAction();
	}


	@Override
	public String askSecondaryAction() throws RemoteException {
		
		return clientInt.askSecondaryAction();
	}


	@Override
	public int getChosenDeck() throws RemoteException {
		
		return clientInt.getChosenDeck();
	}


	@Override
	public int getChosenBalcony() throws RemoteException {
		
		return clientInt.getChosenBalcony();
	}


	@Override
	public int getPermissionCard(ArrayList<String> discoveredCards) throws RemoteException {
		
		return clientInt.getPermissionCard(discoveredCards);
	}


	@Override
	public int askYourPermissionCard(ArrayList<String> permits) throws RemoteException {
		
		return clientInt.askYourPermissionCard(permits);
	}


	@Override
	public String askCityPermit(ArrayList<String> cities) throws RemoteException {
		
		return clientInt.askCityPermit(cities);
	}


	@Override
	public String getKingMovement(ArrayList<String> cities, String currentCity) throws RemoteException {
		
		return clientInt.getKingMovement(cities, currentCity);
	}


	@Override
	public Color getChosenColor(ArrayList<Color> colors) throws RemoteException {
		
		ArrayList<String> names=new ArrayList<>();
		for(Color c:colors){
			
			names.add(this.colorToString(c));
		}
		
		
		return this.stringToColor(clientInt.getChosenColor(names));
	}


	@Override
	public int getProductToPurchase(ArrayList<String> showcase) throws RemoteException, VoidMarketException {
		
		return clientInt.getProductToPurchase(showcase);
	}


	@Override
	public int getPrice() throws RemoteException {
		
		return clientInt.getPrice();
	}


	@Override
	public ArrayList<String> getProductToSell(int assistants, ArrayList<String> permits, ArrayList<String> cards)
			throws RemoteException {
		
		
		
		return clientInt.getProductToSell(assistants, permits, cards);
	}


	/**
	 * @param clientInt the clientInt to set
	 * @throws RemoteException 
	 */
	public void setClientInt() throws RemoteException {
		Registry reg=LocateRegistry.getRegistry(8888); //inizializzo connessione
		
		try {
			SubscriberRMIInterface clientInt = (SubscriberRMIInterface) reg.lookup("CLIENT");
			this.clientInt = clientInt;
		} catch (NotBoundException e) {
			System.out.println("Unable to connect");
		}
		
	}


	@Override
	public void notifyActions(String msg) throws RemoteException {
		this.clientInt.notifyActions(msg);
		
	}


	
	@Override
	public ArrayList<String> matchToUse(ArrayList<String> matches) throws RemoteException {
		
		return this.clientInt.usePoliticCards(matches);
	}
	
	
	@Override
	public void sendCityTokenPath(ArrayList<String> paths) throws RemoteException  {
		
		this.clientInt.setCityTokenPaths(paths);
		
		
	}
	
	

	public String colorToString(Color c){
		
		if (c.equals(Color.YELLOW)) return "giallo";
		if (c.equals(Color.MAGENTA)) return "magenta";
		if (c.equals(Color.BLUE)) return "blu";
		if (c.equals(Color.BLACK)) return "nero";
		if (c.equals(Color.WHITE)) return "bianco";
		if (c.equals(Color.PINK)) return "rosa";
		if (c.equals(Color.ORANGE)) return "arancione";
		if (c.equals(Color.GREEN)) return "verde";
		return null;
	}
	
	
	public Color stringToColor(String s){
		if ("giallo".equals(s)) return Color.YELLOW;
		if ("magenta".equals(s)) return Color.MAGENTA;
		if ("blu".equals(s)) return Color.BLUE;
		if ("nero".equals(s)) return Color.BLACK;
		if ("bianco".equals(s)) return Color.WHITE;
		if ("rosa".equals(s)) return Color.PINK;
		if ("arancione".equals(s)) return Color.ORANGE;
		if ("verde".equals(s)) return Color.GREEN;
		return null;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	@Override
	public void notifyGameStarted() throws RemoteException {
		
		this.clientInt.wakeUpOnStartLock();
		
	}


	@Override
	public void sendDiscoveredCards(ArrayList<String> cards) throws RemoteException {
		
		this.clientInt.setDiscoveredCards(cards);
		
	}


	@Override
	public void sendBalconies(ArrayList<String> balconies) throws RemoteException {
		this.clientInt.setBalconies(balconies);
		
	}


	@Override
	public void sendYourPoliticCard(String card) throws RemoteException {
		this.clientInt.setYourCards(card);
		
	}


	@Override
	public void sendPlayersPermits(ArrayList<String> permits) throws RemoteException {
		this.clientInt.setPlayersPermits(permits);
		
	}


	@Override
	public void sendMapForCLI(ArrayList<String> map) throws RemoteException {
		this.clientInt.setMapForCLI(map);
		
	}


	@Override
	public void sendStats(ArrayList<String> stats) throws RemoteException {
		this.clientInt.setStats(stats);
		
	}


	@Override
	public void sendMapConfiguration(String configuration) throws RemoteException {
		this.clientInt.setMapConfiguration(configuration);
	}


	@Override
	public void sendWinner(String s) throws RemoteException {
		this.clientInt.setWinner(s);
		
	}


	@Override
	public void sendBuiltEmporiums(ArrayList<String> emporiums) throws RemoteException {
		
		this.clientInt.setBuiltEmporiums(emporiums);
		
	}


	@Override
	public void sendException(String exception) throws RemoteException {
		
		this.clientInt.notifyException(exception);
	}


	@Override
	public void setInactive() {
		this.isActive=false;
		
	}


	@Override
	public boolean isActive() {
	
		return this.isActive;
	}


	@Override
	public void sendDrawnCard(String card) throws RemoteException {
		clientInt.setDrawnCard(card);
		
	}


	@Override
	public void notifyRefresh() throws RemoteException {
		clientInt.refresh();
		
	}


	@Override
	public int getChosenBalconyPlusKing() throws RemoteException {
		return this.clientInt.chooseBalconyPlusKing();
	}


	@Override
	public String chooseCity(ArrayList<String> cities) throws RemoteException {
		
		return clientInt.chooseCity(cities);
	}








	


	


	


	


}
