package servercompleto;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Set;

import actions.BuyPermissionCard;
import actions.ChangePermissionCard;
import actions.ConstructionWithKing;
import actions.CouncelorElection;
import actions.DoAnotherPrimaryAction;
import actions.DoNothing;
import actions.EmployAssistant;
import actions.EmporiumConstruction;
import actions.PrimaryAction;
import actions.SecondaryAction;
import actions.SendAssistantToCouncil;
import bonuses.Bonus;
import bonuses.BonusCityKing;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.InvalidInputException;
import exceptions.NoPlayersLeftException;
import exceptions.NotEnoughResourcesException;
import exceptions.OnePlayerLeftException;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.Balcony;
import mappaeoggetti.Card;
import mappaeoggetti.City;
import mappaeoggetti.Councelor;
import mappaeoggetti.Emporium;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mappaeoggetti.Type;
import mvc.Game;

public class Publisher {

	private TurnManager controller;
	
	
	public Publisher(TurnManager controller){
		
		this.controller=controller;
		
	}
	
	public void notifyException(Exception e,ServerView s) throws OnePlayerLeftException, NoPlayersLeftException {
		
			if(e.getClass()==NotEnoughResourcesException.class){
				String msg="You haven't got enough resources to do this";
				try {
					s.sendException(msg);
				} catch (RemoteException e1) {
					controller.removeFromSubscription(s);
					
				}
			}
			if(e.getClass()==InvalidInputException.class){
				String msg="The sumbitted input is not valid";
				try {
					s.sendException(msg);
				} catch (RemoteException e1) {
					controller.removeFromSubscription(s);
				}
			}
			if(e.getClass()==InvalidDoAnotherPrimaryActionChoiceException.class){
				String msg="You have chosen an invalid action for your DoAnotherPrimaryAction Quick Action";
				try {
					s.sendException(msg);
				} catch (RemoteException e1) {
					controller.removeFromSubscription(s);
				}
			}
		
	}
	
	public void notifyWinner(Player player) throws  NoPlayersLeftException {
		
			String msg=Integer.toString(player.getId());
			
	
			for(ServerView s:controller.getClients()){
				try {
					
				
						s.sendWinner(msg);
					
						
					

				} catch (RemoteException e) {
					throw new NoPlayersLeftException();
				}
			}
	}
	
	public void notifyCLIMapSituation(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		ArrayList<String> map=new ArrayList<>();
		
		for(City c:game.getCities()){
			String cityStats="NAME:"+c.getName();
			cityStats=cityStats+" TYPE:"+c.getType().toString();
			cityStats=cityStats+" NEIGHBOURS:";
			for(City n:c.getNeighborhood()){
				cityStats=cityStats+n.getName()+" ";
			}
			if(!c.getEmporiums().isEmpty()){
				cityStats=cityStats+"EMPORIUMS:";
				for(Emporium e:c.getEmporiums()){
					
					cityStats=cityStats+"player"+e.getPlayer().getId()+" ";
				}
				
			}
			
			if(c.getCityToken().getBonuses().get(0).getClass()==BonusCityKing.class){
				cityStats=cityStats+"It's the KINGCITY ";
			}
			else{
				cityStats=cityStats+"CITYBONUS:";
				for(Bonus b:c.getCityToken().getBonuses()){
					cityStats=cityStats+b.getName()+" ";
				}
				
			}
			
			
			map.add(cityStats);	
		}
		
		String kingCity= "The king is in "+game.getKing().getCurrentCity().getName();
		map.add(kingCity);
		
		for(ServerView s:controller.getClients()){
			
				try {
					s.sendMapForCLI(map);
				} catch (RemoteException e) {
					
					controller.removeFromSubscription(s);
				}
			
		}
		
	}
	

	
	
	public void notifyPrimaryAction(PrimaryAction action, int id,Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		for(ServerView s:controller.getClients()){
			this.notifyCLIMapSituation(game);
			this.sendGameStats(game);
			this.sendBalconies(game);
			this.sendDiscoveredCards(game);
			this.sendPlayersPermits(game);
			this.sendYourPoliticCards(game);
			this.sendBuiltEmporiums(game);
			
		}
		
		
		
		for(ServerView s:controller.getClients()){
			
			try {
				if(s.getId()!=id){
					s.notifyActions("Player "+(id)+" has done a  "+this.primaryActionToString(action)+" Primary Action");
					}
			} catch (RemoteException e) {
				controller.removeFromSubscription(s);
			}
			
				try {
					if(s.getId()==id){
					s.notifyActions("You have done a  "+this.primaryActionToString(action)+" Primary Action");
					}
				} catch (RemoteException e) {
					controller.removeFromSubscription(s);
				}
			}
		}
		

	
	
	public void notifySecondaryAction(SecondaryAction action, int id,Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		for(ServerView s:controller.getClients()){
			this.notifyCLIMapSituation(game);
			this.sendGameStats(game);
			this.sendBalconies(game);
			this.sendDiscoveredCards(game);
			this.sendPlayersPermits(game);
			this.sendYourPoliticCards(game);
			 this.sendBuiltEmporiums(game);
			
		}
		
		
		for(ServerView s:controller.getClients()){
			
				try {
					if(s.getId()!=id){
					
					if(!this.secondaryActionToString(action).equals("Do Nothing")){
						s.notifyActions("Player "+(id)+" has done a "+this.secondaryActionToString(action)+" Quick Action");
					}
					else{
						s.notifyActions("Player "+(id)+" skipped the Quick Action");
					}
					}
				} catch (RemoteException e) {
					controller.removeFromSubscription(s);
				}
				try {
					if(s.getId()==id) {
						if(!this.secondaryActionToString(action).equals("Do Nothing")){
							s.notifyActions("You have done a "+this.secondaryActionToString(action)+" Quick Action");
						}
						else{
							s.notifyActions("You skipped the Quick Action");
						}
					}
				} catch (RemoteException e) {
					controller.removeFromSubscription(s);
				}
			}
		
	}
	
	public void notifyGameHasStarted() throws OnePlayerLeftException, NoPlayersLeftException {
		for(ServerView s:controller.getClients()){
			
				try {
					s.notifyGameStarted();
				} catch (RemoteException e) {
					controller.removeFromSubscription(s);
				}
			}
		
	}
	
	public String primaryActionToString(PrimaryAction action){
		if(action.getClass()==BuyPermissionCard.class) 
			return "Buy Permission Card";
		if(action.getClass()==ConstructionWithKing.class) 
			return "Construction With King";
		if(action.getClass()==EmporiumConstruction.class) 
			return "Emporium Construction";
		if(action.getClass()==CouncelorElection.class) 
			return "Councelor Election";
		return null;
	}
	
	public String secondaryActionToString(SecondaryAction action){
		if(action.getClass()==ChangePermissionCard.class) 
			return "Change Permission Card";
		if(action.getClass()==EmployAssistant.class) return "Employ Assistant";
		if(action.getClass()==SendAssistantToCouncil.class) 
			return "Send Assistant To Council";
		if(action.getClass()==DoAnotherPrimaryAction.class) 
			return "Do Another Primary Action";
		if(action.getClass()==DoNothing.class)
			return "Do Nothing";
		return null;
	}
	
	
	/**
	 * send the discovered cards to the subscribers
	 * @param game
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 * @throws RemoteException 
	 */
	public void sendDiscoveredCards(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		ArrayList<PermissionCard> permits=new ArrayList<>();
		ArrayList<String> permitsToString=new ArrayList<>();
		
		for(PermissionDeck deck:game.getDecks()){
			
			for(int i=0; i<2; i++){
				
				permits.add(deck.getDiscoveredCards()[i]);
			}
			
			
		}
		permitsToString.addAll(this.permitsToString(permits));
		
	
	
		
		for(ServerView s:controller.getClients()){
			
				try {
					s.sendDiscoveredCards(permitsToString);
					} catch (RemoteException e) {
						controller.removeFromSubscription(s);
					}  //invia carte scoperte
				}
			
	}
	
	/**
	 * send the built emporiums
	 * @param the game
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 * @throws RemoteException 
	 */
	public void sendBuiltEmporiums(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		ArrayList<String> builtEmporiums=new ArrayList<>();
		for(Player p:game.getPlayers()){
			String temp="";
			for(City c:game.getCities()){
				if(p.hasBuilt(c)) temp=temp+c.getName()+" ";
			}
			builtEmporiums.add(temp);
		}
		for(ServerView s:controller.getClients()){
			
				try {
					s.sendBuiltEmporiums(builtEmporiums);
				} catch (RemoteException e) {
					controller.removeFromSubscription(s);
					
				}  //invia carte scoperte
			}
		
	}
	
	
	
	
	
	/**
	 * send the balconies to the subscribers
	 * @param game
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 * @throws RemoteException
	 */
	public void sendBalconies(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		ArrayList<String> balconies=new ArrayList<>();
		
		for(Balcony b:game.getBalconies()){
			String councelors="";
			for(Councelor c:b.getCouncelors()){
				
				councelors=councelors+this.colorToString(c.getColor())+"-";
				
			}
			
			balconies.add(councelors);
		}
		String councelors="";
		for(Councelor c:game.getKingBalcony().getCouncelors()){
			
			councelors=councelors+this.colorToString(c.getColor())+"-";
			
		}
		balconies.add(councelors);
		

		for(ServerView s:controller.getClients()){
			
				try {
					s.sendBalconies(balconies);
				} catch (RemoteException e) {
					
					controller.removeFromSubscription(s);
				} //invia balconi
			}
		
	}
	
	
	/**
	 * send current player's politics cards
	 * @param game
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 * @throws RemoteException
	 */
	
	public void sendYourPoliticCards(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		
		for(Player p:game.getPlayers()){
			String card="";
			for(Card c:p.getCards()){
				card=card+this.colorToString(c.getColor())+"-";
			}
			for(ServerView s:controller.getClients()){
				
					try {
						if(s.getId()==p.getId()){
							s.sendYourPoliticCard(card);
						}
					} catch (RemoteException e) {
						controller.removeFromSubscription(s);
					}
				}
			
		}
	}
	
	/**
	 * send players permits
	 * @param game
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 * @throws RemoteException
	 */
	
	public void sendPlayersPermits(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		ArrayList<String> playersPermits=new ArrayList<>();
		for(Player p:game.getPlayers()){
			String playerCards="";
			ArrayList<String> stringPermits=this.permitsToString(p.getPermits());
			for(String s:stringPermits){
				playerCards=playerCards+s+"/";
			}
			
			playersPermits.add(p.getId(), playerCards);
		}
		for(ServerView s:controller.getClients()){
			
				try {
					s.sendPlayersPermits(playersPermits);
				} catch (RemoteException e) {
					controller.removeFromSubscription(s);
				} //send players permits
			}
		
	}
	
	public void sendGameStats(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		ArrayList<String> stats=new ArrayList<>();
		
		for(Player p:game.getPlayers()){
			String stat="";
			stat=stat+p.getId()+"#";
			stat=stat+p.getPoints()+"#";
			stat=stat+p.getAssistants()+"#";
			stat=stat+p.getCoins()+"#";
			stat=stat+p.getBuiltEmporiums()+"#";
			stat=stat+p.getNobilityPoints()+"#";
			
			stats.add(stat);
		}
		
		for(ServerView s:controller.getClients()){
			
				try {
					s.sendStats(stats);
				} catch (RemoteException e) {
					
					controller.removeFromSubscription(s);
				} //send game stats
			}
		
		
	}
	
	
	
	public void notifyStartingSituation(Game game) throws OnePlayerLeftException, NoPlayersLeftException {
		
		

		this.sendDiscoveredCards(game);
		
		this.sendBalconies(game);
		
	    this.sendYourPoliticCards(game);
	    
	    this.sendPlayersPermits(game);
	    
	    this.sendGameStats(game);
	    
				
	}
	
	
	
	public ArrayList<String> permitsToString(ArrayList<PermissionCard> list){
		
		
		
		ArrayList<String> permits=new ArrayList<>();
		
		for(PermissionCard c:list){
			String card="ID:"+list.indexOf(c)+"-"+"PERMESSI:";
			for(City n:c.getBuildPermissions()){
			
				card=card+n.getName()+"-";
			
			}
			card=card+"#"+c.getId();
			permits.add(card);
		}
		
		
		return permits;
	}
	
	
	
	public String colorToString(Color c){
		
		if (c.equals(Color.YELLOW)) return "yellow";
		if (c.equals(Color.MAGENTA)) return "magenta";
		if (c.equals(Color.BLUE)) return "blue";
		if (c.equals(Color.BLACK)) return "black";
		if (c.equals(Color.WHITE)) return "white";
		if (c.equals(Color.PINK)) return "pink";
		if (c.equals(Color.ORANGE)) return "orange";
		if (c.equals(Color.GREEN)) return "green";
		return null;
	}

	public void sendDrawnCard(ServerView s,String card) throws OnePlayerLeftException, NoPlayersLeftException {
	
		try {
			s.sendDrawnCard(card);
		} catch (RemoteException e) {
			controller.removeFromSubscription(s);
		}
		
	}

	public void notifyRefresh() throws OnePlayerLeftException, NoPlayersLeftException {
		for(ServerView s:controller.getClients()){
			try {
				s.notifyRefresh();
			} catch (RemoteException e) {
				controller.removeFromSubscription(s);
			}
		}
		
	}
}
