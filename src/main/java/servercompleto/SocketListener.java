package servercompleto;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class SocketListener implements Runnable {
	
	public static int PORT=9999;
	private Broker broker;
	private int port;
	
	public SocketListener(Broker b){
		
		this.broker=b;
		this.port=PORT;
		PORT++;

	}
	
	@Override
	public void run() {
		
		ExecutorService executor = Executors.newCachedThreadPool();
		// creates a new Server socket on the specified port
		
		
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		
		System.out.println("SERVER SOCKET ready on PORT: " + PORT);
		while(true) {
			
				
				
				
				
				try {
					Socket socket=serverSocket.accept();
					ServerSocketView view;
					view = new ServerSocketView(broker,socket);
					executor.submit(view);
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				
				
				
				
			
	}

	}

}
