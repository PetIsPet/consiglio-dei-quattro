package mvc;

import mappaeoggetti.Balcony;
import mappaeoggetti.CardDeck;
import mappaeoggetti.City;
import mappaeoggetti.Councelor;
import mappaeoggetti.CouncelorsColor;
import mappaeoggetti.King;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bonuses.Bonus;
import file.Lettore;
import gamemanagement.GraphMap;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.Type;

public class Game {
	
	private King king;
	private CardDeck cardDeck;
	private ArrayList<PermissionDeck> decks=new ArrayList<PermissionDeck>();
	private ArrayList<Balcony> balconies;
	private ArrayList<Player> players=new ArrayList<>();
	private ArrayList<Color> cardColors=new ArrayList<>();
	private ArrayList<Bonus> bonusPool=new ArrayList<Bonus>();
	private ArrayList<Councelor> councelorsPool=new ArrayList<>();
	private ArrayList<City> cities=new ArrayList<>();
	private CLI cli;
	private int playersID;
	private TurnManager controller;
	private GraphMap map;
	private Balcony kingBalcony;
	private Map<Integer, ArrayList<Bonus>> nobilityScale = new HashMap<Integer, ArrayList<Bonus>>();
	private String mapConfiguration="AAA";
	private ArrayList<String> permitsImagesPaths=new ArrayList<>();
	private ArrayList<PermissionCard> usedPermits=new ArrayList<>();

	

	private String name;
	
	
	public Game(ArrayList<Color> colors, GraphMap map){
		
		this.map=map;
		
		
		
		ArrayList<PermissionDeck> decks=new ArrayList<PermissionDeck>();
		ArrayList<Balcony> balconies=new ArrayList<Balcony>();
		ArrayList<Player> players=new ArrayList<>();
		ArrayList<Color> playersColors=new ArrayList<>();
		ArrayList<Color> cardColors=new ArrayList<>();
		ArrayList<Bonus> bonusPool=new ArrayList<Bonus>();
		ArrayList<Councelor> councelorsPool=new ArrayList<>();
		ArrayList<City> cities=new ArrayList<>();
		
		
		
		this.decks=decks;
		this.players=players;
		this.balconies=balconies;
		
		this.cardColors=cardColors;
		this.bonusPool=bonusPool;
		this.councelorsPool=councelorsPool;
		this.setCities(cities);
		this.playersID=0;
		
		
	}
	
	/**
	 * inizialize the game, with card, deck, assistant, coins and create the balcony.
	 * @throws FileNotFoundException 
	 */
	public void initialize() throws FileNotFoundException {   //inizializza il gioco
		
		
		Lettore loader=new Lettore();
		loader.setMapConfiguration(mapConfiguration);
		loader.load();
		this.setPermitsImagesPaths(loader.getPermitsImagesPaths());
		GraphMap map=new GraphMap(loader); //creo mappa
		map.run();
		this.map=map;
		
		cardColors.add(Color.BLACK);		//crea pull di colori per carte politiche e consiglieri
		cardColors.add(Color.WHITE);
		cardColors.add(Color.ORANGE);
		cardColors.add(Color.MAGENTA);
		cardColors.add(Color.PINK);
		cardColors.add(Color.BLUE);
		
		
		CouncelorsColor councelors=new CouncelorsColor(cardColors,10);
		councelors.setColorPool(cardColors);
		councelors.createCouncelors();
		this.councelorsPool=councelors.getCouncelorsPool();//setto pool consiglieri
		
		
		
		this.setCities(loader.getPoolCities()); //setto pool città
	
		
		PermissionDeck seaDeck=new PermissionDeck(Type.SEASIDE,loader.getDecks().get(1)); //crea i mazzi permesso
		seaDeck.discover();
		decks.add(seaDeck);
		PermissionDeck plainDeck=new PermissionDeck(Type.PLAIN,loader.getDecks().get(2));
		plainDeck.discover();
		decks.add(plainDeck);
		PermissionDeck mountainDeck=new PermissionDeck(Type.MOUNTAIN,loader.getDecks().get(3));
		mountainDeck.discover();
		decks.add(mountainDeck);

		
		
		cardColors.add(Color.YELLOW); //multicolor
		this.cardDeck=new CardDeck(cardColors); //crea mazzo carte politiche
		
		
		int counterAssistants=1;
		int counterCoins=10;
		
        for(Player p:players){  //inizializza le carte in mano
			p.setPoints(0);
			p.setNobilityPoints(0);
        	p.setAssistants(counterAssistants);
			p.setCoins(counterCoins);
			
			for(int j=0; j<6; j++){
				
				cardDeck.drawCard(p);
			}
			
			counterAssistants++;
			counterCoins++;
		}
		
        
		this.cli=new CLI();   //crea la CLI
		
        Balcony seasideBalcony=new Balcony(this,Type.SEASIDE);  //creo balconi
		Balcony plainBalcony=new Balcony(this,Type.PLAIN);
		Balcony mountainBalcony=new Balcony(this,Type.MOUNTAIN);
		
		Balcony kingBalcony=new Balcony(this); //creo balcone del re
		
		this.balconies.add(seasideBalcony);
		this.balconies.add(plainBalcony);
		this.balconies.add(mountainBalcony);
		
		this.kingBalcony=kingBalcony;
		
		
		
		this.king=new King(loader.getCityKing());
		this.nobilityScale=loader.getNobilityScale();
		
		
		}


	public CLI getCli() {
		return cli;
	}


	public void setCli(CLI view) {
		this.cli = view;
	}


	/**
	 * @return the mapConfiguration
	 */
	public String getMapConfiguration() {
		return mapConfiguration;
	}

	/**
	 * @param mapConfiguration the mapConfiguration to set
	 */
	public void setMapConfiguration(String mapConfiguration) {
		this.mapConfiguration = mapConfiguration;
	}

	public ArrayList<PermissionDeck> getDecks() {
		return decks;
	}


	public void setDecks(ArrayList<PermissionDeck> decks) {
		this.decks = decks;
	}


	public ArrayList<Balcony> getBalconies() {
		return balconies;
	}


	public void setBalconies(ArrayList<Balcony> balconies) {
		this.balconies = balconies;
	}


	public ArrayList<Color> getCardColors() {
		return cardColors;
	}


	public void setCardColors(ArrayList<Color> cardColors) {
		this.cardColors = cardColors;
	}


	public List<Councelor> getCouncelorsPool() {
		return councelorsPool;
	}


	public void setCouncelorsPool(ArrayList<Councelor> councelorsPool) {
		this.councelorsPool = councelorsPool;
	}


	public King getKing() {
		return king;
	}


	public void setKing(King king) {
		this.king = king;
	}


	
	/**
	 * @return the players
	 */
	public ArrayList<Player> getPlayers() {
		return players;
	}


	/**
	 * @param players the players to set
	 */
	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}


	public GraphMap getMap() {
		return map;
	}


	public void setMap(GraphMap map) {
		this.map = map;
	}
	
	/**
	 * @return the controller
	 */
	public TurnManager getController() {
		return controller;
	}


	/**
	 * @param controller the controller to set
	 */
	public void setController(TurnManager controller) {
		this.controller = controller;
	}
	

	/**
	 * @return the cardDeck
	 */
	public CardDeck getCardDeck() {
		return cardDeck;
	}

	public void setCardDeck(CardDeck cardDeck) {
		this.cardDeck = cardDeck;
	}

	
	public ArrayList<City> getCities() {
		return cities;
	}


	public void setCities(ArrayList<City> list) {
		this.cities = list;
	}


	public Balcony getKingBalcony() {
		
		return this.kingBalcony;
	}


	/**
	 * @return the nobilityScale
	 */
	public Map<Integer, ArrayList<Bonus>> getNobilityScale() {
		return nobilityScale;
	}

	/**
	 * @param nobilityScale the nobilityScale to set
	 */
	public void setNobilityScale(Map<Integer, ArrayList<Bonus>> nobilityScale) {
		this.nobilityScale = nobilityScale;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the playersID
	 */
	public int getPlayersID() {
		return playersID;
	}
	
	public void setPlayersID(int playersID) {
		this.playersID = playersID;
	}

	public void incrementPlayersID() {
		this.playersID++;
	}
   
	

	public ArrayList<String> getPermitsImagesPaths() {
		return permitsImagesPaths;
	}

	public void setPermitsImagesPaths(ArrayList<String> permitsImagesPaths) {
		this.permitsImagesPaths = permitsImagesPaths;
	}

	
	  public ArrayList<PermissionCard> getUsedPermits() {
		return usedPermits;
	}

	public void setUsedPermits(ArrayList<PermissionCard> usedPermits) {
		this.usedPermits = usedPermits;
	}

	public boolean hasFinished(){
			
			for(Player p:this.players){
				
				if (p.getBuiltEmporiums()==10)
					return true;
				
			}
			return false;
		}




}
