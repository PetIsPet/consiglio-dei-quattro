package mvc;

import java.util.ArrayList;
import java.util.List;

import gamemanagement.TurnManager;
import mappaeoggetti.Card;

public abstract class Observable {
	
	public ArrayList<TurnManager> observers;
	
	public Observable(){
		
		ArrayList<TurnManager> observers=new ArrayList<>();
	}
	
	public void registerObserver(TurnManager observer){
		
		this.observers.add(observer);
	}
	
	public void notifyObservers(){}

	public int usePoliticCard(List<Card> matches) {
		return 0;
	}

	public int chooseAnotherMatch(List<Card> matches) {
		return 0;
	}

	public void printShowcase(ArrayList<String> market) {
		
	}

}
