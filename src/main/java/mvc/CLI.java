package mvc;


import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import exceptions.InvalidInputException;
import exceptions.VoidMarketException;
import gamemanagement.Turn;
import mappaeoggetti.City;


public class CLI extends View implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2782226323253771846L;
	private String errore;
	private Scanner x;
	private Scanner x2;
	private Scanner x3;
	private Scanner x4;
	private Scanner x5;
	private Scanner x6;
	private Scanner x7;
	private Scanner x8;
	private Scanner x9;
	private Scanner x10;
	private Scanner x11;
	private Scanner x12;
	private Scanner x13;
	private Scanner x14;
	private Scanner y;
	private Scanner z;
	private Scanner y2;
	
	public CLI() {
		super();
		this.errore="La tua scelta non è valida";
		
	}

	/**
	 * @return the error
	 */
	public String getErrore() {
		return errore;
	}

	@Override
	public void setTurn(Turn t){
		
		super.shownTurn=t;
	}
	
	
	public void printStats(String yourCards,ArrayList<String> stats,ArrayList<String> permits,ArrayList<String> balconies, ArrayList<String> emporiums){ 
		
		System.out.println("******GAME STATS******");
		
		System.out.println("Game Stats"); //stampa game stats
		
		
		for(String s:stats){
			String stat="";
			String[] tokens=s.split("#");
			stat="PLAYER "+tokens[0]+" ";
			
			stat=stat+"POINTS:"+tokens[1]+" ";
			stat=stat+"ASSISTANTS:"+tokens[2]+" ";
			stat=stat+"COINS:"+tokens[3]+" ";
			stat=stat+"EMPORIUMS:"+tokens[4]+" ";
			stat=stat+"NOBILITY POINTS:"+tokens[5]+" ";
			System.out.println(stat);
		}
		
		for(String s:balconies){
			if(balconies.indexOf(s)==0) System.out.println("SEASIDE BALCONY:");
			if(balconies.indexOf(s)==1) System.out.println("PLAIN BALCONY:");
			if(balconies.indexOf(s)==2) System.out.println("MOUNTAIN BALCONY:");
			if(balconies.indexOf(s)==3) System.out.println("KING'S BALCONY:");
			String[] temp=s.split("-");
			for(String g:temp){
				System.out.println(g);
			}
			
		}
	
		
		System.out.println("These are your politics cards:"); //stampa carte politiche
		String[] politicsCards=yourCards.split("-");
		for(String s:politicsCards){
			
			System.out.println(s);
		}
		
		
		if(!permits.isEmpty()){
			System.out.println("These are players permits:"); //stampa carte permesso
			int id=0;
			for(String s:permits){
				System.out.println("PLAYER "+id);
			  String[] temp=s.split("-");
			  for(String m:temp){
				  System.out.println(m);
			  }
			  id++;
			}
		}
		if(!emporiums.isEmpty()){
			System.out.println("These are players emporiums:");
			for(String s:emporiums){
				System.out.println("PLAYER "+emporiums.indexOf(s)+":"+" "+s);
			}
		}
	}
	
	
	public int askYourPermissionCard(ArrayList<String> cards) throws InvalidInputException{
		
		
		
		for(String c: cards){
			
			 
			System.out.println(c);
			
		}
		
		System.out.println("Choose a permission card to use:");
		x = new Scanner(System.in);
		int input = x.nextInt();
		
		if(input>cards.size()-1){
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
		return input;
		
	}
	
	
	public String askCityPermit(ArrayList<String> cities) throws InvalidInputException{
		
		
		for(String s: cities){
			
			 System.out.println(s);
			
			
		}
		
		System.out.println("Insert the name of the city in which you want to build:");
		x2 = new Scanner(System.in);
		String input = x2.nextLine();
		
		if(!cities.contains(input)) {
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
		
		return input;
	}
	
	
	
	
	
	
	@Override
	public String askPrimaryAction() throws InvalidInputException{  //chiede di scegliere un'azione primaria
		
		
		try{
			System.out.println("Choose the primary action you want to perform:");
		
			System.out.println("1- Buy a permission card \n2- Build an emporium \n3- Built with the king \n4- Councelor's election ");
		
			x3 = new Scanner(System.in);
		
			String input = x3.nextLine();
		
		
			
		
		if(Integer.parseInt(input)==1){
			
			return "BuyPermissionCard";
			
		}
		
		if(Integer.parseInt(input)==2){
			
			return "EmporiumConstruction";
		}
		
		if(Integer.parseInt(input)==3){
			
			return "ConstructionWithKing";
			
		}
		
		if(Integer.parseInt(input)==4){
			
			return "CouncelorElection";
			
		}
		
		else {
			InvalidInputException e=new InvalidInputException();
			throw e;
			
		}
		
		}catch(NumberFormatException e){
			throw new InvalidInputException();
		}
		
		
		
		
		
		
	}

	@Override
	public String askSecondaryAction() throws InvalidInputException {  //chiede di scegliere un'azione secondaria
		
		try{
		System.out.println("Choose the secondary action to perform:");
		System.out.println("1- Discover two new permission cards \n2- Do another primary action \n3- Employ an assistant \n4- Send an assistant to a council \n5- Do nothing");
		x4 = new Scanner(System.in);
		String input = x4.nextLine();
		
       if(Integer.parseInt(input)==1){
			
    	   return "ChangePermissionCard";
			
			
		}
		
		if(Integer.parseInt(input)==2){
			
			return "DoAnotherPrimaryAction";
			
		}
		
		if(Integer.parseInt(input)==3){
			
			return "EmployAssistant";
			
		}
		
		if(Integer.parseInt(input)==4){
			
			return "SendAssistantToCouncil";
			
		}
		if(Integer.parseInt(input)==5){
			return "DoNothing";
		}
		
		else {
			
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
		}catch(NumberFormatException  e1){
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
		
		
	}

	@Override
	public boolean primaryActionFirst(int id) throws InvalidInputException{      //chiede al giocatore quale azione eseguire per prima
		try{
		System.out.println("It's your turn, Player "+id);
	    
		
		System.out.println("Which action do you want to perform first?:");
		System.out.println("1- Primary Action \n2- Secondary Action");
		
		x5 = new Scanner(System.in);
		String input = x5.nextLine();
		
		if(Integer.parseInt(input)==1) 
			return true;
		if(Integer.parseInt(input)==2) 
			return false;
		else throw new InvalidInputException();
		
		}
		catch(NumberFormatException e){
			throw new InvalidInputException();
		}
		
	}
	
	@Override
	public int chooseDeck() throws InvalidInputException{    //chiede di scegliere un mazzo
		
		System.out.println("Choose a deck:");
		System.out.println("1- Seaside deck \n2-Plain deck \n3-Mountain deck");
		x6 = new Scanner(System.in);
		int input = x6.nextInt();
		
		if(input==1) 
			return 0;
		if(input==2) 
			return 1;
		if(input==3) 
			return 2;
		
		else {
			InvalidInputException e=new InvalidInputException();
			throw e;
			
			
		}
			
		
	}
	
	@Override
	public int choosePermissionCard(ArrayList<String> discoveredCards) throws InvalidInputException {
		
		
		for(String s: discoveredCards){
			String[] temp=s.split("#");
			 
			System.out.println(temp[0]);
			
			
		}
		
		System.out.println("Insert the id of which card you want to buy: ");
		
		x7 = new Scanner(System.in);
		int input = x7.nextInt();
		
		if(input!=0){
			if(input!=1){
				InvalidInputException e=new InvalidInputException();
				throw e;
			}
		}
		
		return input;
		}
		
		
	
	@Override
	public int chooseBalcony() throws InvalidInputException{
		
		System.out.println("Choose the balcony you want to modify: \n1- Seaside \n2- Plain \n3- Mountain ");
		
		x8 = new Scanner(System.in);
		int input = x8.nextInt();
		
		if(input!=1){
			if(input!=2){
				if(input!=3){
					InvalidInputException e=new InvalidInputException();
					throw e;
				}
			}
		}
		
		return input-1;
		
		
	}
	
	
	
	@Override
	public int chooseBalconyPlusKing() throws InvalidInputException{
		
		System.out.println("Choose a balcony: \n1- Seaside \n2- Plains \n3-Mountains \n4-King ");
		
		x8 = new Scanner(System.in);
		int input = x8.nextInt();
		
		if(input!=1&&input!=2&&input!=3&&input!=4){
					InvalidInputException e=new InvalidInputException();
					throw e;
				
		}
		
		return input-1;
		
	}
	
	
	@Override
	public String chooseColor(ArrayList<String> colors) throws InvalidInputException{
		List<Integer> indexSet=new ArrayList<>();
		for(String s:colors){
			indexSet.add(colors.indexOf(s));
		}
		System.out.println("Choose a color:");
       for(String color:colors){
    	   
    	   System.out.println("Color: " + color);
    	   System.out.println("Id: " + colors.indexOf(color));
			
		}
       
       x9 = new Scanner(System.in);
		int index = x9.nextInt();
		
		if(!indexSet.contains(index)){
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
       
       return colors.get(index);
			
	}

	
	
	@Override
    public ArrayList<String> usePoliticCard(ArrayList<String> matches) throws InvalidInputException{ //fa scegliere una carta dal pool dei match nell'azione BuyPermissionCard
    	
    	 
		ArrayList<String> tempMatches=new ArrayList<>();
		tempMatches.addAll(matches);
		x10 = new Scanner(System.in);
			int counter=0;
    		List<Integer> indexSet=new ArrayList<>();
    		for(String s:matches){
    			indexSet.add(matches.indexOf(s));
    		}
			ArrayList<String> matchesToUse=new ArrayList<>();
			int index=0;
			
			mainLoop:
	    	while(counter<4){
			System.out.println("These are yours cards that matches with the colors of the balcony's councelors ");
    		for(String card:tempMatches){
    			System.out.println("------------------------------: ");
    		
    			System.out.println("colore: "+ card);
    			System.out.println("id: "+ tempMatches.indexOf(card));
    			
    			
    			System.out.println("------------------------------: ");
    			
    		}
    		
    		
    			
    			System.out.println("Choose a card inserting the properly Id: ");
	    		index=x10.nextInt();
	    		if(!indexSet.contains(index)){
	    			InvalidInputException e=new InvalidInputException();
	    			throw e;
	    		}
	    		matchesToUse.add(tempMatches.get(index));
	    		tempMatches.remove(index);
	    		if(matchesToUse.size()==matches.size()) break;
	    		secondLoop:
	    		while(true){
	    			System.out.println("Choose what to do now: ");
	    			System.out.println("1-Choose another matches \n2-Stop the choice");
	    			index=x10.nextInt();
	    			if(index==1){
	    				counter++;
	    				break secondLoop;
	    			}
	    			if(index==2) break mainLoop;
	    			else{
	    				System.out.println("The submitted input is invalid");
	    			}
	    		}
	    		
	    		
	    		
    		}
    		
    		 return matchesToUse;
    		
  }
    
	
	
	@Override
	public int productToPurchase(int id,ArrayList<String> market) throws VoidMarketException{
		
		if(!market.isEmpty()){
			
			System.out.println("------------------------------: ");
			System.out.println("Buying turn of Player " + id  );
			System.out.println("------------------------------: ");
			
			
				for(String stuff: market){ //stampa la vetrina del market
				
					System.out.println(stuff);
				}
			
			
			
			System.out.println("Insert the Id of the product you want to buy:");
			
			x13 = new Scanner(System.in);
			return x13.nextInt();
		
		}
		else{
			throw new VoidMarketException();
		}
		
	}
	
	
	
	
	
	
	
	
	
	@Override
	public ArrayList<String> productToSell(int id,int assistants,ArrayList<String> permits,ArrayList<String> cards) throws InvalidInputException{  //fa scegliere il prodotto da vendere
		
		ArrayList<String> productToSell=new ArrayList<>();
		
		System.out.println("Selling turn of Player "+id);
		
		System.out.println("You have "+assistants+" assistant");
		
		System.out.println("These are yours permission cards:");
		
		for(String p: permits){
			
			System.out.println(p);
			System.out.println("ID: "+permits.indexOf(p));
		}
		
		System.out.println("These are yours politics card:");
		
		for(String c: cards){
			
			System.out.println(c);
			System.out.println("ID: "+cards.indexOf(c));
		}
		
		
		System.out.println("Choose which product you want to sell:");
		System.out.println("1- Assistant \n2- Permission card \n3- Politic card \n4- I don't want to sell anything");
		
		x14 = new Scanner(System.in);
		String inputString = x14.nextLine();
		
		int input=0;
		try{
			input=Integer.parseInt(inputString);
		}catch(NumberFormatException e){
			throw new InvalidInputException();
		}
		
		
		switch(input){
			case 1:
			productToSell.add("ASSISTANT");
			break;
			case 2:
			productToSell.add("PERMISSION CARD");
			System.out.println("Insert the Id of the permission card you want to sell ");
			y = new Scanner(System.in);
			int input1 = y.nextInt();
			productToSell.add(permits.get(input1));
			break;
			case 3:
			productToSell.add("POLITIC CARD");
			System.out.println("Insert the id of the politic card you want to sell ");
			z = new Scanner(System.in);
			int input2 = z.nextInt();
			productToSell.add(cards.get(input2));
			break;
			case 4:
			productToSell.add("NIENTE");
			break;
			default:
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
		
		return productToSell;
		
	

}
	
	
	
	
	@Override
	public int askPrice(){
		
		System.out.println("Insert a price for your product: ");
		y2 = new Scanner(System.in);
		return y2.nextInt();
		
	}
	
	
	
	public void theWinnerIs(int id){
		
		
		System.out.println("The winner is Player "+ id);
	}
	
	@Override
	public String moveKing(ArrayList<String> cities,String currentCity) throws InvalidInputException {
		ArrayList<String> cityNames=new ArrayList<>();
		ArrayList<City> towns=new ArrayList<>();
		for(String c:cities){
			String[] temp=c.split("#");
			String[] neighbours=temp[1].split("-");
			ArrayList<City> neigh=new ArrayList<>();
			for(String t:neighbours){
				City city=new City(null,null,t);
				neigh.add(city);
			}
			City mycity=new City(neigh,null,temp[0]);
			
			towns.add(mycity);
		}
		
		int price=0;
		
		while(true){
		
		System.out.println("The king is in "+currentCity);
		System.out.println("You can move the king in the following cities:");
		for(City c:towns){
			if(currentCity.equals(c.getName())){
				for(City t:c.getNeighborhood()){
					System.out.println(t.getName());
					cityNames.add(t.getName());
				}
			}
		}
		
		System.out.println("Choose where you want to move the king - type exit to finish the action");
		Scanner z = new Scanner(System.in);
		String name = z.nextLine();
		if("exit".equals(name)) break;
		currentCity=name;
		if(!cityNames.contains(name)){
			InvalidInputException e=new InvalidInputException();
			throw e;
		}
		
		price=price+2;
		
		}
		
		String response=currentCity+"#"+Integer.toString(price);
		return response;
	}
	
	

	public Turn getTurn() {
		
		return super.shownTurn;
	}

	@Override
	public void printStats(ArrayList<String> stats) {

		
	}
	
	public String colorToString(Color c){
		
		if (c==Color.YELLOW) 
			return "giallo";
		if (c==Color.MAGENTA) 
			return "magenta";
		if (c==Color.BLUE) 
			return "blu";
		if (c==Color.BLACK) 
			return "nero";
		if (c==Color.WHITE) 
			return "bianco";
		if (c==Color.PINK) 
			return "rosa";
		if (c==Color.ORANGE) 
			return "arancione";
		if (c==Color.GREEN) 
			return "verde";
		return null;
	}
	
	
	public Color stringToColor(String s){
		if ("giallo".equals(s)) 
			return Color.YELLOW;
		if ("magenta".equals(s)) 
			return Color.MAGENTA;
		if ("blu".equals(s)) 
			return Color.BLUE;
		if ("nero".equals(s)) 
			return Color.BLACK;
		if ("bianco".equals(s)) 
			return Color.WHITE;
		if ("rosa".equals(s)) 
			return Color.PINK;
		if ("arancione".equals(s)) 
			return Color.ORANGE;
		if ("verde".equals(s)) 
			return Color.GREEN;
		return null;
	}

	@Override
	public String chooseCityNobilityBonus(ArrayList<String> cities) {
		System.out.println("These are the cities with your emporiums:");
		for(String s:cities){
			System.out.println(s+" ID:"+cities.indexOf(s));
		}
		System.out.println("Choose a city:");
		Scanner sc=new Scanner(System.in);
		
		String name=sc.nextLine();
		if(cities.contains(name)) return name;
		else{
			try {
				throw new InvalidInputException();
			} catch (InvalidInputException e) {
				chooseCityNobilityBonus(cities);
			}
			
		}
		return name;
		
		
	}

}

