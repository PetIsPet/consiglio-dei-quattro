package mvc;

import java.util.ArrayList;
import java.util.List;

import exceptions.InvalidInputException;
import exceptions.VoidMarketException;
import gamemanagement.Turn;

import mappaeoggetti.Card;



public abstract class View extends Observable  {
	
	
	
	Turn shownTurn; //turno corrente

	
	public abstract String askPrimaryAction() throws InvalidInputException; //chiede di scegliere azione principale
	
	public abstract String askSecondaryAction() throws InvalidInputException; //chiede di scegliere azione secondaria
	
	public abstract boolean primaryActionFirst(int id) throws InvalidInputException; //chiede quale tipo di azione eseguire prima
	
	public abstract int chooseDeck() throws InvalidInputException;  //chiede di scegliere un mazzo permessi
	
	public abstract int askPrice(); //chiede prezzo di un prodotto messo in vendita durante la fase sale

	public abstract void printStats(ArrayList<String> stats); //stampa arraylist di stringhe
	
	public abstract ArrayList<String> productToSell(int id,int assistants,ArrayList<String> permits,ArrayList<String> cards) throws InvalidInputException; //scelta prodotti da vendere
	
	public abstract int productToPurchase(int id, ArrayList<String> market) throws VoidMarketException;//scelta prodotti da comprare
	
	public abstract ArrayList<String> usePoliticCard(ArrayList<String> matches) throws InvalidInputException; //usa  carte politiche
	
	public abstract String chooseColor(ArrayList<String> colors) throws InvalidInputException; //scegli un colore
	
	public abstract int chooseBalcony() throws InvalidInputException;//scegli un balcone
	
	public abstract int chooseBalconyPlusKing() throws InvalidInputException;
	
	public abstract int choosePermissionCard(ArrayList<String> discoveredCards) throws InvalidInputException; //scegli una carta permesso
	
	public abstract int askYourPermissionCard(ArrayList<String> cards) throws InvalidInputException; //scegli una carta permesso ttra quelle in mano
	
	public abstract String askCityPermit(ArrayList<String> cities) throws InvalidInputException; //scegli una città dove costruire tra quelle della carta permesso
	
	public abstract void theWinnerIs(int id);
	
	public abstract String moveKing(ArrayList<String> cities,String currentCity) throws InvalidInputException;
	
	public abstract String chooseCityNobilityBonus(ArrayList<String> cities) ;


	public void setTurn(Turn turn) {
		this.shownTurn=turn;
		
	}

	public void printStats(List<String> stats) {
		
	}

	public int chooseAnotherMatch(ArrayList<Card> matches) {
		
		return 0;
	}

	

}
