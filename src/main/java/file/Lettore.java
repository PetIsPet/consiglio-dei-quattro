package file;

import java.awt.Color;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import bonuses.Bonus;
import bonuses.BonusCityKing;
import bonuses.ChooseCityBonus;
import bonuses.DoAnotherPrimaryActionBonus;
import bonuses.DrawPermissionCard;
import bonuses.DrawPoliticsCard;
import bonuses.GetUsedBonus;
import bonuses.GiveAssistants;
import bonuses.GiveCoins;
import bonuses.GiveNobilityPoints;
import bonuses.GivePoints;
import mappaeoggetti.CardDeck;
import mappaeoggetti.City;
import mappaeoggetti.CityToken;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.Type;


public class Lettore {
	
	
	String sCurrentLine;
	String cCurrentLine;
	String[] line;
	String[] vicini;
	ArrayList<City> poolCities=new ArrayList<City>();
    Type tipo;
   
    private ArrayList<Color> playersColors=new ArrayList<Color>();
    private City cityKing;
    private Map<Integer, ArrayList<Bonus>> nobilityScale=new HashMap<>();
    private ArrayList<CityToken> tokenPool = new ArrayList<>();
	private int numberOfRequiredEmporiums;
	private String mapConfiguration="AAA";
	private Map<Integer, ArrayList<PermissionCard>> decks= new HashMap<>();
	private ArrayList<String> permitsImagesPaths=new ArrayList<>();
	
	
	
	
	/**
	 * reads from file the game settings
	 */
	public Lettore(){
			
		this.poolCities=new ArrayList<>();
		this.playersColors=new ArrayList<>();
		
		
	}
	/**
	 * A method that read a string of color and execute the parse to transform the string in a Color class object
	 * 
	 * @param s the string that the parser must read
	 * @return the color read
	 */
	public Color parseCityColor(String s){
		
		switch(s){
		case "gold":
			return Color.YELLOW;
		case "silver":
			return Color.GRAY;
		case "bronze":
			return Color.ORANGE;
		case "iron":
			return Color.DARK_GRAY;
		case "royalrainbow":
		return Color.CYAN;
		default:
			return null;
		}
	}
	
	
	/**
	 * Method that read the nobilityScale and choose which bonus assign to the player
	 * @param br the bufferedReader to create the file's stream
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	
	public void readNobilityScale(BufferedReader br) throws NumberFormatException, 
		IOException{
		
		int n;
		
		while ((sCurrentLine = br.readLine()) != null) {
			cCurrentLine=sCurrentLine;
			if (cCurrentLine.startsWith("*")) { 
				
				break;
			}
			
			
			line=cCurrentLine.split("-");
			int m=Integer.parseInt(line[0]);
			if(this.nobilityScale.get(m)==null){
				this.nobilityScale.put(m, new ArrayList<Bonus>());
			}
			String[] tempString=line[1].split(",");
			for(int j=0; j<tempString.length; j++){
			
			if (tempString[j].startsWith("c")) {//se bonus è in coin
				n=Integer.parseInt(tempString[j].substring(1));
				GiveCoins giveCoins = new GiveCoins(n);
				this.nobilityScale.get(m).add(giveCoins);
				
				
			}
			if (tempString[j].startsWith("p")) {//se bonus è points
				n=Integer.parseInt(tempString[j].substring(1));
				GivePoints givePoints = new GivePoints(n);
				this.nobilityScale.get(m).add(givePoints);
				
			}
			
			if (tempString[j].startsWith("a")) {//se bonus è assistant
				n=Integer.parseInt(tempString[j].substring(1));
				GiveAssistants giveAssistants = new GiveAssistants(n);
				this.nobilityScale.get(m).add(giveAssistants);
				
			}
			
			if (tempString[j].startsWith("g")) {//se bonus è in città dove ho costruito
				boolean flag = false;
				n=Integer.parseInt(tempString[j].substring(1));
				if(n==1){
					flag=true;
				}
				ChooseCityBonus cityBonus = new ChooseCityBonus(flag);
				this.nobilityScale.get(m).add(cityBonus);
			}
			
			if (tempString[j].startsWith("z")) {//se bonus è una azione primaria
				
				DoAnotherPrimaryActionBonus action = new DoAnotherPrimaryActionBonus();
				this.nobilityScale.get(m).add(action);
			}
			
			if (tempString[j].startsWith("m")) {//se bonus è pesca una carta permesso
				
				DrawPermissionCard draw = new DrawPermissionCard();
				this.nobilityScale.get(m).add(draw);
			}
			
			if (tempString[j].startsWith("d")) {//se bonus è pesca una carta politica
				
				DrawPoliticsCard draw = new DrawPoliticsCard();
				this.nobilityScale.get(m).add(draw);
			}
			
			if (tempString[j].startsWith("b")) {//se bonus è pesca una carta politica
				
				 GetUsedBonus bonus = new GetUsedBonus();
				this.nobilityScale.get(m).add(bonus);
			}
			
			}
	} 
	}
		/**
		 * Method to read the cities's name from a file and return the object
		 * @param br the bufferedReader to create the file's stream
		 * @return
		 * @throws NumberFormatException
		 * @throws IOException
		 */
		public ArrayList<String> readCityName(BufferedReader br) throws NumberFormatException,
		IOException{
		
		ArrayList<String> cities= new ArrayList<>();
		
		
		while ((sCurrentLine = br.readLine()) != null) {//città
			cCurrentLine=sCurrentLine;
			if (cCurrentLine.startsWith("**")) {  //esce dalla prima parte del file
				break;
			}
			
				cities.add(sCurrentLine);
		}
		
		return cities;
	}
			/**
			 * A method that read the cityToken of the cities from a file, reading the different bonus
			 * @param br the bufferedReader to create the file's stream
			 * @throws NumberFormatException
			 * @throws IOException
			 */
		public void readTokenPool(BufferedReader br) throws NumberFormatException,
			IOException{
			
			while ((sCurrentLine = br.readLine()) != null) {
				cCurrentLine=sCurrentLine;
				if (cCurrentLine.startsWith("***")) {
					break;
				}
			
			int n;
			line=cCurrentLine.split("-");
			
			if(!line[0].contains(",")){
			
			if (line[0].startsWith("c")) {//se bonus è in coin
				n=Integer.parseInt(line[0].substring(1));
				GiveCoins giveCoins = new GiveCoins(n);
				CityToken cityToken = new CityToken(line[1]);
				cityToken.getBonuses().add(giveCoins);
				this.tokenPool.add(cityToken);
					
			}
			if (line[0].startsWith("p")) {//se bonus è points
				n=Integer.parseInt(line[0].substring(1));
				GivePoints givePoints = new GivePoints(n);
				CityToken cityToken = new CityToken(line[1]);
				cityToken.getBonuses().add(givePoints);
				this.tokenPool.add(cityToken);
				
			}
			if (line[0].startsWith("n")) {//se bonus è nobility
				n=Integer.parseInt(line[0].substring(1));
				GiveNobilityPoints giveNobilityPoints = new GiveNobilityPoints(n, nobilityScale);
				CityToken cityToken = new CityToken(line[1]);
				cityToken.getBonuses().add(giveNobilityPoints);
				this.tokenPool.add(cityToken);
				
			}
			
			if (line[0].startsWith("a")) {//se bonus è assistant
				n=Integer.parseInt(line[0].substring(1));
				GiveAssistants giveAssistants = new GiveAssistants(n);
				CityToken cityToken = new CityToken(line[1]);
				cityToken.getBonuses().add(giveAssistants);
				this.tokenPool.add(cityToken);
				
			}
			
			if (line[0].startsWith("d")) {//se bonus è assistant
				DrawPoliticsCard draw=new DrawPoliticsCard();
				CityToken cityToken = new CityToken(line[1]);
				cityToken.getBonuses().add(draw);
				this.tokenPool.add(cityToken);
				
			}
		} else {
			
			String [] string= line[0].split(",");
			
			ArrayList<Bonus> bonuses = new ArrayList<>();
			
			for(int i=0; i<2; i++){
				
				
			if (string[i].startsWith("c")) {//se bonus è in coin
				n=Integer.parseInt(string[i].substring(1));
				GiveCoins giveCoins = new GiveCoins(n);
				bonuses.add(giveCoins);
					
			}
			if (string[i].startsWith("p")) {//se bonus è points
				n=Integer.parseInt(string[i].substring(1));
				GivePoints givePoints = new GivePoints(n);
				bonuses.add(givePoints);
				
			}
			if (string[i].startsWith("n")) {//se bonus è nobility
				n=Integer.parseInt(string[i].substring(1));
				GiveNobilityPoints giveNobilityPoints = new GiveNobilityPoints(n, nobilityScale);
				bonuses.add(giveNobilityPoints);
				
			}
			
			if (string[i].startsWith("a")) {//se bonus è assistant
				n=Integer.parseInt(string[i].substring(1));
				GiveAssistants giveAssistants = new GiveAssistants(n);
				bonuses.add(giveAssistants);
				
			}
			
			if (string[i].startsWith("d")) {//se bonus è draw
				DrawPoliticsCard draw=new DrawPoliticsCard();
				bonuses.add(draw);
				
			}
		}
			
			CityToken cityToken2 = new CityToken(line[1]);
			cityToken2.setBonuses(bonuses);
			tokenPool.add(cityToken2);
		}
	}
			
}
		/**
		 * Method that read the neighbours from a file
		 * @param br the bufferedReader to create the file's stream
		 * @throws NumberFormatException
		 * @throws IOException
		 */
		
		public void readCityNeighbours(BufferedReader br) throws NumberFormatException,
			IOException{
			
		while ((sCurrentLine = br.readLine()) != null) {//scorre seconda parte del file con liste di vicini
			cCurrentLine=sCurrentLine;
			if (cCurrentLine.startsWith("****")) { 
				break;
			}
			
			if (cCurrentLine.startsWith(this.mapConfiguration)){
				String stringTemp;
				while((stringTemp = br.readLine()) != "#"){
					if (stringTemp.startsWith("#")) { 
						break;
					}
					String[] neighbourString = stringTemp.split("-");
					City tempCity = null;
					for(City c: this.poolCities){
						
						if(c.getName().equals(neighbourString[0])){
							tempCity=c;
							
						}
					}
					ArrayList<City> neighbours=new ArrayList<>();
					
					tempCity.setType(this.toType(Integer.parseInt(neighbourString[1])));
					tempCity.setColor(this.parseCityColor(neighbourString[3]));
					
					if(neighbourString[2].contains(",")){
						String[] neighbourString2 = neighbourString[2].split(",");
						
						for(int j=0; j<neighbourString2.length; j++){
							for(City c: poolCities){
								if(neighbourString2[j].equals(c.getName())){
									
									neighbours.add(c);
									
								}
							}
						}
						tempCity.setNeighborhood(neighbours);
					}
					else{
						for(City c: poolCities){
							if(neighbourString[2].equals(c.getName())){
								neighbours.add(c);
							}
						
					}
						tempCity.setNeighborhood(neighbours);	
				}
				
				
			}			
		}}		
	}	
		/**
		 * Method that read the player colors from a file
		 * @param br the bufferedReader to create the file's stream
		 * @throws IOException
		 */
		public void readPlayerColors(BufferedReader br) throws IOException{
			
			while ((sCurrentLine = br.readLine()) != null) {
				cCurrentLine=sCurrentLine;
				if (cCurrentLine.startsWith("*****")) {
					break;
				}	
				
				line=cCurrentLine.split(",");
				for(int i=0; i<line.length; i++){
					
					this.getPlayersColors().add(toColor(line[i]));
					
				}
					
			}
			
		}
		/**
		 * Method that read wich City has the initial position of the king, reading that from a file
		 * @param br the bufferedReader to create the file's stream
		 * @throws IOException
		 */
		public void readCityKing(BufferedReader br) throws IOException{
			
			while ((sCurrentLine = br.readLine()) != null) {//scorre seconda parte del file con liste di vicini
				cCurrentLine=sCurrentLine;
				if (cCurrentLine.startsWith("******")) {
					break;
				}
			
				String nameCityKing=cCurrentLine;
										
				for(City d:this.poolCities){
											
					if(d.getName().equals(nameCityKing)) {
						this.cityKing=d;					
					}
				}
		  
			}
			
		}
		/**
		 * Method that read the number of the emporiums that a single city has, reading the information from a file
		 * @param br the bufferedReader to create the file's stream
		 * @throws NumberFormatException
		 * @throws IOException
		 */
		public void readNumberOfEmporium(BufferedReader br) throws NumberFormatException,
			IOException{
			
			int n;
			
			while ((sCurrentLine = br.readLine()) != null) {
				cCurrentLine=sCurrentLine;
				if (cCurrentLine.startsWith("*******")) { 
					
					break;
				}
				
				n=Integer.parseInt(cCurrentLine);
				
				this.setNumberOfRequiredEmporiums(n);
				
				}	
		}
	

	/**
	 * Method that read the PermissionCard from a file, reading the string of the city and the list of the bonus
	 * @param br the bufferedReader to create the file's stream
	 * @throws IOException
	 */
	public void readPermits(BufferedReader br) throws IOException{
		
		while ((sCurrentLine = br.readLine()) != null) {
			cCurrentLine=sCurrentLine;
			if (cCurrentLine.startsWith("********")) { 
				
				break;
			}
			ArrayList<City> cities=new ArrayList<>();
			ArrayList<Bonus> bonus=new ArrayList<>();
			ArrayList<Color> colors=new ArrayList<>();
			colors.add(Color.BLACK);		
			colors.add(Color.WHITE);
			colors.add(Color.ORANGE);
			colors.add(Color.MAGENTA);
			colors.add(Color.PINK);
			colors.add(Color.BLUE);
			colors.add(Color.YELLOW);
			
			String[] tokens=sCurrentLine.split("-");
			
			String[] stringBonus=tokens[2].split(",");
			
			if(tokens[1].contains(",")){
				String[] stringCities=tokens[1].split(",");
				for(int j=0; j<stringCities.length; j++){
					for(City c:this.poolCities){
						if(c.getName().equals(stringCities[j]))  cities.add(c);
					}
				}
			}else{
				for(City c:this.poolCities){
					if(c.getName().equals(tokens[1]))  cities.add(c);
				}
			}
			
			for(int j = 0; j<stringBonus.length; j++){
				
				if(stringBonus[j].startsWith("a")){
					int n=Integer.parseInt(stringBonus[j].substring(1));
					GiveAssistants giveAssistants = new GiveAssistants(n);
					bonus.add(giveAssistants);
					
				}
				
				if(stringBonus[j].startsWith("c")){
					int n=Integer.parseInt(stringBonus[j].substring(1));
					GiveCoins giveCoins = new GiveCoins(n);
					bonus.add(giveCoins);
					
				}
				if(stringBonus[j].startsWith("p")){
					int n=Integer.parseInt(stringBonus[j].substring(1));
					GivePoints givePoints = new GivePoints(n);
					bonus.add(givePoints);
					
				}
				
				if(stringBonus[j].startsWith("n")){
					int n=Integer.parseInt(stringBonus[j].substring(1));
					GiveNobilityPoints givePoints = new GiveNobilityPoints(n, nobilityScale);
					bonus.add(givePoints);
					
				}
				if(stringBonus[j].startsWith("d")){
					DrawPoliticsCard card=new DrawPoliticsCard();
					card.setPoliticDeck(new CardDeck(colors));
					bonus.add(card);
				}
				if(stringBonus[j].startsWith("z")){
					DoAnotherPrimaryActionBonus bonusCard=new DoAnotherPrimaryActionBonus();
					bonus.add(bonusCard);
				}
			}
			
			PermissionCard card=new PermissionCard(bonus,this.toType(Integer.parseInt(tokens[0])),cities);
			card.setImgPath(tokens[3]);
			this.permitsImagesPaths.add(tokens[3]);
			card.setId(this.permitsImagesPaths.indexOf(tokens[3]));
			if(decks.get(Integer.parseInt(tokens[0]))==null) decks.put(Integer.parseInt(tokens[0]),new ArrayList<PermissionCard>());
			decks.get(Integer.parseInt(tokens[0])).add(card);
			
			
			}
		
		
	}
	
	/**
	 * sets the game settings
	 * @throws FileNotFoundException 
	 */
	public void load() throws FileNotFoundException {
		
		
		BufferedReader br = new BufferedReader(new FileReader ("config.txt"));
		
			 
		try {
			
			
			
			this.readNobilityScale(br);
			ArrayList<String> names=this.readCityName(br);
			this.readTokenPool(br);
			Collections.shuffle(tokenPool);
			
			int counter=0;
			
			for(String name:names){
				
				if(!"Juvelar".equals(name)){
					
					City city=new City(null,this.tokenPool.get(counter),name);
					this.poolCities.add(city);
					counter++;
				}
				else{
					CityToken token=new CityToken("niente");
					Bonus bonus=new BonusCityKing();
					token.getBonuses().add(bonus);
					City city=new City(null,token,name);
					this.poolCities.add(city);
				}
				
				
			}
			
			this.readCityNeighbours(br);
			this.readPlayerColors(br);
			this.readCityKing(br);
			this.readNumberOfEmporium(br);
			this.readPermits(br);
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		
		
		
	
	}
	
	/**
	 * 
	 * @return pool of cities
	 */
	public ArrayList<City> getPoolCities() {
		return poolCities;
	}

	/**
	 * 
	 * @param poolCities is the pool of cities
	 */
	public void setPoolCities(ArrayList<City> poolCities) {
		this.poolCities = poolCities;
	}

/**
 * parse the type from file
 * @param tipo type of the land 
 * @return the type of the land
 */
	public Type toType(int tipo){ //parse del tipo letto da file
		
		
		
		if(tipo==3)
			return Type.MOUNTAIN;
		if(tipo==1)
			return Type.SEASIDE;
		if(tipo==2)
			return Type.PLAIN;
		return null;	
			
	}
	
	/**
	 * 
	 * @param colore is the string containing the color
	 * @return color from string
	 */
	public Color toColor(String colore){
		
		switch(colore){
		
		case "y":
			return Color.YELLOW;
		case "b":
		    return Color.BLACK;
		case "o":
		    return Color.ORANGE;
		case "w":
			return Color.WHITE;
		case "m":
			return Color.MAGENTA;
		case "g":
			return Color.GREEN;
		case "p":
			return Color.PINK;
		
			}
		return null;
		
	}
	

	
	/**
	 * @param cityKing the cityKing to set
	 */
	public void setCityKing(City cityKing) {
		this.cityKing = cityKing;
	}




	/**
	 * @return the playersColors
	 */
	public ArrayList<Color> getPlayersColors() {
		return playersColors;
	}

	/**
	 * @return the cityKing
	 */
	public City getCityKing() {
		return cityKing;
	}

	/**
	 * @param mapConfiguration the mapConfiguration to set
	 */
	public void setMapConfiguration(String mapConfiguration) {
		this.mapConfiguration = mapConfiguration;
	}




	/**
	 * @param playersColors the playersColors to set
	 */
	public void setPlayersColors(ArrayList<Color> playersColors) {
		this.playersColors = playersColors;
	}



	/**
	 * @param nobilityScale the nobilityScale to set
	 */
	public void setNobilityScale(Map<Integer, ArrayList<Bonus>> nobilityScale) {
		this.nobilityScale = nobilityScale;
	}




	/**
	 * @return the nobilityScale
	 */
	public Map<Integer, ArrayList<Bonus>> getNobilityScale() {
		return nobilityScale;
	}
	
	/**
	 * @return the decks
	 */
	public Map<Integer, ArrayList<PermissionCard>> getDecks() {
		return decks;
	}

	/**
	 * @return the tokenPool
	 */
	public ArrayList<CityToken> getTokenPool() {
		return tokenPool;
	}




	/**
	 * @param decks the decks to set
	 */
	public void setDecks(Map<Integer, ArrayList<PermissionCard>> decks) {
		this.decks = decks;
	}
	
	



	/**
	 * 
	 * @return the number of required emporium that must be built in a single city
	 */
	public int getNumberOfRequiredEmporiums() {
		return numberOfRequiredEmporiums;
	}



	/**
	 * 
	 * @param numberOfRequiredEmporiums set the number of required emporium that must be built in a single city
	 */
	public void setNumberOfRequiredEmporiums(int numberOfRequiredEmporiums) {
		this.numberOfRequiredEmporiums = numberOfRequiredEmporiums;
	}




	/**
	 * @return the permitsImagesPaths
	 */
	public ArrayList<String> getPermitsImagesPaths() {
		return permitsImagesPaths;
	}
	

	
}
