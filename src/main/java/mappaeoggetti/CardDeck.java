package mappaeoggetti;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import gamemanagement.Player;

/**
 * a class for the card deck
 *
 */
public class CardDeck {
	
	private ArrayList<Color> colorPool=new ArrayList<Color>();
	
	public CardDeck(ArrayList<Color> colors){
		
		this.colorPool=colors;
	}
	
	/**
	 * draws a card
	 * @param player is the current player
	 */
	public Card drawCard(Player player){ 
		
		Random random=new Random();
		
		int num=random.nextInt(colorPool.size());
		
		Card card=new Card(colorPool.get(num));
		
		player.getCards().add(card);
		return card;
					
	}
	
	
}
