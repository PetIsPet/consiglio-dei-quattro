package mappaeoggetti;

import java.awt.Color;
import market.Saleable;

public class Card implements Saleable{
	
	private Color color;
	
	/**
	 * class for cards
	 * @param color is the color of the card
	 */
	public Card(Color color){

		this.setColor(color);
		
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public boolean isAssistant() {
		
		return false;
	}

	@Override
	public boolean isPermissionCard() {
		
		return false;
	}

	@Override
	public boolean isCard() {
		
		return true;
	}
	
	
    

}
