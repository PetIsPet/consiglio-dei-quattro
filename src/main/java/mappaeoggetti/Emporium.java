package mappaeoggetti;

import gamemanagement.Player;

/**
 * a class for emporiums
 *
 */
public class Emporium {
	
	
	 private Player player;
	 
	 /**
	  * 
	  * @param player is the emporium's player
	  */
	 public Emporium( Player player){
		 
		 
		 this.setPlayer(player);
	 }

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	 

}
