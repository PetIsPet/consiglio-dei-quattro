package mappaeoggetti;

public class King {
	
	
	private City currentCity;
	
	/**
	 * @param currentCity the city where the king is
	 */
	public King(City currentCity){
		
		this.currentCity=currentCity;
	}

	/**
	 * @return the currentCity
	 */
	public City getCurrentCity() {
		return currentCity;
	}

	/**
	 * @param currentCity the currentCity to set
	 */
	public void setCurrentCity(City currentCity) {
		this.currentCity = currentCity;
	}
}
