package mappaeoggetti;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PermissionDeck {
	
	private Type type;
	private PermissionCard[] discoveredCards;
	private List<PermissionCard> cards=new ArrayList<>();
	private List<PermissionCard> discardedCards=new ArrayList<>();
	private int maxBonus=3;
	
	
	
	
	

	public PermissionDeck(Type type, List<PermissionCard> cards){
		
		this.discoveredCards=new PermissionCard[2];
		this.type=type;
		Collections.shuffle(cards);
		this.cards=cards;
		this.discover();
	}



/**
 * a class that discovers two new permits
 */
	public void discover(){   
		try{
			for(int i=0; i<2; i++){
				this.discoveredCards[i]=this.cards.get(0);
				this.discardedCards.add(this.cards.remove(0));
			}
		}catch(IndexOutOfBoundsException e){
			this.cards.clear();
			this.cards.addAll(discardedCards);
			this.discardedCards.clear();
			discover();
		}
		
	}
	
	

	
	public PermissionCard replaceCard(){
		
		PermissionCard card=this.cards.get(0);
		this.cards.remove(0);
		return card;
		
		
	}



	public PermissionCard[] getDiscoveredCards() {
		return discoveredCards;
	}



	public void setDiscoveredCards(PermissionCard[] discoveredCards) {
		this.discoveredCards = discoveredCards;
	}
	
	/**
	 * @return the cards
	 */
	public List<PermissionCard> getCards() {
		return cards;
	}

	/**
	 * @param cards the cards to set
	 */
	public void setCards(List<PermissionCard> cards) {
		this.cards = cards;
	}
	
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
