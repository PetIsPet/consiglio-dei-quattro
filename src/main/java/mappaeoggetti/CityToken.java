package mappaeoggetti;

import java.util.ArrayList;

import bonuses.Bonus;

public class CityToken {
	
	private ArrayList<Bonus> bonuses=new ArrayList<>();
	private String path;
	
	
	public CityToken(String s){
		
		this.path=s;
	}

	public ArrayList<Bonus> getBonuses() {
		return bonuses;
	}

	public void setBonuses(ArrayList<Bonus> bonuses) {
		this.bonuses = bonuses;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
