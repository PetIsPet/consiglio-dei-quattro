package mappaeoggetti;

import javax.swing.text.html.HTMLDocument.Iterator;

import mvc.Game;


public class Balcony {
	
	private Game game;
	private Councelor [] councelors;
	Type type;
	
	/**
	 * class for the balconies
	 * @param game is the current game
	 * @param type is the current type
	 */
	public Balcony(Game game, Type type){
		
		this.councelors=new Councelor[4];
		this.game=game;
		for(int i=0; i<4; i++){
			
			this.councelors[i]=this.game.getCouncelorsPool().get(0);
			this.game.getCouncelorsPool().remove(0);
		}
		
		this.type=type;
		
	}
	 

	public Balcony(Game game) { //balcone del re
		
		this.councelors=new Councelor[4];
		this.game=game;
		for(int i=0; i<4; i++){
			
			this.councelors[i]=this.game.getCouncelorsPool().get(0);
			this.game.getCouncelorsPool().remove(0);
			
		}
	}
	
	/**
	 * puts a councelor on a chosen balcony
	 * @param councelor is the councelor that player wants to put
	 * on the balcony
	 */
	public void append (Councelor councelor){ 
		getCouncelors()[0]=getCouncelors()[1];
		getCouncelors()[1]=getCouncelors()[2];
		getCouncelors()[2]=getCouncelors()[3];
		getCouncelors()[3]=councelor;
		
		this.game.getCouncelorsPool().remove(councelor);
		
		
	
	}

	public Councelor [] getCouncelors() {
		return councelors;
	}

	public void setCouncelors(Councelor [] councelors) {
		this.councelors = councelors;
	}

	/**
	 * @return the type of the balcony
	 */
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}

	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	

}
