package mappaeoggetti;

import market.Saleable;

public class Assistant implements Saleable{
/**
 * class for the assistants
 */
	public Assistant(){
		
		
		
	}

	@Override
	public boolean isAssistant() {
		
		return true;
	}
	
	@Override
	public boolean isPermissionCard() {
		
		return false;
	}
	
	@Override
	public boolean isCard() {
		
		return false;
	}
}
