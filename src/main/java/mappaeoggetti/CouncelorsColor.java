package mappaeoggetti;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CouncelorsColor {
	
	ArrayList<Color> colorPool=new ArrayList<>();
	private ArrayList<Councelor> councelorsPool=new ArrayList<>();
	private int colorsNumber;
	
	public CouncelorsColor(ArrayList<Color> colors , int colorsNumber){
		
		this.colorPool=colors;
		this.colorsNumber=colorsNumber;
	
	}
	


	public List<Councelor> createCouncelors(){    //crea pool di consiglieri max per colore
	 
		for(Color c: colorPool){
		
		for(int i=0; i<colorsNumber; i++){
			
			Councelor councelor=new Councelor(c);
			this.councelorsPool.add(councelor);
			
		}
		
	 }
	
	 Collections.shuffle(getCouncelorsPool());
	 return getCouncelorsPool();
	
	}
	
	
	
	public ArrayList<Color> getColorPool() {
		return colorPool;
	}

	/**
	 * @param colorPool the colorPool to set
	 */
	public void setColorPool(ArrayList<Color> colorPool) {
		this.colorPool = colorPool;
	}



	public ArrayList<Councelor> getCouncelorsPool() {
		return councelorsPool;
	}



	public void setCouncelorsPool(ArrayList<Councelor> councelorsPool) {
		this.councelorsPool = councelorsPool;
	}
	
	
	
	public int getColorsNumber() {
		return colorsNumber;
	}



	public void setColorsNumber(int colorsNumber) {
		this.colorsNumber = colorsNumber;
	}


}
