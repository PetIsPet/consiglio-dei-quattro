package mappaeoggetti;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class City {

	public static int playersNumber=4;
	private String name;
	private CityToken cityToken;
	private Type type;
	private List<Emporium> emporiums=new ArrayList<Emporium>();
	private List<City> neighborhood=new ArrayList<City>(); //città vicine
	private boolean controlled;
	private Color color;
	
	
	/**
	 * 
	 * @param nieghborhood is the list of neighbour of the city 
	 * @param cityToken is an object that have a list of bonus and the path to set them from a file
	 * @param name is the city name
	 */

	public City(List<City> neighborhood, CityToken cityToken, String name ){        //costruttore
		
		this.setNeighborhood(neighborhood);
		this.setCityToken(cityToken);
		this.setName(name);
		this.emporiums=new ArrayList<>();
		this.controlled=false;
	}
	
	/**
	 * 
	 * @param nieghborhood is the list of neighbour of the city 
	 * @param cityToken is an object that have a list of bonus and the path to set them from a file
	 * @param name is the city name
	 * @param playersNumber is the number of player used for setting the number of Emporium that could be built in the city
	 */
	  public City(List<City> neighborhood, CityToken cityToken, String name, int playersNumber ){        //costruttore
			
			this.setNeighborhood(neighborhood);
			this.setCityToken(cityToken);
			this.setName(name);
			this.emporiums=new ArrayList<>();
		}
    

/**
 * @return the emporiums
 */
public List<Emporium> getEmporiums() {
	return emporiums;
}

/**
 * @param emporiums the emporiums to set
 */
public void setEmporiums(List<Emporium> emporiums) {
	this.emporiums = emporiums;
}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public List<City> getNeighborhood() {
		return neighborhood;
	}



	public void setNeighborhood(List<City> neighborhood2) {
		this.neighborhood = neighborhood2;
	}

	public CityToken getCityToken() {
		return cityToken;
	}

	public void setCityToken(CityToken cityToken) {
		this.cityToken = cityToken;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the controlled
	 */
	public boolean isControlled() {
		return controlled;
	}

	/**
	 * @param controlled the controlled to set
	 */
	public void setControlled(boolean controlled) {
		this.controlled = controlled;
	}


	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
		
	}
