package mappaeoggetti;

import java.awt.Color;

/**
 * a class for the councelors
 *
 */
public class Councelor {
	
	 private Color color;
	 
	 /**
	  * 
	  * @param color councelor's color
	  */
	 public Councelor(Color color){
		 
		 this.setColor(color);
	 }

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	 

}
