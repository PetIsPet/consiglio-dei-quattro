package mappaeoggetti;

import java.util.ArrayList;
import java.util.List;

import bonuses.Bonus;
import market.Saleable;

/**
 * 
 * a class for permissioncards
 */
public class PermissionCard implements Saleable{
	
	private ArrayList<City> buildPermissions=new ArrayList<>();  //permessi di costruzione della carta
	private int id;
	private ArrayList<Bonus> bonus=new ArrayList<>();
	private static int counter;
	private String imgPath;
	
	/**
	 * 
	 * @param bonus is the bonus associated to the card
	 * @param type is the type of the card
	 * @param buildPermissions the cities that can be build
	 */
	public PermissionCard (ArrayList<Bonus> bonus, Type type, ArrayList<City> buildPermissions){
		
		this.setBuildPermissions(buildPermissions);
		this.setBonus(bonus);
		this.setId(counter);
		counter++;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public List<City> getBuildPermissions() {
		return buildPermissions;
	}


	public void setBuildPermissions(ArrayList<City> buildPermissions2) {
		this.buildPermissions = buildPermissions2;
	}


	@Override
	public boolean isAssistant() {
		
		return false;
	}


	@Override
	public boolean isPermissionCard() {
		
		return true;
	}


	@Override
	public boolean isCard() {
		
		return false;
	}


	public ArrayList<Bonus> getBonus() {
		return bonus;
	}


	public void setBonus(ArrayList<Bonus> bonus) {
		this.bonus = bonus;
	}


	public String getImgPath() {
		return imgPath;
	}


	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

}
