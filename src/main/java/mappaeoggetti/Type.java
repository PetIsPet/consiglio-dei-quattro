package mappaeoggetti;

public enum Type {
	
	MOUNTAIN, PLAIN, SEASIDE;
	
	@Override
	public String toString(){
		
		if (this==Type.MOUNTAIN) 
			return "MOUNTAIN";
		
		if (this==Type.PLAIN) 
			return "PLAIN";
		
		if (this==Type.SEASIDE) 
			return "SEASIDE";
		
		else return "";
	}
	
   

}
