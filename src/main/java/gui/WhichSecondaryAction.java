package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * class that allows the player to choose a secondary action
 *
 */
public class WhichSecondaryAction extends JFrame implements Runnable{
	
	String string;
	GUI gui;

	public static final long serialVersionUID = 1L;
	
	public WhichSecondaryAction(GUI gui){
		/**
		 * looks new look&feel
		 */
	
		this.gui=gui;
		/**
		 * create panels, buttons and listeners
		 */
		
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton("Use an Assistant to replace permission cards from a Deck");
		JButton jb2 = new JButton("Use three Assistants to do another primary action");
		JButton jb3 = new JButton("Spend 3 Coinst to buy an Assistant");
		JButton jb4 = new JButton("Use an Assistant to add a councelor to a balcony");
		JButton jb5 = new JButton("Don't do a secondary Action");
		
		jb1.addActionListener(new ActionListener() {
		

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichSecondaryAction.this.dispose();
				string="ChangePermissionCard";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichSecondaryAction.this.dispose();
				string="DoAnotherPrimaryAction";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichSecondaryAction.this.dispose();
				string="EmployAssistant";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichSecondaryAction.this.dispose();
				string="SendAssistantToCouncil";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichSecondaryAction.this.dispose();
				string="DoNothing";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new GridLayout(5,1));
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("Select a secondary action"));
		
		buttonsPanel.add(jb1);
		buttonsPanel.add(jb2);
		buttonsPanel.add(jb3);
		buttonsPanel.add(jb4);
		buttonsPanel.add(jb5);
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
	
		public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	

}