package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *ask a city from a permission card
 */
public class AskCityPermit extends JFrame implements Runnable{
    String string;
    GUI gui;
    ArrayList<String> citiesExtracted = new ArrayList<>();
    public static final long serialVersionUID = 1L;
/**
 * First extract the names of the cities from the strings.
 * Then creates an actionListener that sets the name of the chosen city on gui, closes the frame and wakeUps the gui.
 * Then creates a JButton for every city, and binds the ActionListener. 
 * Then creates the Frame, adding the proper panels and setting the layouts.  
 * @param gui the gui
 * @param cities the cities from the publisher
 */
    public AskCityPermit(GUI gui, ArrayList<String> cities){
        this.gui=gui;
        JPanel textPanel = new JPanel();
        JPanel buttonsPanel = new JPanel();
        for (String s:cities){
        	String[]temp=s.split("#");
        	citiesExtracted.add(temp[0]);
        }
        	
        ActionListener btnaction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                AskCityPermit.this.dispose();
            	gui.setString(((JButton) ae.getSource()).getText());
                gui.wakeUp();
            }
        };
        for (String c:citiesExtracted){
            JButton button =new JButton(c);
            buttonsPanel.add(button);
            button.addActionListener(btnaction);
        }


        this.setLayout(new BorderLayout());
        textPanel.setLayout(new FlowLayout());
        buttonsPanel.setLayout(new FlowLayout());

        textPanel.add(new JLabel("Choose a city"));

        this.add(textPanel,BorderLayout.NORTH);
        this.add(buttonsPanel,BorderLayout.SOUTH);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setVisible(true);
    }

   
    @Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}

}