package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 * class that allows the player to choose which politic card he wants to use
 *
 */
public class UsePoliticCard extends JFrame implements Runnable{
	
	private String string;
	private int integer;
	public static final long serialVersionUID = 1L;
	
	public UsePoliticCard(GUI gui, ArrayList<String> tempMatches){
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		 ActionListener btnaction = new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent ae) {
	                UsePoliticCard.this.dispose();
	            	gui.setInteger(((JButtonWithInteger) ae.getSource()).getInteger());
	                gui.wakeUp();
	            }
	        };
	        
		for (String card:tempMatches){
			BufferedImage img = null;
			try {
				try {
					img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/politic_cards/"+card+"p.png").toURI()));
				} catch (IOException e) {
				System.out.println("Unable to find file");
				}
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon icon = new ImageIcon(img);
			Image image = icon.getImage(); 
			Image newimg = image.getScaledInstance(39, 60,  java.awt.Image.SCALE_SMOOTH); 
			icon = new ImageIcon(newimg); 
            JButtonWithInteger button =new JButtonWithInteger(icon);
            button.setInteger(tempMatches.indexOf(card));
            buttonsPanel.add(button);
            button.addActionListener(btnaction);
        }
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new FlowLayout());
		
		/**
		 * set panels
		 */

		textPanel.add(new JLabel("These are the matching cards, choose one by clicking on it"));
	
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}



	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	
}
