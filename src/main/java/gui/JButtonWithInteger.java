package gui;
import javax.swing.ImageIcon;
import javax.swing.JButton;
public class JButtonWithInteger extends JButton{

	int integer;
	public JButtonWithInteger(ImageIcon icon){
		super(icon);
	}
	public JButtonWithInteger(String string){
		super(string);
	}
	
	public int getInteger() {
		return integer;
	}
	public void setInteger(int integer) {
		this.integer = integer;
	}
	
	
}
