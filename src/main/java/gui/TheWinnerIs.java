package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * class that shows the winner
 */
public class TheWinnerIs extends JFrame implements Runnable{
	
	private static final long serialVersionUID = 1L;

	public TheWinnerIs(GUI gui, int id){
		
		/**
		 * set frame 
		 */
		this.getContentPane().add(new JLabel ("TheWinnerIs: "+id));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
		
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	

}
