package gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;


/**
 * class that shows the showcase
 *
 */
public class ProductToPurchase extends JFrame implements Runnable{
	private static final long serialVersionUID = 1L;
	
	public ProductToPurchase(GUI gui, int id, ArrayList<String> strings){
		/**
	 *creates the panels 
	 */
	JPanel textPanel = new JPanel();
	JPanel buttonsPanel = new JPanel();	
	buttonsPanel.setLayout(new GridLayout(strings.size(),3));
	
	 ActionListener btnaction = new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent ae) {
             ProductToPurchase.this.dispose();
         	gui.setInteger(((JButtonWithInteger) ae.getSource()).getInteger());
             gui.wakeUp();
         }
     };
	
     for(String s:strings){
    	 String[] temp=this.stringToArray(s);
    	 if("PERMISSIONCARD".equals(temp[1])){
    		 BufferedImage img = null;
 			try {
 				try {
 					img = ImageIO.read(new File(getClass().getClassLoader().getResource(temp[4]).toURI()));
 				} catch (IOException e) {
 				System.out.println("Unable to find file");
 				}
 			} catch (URISyntaxException e) {
 				System.out.println("Unable to find file");
 			}
 			ImageIcon icon = new ImageIcon(img);
 			Image image = icon.getImage(); 
 			Image newimg = image.getScaledInstance(70, 70,  java.awt.Image.SCALE_SMOOTH); 
 			icon = new ImageIcon(newimg); 
             JButtonWithInteger button =new JButtonWithInteger(icon);
             button.setInteger(Integer.parseInt(temp[3]));
             buttonsPanel.add(button);
             button.addActionListener(btnaction);
             buttonsPanel.add(new JLabel("Price: "+temp[2]));
             buttonsPanel.add(new JLabel("Owner: "+temp[0]));
    	 }
    	 if("POLITICCARD".equals(temp[1])){
    		 BufferedImage img = null;
 			try {
 				try {
 					img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/politic_cards/"+temp[4]+"p.png").toURI()));
 				} catch (IOException e) {
 				System.out.println("Unable to find file");
 				}
 			} catch (URISyntaxException e) {
 				System.out.println("Unable to find file");
 			}
 			ImageIcon icon = new ImageIcon(img);
 			Image image = icon.getImage(); 
 			Image newimg = image.getScaledInstance(45, 70,  java.awt.Image.SCALE_SMOOTH); 
 			icon = new ImageIcon(newimg); 
             JButtonWithInteger button =new JButtonWithInteger(icon);
             button.setInteger(Integer.parseInt(temp[3]));
             buttonsPanel.add(button);
             button.addActionListener(btnaction);
             buttonsPanel.add(new JLabel("Price: "+temp[2]));
             buttonsPanel.add(new JLabel("Owner: "+temp[0]));
    	 }
     	 if("ASSISTANT".equals(temp[1])){
    		 BufferedImage img = null;
 			try {
 				try {
 					img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/assistant.png").toURI()));
 				} catch (IOException e) {
 				System.out.println("Unable to find file");
 				}
 			} catch (URISyntaxException e) {
 				System.out.println("Unable to find file");
 			}
 			ImageIcon icon = new ImageIcon(img);
 			Image image = icon.getImage(); 
 			Image newimg = image.getScaledInstance(70, 70,  java.awt.Image.SCALE_SMOOTH); 
 			icon = new ImageIcon(newimg); 
             JButtonWithInteger button =new JButtonWithInteger(icon);
             button.setInteger(Integer.parseInt(temp[3]));
             buttonsPanel.add(button);
             button.addActionListener(btnaction);
             buttonsPanel.add(new JLabel("Price: "+temp[2]));
             buttonsPanel.add(new JLabel("Owner: "+temp[0]));
    	 }
     }


	
	/**
	 * sets the panels
	 */
	textPanel.add(new JLabel ("This is the market:"));
	
	/**
	 * add table to frame
	 */
	this.setLayout(new BorderLayout());
	this.add(textPanel, BorderLayout.NORTH);
	this.add(buttonsPanel, BorderLayout.SOUTH);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.pack();
	this.setLocationRelativeTo(null);
	this.setAlwaysOnTop(true);
	this.setResizable(false);
	this.setVisible(true);
	}
	
	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	
	public String[] stringToArray(String string)
	{
		String[] strings = new String[5];
		String[] strings2 = string.split(" ");
		for(String s:strings2){
		strings[0]=strings2[1];//PLAYERn
		strings[1]=strings2[3];//ASSISTANT/PERMISSIONCARD/POLITICCARD
		strings[2]=strings2[5];//prezzo
		strings[3]=strings2[7];//id (quello che ritorno)
		if(strings2.length==10){
			if("PERMISSIONCARD".equals(strings2[3])){
				String[] temp=strings2[9].split("#");
				strings[4]=temp[1];
			}
			else{
				strings[4]=strings2[9];
			}
		}
		else strings[4]=" ";//assistente
		
		
		
	}
	return strings;
	}
}
	
