package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * class that allows the player to choose a balcony (used when a player wants to add a councillor to a balcony)
 *
 */
public class ChooseBalconyPlusKing extends JFrame implements Runnable{
	
	int integer;
	GUI gui;

	public static final long serialVersionUID = 1L;
	
	public ChooseBalconyPlusKing(GUI gui){

		this.gui=gui;
		
		/**
		 * create panels, buttons and listeners
		 */
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton("Seaside Balcony");
		JButton jb2 = new JButton("Plains Balcony");
		JButton jb3 = new JButton("Mountains Balcony");
		JButton jb4 = new JButton("King Balcony");
		jb1.addActionListener(new ActionListener() {
		

			@Override
			public void actionPerformed(ActionEvent e) {
				ChooseBalconyPlusKing.this.dispose();
				integer=0;
				gui.setInteger(integer);
				gui.wakeUp();
			}
			
		});
		jb2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ChooseBalconyPlusKing.this.dispose();
				integer=1;
				gui.setInteger(integer);
				gui.wakeUp();
			}
			
		});
		jb3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ChooseBalconyPlusKing.this.dispose();
				integer=2;
				gui.setInteger(integer);
				gui.wakeUp();
			}
			
		});
		jb4.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ChooseBalconyPlusKing.this.dispose();
				integer=3;
				gui.setInteger(integer);
				gui.wakeUp();
			}
			
		});
		
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new GridLayout(1,4));
		
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("Choose a balcony"));
		
		buttonsPanel.add(jb1);
		buttonsPanel.add(jb2);
		buttonsPanel.add(jb3);
		buttonsPanel.add(jb4);
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
	


	public int getInteger() {
		return integer;
	}



	public void setInteger(int integer) {
		this.integer = integer;
	}



	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	

}