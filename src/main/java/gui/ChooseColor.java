package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * class that asks to choose a color
 *
 */
public class ChooseColor extends JFrame implements Runnable{
	String string;
	int integer;
	GUI gui;
	
	private static final long serialVersionUID = 1L;
	
	public ChooseColor(GUI gui, ArrayList<String> colors){
	this.gui=gui;

	JPanel textPanel = new JPanel();
	JPanel buttonsPanel = new JPanel();
	
	  ActionListener btnaction = new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent ae) {
              ChooseColor.this.dispose();
          	gui.setString(((JButton) ae.getSource()).getText());
              gui.wakeUp();
          }
      };
      
      for (String c:colors){
          JButton button =new JButton(c);
          buttonsPanel.add(button);
          button.addActionListener(btnaction);
      }
      textPanel.add(new JLabel("Choose a color"));
	/**
	 * add table to frame
	 */
	this.setLayout(new BorderLayout());
	this.add(textPanel,BorderLayout.NORTH);
	this.add(buttonsPanel,BorderLayout.SOUTH);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.pack();
	this.setLocationRelativeTo(null);
	this.setAlwaysOnTop(true);
	this.setResizable(false);
	this.setVisible(true);
	}
	
	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
}