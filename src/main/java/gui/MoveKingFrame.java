package gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import exceptions.InvalidInputException;
import mappaeoggetti.City;
/**
 * class that shows a frame where the player choose where to move the king
 */
public class MoveKingFrame extends JFrame implements Runnable{
	String string;
	private static final long serialVersionUID = 1L;

	public MoveKingFrame(GUI gui, String currentKingCity,  ArrayList<City> townsKing, ArrayList<String>cityNamesKing,int priceKing) {
			/**
		 * create the panels
		 */
		JPanel textPanel1 = new JPanel();
		JPanel textPanel2 = new JPanel();
		JPanel buttonsPanel = new JPanel();
		
		   ActionListener btnaction = new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent ae) {
	                MoveKingFrame.this.dispose();
	            	gui.setString(((JButton) ae.getSource()).getText());
	                gui.wakeUp();
	            }
	        };
		
		for(City c:townsKing){
			if(currentKingCity.equals(c.getName())){
				for(City t:c.getNeighborhood()){
					JButton button = new JButton(t.getName());
					button.addActionListener(btnaction);
					buttonsPanel.add(button);
					cityNamesKing.add(t.getName());
				}
			}
		}
		JButton button = new JButton("exit");
		button.addActionListener(btnaction);
		buttonsPanel.add(button);
	
		
	
		
		textPanel1.add(new JLabel ("The king is in: "+currentKingCity));
		textPanel2.add(new JLabel ("You can move the king in the following cities:"));
	
		/**
		 * sets the frame
		 */
		this.setLayout(new BorderLayout());

		this.add(textPanel1,BorderLayout.NORTH);
		this.add(textPanel2,BorderLayout.CENTER);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
@Override
public void run() {
	// TODO Auto-generated method stub
	
}
}