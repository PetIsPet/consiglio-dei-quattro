package gui;

public class GUIHandler implements Runnable {

	GUI gui;
	
	public GUIHandler(GUI gui){
		this.gui=gui;
	}
	
	@Override
	public void run() {
		
	try {
		gui.goToSleep();
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
		
	}

}
