package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * class that shows a message to the player
 *
 */
public class ShowMessage extends JFrame implements Runnable{
	
	GUI gui;
	String string;
	public static final long serialVersionUID = 1L;
	
	public ShowMessage (GUI gui, String string){
		
		this.gui=gui;
		this.string=string;
		
		/**
		 * create panels, buttons and listeners
		 */
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton ("OK");
		jb1.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ShowMessage.this.dispose();
				gui.wakeUp();
			}
			
		});
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new FlowLayout());
		
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel(string));
		buttonsPanel.add(jb1);
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	 
}