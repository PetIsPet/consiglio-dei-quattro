package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * class that allows the player to choose the politicCard he wants to sell
 *
 */
public class PoliticCardToSell extends JFrame implements Runnable{
	
	String string;
	GUI gui;
	int integer;
	public static final long serialVersionUID = 1L;
	
	public PoliticCardToSell(GUI gui){
		
		this.gui=gui;
		
		/**
		 * create panels, the textField and the listener
		 */
		JPanel textPanel = new JPanel();
		JPanel typePanel = new JPanel();
		JTextField jTextField = new JTextField(2);
		jTextField.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						string = jTextField.getText();
						PoliticCardToSell.this.dispose();
						integer = Integer.parseInt(string);
						gui.setInteger(integer);
						gui.wakeUp();
					}
		});
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layout
		 */
		textPanel.setLayout(new FlowLayout());
		typePanel.setLayout(new FlowLayout());
		
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("Insert the id of the chosen card"));
		typePanel.add(jTextField);
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(typePanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}

	
	
	public int getInteger() {
		return integer;
	}



	public void setInteger(int integer) {
		this.integer = integer;
	}



	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	
}

