package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 * class that allows the player to choose between using another card or not
 *
 */
public class ChooseAnotherMatch extends JFrame implements Runnable{
	
	private String string;
	private int integer;
	public static final long serialVersionUID = 1L;
	
	public ChooseAnotherMatch(GUI gui){
		
	
		
		/**
		 * create panels, buttons and listeners
		 */
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton ("Choose another match");
		JButton jb2 = new JButton ("Don't choose another match");
		jb1.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ChooseAnotherMatch.this.dispose();
				gui.setInteger(1);
				gui.wakeUp();
			}
			
		});
		
		jb2.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ChooseAnotherMatch.this.dispose();
				gui.setInteger(2);
				gui.wakeUp();
			}
			
		});
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new GridLayout(2,1));
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("Do you want to choose another match?"));
		buttonsPanel.add(jb1);
		buttonsPanel.add(jb2);
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}



	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	
}