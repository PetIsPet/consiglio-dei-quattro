package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * class that allows the player to choose between primary or secondary action
 *
 */
public class WhichAction extends JFrame implements Runnable{
	
	Boolean flag;
	

	GUI gui;
	int id;
	public static final long serialVersionUID = 1L;
	
	public WhichAction(GUI gui, int id){
		
		this.gui=gui;
		this.id=id;
		
		/**
		 * create panels, buttons and listeners
		 */
		JPanel textPanel = new JPanel();
		JPanel textPanel2 = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton ("Primary");
		JButton jb2 = new JButton ("Secondary");
		jb1.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichAction.this.dispose();
				flag=true;
				gui.setFlag(flag);
				gui.wakeUp();
			}
			
		});
		
		jb2.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichAction.this.dispose();
				flag=false;
				gui.setFlag(flag);
				gui.wakeUp();
			}
			
		});
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		textPanel2.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new BorderLayout());
		
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("It's your turn Player "+id));
		textPanel2.add(new JLabel("Which kind of action do you wanna perform first?"));
		buttonsPanel.add(jb1, BorderLayout.WEST);
		buttonsPanel.add(jb2, BorderLayout.EAST);
	
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(textPanel2,BorderLayout.CENTER);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	
	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	
}
