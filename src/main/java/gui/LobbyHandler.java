package gui;

import servercompleto.ClientInHandler;
import servercompleto.ClientOutHandler;
import servercompleto.SubscriberRMI;

public class LobbyHandler implements Runnable {
	
	private ClientOutHandler socket;
	private SubscriberRMI rmi;
	private boolean isRMI=false;
	public boolean isSocket=false;

	
	public LobbyHandler(ClientOutHandler socket){
		
		this.socket=socket;
		this.isSocket=true;
	}
	
	public LobbyHandler(SubscriberRMI rmi){
		
		this.rmi=rmi;
		this.isRMI=true;
	}
	
	
	@Override
	public void run() {
		if(isSocket){
			try {
				socket.goToSleep();
			} catch (InterruptedException e) {
				e.printStackTrace();
			};
		}
		if(isRMI){
			try {
				this.rmi.goToSleep();
			} catch (InterruptedException e) {
				e.printStackTrace();
			};

	}
	}
}
