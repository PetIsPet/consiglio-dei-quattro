package gui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import exceptions.InvalidInputException;
import exceptions.VoidMarketException;
import mappaeoggetti.City;
import mvc.View;

public class GUI extends View implements Serializable{

	private String string;
	private int integer;
	private boolean flag;
	private ArrayList<String> strings = new ArrayList<>();

	private static final long serialVersionUID = 1L;
	/**
	 * notify all threads
	 */	
	public void wakeUp(){
		synchronized (this){
			notifyAll();
				}
			}
	
	public void goToSleep() throws InterruptedException{
		synchronized (this){
			wait();
		}
	}

	@Override
	public String askPrimaryAction() {
		WhichPrimaryAction a = new WhichPrimaryAction(this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.string;
	}

	@Override
	public String askSecondaryAction() {
		WhichSecondaryAction a = new WhichSecondaryAction(this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.string;
	}

	@Override
	public boolean primaryActionFirst(int id) {
		WhichAction a = new WhichAction(this,id);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.flag;
	}

	@Override
	public int chooseDeck() {
		ChooseDeck a = new ChooseDeck (this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}

	@Override
	public int askPrice() {
		SetPrice a = new SetPrice (this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}

	@Override
	public void printStats(ArrayList<String> stats) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<String> productToSell(int id, int assistants, ArrayList<String> permits, ArrayList<String> cards) {
		ProductToSell a = new ProductToSell (this, id, assistants, permits, cards);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		if (this.strings.get(0)=="PERMISSION CARD"){
			PermissionCardToSell b = new PermissionCardToSell(this);
			GUIHandler guiHandler2 = new GUIHandler(this);
			Thread v = new Thread(b);
			Thread w = new Thread(guiHandler2);
			v.start();
			w.start();
			try {
				w.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			strings.add(permits.get(integer));
			return this.strings;
		}
		
		if (this.strings.get(0)=="POLITIC CARD"){
			PoliticCardToSell c = new PoliticCardToSell(this);
			GUIHandler guiHandler3 = new GUIHandler(this);
			Thread x = new Thread(c);
			Thread y = new Thread(guiHandler3);
			x.start();
			y.start();
			try {
				y.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			strings.add(cards.get(integer));
			return this.strings;
		}
		
		return this.strings;
	}

	@Override
	public ArrayList<String> usePoliticCard(ArrayList<String> matches) {
		ArrayList<String> tempMatches=new ArrayList<>();
		tempMatches.addAll(matches);
			int counter=0;
    		List<Integer> indexSet=new ArrayList<>();
    		for(String s:matches){
    			indexSet.add(matches.indexOf(s));
    		}
			ArrayList<String> matchesToUse=new ArrayList<>();
			int index=0;
		
			while(counter<4){
				index=chooseMatch(this,tempMatches);

				matchesToUse.add(tempMatches.get(index));
	    		tempMatches.remove(index);
	    		if(matchesToUse.size()==matches.size()) break;
	    			index=chooseAnotherMatch(this);
	    			if(index==1){
	    				counter++;
	    			}
	    			if(index==2){
	    				break;
	    			}
	    				
			}
			
			
		return matchesToUse;
	}
	
	public int chooseMatch(GUI gui, ArrayList<String> tempMatches){
		UsePoliticCard a = new UsePoliticCard (this,tempMatches);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return integer;
	}
	
	public int chooseAnotherMatch(GUI gui){
		ChooseAnotherMatch a = new ChooseAnotherMatch (this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return integer;
	}
	
	@Override
	public String chooseColor(ArrayList<String> colors) {
		ChooseColor a = new ChooseColor (this,colors);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.string;
	}

	@Override
	public int chooseBalcony() {
		ChooseBalcony a = new ChooseBalcony (this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}

	@Override
	public int choosePermissionCard(ArrayList<String> discoveredCards) {
		ChoosePermissionCard a = new ChoosePermissionCard (this,discoveredCards);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}
		

	@Override
	public int askYourPermissionCard(ArrayList<String> cards) {
		AskYourPermissionCard a = new AskYourPermissionCard (this,cards);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}

	@Override
	public String askCityPermit(ArrayList<String> cities) {
		AskCityPermit a = new AskCityPermit (this,cities);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return string;
	}

	@Override
	public void theWinnerIs(int id) {
		TheWinnerIs a = new TheWinnerIs (this,id);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public String moveKing(ArrayList<String> cities, String currentCity) {
		ArrayList<String> cityNames=new ArrayList<>();
		ArrayList<City> towns=new ArrayList<>();
		for(String c:cities){
			String[] temp=c.split("#");
			String[] neighbours=temp[1].split("-");
			ArrayList<City> neigh=new ArrayList<>();
			for(String t:neighbours){
				City city=new City(null,null,t);
				neigh.add(city);
			}
			City mycity=new City(neigh,null,temp[0]);
			
			towns.add(mycity);
		}
		
		int price=0;
		while(true){
			string=moveKingFrame(currentCity,towns,cityNames,price);
			if("exit".equals(string)){
				break;
			}
			else
				currentCity=string;
				price=price+2;
		}
		string=currentCity+"#"+Integer.toString(price);
		return string;
	}
	
	public String moveKingFrame(String currentKingCity, ArrayList<City> townsKing, ArrayList<String>cityNamesKing,int priceKing){
		MoveKingFrame a = new MoveKingFrame (this,currentKingCity,townsKing,cityNamesKing,priceKing);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return string;
		
	}

	public String getString() {
		synchronized (this){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return string;
	}
	}
	
	public void setString(String string) {
		this.string = string;
	}

	public boolean getFlag() {
		synchronized (this){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return flag;
	}
	}
	
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public int getInteger() {
		synchronized (this){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return integer;
	}
	}

	public void setInteger(int integer) {
		this.integer = integer;
	}

	public ArrayList<String> getStrings() {
		synchronized (this){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return strings;
	}
	}
	
	public void setStrings(ArrayList<String> strings) {
		this.strings = strings;
	}

	@Override
	public int chooseBalconyPlusKing() throws InvalidInputException {
		ChooseBalconyPlusKing a = new ChooseBalconyPlusKing (this);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}

	@Override
	public int productToPurchase(int id, ArrayList<String> market) throws VoidMarketException {
		if(market.isEmpty()) throw new VoidMarketException();
		ProductToPurchase a = new ProductToPurchase (this,id,market);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.integer;
	}

	@Override
	public String chooseCityNobilityBonus(ArrayList<String> cities) {
		ChooseCity a = new ChooseCity (this,cities);
		GUIHandler guiHandler = new GUIHandler(this);
		Thread t = new Thread(a);
		Thread u = new Thread(guiHandler);
		t.start();
		u.start();
		try {
			u.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this.string;
	}



}
