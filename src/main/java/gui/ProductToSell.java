package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * class that allows the player to choose to sell a product
 *
 */
public class ProductToSell extends JFrame implements Runnable{
	
	private ArrayList<String> strings = new ArrayList<>();
	private GUI gui;
	
	
	public static final long serialVersionUID = 1L;
	
	public ProductToSell(GUI gui, int id, int assistants, ArrayList<String> permits, ArrayList<String> cards){
		
		this.gui=gui;
		
		/**
		 * create panels, buttons and listeners
		 */
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton ("Assistant");
		JButton jb2 = new JButton ("PermissionCard");
		JButton jb3 = new JButton ("Politic Card");
		JButton jb4 = new JButton ("Nothing");
		jb1.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ProductToSell.this.dispose();
				strings.add("ASSISTANT");
				gui.setStrings(strings);
				gui.wakeUp();
			}
			
		});
		
		jb2.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ProductToSell.this.dispose();
				strings.add("PERMISSION CARD");
				gui.setStrings(strings);
				gui.wakeUp();
			}
			
		});
		
		jb3.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ProductToSell.this.dispose();
				strings.add("POLITIC CARD");
				gui.setStrings(strings);
				gui.wakeUp();
			}
			
		});

		jb4.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				ProductToSell.this.dispose();
				strings.add("NIENTE");
				gui.setStrings(strings);
				gui.wakeUp();
			}
			
		});
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new GridLayout(1,4));
		
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("Choose what you want to sell"));
		buttonsPanel.add(jb1);
		buttonsPanel.add(jb2);
		buttonsPanel.add(jb3);
		buttonsPanel.add(jb4);
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	

	public ArrayList<String> getStrings() {
		return strings;
	}



	public void setStrings(ArrayList<String> strings) {
		this.strings = strings;
	}



	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	
}
