package gui;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class GameTable extends JFrame implements Runnable{
	
	JPanel mapPanel = new JPanel();
	JPanel mountainsPermissionDeckPanel = new JPanel();	
	JPanel plainsPermissionDeckPanel = new JPanel();
	JPanel seasidePermissionDeckPanel = new JPanel();
	JPanel seasideBalconyPanel = new JPanel();
	JPanel plainsBalconyPanel = new JPanel();
	JPanel mountainsBalconyPanel = new JPanel();
	JPanel kingBalconyPanel = new JPanel();
	JPanel politicCardPanel = new JPanel();
	JPanel publisherPanel = new JPanel();
	JPanel permissionCards3Panel = new JPanel();
	JPanel permissionCards2Panel = new JPanel();
	JPanel permissionCards1Panel = new JPanel();
	JPanel permissionCards0Panel = new JPanel();
	JTextArea publisherTextArea = new JTextArea();
	JPanel player0SituationPanel = new JPanel();
	JPanel player1SituationPanel = new JPanel();
	JPanel player2SituationPanel = new JPanel();
	JPanel player3SituationPanel = new JPanel();
	JPanel nobilityPanel = new JPanel();
	JPanel bonusPanel= new JPanel();
	private static final long serialVersionUID = 1L;
	/**
	 * creates toolkit
	 */
	Toolkit tk = Toolkit.getDefaultToolkit();
	Dimension dim = tk.getScreenSize();
	
	/**
	 * constructor
	 */
	public GameTable(){
	
		/**
		 * loads new look&feel
		 */
		try {
	        UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
	    } catch (Exception e) { 
	    	System.out.println("LookAndFeel not loaded properly");
	    }
	
	/**
	 * creates panels, scrollPanes, splitPanes
	 */
		JScrollPane nobilityScroll = new JScrollPane(nobilityPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JScrollPane publisherScroll = new JScrollPane(publisherPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    JScrollPane politicCardScroll = new JScrollPane(politicCardPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JSplitPane politicAndPublisherPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, politicCardScroll, publisherScroll);
	    JSplitPane eastPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, politicAndPublisherPane, nobilityScroll);
		JSplitPane player34SituationPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, player2SituationPanel, player3SituationPanel);
		JSplitPane player12SituationPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, player0SituationPanel, player1SituationPanel);
		JSplitPane playersSituationPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, player12SituationPanel, player34SituationPanel);
		JScrollPane permission1Scroll = new JScrollPane(permissionCards0Panel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JScrollPane permission2Scroll = new JScrollPane(permissionCards1Panel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JScrollPane permission3Scroll = new JScrollPane(permissionCards2Panel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JScrollPane permission4Scroll = new JScrollPane(permissionCards3Panel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JSplitPane permissionCards34Pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, permission3Scroll, permission4Scroll);
		JSplitPane permissionCards12Pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, permission1Scroll, permission2Scroll);
		JSplitPane permissionCardsPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, permissionCards12Pane, permissionCards34Pane);
		JSplitPane permitsAndSituationPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, permissionCardsPane,playersSituationPanel );
		JSplitPane balcony34Pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mountainsBalconyPanel, kingBalconyPanel);
		JSplitPane balcony12Pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, seasideBalconyPanel, plainsBalconyPanel);
		JSplitPane balconyPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, balcony12Pane, balcony34Pane);
		JSplitPane seasideAndPlainsDeckPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, seasidePermissionDeckPanel, plainsPermissionDeckPanel);
		JSplitPane mountainsAndKingPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mountainsPermissionDeckPanel, bonusPanel);
		JSplitPane permissionDeckPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, seasideAndPlainsDeckPane, mountainsAndKingPane);
		JSplitPane decksAndBalconyPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, permissionDeckPane, balconyPane);// 
		JSplitPane northPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mapPanel, eastPane);//
	    JSplitPane southPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, decksAndBalconyPane, permitsAndSituationPane);// 
	    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, northPane, southPane);//
	    
	  
    /**
     * sets gameTable layout
     */
    mountainsPermissionDeckPanel.setLayout(new GridLayout(1,3));
    plainsPermissionDeckPanel.setLayout(new GridLayout(1,3));
    seasidePermissionDeckPanel.setLayout(new GridLayout(1,3));
    seasideBalconyPanel.setLayout(new GridLayout(1,4));
    plainsBalconyPanel.setLayout(new GridLayout(1,4));
    mountainsBalconyPanel.setLayout(new GridLayout(1,4));
    kingBalconyPanel.setLayout(new GridLayout(1,4));
    publisherTextArea.setSize(160,95);
    bonusPanel.setLayout(new GridLayout(3,4));
    
    /**
     * size splitPane
     */
    splitPane.setDividerSize(0);
    splitPane.setDividerLocation(300);
    splitPane.setEnabled( false );
    
    /**
     * size southPane
     */
    southPane.setDividerSize(0);
    southPane.setDividerLocation(150);
    southPane.setEnabled( false );
    
    /**
     * size northPane
     */
    northPane.setDividerSize(0);
    northPane.setDividerLocation(600);
    northPane.setEnabled( false );
    
    /**
     * size decksAndBalconyPane
     */
    decksAndBalconyPane.setDividerSize(0);
    decksAndBalconyPane.setDividerLocation(75);
    decksAndBalconyPane.setEnabled( false );
    
    /**
     * size permissionDeckPane
     */
    permissionDeckPane.setDividerSize(0);
    permissionDeckPane.setDividerLocation(400);
    permissionDeckPane.setEnabled( false );
    
    /**
     * size mountainsAndKingPane
     */
    mountainsAndKingPane.setDividerSize(0);
    mountainsAndKingPane.setDividerLocation(200);
    mountainsAndKingPane.setEnabled( false );
    
    /**
     * size seasideAndPlainsDeckPan
     */
    seasideAndPlainsDeckPane.setDividerSize(0);
    seasideAndPlainsDeckPane.setDividerLocation(200);
    seasideAndPlainsDeckPane.setEnabled( false );
    
    /**
     * size balconyPane
     */
    balconyPane.setDividerSize(0);
    balconyPane.setDividerLocation(400);
    balconyPane.setEnabled( false );
    
    /**
     * size balcony12Pane
     */
    balcony12Pane.setDividerSize(0);
    balcony12Pane.setDividerLocation(200);
    balcony12Pane.setEnabled( false );

    /**
     * size balcony34Pane
     */
    balcony34Pane.setDividerSize(0);
    balcony34Pane.setDividerLocation(200);
    balcony34Pane.setEnabled( false );
    
    /**
     * size  permitsAndSituationPane
     */
    permitsAndSituationPane.setDividerSize(0);
    permitsAndSituationPane.setDividerLocation(75);
    permitsAndSituationPane.setEnabled( false );
    
    /**
     * size  permissionCards12Pane
     */
    permissionCards12Pane.setDividerSize(0);
    permissionCards12Pane.setDividerLocation(200);
    permissionCards12Pane.setEnabled( false );
    
    /**
     * size  permissionCards34Pane
     */
    permissionCards34Pane.setDividerSize(0);
    permissionCards34Pane.setDividerLocation(200);
    permissionCards34Pane.setEnabled( false );
    
    /**
     * size  permissionCardsPane
     */
    permissionCardsPane.setDividerSize(0);
    permissionCardsPane.setDividerLocation(400);
    permissionCardsPane.setEnabled( false );
    
    /**
     * size  playersSituationPanel
     */
    playersSituationPanel.setDividerSize(0);
    playersSituationPanel.setDividerLocation(400);
    playersSituationPanel.setEnabled( false );
    
    /**
     * size  player12SituationPanel
     */
    player12SituationPanel.setDividerSize(0);
    player12SituationPanel.setDividerLocation(200);
    player12SituationPanel.setEnabled( false );
    
    /**
     * size  player12SituationPanel
     */
    player34SituationPanel.setDividerSize(0);
    player34SituationPanel.setDividerLocation(200);
    player34SituationPanel.setEnabled( false );
    
    /**
     * size politicAndPublisherPane
     */
    politicAndPublisherPane.setDividerSize(0);
    politicAndPublisherPane.setDividerLocation(100);
    politicAndPublisherPane.setEnabled( false );
    
    /**
     * size eastPane
     */
    eastPane.setDividerSize(0);
    eastPane.setDividerLocation(200);
    eastPane.setEnabled( false );
    
    /**
	 * add tooltips
	 */
	player0SituationPanel.setToolTipText("Situation of Player 0.");
	player1SituationPanel.setToolTipText("Situation of Player 1.");
	player2SituationPanel.setToolTipText("Situation of Player 2.");
	player3SituationPanel.setToolTipText("Situation of Player 3.");
	mapPanel.setToolTipText("This is the map. The bonuses for each city and the emporiums are shown here.");
    politicCardPanel.setToolTipText("These are your Politic Cards.");
    publisherPanel.setToolTipText("Here are reported the action of the palyers.");
    nobilityPanel.setToolTipText("This is the nobility Scale. You can see the bonus given when you reach a number of nobility points.");
    seasidePermissionDeckPanel.setToolTipText("This is the Deck of Permission Cards for seaside area.");
    plainsPermissionDeckPanel.setToolTipText("This is the Deck of Permission Cards for plains area.");
    mountainsPermissionDeckPanel.setToolTipText("This is the Deck of Permission Cards for mountains area.");
    bonusPanel.setToolTipText("These are the bonus given by building emporiums in cities of the same color or of the same area.");
    seasideBalconyPanel.setToolTipText("This is the Balcony of the seaside area.");
    plainsBalconyPanel.setToolTipText("This is the Balcony of the plains area.");
    mountainsBalconyPanel.setToolTipText("This is the Balcony of the mountains area.");
    kingBalconyPanel.setToolTipText("This is the king's Balcony.");
    permissionCards3Panel.setToolTipText("These are the Permission Cards of player 3.");
    permissionCards2Panel.setToolTipText("These are the Permission Cards of player 2.");
    permissionCards1Panel.setToolTipText("These are the Permission Cards of player 1.");
    permissionCards0Panel.setToolTipText("These are the Permission Cards of player 0.");
    
    
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //this.setUndecorated(true);
    this.add(splitPane);
    this.setSize(800,600);
    int xPos = (dim.width/2) - (this.getWidth()/2);
	int yPos = (dim.height/2) - (this.getHeight()/2);
	this.setLocation(xPos,yPos);
	this.setResizable(false);
    this.setVisible(true);
    
}  
	/**
	 * shows the nobilityScale
	 * @throws IOException
	 */
	public void printNobility() throws IOException{
		/**
		 * loads the map
		 */
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/nobility.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon icon = new ImageIcon(img);
		Image image = icon.getImage(); 
		Image newimg = image.getScaledInstance(780, 65,  java.awt.Image.SCALE_SMOOTH); 
		icon = new ImageIcon(newimg); 
		nobilityPanel.add(new JLabel(icon));
		nobilityPanel.revalidate();
		nobilityPanel.repaint();
	}
	/**
	 * shows the map
	 * @param cityTokenPaths the ArrayList that contains the paths of the images
	 * @throws IOException
	 */
	public void printMap (ArrayList<String> cityTokenPaths, String mapConfig, ArrayList<String> emporiums, String kingPosition) throws IOException{ 
	mapPanel.removeAll();
	
	/**
	 * loads the map
	 */
	BufferedImage img = null;
	try {
		img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/map/"+mapConfig+".png").toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	/**
	 * loads the bonus for city A
	 */
	BufferedImage img2 = null;
	try {
		img2 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(0)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city B
	 */
	BufferedImage img3 = null;
	try {
		img3 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(1)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city C
	 */
	BufferedImage img4 = null;
	try {
		img4 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(2)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city D
	 */
	BufferedImage img5 = null;
	try {
		img5 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(3)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city E
	 */
	BufferedImage img6 = null;
	try {
		img6 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(4)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city F
	 */
	BufferedImage img7 = null;
	try {
		img7 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(5)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city G
	 */
	BufferedImage img8 = null;
	try {
		img8 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(6)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city H
	 */
	BufferedImage img9 = null;
	try {
		img9 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(7)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city I
	 */
	BufferedImage img10 = null;
	try {
		img10 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(8)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city K
	 */
	BufferedImage img11 = null;
	try {
		img11 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(10)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city L
	 */
	BufferedImage img12 = null;
	try {
		img12 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(11)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city M
	 */
	BufferedImage img13 = null;
	try {
		img13 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(12)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city N
	 */
	BufferedImage img14 = null;
	try {
		img14 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(13)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads the bonus for city O
	 */
	BufferedImage img15 = null;
	try {
		img15 = ImageIO.read(new File(getClass().getClassLoader().getResource(cityTokenPaths.get(14)).toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	
	/**
	 * loads emprorium of player 0
	 */
	BufferedImage emp0 = null;
	try {
		emp0 = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player0.png").toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	/**
	 * loads emprorium of player 1
	 */
	BufferedImage emp1 = null;
	try {
		emp1 = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player1.png").toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	/**
	 * loads emprorium of player 2
	 */
	BufferedImage emp2 = null;
	try {
		emp2 = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player2.png").toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	/**
	 * loads emprorium of player 3
	 */
	BufferedImage emp3 = null;
	try {
		emp3 = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player3.png").toURI()));
	} catch (URISyntaxException e) {
		System.out.println("Unable to find file");
	}
	/**
	   * loads the king
	   */
	  BufferedImage kingImg = null;
	  try {
	   kingImg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/king.png").toURI()));
	  } catch (URISyntaxException e) {
		  System.out.println("Unable to find file");
	  }
	
    /**
	 * sets the dimension of the image 
	 */
	int w = img.getWidth();
    int h = img.getHeight();
	BufferedImage combined = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
	
	Graphics g = combined.getGraphics();
	/**
	 * draws the image
	 */
	g.drawImage(img, 0, 0, null);
	/**
	 * draws bonus on city A
	 */
	g.drawImage(img2, 8, -12, null);
	/**
	 * draws bonus on city B
	 */
	g.drawImage(img3, -7, 243, null);
	/**
	 * draws bonus on city C
	 */
	g.drawImage(img4, 260, 60, null);
	/**
	 * draws bonus on city E
	 */
	g.drawImage(img6, 77, 495, null);
	/**
	 * draws bonus on city I
	 */
	g.drawImage(img10, 845, 10, null);
	/**
	 * draws bonus on city K
	 */
	g.drawImage(img11, 1140, -3, null);
	/**
	 * draws bonus on city L
	 */
	g.drawImage(img12, 1088, 268, null);
	/**
	 * draws bonus on city N
	 */
	g.drawImage(img14, 1362, 138, null);
	/**
	 * draws bonus on city O
	 */
	g.drawImage(img15, 1372, 415, null);
	
	switch (mapConfig){
	case "ABA":
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 243, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 562, 8, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 560, 258, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 560, 483, null);
			/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1068, 530, null);
		break;
	case "ABB":
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 243, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 562, 8, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 560, 258, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 560, 483, null);
			/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1068, 530, null);
		break;
	case "BAA":
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 225, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 557, 20, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 572, 248, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 600, 463, null);
			/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1068, 530, null);
		break;
	case "BAB":
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 225, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 557, 20, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 572, 248, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 600, 463, null);
			/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1068, 530, null);
		break;
	case "BBA":
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 225, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 564, 10, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 560, 258, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 565, 483, null);
			/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1068, 530, null);
		break;
	case "BBB":
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 225, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 566, 8, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 560, 258, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 565, 483, null);
			/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1068, 530, null);
		break;
	default:
		/**
		 * draws bonus on city D
		 */
		g.drawImage(img5, 243, 307, null);
		/**
		 * draws bonus on city F
		 */
		g.drawImage(img7, 557, 8, null);
		/**
		 * draws bonus on city G
		 */
		g.drawImage(img8, 572, 248, null);
		/**
		 * draws bonus on city H
		 */
		g.drawImage(img9, 600, 463, null);
		/**
		 * draws bonus on city M
		 */
		g.drawImage(img13, 1072, 528, null);
		break;
	}
	if(emporiums.size()>0){
	String[] temp=emporiums.get(0).split(" ");
	for(String te:temp){
		if("Arkon".equals(te)){
		/**
		 * draws 0 on A
		 */
		g.drawImage(emp0, 51, 166, null);
		}
		if("Burgen".equals(te)){
		/**
		 * draws 0 on B
		 */
		g.drawImage(emp0, 26, 445, null);
		}
		if("Castrum".equals(te)){
		/**
		 * draws 0 on C
		 */
		g.drawImage(emp0, 310, 255, null);
		}
		if("Dorful".equals(te)){
		/**
		 * draws 0 on D
		 */
		g.drawImage(emp0, 295, 498, null);
		}
		if("Esti".equals(te)){
		/**
		 * draws 0 on E
		 */
		g.drawImage(emp0, 120, 690, null);
		}
		if("Framek".equals(te)){
		/**
		 * draws 0 on F
		 */
		g.drawImage(emp0, 600, 204, null);
		}
		if("Graden".equals(te)){
		/**
		 * draws 0 on G
		 */
		g.drawImage(emp0, 620, 444, null);
		}
		if("Hellar".equals(te)){
		/**
		 * draws 0 on H
		 */
		g.drawImage(emp0, 645, 657, null);
		}
		if("Indur".equals(te)){
		/**
		 * draws 0 on I
		 */
		g.drawImage(emp0, 892, 200, null);
		}
		if("Juvelar".equals(te)){
		/**
		 * draws 0 on J
		 */
		g.drawImage(emp0, 865, 522, null);
		}
		if("Kultos".equals(te)){
		/**
		 * draws 0 on K
		 */
		g.drawImage(emp0, 1186, 194, null);
		}
		if("Lyram".equals(te)){
		/**
		 * draws 0 on L
		 */
		g.drawImage(emp0, 1154, 452, null);
		}
		if("Merkatim".equals(te)){
		/**
		 * draws 0 on M
		 */
		g.drawImage(emp0, 1128, 717, null);
		}
		if("Naris".equals(te)){
		/**
		 * draws 0 on N
		 */
		g.drawImage(emp0, 1394, 333, null);	
		}
		if("Osium".equals(te)){
		/**
		 * draws 0 on O
		 */
		g.drawImage(emp0, 1403, 613, null);
		}
	}
	}
	if(emporiums.size()>1){
		String temp[]=emporiums.get(1).split(" ");
		for(String te1:temp){
		if("Arkon".equals(te1)){
		/**
		 * draws 1 on A
		 */
		g.drawImage(emp1, 88, 171, null);
		}
		if("Burgen".equals(te1)){
		/**
		 * draws 1 on B
		 */
		g.drawImage(emp1, 63, 450, null);
		}
		if("Castrum".equals(te1)){
		/**
		 * draws 1 on C
		 */
		g.drawImage(emp1, 347, 260, null);
		}
		if("Dorful".equals(te1)){
		/**
		 * draws 1 on D
		 */
		g.drawImage(emp1, 332, 503, null);
		}
		if("Esti".equals(te1)){
		/**
		 * draws 1 on E
		 */
		g.drawImage(emp1, 157, 695, null);
		}
		if("Framek".equals(te1)){
		/**
		 * draws 1 on F
		 */
		g.drawImage(emp1, 637, 209, null);
		}
		if("Graden".equals(te1)){
		/**
		 * draws 1 on G
		 */
		g.drawImage(emp1, 657, 449, null);
		}
		if("Hellar".equals(te1)){
		/**
		 * draws 1 on H
		 */
		g.drawImage(emp1, 682, 662, null);
		}
		if("Indur".equals(te1)){
		/**
		 * draws 1 on I
		 */
		g.drawImage(emp1, 929, 205, null);
		}
		if("Juvelar".equals(te1)){
		/**
		 * draws 1 on J
		 */
		g.drawImage(emp1, 902, 527, null);
		}
		if("Kultos".equals(te1)){
		/**
		 * draws 1 on K
		 */
		g.drawImage(emp1, 1223, 199, null);
		}
		if("Lyram".equals(te1)){
		/**
		 * draws 1 on L
		 */
		g.drawImage(emp1, 1191, 457, null);
		}
		if("Merkatim".equals(te1)){
		/**
		 * draws 1 on M
		 */
		g.drawImage(emp1, 1165, 722, null);
		}
		if("Naris".equals(te1)){
		/**
		 * draws 1 on N
		 */
		g.drawImage(emp1, 1431, 338, null);
		}
		if("Osium".equals(te1)){
		/**
		 * draws 1 on O
		 */
		g.drawImage(emp1, 1440, 618, null);
		}
		}
		
	}


if(emporiums.size()>2){
		String[]temp=emporiums.get(2).split(" ");
		for(String te2:temp){
		if("Arkon".equals(te2)){
		/**
		 * draws 2 on A
		 */
		g.drawImage(emp2, 125, 176, null);
		}
		if("Burgen".equals(te2)){
		/**
		 * draws 2 on B
		 */
		g.drawImage(emp2, 100, 455, null);
		}
		if("Castrum".equals(te2)){
		/**
		 * draws 2 on C
		 */
		g.drawImage(emp2, 384, 265, null);
		}
		if("Dorgul".equals(te2)){
		/**
		 * draws 2 on D
		 */
		g.drawImage(emp2, 369, 508, null);
		}
		if("Esti".equals(te2)){
		/**
		 * draws 2 on E
		 */
		g.drawImage(emp2, 194, 700, null);
		}
		if("Framek".equals(te2)){
		/**
		 * draws 2 on F
		 */
		g.drawImage(emp2, 674, 214, null);
		}
		if("Graden".equals(te2)){
		/**
		 * draws 2 on G
		 */
		g.drawImage(emp2, 694, 454, null);
		}
		if("Hellar".equals(te2)){
		/**
		 * draws 2 on H
		 */
		g.drawImage(emp2, 719, 667, null);
		}
		if("Indur".equals(te2)){
		/**
		 * draws 2 on I
		 */
		g.drawImage(emp2, 966, 210, null);
		}
		if("Juvelar".equals(te2)){
		/**
		 * draws 2 on J
		 */
		g.drawImage(emp2, 939, 532, null);
		}
		if("Kultos".equals(te2)){
		/**
		 * draws 2 on K
		 */
		g.drawImage(emp2, 1260, 204, null);
		}
		if("Lyram".equals(te2)){
		/**
		 * draws 2 on L
		 */
		g.drawImage(emp2, 1228, 462, null);
		}
		if("Merkatim".equals(te2)){
		/**
		 * draws 2 on M
		 */
		g.drawImage(emp2, 1202, 727, null);
		}
		if("Naris".equals(te2)){
		/**
		 * draws 2 on N
		 */
		g.drawImage(emp2, 1468, 343, null);
		}
		if("Osium".equals(te2)){
		/**
		 * draws 2 on O
		 */
		g.drawImage(emp2, 1477, 623, null);
		}
		}
}
		if(emporiums.size()>3){
		String[] temp=emporiums.get(3).split(" ");
		for(String te3:temp){
		if("Arkon".equals(te3)){
		/**
		 * draws 3 on A
		 */
		g.drawImage(emp3, 162, 181, null);
		}
		if("Burgen".equals(te3)){
		/**
		 * draws 3 on B
		 */
		g.drawImage(emp3, 137, 460, null);
		}
		if("Castrum".equals(te3)){
		/**
		 * draws 3 on C
		 */
		g.drawImage(emp3, 421, 270, null);
		}
		if("Dorful".equals(te3)){
		/**
		 * draws 3 on D
		 */
		g.drawImage(emp3, 406, 513, null);
		}
		if("Esti".equals(te3)){
		/**
		 * draws 3 on E
		 */
		g.drawImage(emp3, 231, 705, null);
		}
		if("Framek".equals(te3)){
		/**
		 * draws 3 on F
		 */
		g.drawImage(emp3, 711, 219, null);
		}
		if("Graden".equals(te3)){
		/**
		 * draws 3 on G
		 */
		g.drawImage(emp3, 731, 459, null);
		}
		if("Hellar".equals(te3)){
		/**
		 * draws 3 on H
		 */
		g.drawImage(emp3, 756, 672, null);
		}
		if("Indur".equals(te3)){
		/**
		 * draws 3 on I
		 */
		g.drawImage(emp3, 1003, 215, null);
		}
		if("Juvelar".equals(te3)){
		/**
		 * draws 3 on J
		 */
		g.drawImage(emp3, 976, 537, null);
		}
		if("Kultos".equals(te3)){
			/**
			 * draws 3 on K
			 */
			g.drawImage(emp3, 1297, 209, null);
		}
		if("Lyram".equals(te3)){
			/**
			 * draws 3 on L
			 */
			g.drawImage(emp3, 1265, 467, null);
		}
		if("Merkatim".equals(te3)){
			/**
			 * draws 3 on M
			 */
			g.drawImage(emp3, 1239, 732, null);
		}
		if("Naris".equals(te3)){
			/**
			 * draws 3 on N
			 */
			g.drawImage(emp3, 1505, 348, null);
		}
		if("Osium".equals(te3)){
			/**
			 * draws 3 on O
			 */
			g.drawImage(emp3, 1512, 628, null);
		}
		}
		}
		String[] extractedKingPosition=kingPosition.split(" ");
		/**
		 * draws the king on A
		 */
		if("Arkon".equals(extractedKingPosition[4])){

			g.drawImage(kingImg, 90, 80, null);

		}

		/**
		 * draws the king on B
		 */
		if("Burgen".equals(extractedKingPosition[4])){

		
			g.drawImage(kingImg, 70, 350, null);

		}

		/**
		 * draws the king on C
		 */
		if("Castrum".equals(extractedKingPosition[4])){

		
			g.drawImage(kingImg, 350, 160, null);

		}
		/**
		 * draws the king on D
		 */
		if("Dorful".equals(extractedKingPosition[4])){

		
			g.drawImage(kingImg, 340, 410, null);

		}
		/**
		 * draws the king on E
		 */
		if("Esti".equals(extractedKingPosition[4])){

		
			g.drawImage(kingImg, 160, 600, null);

		}
		/**
		 * draws the king on F
		 */
		if("Framek".equals(extractedKingPosition[4])){

			g.drawImage(kingImg, 650, 120, null);

		}/**
		 * draws the king on G
		 */
		if("Graden".equals(extractedKingPosition[4])){

			g.drawImage(kingImg, 670, 355, null);

		}
		/**
		 * draws the king on H
		 */
		if("Hellar".equals(extractedKingPosition[4])){

			g.drawImage(kingImg, 700, 570, null);

		}
		/**
		 * draws the king on I
		 */
		if("Indur".equals(extractedKingPosition[4])){

			g.drawImage(kingImg, 930, 115, null);

		}
		/**
		 * draws the king on J
		 */
		if("Juvelar".equals(extractedKingPosition[4])){

			/**
			 * draws the king
			 */
			g.drawImage(kingImg, 900, 430, null);

		}
		/**
		 * draws the king on K
		 */
		if("Kultos".equals(extractedKingPosition[4])){

			/**
			 * draws the king
			 */
			g.drawImage(kingImg, 1220, 100, null);

		}
		/**
		 * draws the king on L
		 */
		if("Lyram".equals(extractedKingPosition[4])){

			/**
			 * draws the king
			 */
			g.drawImage(kingImg, 1200, 360, null);

		}
		/**
		 * draws the king on M
		 */
		if("Merkatim".equals(extractedKingPosition[4])){

			/**
			 * draws the king
			 */
			g.drawImage(kingImg, 1170, 630, null);

		}
		/**
		 * draws the king on N
		 */
		if("Naris".equals(extractedKingPosition[4])){

			/**
			 * draws the king
			 */
			g.drawImage(kingImg, 1440, 250, null);

		}
		/**
		 * draws the king on O
		 */
		if("Osium".equals(extractedKingPosition[4])){

			/**
			 * draws the king
			 */
			g.drawImage(kingImg, 1450, 525, null);

		}


	/**
	 * create an image of the drawn map
	 */
	ImageIcon icon = new ImageIcon(combined);
	Image image = icon.getImage(); 
	Image newimg = image.getScaledInstance(590, 290,  java.awt.Image.SCALE_SMOOTH); 
	icon = new ImageIcon(newimg); 
	mapPanel.add(new JLabel(icon));
	mapPanel.revalidate();
	mapPanel.repaint();
	}
	/**
	 * 	shows PermissionDecks
	 * @param extractFromHere extract image paths from here
	 * @throws IOException
	 */
	public void printPermissionDecks (ArrayList<String> extractFromHere) throws IOException{
		
		seasidePermissionDeckPanel.removeAll();
		plainsPermissionDeckPanel.removeAll();
		mountainsPermissionDeckPanel.removeAll();
		
		String [] permissionDeckPaths = new String[6];
		int i=0;
		for (String s:extractFromHere){
			String [] temp=s.split("#");
			permissionDeckPaths[i]="images/permit_tiles/permesso"+temp[1]+".png";
			i++;
		}
		/**
		 * loads seasidePermissionDeck images
		 */
		BufferedImage imgSeasideBack = null;
		try {
			imgSeasideBack = ImageIO.read(new File(getClass().getClassLoader().getResource("images/backs/backseaside.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgSeasideLeft = null;
		try {
			imgSeasideLeft = ImageIO.read(new File(getClass().getClassLoader().getResource(permissionDeckPaths[0]).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgSeasideRight = null;
		try {
			imgSeasideRight = ImageIO.read(new File(getClass().getClassLoader().getResource(permissionDeckPaths[1]).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		/**
		 * creates the images for seasidePermissionDeck
		 */
		ImageIcon seasideBackIcon = new ImageIcon(imgSeasideBack);
		Image image = seasideBackIcon.getImage(); 
		Image newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		seasideBackIcon = new ImageIcon(newimg); 
		ImageIcon seasideLeftIcon = new ImageIcon(imgSeasideLeft);
		image = seasideLeftIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		seasideLeftIcon = new ImageIcon(newimg); 
		ImageIcon seasideRightIcon = new ImageIcon(imgSeasideRight);
		image = seasideRightIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		seasideRightIcon = new ImageIcon(newimg); 
		/**
		 * loads plainsPermissionDeck images
		 */
		BufferedImage imgPlainsBack = null;
		try {
			imgPlainsBack = ImageIO.read(new File(getClass().getClassLoader().getResource("images/backs/backplains.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgPlainsLeft = null;
		try {
			imgPlainsLeft = ImageIO.read(new File(getClass().getClassLoader().getResource(permissionDeckPaths[2]).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgPlainsRight = null;
		try {
			imgPlainsRight = ImageIO.read(new File(getClass().getClassLoader().getResource(permissionDeckPaths[3]).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		/**
		 * creates the images for plainsPermissionDeck
		 */
		ImageIcon plainsBackIcon = new ImageIcon(imgPlainsBack);
		image = plainsBackIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		plainsBackIcon = new ImageIcon(newimg); 
		ImageIcon plainsLeftIcon = new ImageIcon(imgPlainsLeft);
		image = plainsLeftIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		plainsLeftIcon = new ImageIcon(newimg);
		ImageIcon plainsRightIcon = new ImageIcon(imgPlainsRight);
		image = plainsRightIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		plainsRightIcon = new ImageIcon(newimg); 
		/**
		 * loads mountainsPermissionDeck images
		 */
		BufferedImage imgMountainsBack = null;
		try {
			imgMountainsBack = ImageIO.read(new File(getClass().getClassLoader().getResource("images/backs/backmountains.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgMountainsLeft = null;
		try {
			imgMountainsLeft = ImageIO.read(new File(getClass().getClassLoader().getResource(permissionDeckPaths[4]).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgMountainsRight = null;
		try {
			imgMountainsRight = ImageIO.read(new File(getClass().getClassLoader().getResource(permissionDeckPaths[5]).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		
		/**
		 * creates the images for mountainsPermissionDeck
		 */
		ImageIcon mountainsBackIcon = new ImageIcon(imgMountainsBack);
		image = mountainsBackIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		mountainsBackIcon = new ImageIcon(newimg); 
		ImageIcon mountainsLeftIcon = new ImageIcon(imgMountainsLeft);
		image = mountainsLeftIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		mountainsLeftIcon = new ImageIcon(newimg);
		ImageIcon mountainsRightIcon = new ImageIcon(imgMountainsRight);
		image = mountainsRightIcon.getImage(); 
		newimg = image.getScaledInstance(65, 65,  java.awt.Image.SCALE_SMOOTH); 
		mountainsRightIcon = new ImageIcon(newimg);
		
		seasidePermissionDeckPanel.add(new JLabel (seasideBackIcon));
		seasidePermissionDeckPanel.add(new JLabel (seasideLeftIcon));
		seasidePermissionDeckPanel.add(new JLabel (seasideRightIcon));
		plainsPermissionDeckPanel.add(new JLabel (plainsBackIcon));
		plainsPermissionDeckPanel.add(new JLabel (plainsLeftIcon));
		plainsPermissionDeckPanel.add(new JLabel (plainsRightIcon));
		mountainsPermissionDeckPanel.add(new JLabel (mountainsBackIcon));
		mountainsPermissionDeckPanel.add(new JLabel (mountainsLeftIcon));
		mountainsPermissionDeckPanel.add(new JLabel (mountainsRightIcon));
		seasidePermissionDeckPanel.revalidate();
		plainsPermissionDeckPanel.revalidate();
		mountainsPermissionDeckPanel.revalidate();
		seasidePermissionDeckPanel.repaint();
		plainsPermissionDeckPanel.repaint();
		mountainsPermissionDeckPanel.repaint();
	
	}
	/**
	 *shows the balconies 
	 * @param extractFromHere contains the paths of the images
	 * @throws IOException
	 */
	public void printBalconies (ArrayList<String> extractFromHere) throws IOException{
		
		seasideBalconyPanel.removeAll();
		plainsBalconyPanel.removeAll();
		mountainsBalconyPanel.removeAll();
		kingBalconyPanel.removeAll();
		
		ArrayList<String> balconyPath = new ArrayList<>(); 
		
		for(String s:extractFromHere){
			String[] temp=s.split("-");
			for(String t:temp){
				String temp1="images/councillors/"+t+"c.png";
				balconyPath.add(temp1);
			}
			
		}
		/**
		 * loads seasideBalcony images
		 */
		BufferedImage imgSeasideBalcony1 = null;
		try {
			imgSeasideBalcony1 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgSeasideBalcony2 = null;
		try {
			imgSeasideBalcony2 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgSeasideBalcony3 = null;
		try {
			imgSeasideBalcony3 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgSeasideBalcony4 = null;
		try {
			imgSeasideBalcony4 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		
		/**
		 * creates the images for seasideBalcony
		 */
		ImageIcon seasideBalconyIcon1 = new ImageIcon(imgSeasideBalcony1);
		Image image = seasideBalconyIcon1.getImage(); 
		Image newimg = image.getScaledInstance(46, 70,  java.awt.Image.SCALE_SMOOTH); 
		seasideBalconyIcon1 = new ImageIcon(newimg); 
		ImageIcon seasideBalconyIcon2 = new ImageIcon(imgSeasideBalcony2);
		image = seasideBalconyIcon2.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		seasideBalconyIcon2 = new ImageIcon(newimg); 
		ImageIcon seasideBalconyIcon3 = new ImageIcon(imgSeasideBalcony3);
		image = seasideBalconyIcon3.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		seasideBalconyIcon3 = new ImageIcon(newimg); 
		ImageIcon seasideBalconyIcon4 = new ImageIcon(imgSeasideBalcony4);
		image = seasideBalconyIcon4.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		seasideBalconyIcon4 = new ImageIcon(newimg); 
		/**
		 * loads plainsBalcony images
		 */
		BufferedImage imgPlainsBalcony1 = null;
		try {
			imgPlainsBalcony1 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgPlainsBalcony2 = null;
		try {
			imgPlainsBalcony2 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgPlainsBalcony3 = null;
		try {
			imgPlainsBalcony3 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgPlainsBalcony4 = null;
		try {
			imgPlainsBalcony4 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		
		/**
		 * creates the images for plainsBalcony
		 */
		ImageIcon plainsBalconyIcon1 = new ImageIcon(imgPlainsBalcony1);
		image = plainsBalconyIcon1.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		plainsBalconyIcon1 = new ImageIcon(newimg); 
		ImageIcon plainsBalconyIcon2 = new ImageIcon(imgPlainsBalcony2);
		image = plainsBalconyIcon2.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		plainsBalconyIcon2 = new ImageIcon(newimg); 
		ImageIcon plainsBalconyIcon3 = new ImageIcon(imgPlainsBalcony3);
		image = plainsBalconyIcon3.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		plainsBalconyIcon3 = new ImageIcon(newimg); 
		ImageIcon plainsBalconyIcon4 = new ImageIcon(imgPlainsBalcony4);
		image = plainsBalconyIcon4.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		plainsBalconyIcon4 = new ImageIcon(newimg); 
		
		/**
		 * loads mountainsBalcony images
		 */
		BufferedImage imgMountainsBalcony1 = null;
		try {
			imgMountainsBalcony1 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgMountainsBalcony2 = null;
		try {
			imgMountainsBalcony2 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgMountainsBalcony3 = null;
		try {
			imgMountainsBalcony3 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgMountainsBalcony4 = null;
		try {
			imgMountainsBalcony4 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		
		/**
		 * creates the images for kingBalcony
		 */
		ImageIcon mountainsBalconyIcon1 = new ImageIcon(imgMountainsBalcony1);
		image = mountainsBalconyIcon1.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		mountainsBalconyIcon1 = new ImageIcon(newimg); 
		ImageIcon mountainsBalconyIcon2 = new ImageIcon(imgMountainsBalcony2);
		image = mountainsBalconyIcon2.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		mountainsBalconyIcon2 = new ImageIcon(newimg); 
		ImageIcon mountainsBalconyIcon3 = new ImageIcon(imgMountainsBalcony3);
		image = mountainsBalconyIcon3.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		mountainsBalconyIcon3 = new ImageIcon(newimg); 
		ImageIcon mountainsBalconyIcon4 = new ImageIcon(imgMountainsBalcony4);
		image = mountainsBalconyIcon4.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		mountainsBalconyIcon4 = new ImageIcon(newimg); 
		
		/**
		 * loads kingBalcony images
		 */
		BufferedImage imgKingBalcony1 = null;
		try {
			imgKingBalcony1 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgKingBalcony2 = null;
		try {
			imgKingBalcony2 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgKingBalcony3 = null;
		try {
		imgKingBalcony3	 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		BufferedImage imgKingBalcony4 = null;
		try {
			imgKingBalcony4 = ImageIO.read(new File(getClass().getClassLoader().getResource(balconyPath.remove(0)).toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		
		/**
		 * creates the images for kingBalcony
		 */
		ImageIcon kingBalconyIcon1 = new ImageIcon(imgKingBalcony1);
		image = kingBalconyIcon1.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		kingBalconyIcon1 = new ImageIcon(newimg); 
		ImageIcon kingBalconyIcon2 = new ImageIcon(imgKingBalcony2);
		image = kingBalconyIcon2.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH); 
		kingBalconyIcon2 = new ImageIcon(newimg); 
		ImageIcon kingBalconyIcon3 = new ImageIcon(imgKingBalcony3);
		image = kingBalconyIcon3.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH);
		kingBalconyIcon3 = new ImageIcon(newimg); 
		ImageIcon kingBalconyIcon4 = new ImageIcon(imgKingBalcony4);
		image = kingBalconyIcon4.getImage(); 
		newimg = image.getScaledInstance(46, 70, java.awt.Image.SCALE_SMOOTH);
		kingBalconyIcon4 = new ImageIcon(newimg); 
		
		seasideBalconyPanel.add(new JLabel(seasideBalconyIcon1));
		plainsBalconyPanel.add(new JLabel(plainsBalconyIcon1));
		mountainsBalconyPanel.add(new JLabel(mountainsBalconyIcon1));
		kingBalconyPanel.add(new JLabel(kingBalconyIcon1));
		seasideBalconyPanel.add(new JLabel(seasideBalconyIcon2));
		plainsBalconyPanel.add(new JLabel(plainsBalconyIcon2));
		mountainsBalconyPanel.add(new JLabel(mountainsBalconyIcon2));
		kingBalconyPanel.add(new JLabel(kingBalconyIcon2));
		seasideBalconyPanel.add(new JLabel(seasideBalconyIcon3));
		plainsBalconyPanel.add(new JLabel(plainsBalconyIcon3));
		mountainsBalconyPanel.add(new JLabel(mountainsBalconyIcon3));
		kingBalconyPanel.add(new JLabel(kingBalconyIcon3));
		seasideBalconyPanel.add(new JLabel(seasideBalconyIcon4));
		plainsBalconyPanel.add(new JLabel(plainsBalconyIcon4));
		mountainsBalconyPanel.add(new JLabel(mountainsBalconyIcon4));
		kingBalconyPanel.add(new JLabel(kingBalconyIcon4));
		seasideBalconyPanel.revalidate();
		plainsBalconyPanel.revalidate();
		mountainsBalconyPanel.revalidate();
		kingBalconyPanel.revalidate();
		seasideBalconyPanel.repaint();
		plainsBalconyPanel.repaint();
		mountainsBalconyPanel.repaint();
		kingBalconyPanel.repaint();
		
	}
	/**
	 * shows PoliticCard
	 * @param extractFromHere the paths of the images
	 * @throws IOException
	 */
	public void printPoliticCard (String extractFromHere) throws IOException{
		politicCardPanel.removeAll();
		ArrayList<String> politicPath = new ArrayList<>();
		String[] temp=extractFromHere.split("-");
		for (String t:temp){
			String temp1="images/politic_cards/"+t+"p.png";
			politicPath.add(temp1);
		}
		for(String p:politicPath){
			BufferedImage tempImg = null;
			try {
				tempImg = ImageIO.read(new File(getClass().getClassLoader().getResource(p).toURI()));
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon tempIcon = new ImageIcon(tempImg);
			Image image = tempIcon.getImage(); 
			Image newimg = image.getScaledInstance(45, 70,  java.awt.Image.SCALE_SMOOTH); 
			tempIcon = new ImageIcon(newimg); 
			politicCardPanel.add(new JLabel(tempIcon));
		}
		politicCardPanel.revalidate();
		politicCardPanel.repaint();
	}
	/**
	 * shows the publisher
	 * @param publisherText is the text from the publisher
	 */
	public void printPublisher(String publisherText){
		publisherTextArea.setLineWrap(true);
		publisherTextArea.setWrapStyleWord(true);
		publisherTextArea.setEditable(false);
		publisherTextArea.setText(publisherText+"\n");
		publisherPanel.add(publisherTextArea);
		publisherPanel.revalidate();
		publisherPanel.repaint();
		
		
	}
	/**
	 * shows the PermissionCards for each player
	 * @param extractFromHere
	 * @throws IOException
	 */
	public void printPermissionCards(ArrayList<String> extractFromHere) throws IOException{
		permissionCards0Panel.removeAll();
		permissionCards1Panel.removeAll();
		permissionCards2Panel.removeAll();
		permissionCards3Panel.removeAll();
		ArrayList<String> playerPermissionCardPath= new ArrayList<>();
		String[] temp=null;
		
		if(!extractFromHere.get(0).isEmpty()){
			playerPermissionCardPath.clear();
			temp=extractFromHere.get(0).split("/");
			for(String t:temp){
				String[] temp1=t.split("#");
				
				String temp2="images/permit_tiles/permesso"+temp1[1]+".png";
				playerPermissionCardPath.add(temp2);
			}
		
			/**
			 * set permissionCard of player 1
			 */
			for(String p:playerPermissionCardPath){
				BufferedImage tempImg = null;
				try {
					tempImg = ImageIO.read(new File(getClass().getClassLoader().getResource(p).toURI()));
				} catch (URISyntaxException e) {
					System.out.println("Unable to find file");
				}
				ImageIcon tempIcon = new ImageIcon(tempImg);
				Image image = tempIcon.getImage(); 
				Image newimg = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH); 
				tempIcon = new ImageIcon(newimg); 
				permissionCards0Panel.add(new JLabel(tempIcon));
			
			}
		}
		
		if(!extractFromHere.get(1).isEmpty()){
			playerPermissionCardPath.clear();
			temp=extractFromHere.get(1).split("/");
			for(String t:temp){
				String[] temp1=t.split("#");
				String temp2="images/permit_tiles/permesso"+temp1[1]+".png";
				playerPermissionCardPath.add(temp2);
			}
		
			/**
			 * set permissionCard of player 2
			 */
			for(String p:playerPermissionCardPath){
				BufferedImage tempImg = null;
				try {
					tempImg = ImageIO.read(new File(getClass().getClassLoader().getResource(p).toURI()));
				} catch (URISyntaxException e) {
					System.out.println("Unable to find file");
				}
				ImageIcon tempIcon = new ImageIcon(tempImg);
				Image image = tempIcon.getImage(); 
				Image newimg = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH); 
				tempIcon = new ImageIcon(newimg); 
				permissionCards1Panel.add(new JLabel(tempIcon));
			}
		}
		if(extractFromHere.size()>2){
			if(!extractFromHere.get(2).isEmpty()){
				playerPermissionCardPath.clear();
				temp=extractFromHere.get(2).split("/");
				for(String t:temp){
					String[] temp1=t.split("#");
					String temp2="images/permit_tiles/permesso"+temp1[1]+".png";
					playerPermissionCardPath.add(temp2);
				}
				
				/**
				 * set permissionCard of player 3
				 */
				for(String p:playerPermissionCardPath){
					BufferedImage tempImg = null;
					try {
						tempImg = ImageIO.read(new File(getClass().getClassLoader().getResource(p).toURI()));
					} catch (URISyntaxException e) {
						System.out.println("Unable to find file");
					}
					ImageIcon tempIcon = new ImageIcon(tempImg);
					Image image = tempIcon.getImage(); 
					Image newimg = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH); 
					tempIcon = new ImageIcon(newimg); 
					permissionCards2Panel.add(new JLabel(tempIcon));
				}
			}
		}
		if(extractFromHere.size()>3){
			if(!extractFromHere.get(3).isEmpty()){
				playerPermissionCardPath.clear();
				temp=extractFromHere.get(3).split("/");
				for(String t:temp){
					String[] temp1=t.split("#");
					String temp2="images/permit_tiles/permesso"+temp1[1]+".png";
					playerPermissionCardPath.add(temp2);
				}
			
				/**
				 * set permissionCard of player 4
				 */
				for(String p:playerPermissionCardPath){
					BufferedImage tempImg = null;
					try {
						tempImg = ImageIO.read(new File(getClass().getClassLoader().getResource(p).toURI()));
					} catch (URISyntaxException e) {
						System.out.println("Unable to find file");
					}
					ImageIcon tempIcon = new ImageIcon(tempImg);
					Image image = tempIcon.getImage(); 
					Image newimg = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH); 
					tempIcon = new ImageIcon(newimg); 
					permissionCards3Panel.add(new JLabel(tempIcon));
				}
			}
		}
		
		permissionCards0Panel.revalidate();
		permissionCards1Panel.revalidate();
		permissionCards2Panel.revalidate();
		permissionCards3Panel.revalidate();
		permissionCards0Panel.repaint();
		permissionCards1Panel.repaint();
		permissionCards2Panel.repaint();
		permissionCards3Panel.repaint();
		
	}
	/**
	 * show the Situation of the players, such as number of points, cards,...
	 * @param extractFromHere the string from whom extract the statistics
	 * @throws IOException 
	 */
	public void printPlayerStats(ArrayList<String> extractFromHere) throws IOException{
		player0SituationPanel.removeAll();
		player1SituationPanel.removeAll();
		player2SituationPanel.removeAll();
		player3SituationPanel.removeAll();
		/**
		 * loads Image for Player 0
		 */
		BufferedImage pl0img = null;
		try {
			pl0img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/player0token.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon pl0icon = new ImageIcon(pl0img);
		Image image = pl0icon.getImage(); 
		Image newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		pl0icon = new ImageIcon(newimg); 
		/**
		 * loads Image for Player 1
		 */
		BufferedImage pl1img = null;
		try {
			pl1img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/player1token.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon pl1icon = new ImageIcon(pl1img);
		image = pl1icon.getImage(); 
		newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		pl1icon = new ImageIcon(newimg); 
		
		/**
		 * loads Image for points
		 */
		BufferedImage poimg = null;
		try {
			poimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/points.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon poicon = new ImageIcon(poimg);
		image = poicon.getImage(); 
		newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		poicon = new ImageIcon(newimg); 
		/**
		 * loads Image for assistants
		 */
		BufferedImage aimg = null;
		try {
			aimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/assistant.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon aicon = new ImageIcon(aimg);
		image = aicon.getImage(); 
		newimg = image.getScaledInstance(40, 40,  java.awt.Image.SCALE_SMOOTH); 
		aicon = new ImageIcon(newimg); 
		/**
		 * loads Image for coins
		 */
		BufferedImage coimg = null;
		try {
			coimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/coin.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon coicon = new ImageIcon(coimg);
		image = coicon.getImage(); 
		newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		coicon = new ImageIcon(newimg); 
		/**
		 * loads Image for emporiums of player 0
		 */
		BufferedImage em0pimg = null;
		try {
			em0pimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player0.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon em0picon = new ImageIcon(em0pimg);
		image = em0picon.getImage(); 
		newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		em0picon = new ImageIcon(newimg); 
		
		/**
		 * loads Image for emporiums of player 1
		 */
		BufferedImage em1pimg = null;
		try {
			em1pimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player1.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon em1picon = new ImageIcon(em1pimg);
		image = em1picon.getImage(); 
		newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		em1picon = new ImageIcon(newimg); 
		/**
		 * loads Image for nobility points
		 */
		BufferedImage npimg = null;
		try {
			npimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/nobilityPoints.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon npicon = new ImageIcon(npimg);
		image = npicon.getImage(); 
		newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
		npicon = new ImageIcon(newimg); 
		
		String[] temp;
    	player0SituationPanel.setLayout(new GridLayout(1,12));
    	player1SituationPanel.setLayout(new GridLayout(1,12));
		temp=extractFromHere.get(0).split("#");
		player0SituationPanel.add(new JLabel(pl0icon));
		player0SituationPanel.add(new JLabel("0"));
		player0SituationPanel.add(new JLabel(poicon));
		player0SituationPanel.add(new JLabel(temp[1]));
		player0SituationPanel.add(new JLabel(aicon));
		player0SituationPanel.add(new JLabel(temp[2]));
		player0SituationPanel.add(new JLabel(coicon));
		player0SituationPanel.add(new JLabel(temp[3]));
		player0SituationPanel.add(new JLabel(em0picon));
		player0SituationPanel.add(new JLabel(temp[4]));
		player0SituationPanel.add(new JLabel(npicon));
		player0SituationPanel.add(new JLabel(temp[5]));
		temp=extractFromHere.get(1).split("#");
		player1SituationPanel.add(new JLabel(pl1icon));
		player1SituationPanel.add(new JLabel("1"));
		player1SituationPanel.add(new JLabel(poicon));
		player1SituationPanel.add(new JLabel(temp[1]));
		player1SituationPanel.add(new JLabel(aicon));
		player1SituationPanel.add(new JLabel(temp[2]));
		player1SituationPanel.add(new JLabel(coicon));
		player1SituationPanel.add(new JLabel(temp[3]));
		player1SituationPanel.add(new JLabel(em1picon));
		player1SituationPanel.add(new JLabel(temp[4]));
		player1SituationPanel.add(new JLabel(npicon));
		player1SituationPanel.add(new JLabel(temp[5]));
		if(extractFromHere.size()>2){
			/**
			 * loads Image for Player 2
			 */
			BufferedImage pl2img = null;
			try {
				pl2img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/player2token.png").toURI()));
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon pl2icon = new ImageIcon(pl2img);
			image = pl2icon.getImage(); 
			newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
			pl2icon = new ImageIcon(newimg); 
			/**
			 * loads Image for emporiums of player 2
			 */
			BufferedImage em2pimg = null;
			try {
				em2pimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player2.png").toURI()));
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon em2picon = new ImageIcon(em2pimg);
			image = em2picon.getImage(); 
			newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
			em2picon = new ImageIcon(newimg); 
			temp=extractFromHere.get(1).split("#");
			player2SituationPanel.setLayout(new GridLayout(1,12));
			player2SituationPanel.add(new JLabel(pl2icon));
			player2SituationPanel.add(new JLabel("2"));
			player2SituationPanel.add(new JLabel(poicon));
			player2SituationPanel.add(new JLabel(temp[1]));
			player2SituationPanel.add(new JLabel(aicon));
			player2SituationPanel.add(new JLabel(temp[2]));
			player2SituationPanel.add(new JLabel(coicon));
			player2SituationPanel.add(new JLabel(temp[3]));
			player2SituationPanel.add(new JLabel(em2picon));
			player2SituationPanel.add(new JLabel(temp[4]));
			player2SituationPanel.add(new JLabel(npicon));
			player2SituationPanel.add(new JLabel(temp[5]));
		}
		else
			player2SituationPanel.add(new JLabel("Player 2 is not playing"));
		if(extractFromHere.size()>3){
			/**
			 * loads Image for Player 3
			 */
			BufferedImage pl3img = null;
			try {
				pl3img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/player_situation/player3token.png").toURI()));
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon pl3icon = new ImageIcon(pl3img);
			image = pl3icon.getImage(); 
			newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
			pl3icon = new ImageIcon(newimg); 
			/**
			 * loads Image for emporiums of player 3
			 */
			BufferedImage em3pimg = null;
			try {
				em3pimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/emporiums/player3.png").toURI()));
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon em3picon = new ImageIcon(em3pimg);
			image = em3picon.getImage(); 
			newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH); 
			em3picon = new ImageIcon(newimg); 
			temp=extractFromHere.get(1).split("#");
			player3SituationPanel.setLayout(new GridLayout(1,12));
			player3SituationPanel.add(new JLabel(pl3icon));
			player3SituationPanel.add(new JLabel("3"));
			player3SituationPanel.add(new JLabel(poicon));
			player3SituationPanel.add(new JLabel(temp[1]));
			player3SituationPanel.add(new JLabel(aicon));
			player3SituationPanel.add(new JLabel(temp[2]));
			player3SituationPanel.add(new JLabel(coicon));
			player3SituationPanel.add(new JLabel(temp[3]));
			player3SituationPanel.add(new JLabel(em3picon));
			player3SituationPanel.add(new JLabel(temp[4]));
			player3SituationPanel.add(new JLabel(npicon));
			player3SituationPanel.add(new JLabel(temp[5]));
		}
		else
			player3SituationPanel.add(new JLabel("Player 3 is not playing"));
		
		
		player0SituationPanel.revalidate();
		player1SituationPanel.revalidate();
		player2SituationPanel.revalidate();
		player3SituationPanel.revalidate();
		player0SituationPanel.repaint();
		player1SituationPanel.repaint();
		player2SituationPanel.repaint();
		player3SituationPanel.repaint();
		
		
	}
	/**
	 * shows the bonus tiles
	 * @throws IOException
	 */
	public void printBonus() throws IOException{
		bonusPanel.removeAll();
		/**
		 * loads image for king's bonus 1
		 */
		BufferedImage k1img = null;
		try {
			k1img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/king1.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon k1icon = new ImageIcon(k1img);
		Image image = k1icon.getImage(); 
		Image newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		k1icon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(k1icon));
		/**
		 * loads image for king's bonus 2
		 */
		BufferedImage k2img = null;
		try {
			k2img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/king2.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon k2icon = new ImageIcon(k2img);
		image = k2icon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		k2icon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(k2icon));
		/**
		 * loads image for king's bonus 3
		 */
		BufferedImage k3img = null;
		try {
			k3img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/king3.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon k3icon = new ImageIcon(k3img);
		image = k3icon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		k3icon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(k3icon));
		/**
		 * loads image for king's bonus 4
		 */
		BufferedImage k4img = null;
		try {
			k4img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/king4.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon k4icon = new ImageIcon(k4img);
		image = k4icon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		k4icon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(k4icon));
		/**
		 * loads image for king's bonus 5
		 */
		BufferedImage k5img = null;
		try {
			k5img = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/king5.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon k5icon = new ImageIcon(k5img);
		image = k5icon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		k5icon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(k5icon));
		/**
		 * loads image for iron bonus
		 */
		BufferedImage ironimg = null;
		try {
			ironimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/iron.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon ironicon = new ImageIcon(ironimg);
		image = ironicon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		ironicon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(ironicon));
		/**
		 * loads image for bronze bonus
		 */
		BufferedImage bronzeimg = null;
		try {
			bronzeimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/bronze.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon bronzeicon = new ImageIcon(bronzeimg);
		image =bronzeicon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		bronzeicon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(bronzeicon));
		/**
		 * loads image for silver bonus
		 */
		BufferedImage silverimg = null;
		try {
			silverimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/silver.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon silvericon = new ImageIcon(silverimg);
		image = silvericon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		silvericon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(silvericon));
		/**
		 * loads image for gold bonus
		 */
		BufferedImage goldimg = null;
		try {
			goldimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/gold.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon goldicon = new ImageIcon(goldimg);
		image =goldicon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		goldicon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(goldicon));
		/**
		 * loads image for seaside bonus
		 */
		BufferedImage seasideimg = null;
		try {
			seasideimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/seaside.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon seasideicon = new ImageIcon(seasideimg);
		image =seasideicon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		seasideicon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(seasideicon));
		/**
		 * loads image for plains bonus
		 */
		BufferedImage plainsimg = null;
		try {
			plainsimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/plains.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon plainsicon = new ImageIcon(plainsimg);
		image =plainsicon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		plainsicon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(plainsicon));
		/**
		 * loads image for mountains bonus
		 */
		BufferedImage mountainsimg = null;
		try {
			mountainsimg = ImageIO.read(new File(getClass().getClassLoader().getResource("images/bonus_tiles/mountains.png").toURI()));
		} catch (URISyntaxException e) {
			System.out.println("Unable to find file");
		}
		ImageIcon mountainsicon = new ImageIcon(mountainsimg);
		image =mountainsicon.getImage(); 
		newimg = image.getScaledInstance(46, 23,  java.awt.Image.SCALE_SMOOTH); 
		mountainsicon = new ImageIcon(newimg); 
		bonusPanel.add(new JLabel(mountainsicon));
		
		
		
		bonusPanel.revalidate();
		bonusPanel.repaint();
	}
	
  	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			
			public void run() {
				
				}
			
		});
		
	}
	
}