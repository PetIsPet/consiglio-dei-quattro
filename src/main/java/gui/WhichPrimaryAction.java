package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * class that allows the player to choose a primary action
 *
 */
public class WhichPrimaryAction extends JFrame implements Runnable{
	
	String string;
	GUI gui;

	public static final long serialVersionUID = 1L;
	
	public WhichPrimaryAction(GUI gui){
		/**
		 * looks new look&feel
		 */
	
		this.gui=gui;
		
		/**
		 * create panels, buttons and listeners
		 */
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JButton jb1 = new JButton("Buy a permission card");
		JButton jb2 = new JButton("Build an emporium");
		JButton jb3 = new JButton("Build an emporium through king");
		JButton jb4 = new JButton("Add a councelor to a balcony");
		jb1.addActionListener(new ActionListener() {
		

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichPrimaryAction.this.dispose();
				string="BuyPermissionCard";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichPrimaryAction.this.dispose();
				string="EmporiumConstruction";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichPrimaryAction.this.dispose();
				string="ConstructionWithKing";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		jb4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WhichPrimaryAction.this.dispose();
				string="CouncelorElection";
				gui.setString(string);
				gui.wakeUp();
			}
			
		});
		
		/**
		 * frame layout
		 */
		this.setLayout(new BorderLayout());
		
		/**
		 * panels' layouts
		 */
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new GridLayout(4,1));
		
		
		/**
		 * set panels
		 */
		textPanel.add(new JLabel("Select a primary action"));
		
		buttonsPanel.add(jb1);
		buttonsPanel.add(jb2);
		buttonsPanel.add(jb3);
		buttonsPanel.add(jb4);
	
		/**
		 * set frame
		 */
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
	
		public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	

}