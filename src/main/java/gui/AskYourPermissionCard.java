package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * ask which permission card the player wants to use
 */
public class AskYourPermissionCard extends JFrame implements Runnable{
	String string;
	int integer;
	GUI gui;

	public static final long serialVersionUID = 1L;
	/**
	 * Creates an action listener that sets the ID of the permission cards shown
	 * as JButtonWithInteger. Then creates a buttons for every permission cards
	 * and adds them to a panel. Then sets the panel on the frame, in determinated Layouts.
	 * @param gui the gui
	 * @param cards the permission cards of the player
	 */
	public AskYourPermissionCard(GUI gui, ArrayList<String> cards){
		
		this.gui=gui;
		int i=0;
		
		JPanel textPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		
		   ActionListener btnaction = new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent ae) {
	                AskYourPermissionCard.this.dispose();
	            	gui.setInteger(((JButtonWithInteger) ae.getSource()).getInteger());
	                gui.wakeUp();
	            }
	        };
		for (String c:cards){
			String [] temp=c.split("#");
			BufferedImage img = null;
			try {
				try {
					img = ImageIO.read(new File(getClass().getClassLoader().getResource(temp[1]).toURI()));
				} catch (IOException e) {
				System.out.println("Unable to find file");
				}
			} catch (URISyntaxException e) {
				System.out.println("Unable to find file");
			}
			ImageIcon icon = new ImageIcon(img);
			Image image = icon.getImage(); 
			Image newimg = image.getScaledInstance(70, 70,  java.awt.Image.SCALE_SMOOTH); 
			icon = new ImageIcon(newimg); 
            JButtonWithInteger button =new JButtonWithInteger(icon);
            button.setInteger(i);
            i++;
            buttonsPanel.add(button);
            button.addActionListener(btnaction);
        }
	
		this.setLayout(new BorderLayout());
		
		textPanel.setLayout(new FlowLayout());
		buttonsPanel.setLayout(new FlowLayout());
		
		textPanel.add(new JLabel("Choose a permission card typing its id (from left to right starting from 0):"));
		
		this.add(textPanel,BorderLayout.NORTH);
		this.add(buttonsPanel,BorderLayout.SOUTH);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
	}
	


	public int getInteger() {
		return integer;
	}



	public void setInteger(int integer) {
		this.integer = integer;
	}



	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run() {
				
				}
			
			
		});
		
	}
	

}