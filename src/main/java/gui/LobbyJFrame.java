package gui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import servercompleto.ClientInHandler;
import servercompleto.ClientOutHandler;
import servercompleto.SubscriberRMI;


/**
 * class that shows the lobby
 *
 */
public class LobbyJFrame extends JFrame implements Runnable{
	
	ClientOutHandler clienOuHandler;
	SubscriberRMI subscriberRMI;
	private static final long serialVersionUID = 1L;
	
	/**
	 * constructor with strings and RMI subscriber 
	 * @param games are the games
	 * @param subscriberRMI is the subscriber using RIM
	 */
	public LobbyJFrame(ArrayList<String> games, SubscriberRMI subscriberRMI){
	
	this.subscriberRMI=subscriberRMI;	
	
	/**
	 * headers of the table
	 */
	String[] columnNames = {"Game"};
	
	/**
	 * creates the panels
	 */
	JPanel lobbyPanel = new JPanel();
	JPanel typePanel = new JPanel ();
	
	/**
	 * create the table
	 */
	JTable table = new JTable(new DefaultTableModel(columnNames, 0));
	DefaultTableModel model = (DefaultTableModel) table.getModel();
	
	for (String s:games){
		model.addRow(new Object[]{s});
	}
	
	/** 
	 * create the textField
	 */
	JTextField jTextField = new JTextField(10);
	jTextField.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					String string = jTextField.getText();
					LobbyJFrame.this.dispose();
					subscriberRMI.setChosenGame(string);
					subscriberRMI.wakeUp();
				}
	});
	
	/**
	 * set panels
	 */
	JPanel textPanel = new JPanel();
	JPanel textPanel2 = new JPanel();
	textPanel.add(new JLabel ("Available games:"));
	textPanel2.add(new JLabel ("Type the name of the game you want to enter:"));
	lobbyPanel.add(table);
	typePanel.add(jTextField);
	typePanel.requestFocus();
	
	/**
	 * add table to frame
	 */
	this.setLayout(new GridLayout(4,1));
	
	this.add(textPanel);
	this.add(lobbyPanel);
	this.add(textPanel2);
	this.add(typePanel);
	this.setTitle("Games");
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.pack();
	this.setLocationRelativeTo(null);
	this.setAlwaysOnTop(true);
	this.setResizable(false);
	this.setVisible(true);
	}
	
		/**
		 * Constructor with strings and clientinhandler
		 * @param games are the game
		 * @param clientOutHandler is the socket subscriber
		 */
		public LobbyJFrame(ArrayList<String> games, ClientOutHandler clientOutHandler){
		/**
		 * headers of the table
		 */
		String[] columnNames = {"Game"};
		
		/**
		 * creates the panels
		 */
		JPanel lobbyPanel = new JPanel();
		JPanel typePanel = new JPanel ();
		
		/**
		 * create the table
		 */
		JTable table = new JTable(new DefaultTableModel(columnNames, 0));
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		for (String s:games){
			model.addRow(new Object[]{s});
		}
		
		/** 
		 * create the textField
		 */
		JTextField jTextField = new JTextField(10);
		jTextField.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						String string = jTextField.getText();
						LobbyJFrame.this.dispose();
						clientOutHandler.setChosenGame(string);
						clientOutHandler.wakeUp();
					}
		});
		
		/**
		 * set panels
		 */
		JPanel textPanel = new JPanel();
		JPanel textPanel2 = new JPanel();
		textPanel.add(new JLabel ("Available games:"));
		textPanel2.add(new JLabel ("Type the name of the game you want to enter:"));
		lobbyPanel.add(table);
		typePanel.add(jTextField);
		typePanel.requestFocus();
		/**
		 * add table to frame
		 */
		this.setLayout(new GridLayout(4,1));
		
		this.add(textPanel);
		this.add(lobbyPanel);
		this.add(textPanel2);
		this.add(typePanel);
		this.setTitle("Games");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setResizable(false);
		this.setVisible(true);
		}
	
		@Override
		public void run() {
			SwingUtilities.invokeLater(new Runnable(){
				
				@Override
				public void run() {
					
					}
				
				
			});
			
		}
}