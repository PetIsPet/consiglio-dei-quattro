package gamemanagement;

import java.awt.Color;
import java.util.ArrayList;

import mappaeoggetti.Card;
import mappaeoggetti.City;
import mappaeoggetti.Emporium;
import mappaeoggetti.PermissionCard;
import mvc.CLI;

public class Player {
	
	private int id;                     //identificativo
	private int points;                 //punti del giocatore
	private int coins;                
	private int nobilityPoints;
	private int assistants;
	private ArrayList<Card> cards=new ArrayList<Card>();             //carte in mano
	private int builtEmporiums;
	private ArrayList<PermissionCard> permits=new ArrayList<PermissionCard>();
	private Color color;
	private CLI cli;
	private boolean isActive=true;
	
	
	/**
	 * 
	 * @param id is the player's ID
	 * @param color is the player's color
	 * @param cli is current cli
	 */
public Player(int id, Color color, CLI cli){
		
		this.id=id;
		this.setPoints(0);
		this.setCoins(0);
		this.setNobilityPoints(0);
		this.setAssistants(0);
		this.builtEmporiums=0;
		this.setColor(color);
		this.setCli(cli);
		this.permits=new ArrayList<>();
		this.cards=new ArrayList<>();
	}

	/**
	 * @return the builtEmporiums
	 */
	public int getBuiltEmporiums() {
		return builtEmporiums;
	}


	/**
	 * @param builtEmporiums the builtEmporiums to set
	 */
	public void setBuiltEmporiums(int builtEmporiums) {
		this.builtEmporiums = builtEmporiums;
	}
	
	
	public boolean hasFinished(){     //controlla se il giocatore ha raggiunto il massimo di empori
		
		if (builtEmporiums==10) {
			return true;
		}
		return false;
	}


	public ArrayList<Card> getCards() {
		return cards;
	}


	public void setCards(ArrayList<Card> cards) {
		this.cards = cards;
	}


	public ArrayList<PermissionCard> getPermits() {
		return permits;
	}


	public void setPermits(ArrayList<PermissionCard> permits) {
		this.permits = permits;
	}


	public int getAssistants() {
		return assistants;
	}


	public void setAssistants(int assistants) {
		this.assistants = assistants;
	}


	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id=id;
	}


	public CLI getCli() {
		return cli;
	}


	public void setCli(CLI cli) {
		this.cli = cli;
	}


	public int getCoins() {
		return coins;
	}


	public void setCoins(int coins) {
		this.coins = coins;
	}


	public int getNobilityPoints() {
		return nobilityPoints;
	}


	public void setNobilityPoints(int nobilityPoints) {
		this.nobilityPoints = nobilityPoints;
	}


	public int getPoints() {
		return points;
	}


	public void setPoints(int points) {
		this.points = points;
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}

	public boolean hasBuilt(City city) {
		for(Emporium e: city.getEmporiums()){
			
			if(e.getPlayer()==this)
				return true;
		}
			
		return false;
	}

	public boolean isActive() {
		return this.isActive;
	}
	
	public void setInactive(){
		this.isActive=false;
	}
	

}
