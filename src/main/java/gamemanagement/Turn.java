package gamemanagement;

import java.util.ArrayList;
import java.util.List;

import actions.PrimaryAction;
import actions.SecondaryAction;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;

public class Turn {

	private List<PrimaryAction> primaryActionPool;
	private Player player;
	
	/**
	 * 
	 * @param player is the current player
	 */
	public Turn(Player player){
		
		this.setPlayer(player);
		this.primaryActionPool=new ArrayList<>();
	}

	/**
	 * executes primary action
	 * @throws NotEnoughResourcesException 
	 * @throws InvalidInputException 
	 */
	public void primaryAction() throws NotEnoughResourcesException, InvalidInputException{
		
		for(PrimaryAction a: primaryActionPool) {
			a.doPrimaryAction(); 
			
		}
		
	}
	
	/**
	 * executes a secondary action
	 * @param action is the secondary action 
	 * @throws NotEnoughResourcesException 
	 * @throws InvalidDoAnotherPrimaryActionChoiceException 
	 */
	public void secondaryAction(SecondaryAction action) throws NotEnoughResourcesException, InvalidDoAnotherPrimaryActionChoiceException{
		
			action.doSecondaryAction();
			
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public List<PrimaryAction> getPrimaryActionPool() {
		return primaryActionPool;
	}
	
	public void addActionToPool(PrimaryAction a){
		this.primaryActionPool.clear();
		this.primaryActionPool.add(a);
	}

	public void setPrimaryActionPool(List<PrimaryAction> primaryActionPool) {
		this.primaryActionPool = primaryActionPool;
	}
	
}
