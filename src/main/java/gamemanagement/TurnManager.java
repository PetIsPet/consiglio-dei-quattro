package gamemanagement;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import actions.BuyPermissionCard;
import actions.ChangePermissionCard;
import actions.ConstructionWithKing;
import actions.CouncelorElection;
import actions.DoAnotherPrimaryAction;
import actions.DoNothing;
import actions.EmployAssistant;
import actions.EmporiumConstruction;
import actions.PrimaryAction;
import actions.SecondaryAction;
import actions.SendAssistantToCouncil;
import bonuses.AssignColorAndRegionPoints;
import bonuses.Bonus;
import exceptions.InvalidDoAnotherPrimaryActionChoiceException;
import exceptions.InvalidInputException;
import exceptions.NoPlayersLeftException;
import exceptions.NotEnoughResourcesException;
import exceptions.OnePlayerLeftException;
import exceptions.VoidMarketException;
import gamemanagement.Player;
import gamemanagement.RegularTurn;
import gamemanagement.Turn;
import mappaeoggetti.Assistant;
import mappaeoggetti.Balcony;
import mappaeoggetti.Card;
import mappaeoggetti.City;
import mappaeoggetti.Councelor;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import mappaeoggetti.Type;
import market.Market;
import market.MarketStuff;
import market.Purchase;
import market.Sale;
import market.Saleable;
import mvc.Game;
import servercompleto.Publisher;
import servercompleto.ServerView;



public class TurnManager{

	
	private Set<ServerView> clients;
	private Game game;
	private Publisher publisher;
	private AssignColorAndRegionPoints bonus=new AssignColorAndRegionPoints(game);
	
	
	
	public TurnManager(Set<ServerView> set, Game game){
		
		this.clients=set;
		this.setGame(game);
		this.publisher=new Publisher(this);
		
	}
	
	/**
	 * a method to assign a RegularTurn to every subscriber, followed by the market phase
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 * @throws RemoteException
	 */
	public void assignRegularTurn() throws NoPlayersLeftException {
		
		try{
		this.sendCityTokenPaths();
		this.sendMapConfiguration();
		this.publisher.notifyStartingSituation(this.game);
		this.publisher.notifyCLIMapSituation(game);
		this.publisher.notifyGameHasStarted();
		
		
		
		
		
		while(!this.hasFinished()){
			
			for(Player p: game.getPlayers()){ //associo al player il client
				try{
				ServerView s = null;
				try {
					s = this.matchPlayerWithView(p);
				} catch (RemoteException | NoSuchElementException e1) {
					this.removeFromSubscription(s);
				}
				
				
				
						RegularTurn turn=new RegularTurn(p);  //crea turno per ogni giocatore
						
						
						
						publisher.sendDrawnCard(s, this.colorToString(game.getCardDeck().drawCard(p).getColor()));
						
						
					    boolean choice=true;
						try {
							choice = s.primaryActionFirst();
						} catch (RemoteException | NoSuchElementException e1) {
							this.removeFromSubscription(s);
						}
						
					    
						if(choice) {   //decide se eseguire prima primaria o secondaria
							
							try {
								executePrimaryAction(turn,s,p);
								if(this.hasFinished()) break;
								executeSecondaryAction(turn,s,p);
							} catch (RemoteException | NoSuchElementException e1) {
								this.removeFromSubscription(s);
							}
							
							}
						else{
							
							try {
								executeSecondaryAction(turn,s,p);
								executePrimaryAction(turn,s,p);
								if(this.hasFinished()) break;
							} catch (RemoteException | NoSuchElementException e1) {
								this.removeFromSubscription(s);
							}
							
							
						}
						
					}
				catch(NullPointerException e2){
					continue;
						}
			
			}
				Market market=new Market();   //parte la fase di market
				
				
				
				for(Player p: game.getPlayers()){    //fase di vendita
					try{
						
						ServerView s = null;
						try {
							s = this.matchPlayerWithView(p);
						} catch (RemoteException | NoSuchElementException e1) {
							this.removeFromSubscription(s);
						}
						
						
						Sale sale=new Sale(p);
						
						
						
						
						Saleable product;
						try {
							product = this.parseSaleable(s.getProductToSell( p.getAssistants(), this.permitsToString(p.getPermits()), this.playerCardsToString(p.getCards())),p);
							if (product!=null){
								int price = s.getPrice();
					
								sale.act(product, price, market);
							}
						} catch (RemoteException | NoSuchElementException e1) {
							this.removeFromSubscription(s);
						
						
						
						}
						
				}catch(NullPointerException e2){
					
							continue;
								}	
					
				}
			
				if(!market.getShowcase().isEmpty()){
				
				
					for(Player p: game.getPlayers()){    //fase di acquisto
						try{
						ServerView s=null;
							try {
								s = this.matchPlayerWithView(p);
							} catch (RemoteException | NoSuchElementException e1) {
								this.removeFromSubscription(s);
							}
							
							
						try {
							
							Purchase purchase=new Purchase(p,game.getPlayers());
							int index;
							try {
								index = s.getProductToPurchase(this.marketToString(market));
								purchase.act(market, index);
								market.getShowcase().remove(index);
							
							} catch (VoidMarketException e) {
								e.printStackTrace();
								continue;
							}
							
						} catch (RemoteException | NoSuchElementException e1) {
							this.removeFromSubscription(s);
							
						} 
						
						
						
						}catch(NullPointerException e2){
							
							continue;
							
								}
					}
				
					
					publisher.sendGameStats(game);
					publisher.sendYourPoliticCards(game);
					publisher.sendPlayersPermits(game);
					publisher.notifyRefresh();
				
			}
			
			
			}
		
		assignFinalTurn();
			
		}
		
		catch(OnePlayerLeftException e){
			
			this.publisher.notifyWinner(e.getPlayer());
		}
		
		
		
	}
	
	/**
	 * a method to assign the final turn
	 * @throws OnePlayerLeftException 
	 * @throws NoPlayersLeftException 
	 * @throws RemoteException
	 */
	public void assignFinalTurn() throws OnePlayerLeftException, NoPlayersLeftException  {
		
		ArrayList<Player> playersFinalTurn=new ArrayList<>();
		
		for(Player p:game.getPlayers()){
			if(!p.hasFinished()) playersFinalTurn.add(p);
		}
		
		
		Iterator<ServerView> itr=clients.iterator();
		
		for(Player p: playersFinalTurn){
			
			ServerView s=itr.next(); //associo al player il client
			
				if(!(p.hasFinished())){
				RegularTurn turn=new RegularTurn(p);  //crea turno per ogni giocatore
				
					try {
						if(s.primaryActionFirst()) {   //decide se eseguire prima primaria o secondaria
							
							executePrimaryAction(turn,s,p);
							executeSecondaryAction(turn,s,p);
							}
						else{
							
							executeSecondaryAction(turn,s,p);
							executePrimaryAction(turn,s,p);
						}
					} catch (RemoteException | NoSuchElementException e1) {
						this.removeFromSubscription(s);
					}
				}
			
			}
		
		
		
		this.publisher.notifyWinner(this.hasWinTheGame());
		
	}
	
	/**
	 * a method to control if a player has reached the limit of built emporiums
	 * @return
	 */
	public boolean hasFinished(){
		
		for(Player p:game.getPlayers()){
			if(p.hasFinished()) return true;
		}
		return false;
	}
	
	/**
	 * a method to find the winner of the game
	 * @return the winner
	 * @throws NullPointerException
	 */
	public Player hasWinTheGame() throws NullPointerException{
	
		ArrayList<Player> players=game.getPlayers();
		int maxPoints=0;
		int maxPoints2=0;
		for(Player p:players){
			
			if(p.getNobilityPoints()>maxPoints) maxPoints=p.getNobilityPoints();
			
		}
		
		ArrayList<Player> first=new ArrayList<>();
		
		
		for(Player p:players){    //assegno bonus finale punti nobiltà
			
			if(p.getNobilityPoints()==maxPoints) first.add(p);
		}
		
		if(first.size()>1) {
			
			for(Player p:first){
				p.setPoints(p.getPoints()+5);
			}
		}
		else {
			
			for(Player p:players){
				if(p.getNobilityPoints()!=maxPoints){
					if(p.getNobilityPoints()>maxPoints2) maxPoints2=p.getNobilityPoints();
				}
				
			}
			for(Player p:players){
				if(p.getNobilityPoints()==maxPoints) p.setPoints(p.getPoints()+5);
				if(p.getNobilityPoints()==maxPoints2) p.setPoints(p.getPoints()+2);
			}
		}
		
		int maxPermits=0; //assegno bonus finale numero di permessi
		
		for(Player p:game.getPlayers()){
			
			if(p.getPermits().size()>maxPermits) maxPermits=p.getPermits().size();
		}
		
		for(Player p:game.getPlayers()){
			if(p.getPermits().size()==maxPermits) p.setPoints(p.getPoints()+3);
		}
		
		
		for(Player p:bonus.getMap().keySet()){    //assegno bonus costruzione
			Object temp=bonus.getMap().get(p);
			if(temp.equals(Color.DARK_GRAY)) p.setPoints(p.getPoints()+5);
			if(temp.equals(Color.YELLOW)) p.setPoints(p.getPoints()+20);
			if(temp.equals(Color.GRAY)) p.setPoints(p.getPoints()+12);
			if(temp.equals(Color.ORANGE)) p.setPoints(p.getPoints()+5);
			if(temp.equals(Type.MOUNTAIN)) p.setPoints(p.getPoints()+5);
			if(temp.equals(Type.PLAIN)) p.setPoints(p.getPoints()+5);
			if(temp.equals(Type.SEASIDE)) p.setPoints(p.getPoints()+5);
		}
		
		 
		for(Player p:bonus.getConstructionOrder()){  //assegno bonus del re
			switch(bonus.getConstructionOrder().indexOf(p)){
			case 0:
				p.setPoints(p.getPoints()+25);
			case 1:
				p.setPoints(p.getPoints()+18);
			case 2:
				p.setPoints(p.getPoints()+12);
			case 3:
				p.setPoints(p.getPoints()+7);
			case 4:
				p.setPoints(p.getPoints()+3);
			}
		}
		 
		
		
		Player winner=new Player(0,null,null);
		int pointsMax=0;
		for(Player p:game.getPlayers()){

		 if(p.getPoints()>pointsMax) {
		  pointsMax=p.getPoints();
		  winner=p;
		 }
		}

		return winner;
				
		}

	
	/**
	 * a method to ask and execute the primary actions of the turn
	 * @param turn
	 * @param s
	 * @param p
	 * @throws RemoteException
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 */
	public void executePrimaryAction(Turn turn,ServerView s,Player p) throws RemoteException, OnePlayerLeftException, NoPlayersLeftException{  //si occupa di aggiungere l'azione al pull e di eseguirla
		PrimaryAction action=parsePrimaryAction(s.askPrimaryAction(),p,s);
		
		turn.addActionToPool(action); 
		try {
			turn.primaryAction();
			publisher.notifyPrimaryAction(action, s.getId(),this.game);
		} catch (NotEnoughResourcesException|InvalidInputException  e) {
			
			publisher.notifyException(e, s);
			executePrimaryAction(turn,s,p);
		} 
		
	}
	
	/**
	 * a method to ask and execute the secondary action
	 * @param turn
	 * @param s
	 * @param p
	 * @throws RemoteException
	 * @throws NoPlayersLeftException 
	 * @throws OnePlayerLeftException 
	 */
	public void executeSecondaryAction(Turn turn,ServerView s,Player p) throws RemoteException, OnePlayerLeftException, NoPlayersLeftException{  //si occupa di chiedere l'azione secondaria e eseguirla
		
		SecondaryAction action=parseSecondaryAction(s.askSecondaryAction(),p,s,turn);
		
		try {
			
			turn.secondaryAction(action);
			if(action.getClass()==ChangePermissionCard.class) {
				ChangePermissionCard action2=(ChangePermissionCard) action;
				for(PermissionDeck deck:game.getDecks()){
					
					int i=game.getDecks().indexOf(deck);
					
					if(deck.getType()==action2.getDeck().getType())
						game.getDecks().set(i, action2.getDeck());
				}
			}
			publisher.notifySecondaryAction(action, s.getId(),this.getGame());
		} catch (NotEnoughResourcesException | InvalidDoAnotherPrimaryActionChoiceException e) {
			e.printStackTrace();
			
			publisher.notifyException(e, s);
			executeSecondaryAction(turn,s,p);
		} 
		
	
	}
	
	


	/**
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * setter for the game
	 * @param game
	 */
	public void setGame(Game game) {
		this.game = game;
	}


	/**
	 * 
	 * @param name
	 * @param p
	 * @param s
	 * @return the primary action parsed from the string received from the view of the subscriber
	 * @throws RemoteException
	 */
	public PrimaryAction parsePrimaryAction(String name, Player p,ServerView s ) throws RemoteException{
		
		
		if("BuyPermissionCard".equals(name)){
			PermissionDeck deck=parseDeck(s.getChosenDeck());
			ArrayList<PermissionCard> discovered=new ArrayList<>();
			for(int h=0; h<deck.getDiscoveredCards().length; h++){
				
				discovered.add(deck.getDiscoveredCards()[h]);
			}
			int index=s.getPermissionCard(this.discoveredCardsToString(discovered));
			
			
			BuyPermissionCard action= new BuyPermissionCard(p,deck,index,matchBalconyWithDeck(deck),game.getDecks(),s, this);
			
			action.createMatchPool();
			
			ArrayList<Card> cardMatches=action.getMatches();
		
			ArrayList<Card> matchesToUse=new ArrayList<>();
			ArrayList<String> stringMatches=s.matchToUse(playerCardsToString(cardMatches));
			
			Map <Card,Boolean> set=new HashMap<>();
			for(Card c:cardMatches){
				set.put(c, false);
			}
			
			for(String m:stringMatches){
					
				for(Card c:cardMatches){
					if(this.colorToString(c.getColor()).equals(m)){
						if(!set.get(c)){
							set.put(c, true);
							matchesToUse.add(c);
							break;
						}
					}
				}
					
			}
			
			action.setMatches(matchesToUse);
			return action;
			
		}
		
		if("EmporiumConstruction".equals(name)){
			if(p.getPermits().isEmpty()){
				s.notifyActions("You can't! You have no Permission Cards!");
				return parsePrimaryAction(s.askPrimaryAction(),p,s);
				
			}
			/**
			 * choose the PermissionCard
			 */
			int k=s.askYourPermissionCard(permitsToString(p.getPermits()));
			/**
			 * choose the city where you want to build
			 */
			String cityName=s.askCityPermit(citiesToString(p.getPermits().get(k).getBuildPermissions()));
			
			game.getUsedPermits().add(p.getPermits().remove(k));
			
			return new EmporiumConstruction(p,parseCity(cityName),game.getDecks(),s, this);
		}
		
		if("ConstructionWithKing".equals(name)){
			
			Balcony b=game.getKingBalcony();
			String response=s.getKingMovement(citiesToString(game.getCities()),game.getKing().getCurrentCity().getName());
			String[] temp=response.split("#");
			City target=this.parseCity(temp[0]);
			
			int price=Integer.parseInt(temp[1]);
			ConstructionWithKing action=new ConstructionWithKing(game.getKing(),p,game.getMap(),b,target, null);
			action.setPrice(price);
			action.createMatchPool();
			ArrayList<Card> cardMatches=action.getMatches();
			ArrayList<Card> matchesToUse=new ArrayList<>();
			ArrayList<String> stringMatches=s.matchToUse(playerCardsToString(cardMatches));
			Iterator<Card> itr=cardMatches.iterator();
			Map <Card,Boolean> set=new HashMap<>();
			for(Card c:cardMatches){
				set.put(c, false);
			}
			
			for(String m:stringMatches){
					
				for(Card c:cardMatches){
					if(this.colorToString(c.getColor()).equals(m)){
						if(!set.get(c)){
							set.put(c, true);
							matchesToUse.add(c);
							break;
						}
					}
				}
					
			}
			
			action.setMatches(matchesToUse);
			
			return action;
		}
		
		if("CouncelorElection".equals(name)){
			int response=s.getChosenBalconyPlusKing();
			Balcony b=null;
			if(response==3) b=game.getKingBalcony();
			else{
				b=game.getBalconies().get(response);
			}
				ArrayList<Color> colors=new ArrayList<>();
				colors.addAll(game.getCardColors());
				colors.remove(Color.YELLOW);
				Color color=s.getChosenColor(colors);
				
				for(Councelor c:game.getCouncelorsPool()){
					
					if(c.getColor()==color){
						game.getCouncelorsPool().remove(c);
						break;
					}
				}
				return new CouncelorElection(p,color,b);
			
		}
		
		return null;
	}
	
	
	
	/**
	 * 
	 * @param name
	 * @param p
	 * @param s
	 * @param t
	 * @return the secondary action parsed from the view of the subscriber
	 * @throws RemoteException
	 */
	public SecondaryAction parseSecondaryAction(String name, Player p,ServerView s,Turn t) throws RemoteException{
		
		if("ChangePermissionCard".equals(name)){
			
			return new ChangePermissionCard(p,parseDeck(s.getChosenDeck()));
		}
		
		if("DoAnotherPrimaryAction".equals(name)){
			
			return new DoAnotherPrimaryAction(p,parsePrimaryAction(s.askPrimaryAction(),p,s),t);
		}
		
		if("EmployAssistant".equals(name)){
			
			return new EmployAssistant(p);
		}
		
		if("DoNothing".equals(name)) {
			
			return new DoNothing();
		}
		
		if("SendAssistantToCouncil".equals(name)){
			int response=s.getChosenBalconyPlusKing();
			Balcony b=null;
			if(response==3) b=game.getKingBalcony();
			else{
				b=game.getBalconies().get(response);
			}
				ArrayList<Color> colors=new ArrayList<>();
				colors.addAll(game.getCardColors());
				colors.remove(Color.YELLOW);
				Color color=s.getChosenColor(colors);
				for(Councelor c:game.getCouncelorsPool()){
					
					if(c.getColor()==color){
						game.getCouncelorsPool().remove(c);
						break;
					}
				}
				return new SendAssistantToCouncil(p,b,color);
			
		}
		return null;
		
	}
	
	
	/**
	 * 
	 * @param product
	 * @param p
	 * @return the saleable parsed from an arraylist which is the product to sell
	 */
	public Saleable parseSaleable(ArrayList<String> product, Player p){
		
		if("ASSISTANT".equals(product.get(0))) 
			return new Assistant();
		if("PERMISSION CARD".equals(product.get(0))){
			
			String[] temp=product.get(1).split("-");
			String[] temp1=temp[0].split(":");
			
			int id=Integer.parseInt(temp1[1]);
			for(PermissionCard card:p.getPermits()){
				
				if(p.getPermits().indexOf(card)==id)
					return card;
			}
			
		}
		if("POLITIC CARD".equals(product.get(0))){
			
			for(Card card:p.getCards()){
				
				if(this.colorToString(card.getColor()).equals(product.get(1))) 
					return card;
			}
		}
		if("NIENTE".equals(product.get(0))){
			
			return null;
			
		}
		
		return null;
		
	}
	
	
	/**
	 * 
	 * @param name
	 * @return the city parsed from its name
	 */
	public City parseCity(String name){
		
		for(City c:game.getCities()){
			
			if(c.getName().equals(name)) 
				return c;
		}
		return null;
	}
	
	/**
	 * 
	 * @param index
	 * @return the deck from its index
	 */
	public PermissionDeck parseDeck(int index){
		
		return game.getDecks().get(index);
	}
	
	/**
	 * 
	 * @param list
	 * @return permits transformed into an arrayList of string meant to be sent to a subscriber's view
	 */
	public ArrayList<String> permitsToString(ArrayList<PermissionCard> list){
		
		ArrayList<String> permits=new ArrayList<>();
		
		for(PermissionCard c:list){
			String card="ID:"+list.indexOf(c)+"-"+"PERMESSI:";
			for(City n:c.getBuildPermissions()){
			
				card=card+n.getName()+"-";
			
			}
			card=card+"#"+c.getImgPath();
			permits.add(card);
		}
		
		return permits;
	}
	
	
	public ArrayList<String> discoveredCardsToString(ArrayList<PermissionCard> list){
		
		ArrayList<String> permits=new ArrayList<>();
		
		for(PermissionCard c:list){
			String card="";
			card="PERMITS: ";
			for(City n:c.getBuildPermissions()){
				card=card+n.getName()+" ";
			}
			card=card+"BONUS: ";
			for(Bonus m:c.getBonus()){
				card=card+m.getName()+" ";
			}
			card=card+"ID: "+list.indexOf(c);
			card=card+"#"+c.getId();
			permits.add(card);
		}
		
		return permits;
	}
	
	/**
	 * 
	 * @param cities
	 * @return an arrayList with the names of the cities
	 */
	public ArrayList<String> citiesToString(List<City> cities){
		
		ArrayList<String> names=new ArrayList<>();
		
		for(City c:cities){
			
			String name=c.getName()+"#";
			for(City v:c.getNeighborhood()){
				name=name+v.getName()+"-";
			}
			names.add(name);
		}
		return names;
	}
	
	
	/**
	 * @param the deck
	 * @return the balcony matched with the deck
	 */
	public Balcony matchBalconyWithDeck(PermissionDeck deck){
		
		for(Balcony b:game.getBalconies()){
			if(game.getBalconies().indexOf(b)==game.getDecks().indexOf(deck)){
				
				return b;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param market
	 * @return the market transformed into an arraylist of strings ready to be printed
	 */
	public ArrayList<String> marketToString(Market market){
		
		ArrayList<String> showcase=new ArrayList<>();
		for(MarketStuff stuff:market.getShowcase()){
			
			if(stuff.getProduct().getClass().equals(Assistant.class)){
				
				String owner="OWNER: PLAYER"+stuff.getOwner().getId();
				String type="TYPE: ASSISTANT";
				String price="PRICE: "+stuff.getPrice();
				String id="PRODUCTID: "+market.getShowcase().indexOf(stuff);
				showcase.add(owner+" "+type+" "+price+" "+id);
			}
			
			if(stuff.getProduct().getClass().equals(Card.class)){
				
				String owner="OWNER: PLAYER"+stuff.getOwner().getId();
				String type="TYPE: POLITICCARD";
				Card card=(Card) stuff.getProduct();
				String color="COLOR: "+this.colorToString(card.getColor());
				String price="PRICE: "+stuff.getPrice();
				String id="PRODUCTID: "+market.getShowcase().indexOf(stuff);
				showcase.add(owner+" "+type+" "+price+" "+id+" "+color);
			}
			
			if(stuff.getProduct().getClass().equals(PermissionCard.class)){
				
				String owner="OWNER: PLAYER"+stuff.getOwner().getId();
				String type="TYPE: PERMISSIONCARD";
				PermissionCard card=(PermissionCard) stuff.getProduct();
				String permits="PERMITS: ";
				for(City c:card.getBuildPermissions()){
					permits=permits+c.getName()+",";
				}
				String price="PRICE: "+stuff.getPrice();
				String id="PRODUCTID: "+market.getShowcase().indexOf(stuff);	
				String imgpath="#"+card.getImgPath();
				showcase.add(owner+" "+type+" "+price+" "+id+" "+permits+imgpath);
			}
		}
		return showcase;
	}
	
	/**
	 * 
	 * @param s
	 * @return a string which represents the type of the saleable
	 */
	public String saleableToString(Saleable s){
		
		if(s.isAssistant()) 
			return "ASSISTANT";
		if(s.isCard()) 
			return "POLITIC CARD";
		if(s.isPermissionCard()) 
			return "PERMISSION CARD";
		return null;
	}
	/**
	 * 
	 * @param list
	 * @return an arrayList of strings that represents the cards
	 */
	public ArrayList<String> playerCardsToString(ArrayList<Card> list){
		
		ArrayList<String> cards=new ArrayList<>();
		
		for(Card c:list){
			
			String color=colorToString(c.getColor());
			cards.add(color);
			
		}
		return cards;
	}
	
	/**
	 * sends cityToken paths to the players
	 * @throws OnePlayerLeftException 
	 * @throws NoPlayersLeftException 
	 * 
	 */
	public void sendCityTokenPaths() throws OnePlayerLeftException, NoPlayersLeftException {
		
		ArrayList<String> paths=new ArrayList<>();
		for(City c:game.getCities()){
			
			paths.add(c.getCityToken().getPath());
		}
		
		for(ServerView s:clients){
			
				try {
					s.sendCityTokenPath(paths);
				} catch (RemoteException | NoSuchElementException e1) {
					this.removeFromSubscription(s);
					
				}
			}
		}
	
	
	public void sendMapConfiguration() throws OnePlayerLeftException, NoPlayersLeftException {
		
		for(ServerView s:clients){
			
				try {
					s.sendMapConfiguration(game.getMapConfiguration());
				} catch (RemoteException | NoSuchElementException e1) {
					this.removeFromSubscription(s);
				
			}
		}
	}
	

	
	
 	
	
	public String colorToString(Color c){
		
		if (c.equals(Color.YELLOW)) return "yellow";
		if (c.equals(Color.MAGENTA)) return "magenta";
		if (c.equals(Color.BLUE)) return "blue";
		if (c.equals(Color.BLACK)) return "black";
		if (c.equals(Color.WHITE)) return "white";
		if (c.equals(Color.PINK)) return "pink";
		if (c.equals(Color.ORANGE)) return "orange";
		if (c.equals(Color.GREEN)) return "green";
		return null;
	}
	
	
	public AssignColorAndRegionPoints getBonus() {
		return bonus;
	}

	public void setBonus(AssignColorAndRegionPoints bonus) {
		this.bonus = bonus;
	}

	public Color stringToColor(String s){
		if ("giallo".equals(s)) return Color.YELLOW;
		if ("magenta".equals(s)) return Color.MAGENTA;
		if ("blu".equals(s)) return Color.BLUE;
		if ("nero".equals(s)) return Color.BLACK;
		if ("bianco".equals(s)) return Color.WHITE;
		if ("rosa".equals(s)) return Color.PINK;
		if ("arancione".equals(s)) return Color.ORANGE;
		if ("verde".equals(s)) return Color.GREEN;
		return null;
	}
	
	public ServerView matchPlayerWithView (Player p) throws RemoteException{
		for(ServerView s:this.clients){
			if(s.getId()==p.getId()) return s;
		}
		return null;
	}
	
	/**
	 * @return the clients
	 */
	public Set<ServerView> getClients() {
		return clients;
	}
	
	/**
	 * @return the publisher
	 */
	public Publisher getPublisher() {
		return publisher;
	}

	public void removeFromSubscription(ServerView s) throws OnePlayerLeftException, NoPlayersLeftException {
		
		this.clients.remove(s);
		if(this.clients.size()==1){
			OnePlayerLeftException e;
			try {int playerId=0;
				for(ServerView last:this.clients){
					playerId=last.getId();
				}
				
				e = new OnePlayerLeftException(this.game.getPlayers().get(playerId));
			} catch (RemoteException | NoSuchElementException e1) {
				NoPlayersLeftException e2=new NoPlayersLeftException();
				throw e2;
			}
			throw e;
		}
	}
	
	public Player matchViewWithPlayer(ServerView s) throws RemoteException{
		for(Player p:this.game.getPlayers()){
			if(p.getId()==s.getId()) return p;
		}
		return null;
	}

	
}
