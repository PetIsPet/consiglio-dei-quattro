package gamemanagement;

public class MarketTurn {
	
	private Player player;
	
	/**
	 * 
	 * @param player is the current player
	 */
	public MarketTurn(Player player){
		
		this.setPlayer(player);
	}
	
	public void act(){}
	
	/**
	 * 
	 * @return player
	 */
	public Player getPlayer() {
		return player;
	}

		/**
		 * action overriding in sale and purchase
		 * @param player is the player
		 */
	public void setPlayer(Player player) {
		this.player = player;
	}; 
	
}
