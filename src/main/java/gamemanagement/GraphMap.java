package gamemanagement;

import java.util.ArrayList;
import java.util.List;
import org.jgrapht.graph.SimpleGraph;

import file.Lettore;
import mappaeoggetti.City;
import org.jgrapht.alg.*;

public class GraphMap {
	
	private SimpleGraph<City, String> graph;
	private List<City> poolCities=new ArrayList<City>();
	
	
	public GraphMap(Lettore lettore){
		
		this.poolCities = lettore.getPoolCities();
		this.graph=new SimpleGraph<>(String.class);
	}
	
	
	/**
	 * @return the graph
	 */
	public  SimpleGraph<City, String> getGraph() {
		return graph;
	}


	/**
	 * @param graph the graph to set
	 */
	public void setGraph(SimpleGraph<City, String> graph) {
		this.graph = graph;
	}

	/**
	 * creates the graph
	 */
	public void run(){
		
		for(City c : poolCities){
			
			graph.addVertex(c);
		}
		
		for(City c : poolCities){
			
			for(City n : c.getNeighborhood()){
				
				graph.addEdge(c, n);
			}
		}
			
	}
	

	
	
	
	
}
