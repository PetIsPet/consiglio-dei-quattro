package exceptions;

import gamemanagement.Player;

public class OnePlayerLeftException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Player player;
	
	
	public OnePlayerLeftException(Player player){
		this.setPlayer(player);
	}


	public Player getPlayer() {
		return player;
	}


	public void setPlayer(Player player) {
		this.player = player;
	}

}
