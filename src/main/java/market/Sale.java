package market;

import gamemanagement.MarketTurn;
import gamemanagement.Player;

public class Sale extends MarketTurn {

	
	

	public Sale(Player player) {
		super(player);
		
	}

	/**
	 * sell a product
	 * @param product is the product
	 * @param price is the price of the product
	 * @param market is the market
	 */
	public void act(Saleable product, int price, Market market){  
		
		MarketStuff marketStuff=new MarketStuff(price,this.getPlayer(),product);
		market.getShowcase().add(marketStuff);
		
		
	}
    

	
}
