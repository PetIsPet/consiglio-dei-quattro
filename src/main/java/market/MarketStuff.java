package market;

import gamemanagement.Player;

public class MarketStuff {
	
	private int price;
	private Player owner;
	private Saleable product;
	
	public MarketStuff(int price, Player owner, Saleable product){
		
		this.price=price;
		this.owner=owner;
		this.product=product;;
	}

	public Saleable getProduct() {
		return product;
	}

	public void setProduct(Saleable product) {
		this.product = product;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
