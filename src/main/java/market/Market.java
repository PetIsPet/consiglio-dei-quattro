package market;

import java.util.ArrayList;


public class Market {
	
	private ArrayList<MarketStuff> showCase;    //vetrina del market
	
	public Market(){
		
		this.showCase=new ArrayList<>();
	}

	public ArrayList<MarketStuff> getShowcase() {
		return showCase;
	}

	public void setShowcase(ArrayList<MarketStuff> showcase) {
		this.showCase = showcase;
	}
	
	

}
