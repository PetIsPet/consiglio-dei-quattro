package market;


import java.util.ArrayList;

import gamemanagement.MarketTurn;
import gamemanagement.Player;
import mappaeoggetti.Assistant;
import mappaeoggetti.Card;
import mappaeoggetti.PermissionCard;

public class Purchase extends MarketTurn {

	ArrayList<Player> players=new ArrayList<>();
	
	public Purchase(Player player,ArrayList<Player> players) {
		super(player);
		this.players.addAll(players);
		
	}
	/**
	 * purchase the product by index
	 * @param market is the market
	 * @param index is the index of the product
	 */
	public void act(Market market, int index){   
		
		purchase(market.getShowcase().get(index).getProduct(),market,index);
	}

/**
 * generic purchase method
 * @param product is the product
 * @param market is the market
 * @param index of the product
 */
   public void purchase(Saleable product, Market market,int index){
	  
	   
	   if(product.getClass()==Assistant.class){ 
		   Assistant a=(Assistant) product;
		   this.purchase(a,market,index);
		   }
	   if(product.getClass()==Card.class) {
		   Card card=(Card) product;
		   this.purchase(card,market, index);
	   }
	   if(product.getClass()==PermissionCard.class) {
		   PermissionCard card=(PermissionCard) product;
		   this.purchase(card,market, index);
	   }
   }; 
	/**
	 * purchase a PoliticCard
	 * @param card is the card
	 * @param market is the market
	 * @param index index of the product
	 */
	public void purchase(Card card, Market market, int index){
	   super.getPlayer().getCards().add(card);
	   Player p=this.matchOwner(market.getShowcase().get(index).getOwner().getId());
	   int price=market.getShowcase().get(index).getPrice();
	   p.getCards().remove(card);
	   p.setCoins(p.getCoins()+price);
	   super.getPlayer().setCoins(super.getPlayer().getCoins()-price);
	  
   }
   /**
    * purchase a PermissionCard
    * @param card is the card
    * @param market is the market
    * @param index is the index of the product
    */
   public void purchase(PermissionCard card, Market market, int index){
	   super.getPlayer().getPermits().add(card);
	   Player p=this.matchOwner(market.getShowcase().get(index).getOwner().getId());
	   int price=market.getShowcase().get(index).getPrice();
	   p.getPermits().remove(card);
	   p.setCoins(p.getCoins()+price);
	   super.getPlayer().setCoins(super.getPlayer().getCoins()-price);
	   
   }
	/**
	 * purchase an assistant
	 * @param market is the market
	 * @param index is the index of the product
	 */
   public void purchase(Assistant a,Market market, int index){ 
	   super.getPlayer().setAssistants(super.getPlayer().getAssistants() + 1);
	   Player p=this.matchOwner(market.getShowcase().get(index).getOwner().getId());
	   int price=market.getShowcase().get(index).getPrice();
	   p.setAssistants(p.getAssistants() - 1);
	   p.setCoins(p.getCoins()+price);
	   super.getPlayer().setCoins(super.getPlayer().getCoins()-price);
	  
   }
   
   
   public Player matchOwner(int id){
	   for(Player p:this.players){
		   if(p.getId()==id) return p;
	   }
	return null;
	   
   }

}
