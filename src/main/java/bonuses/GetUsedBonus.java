package bonuses;

import gamemanagement.Player;
import mappaeoggetti.PermissionCard;

public class GetUsedBonus extends Bonus {

	private PermissionCard permissionCard;
	
	/**
	 * Method that for the permission card the player has choose, get the bonus of the card and give it to the player
	 */
	@Override
	public void act(Player p) {

		for(Bonus bonus: permissionCard.getBonus()){
			bonus.act(p);
		}
	}

	/**
	 * 
	 * @return the permission card the player has choose
	 */
	public PermissionCard getPermissionCard() {
		return permissionCard;
	}

	/**
	 * 
	 * @param permissionCard set the permission card choose by the player
	 */
	public void setPermissionCard(PermissionCard permissionCard) {
		this.permissionCard = permissionCard;
	}
}
