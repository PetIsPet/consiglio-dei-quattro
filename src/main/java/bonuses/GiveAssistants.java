package bonuses;

import gamemanagement.Player;

public class GiveAssistants extends Bonus{
	
	private int numberOfAssistants;
	private String name="Assistants Bonus";
	
	/**
	 * 
	 * @param n is the number of assistants
	 */
	public GiveAssistants(int n){
		
		this.numberOfAssistants=n;
		
	}
	
	
	/**
	 * 
	 * gives numberOfAssistants assistants to the player
	 */
	@Override
	public void act (Player player){
		
		player.setAssistants(player.getAssistants()+this.numberOfAssistants);
	}
	
	/**
	 * 
	 * @return the number of assistants to give
	 */

	public int getNumberOfAssistants() {
		return numberOfAssistants;
	}
	
	/**
	 * 
	 * 
	 * @param numberOfAssistants the amount of the assistants to set
	 */

	public void setNumberOfAssistants(int numberOfAssistants) {
		this.numberOfAssistants = numberOfAssistants;
	}
	
	/**
	 * 
	 * @return the name of the bonus
	 */
	@Override
	public String getName() {
		return name;
	}

	
	/**
	 * 
	 * @param name the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}