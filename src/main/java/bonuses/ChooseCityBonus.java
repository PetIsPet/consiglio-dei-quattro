package bonuses;

import java.util.ArrayList;

import gamemanagement.Player;
import mappaeoggetti.City;

public class ChooseCityBonus extends Bonus {

	private ArrayList<City> cities = new ArrayList<City>();
	private boolean flag = true;
	
	/**
	 * The Nobility bonus that allow the player to choose a bonus from a city in which he has already built
	 * @param flag
	 */
	public ChooseCityBonus(Boolean flag){
		
		this.flag = flag;
	}

	/**
	 * Method that check if the player that would obtain a bonus from a city thanks to the nobility scale is different from a nobilityPoints bonus
	 * @param player the player who wants to obtain the bonus
	 * @param city the city in which the player has built
	 * @return if the bonus of the city choose by the player is a nobilityPoints bonus or not
	 */
	public boolean controlCity(Player player, City city){
		
		if(player.hasBuilt(city)){
			
			if(!isNobilityPoints(city.getCityToken().getBonuses())){
				 
				return true;
			}
				
		}
			return false;
		
	}
	/**
	 * A method that check if the bonus is a NobilityPoints bonus
	 * @param bonuses the list in which are located the bonus to check
	 * @return if the list contains a nobilityPoints bonus or not
	 */
	public boolean isNobilityPoints (ArrayList<Bonus> bonuses){
		for (Bonus b:bonuses){
			if (b.getClass()==GiveNobilityPoints.class)
				return true;
		}
		return false;
		
	}
	
	/**
	 * Method that give the player the bonus he has choose 
	 */
	public void act(Player player){
			
			for(City city: this.cities){
				for(Bonus b:city.getCityToken().getBonuses()){
					b.act(player);
				}
			}
		
	}

	/**
	 * 
	 * @return the flag
	 */
	public boolean isFlag() {
		return flag;
	}

	/**
	 * 
	 * @param flag set the flag
	 */
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	/**
	 * 
	 * @return the list of cities
	 */
	public ArrayList<City> getCities() {
		return cities;
	}

	/**
	 * 
	 * @param cities set the list of cities
	 */
	public void setCities(ArrayList<City> cities) {
		this.cities = cities;
	}
}
