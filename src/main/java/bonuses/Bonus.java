 package bonuses;

import java.util.ArrayList;

import actions.PrimaryAction;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.CardDeck;
import mappaeoggetti.City;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import servercompleto.ServerView;

public abstract class Bonus {
	
	private String name;


	/**
	 * 
	 * @param p is the player who receives the bonus
	 */
	public abstract void act(Player p);

	/**
	 * 
	 * @return the name of the bonus
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name is the name of the bonus
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 
	 * @param deck the Politic deck to set
	 */
	public void setPoliticDeck(CardDeck deck){
		
	}
	/**
	 * 
	 * @param deck the Permission deck to set
	 */
	public void setDeck(PermissionDeck deck){
		
	}
	/**
	 * 
	 * @param decks the deck to set
	 * @param s the server view to set
	 */
	public void setNobilityStuff(ArrayList<PermissionDeck> decks, ServerView s,TurnManager t){
		
	}
	/**
	 * 
	 * @param choice the choice to make in Permission deck
	 */
	public void setChoice(int choice){
		
	}
	
	public void setAction(PrimaryAction p){
	}
	
	public void setPermit(PermissionCard c){
		
	}
	
	public void setCities(ArrayList<City> cities){
		
	}

}