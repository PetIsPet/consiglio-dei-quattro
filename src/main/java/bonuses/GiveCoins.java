package bonuses;

import gamemanagement.Player;

public class GiveCoins extends Bonus {
	private int numberOfCoins=0;
	private String name="Coins Bonus";

	/**
	 * 
	 * @param numberOfCoins is the number of given coins
	 */
	public GiveCoins(int numberOfCoins){
		
		this.numberOfCoins=numberOfCoins;
	}
	
	
	/**
	 * 
	 * gives numberOfCoins coins to the player
	 */
	@Override
	public void act (Player player){
		
		player.setCoins(player.getCoins()+numberOfCoins);
	}
	
	/**
	 * 
	 * 
	 * @return the number of the coins to give
	 */

	public int getNumberOfCoins() {
		return numberOfCoins;
	}
	
	/**
	 * 
	 * 
	 * @param numberOfCoins to set
	 */

	public void setNumberOfCoins(int numberOfCoins) {
		this.numberOfCoins = numberOfCoins;
	}

	/**
	 * 
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}