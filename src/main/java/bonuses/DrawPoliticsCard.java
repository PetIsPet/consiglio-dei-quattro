package bonuses;


import gamemanagement.Player;
import mappaeoggetti.CardDeck;

public class DrawPoliticsCard extends Bonus {

	private CardDeck deck;
	
	/**
	 * The method that allow the player to draw a politics card thanks to a bonus
	 */
	@Override
	public void act(Player p) {
		
		deck.drawCard(p);

	}

	/**
	 * 
	 * @return the deck where the player draw a PoliticCard
	 */
	public CardDeck getDeck() {
		return deck;
	}

	/**
	 * 
	 * @param deck set the deck from where the player draw the PoliticCard
	 */
	@Override
	public void setPoliticDeck(CardDeck deck) {
		this.deck = deck;
	}
	


}