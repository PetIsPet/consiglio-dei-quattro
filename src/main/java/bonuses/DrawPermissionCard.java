package bonuses;

import gamemanagement.Player;
import mappaeoggetti.PermissionDeck;

public class DrawPermissionCard extends Bonus {

	private PermissionDeck deck;
	private int choice;
	
	/**
	 * The method that allows the player to draw a permission card. The method also replace the card with a new discover PermissionCard
	 */
	@Override
	public void act(Player player) {
		
		player.getPermits().add(this.deck.getDiscoveredCards()[choice]);
		
		this.deck.getDiscoveredCards()[choice]=this.deck.replaceCard();
	}

	/**
	 * 
	 * @return the deck from where the player draw a PermissionCard
	 */
	public PermissionDeck getDeck() {
		return deck;
	}

	/**
	 * 
	 * @param deck set the deck from where the player draw the PermissionCard
	 */
	@Override
	public void setDeck(PermissionDeck deck) {
		this.deck = deck;
	}

	@Override
	public void setChoice(int choice){
		this.choice=choice;
	}

	public int getChoice() {
		return choice;
	}
}
