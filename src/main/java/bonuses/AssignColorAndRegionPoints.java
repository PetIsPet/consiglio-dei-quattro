package bonuses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import gamemanagement.Player;
import mappaeoggetti.City;

import mvc.Game;

public class AssignColorAndRegionPoints {
	
	private Game game;
	private ArrayList<Player> constructionOrder = new ArrayList<>();
	private boolean flag = false;
	private Map<Player, Object> map = new HashMap<>();
	private ArrayList<Object> colorType = new ArrayList<>();

	/**
	 * AssignColorAndRegionPoints is a method that assign at the end of the game Points if a player
	 * has built in cities whit the same color, or in all city of a specific region, as the first player of the game
	 * @param game the game in which player are playing
	 */
	public AssignColorAndRegionPoints(Game game){
		
		this.game = game;
		
	}
	
	/**
	 * AssignColorPoints is the method that assign at the end of the game Points if a player has built in cities whit the same color
	 * @param player is the player to be controlled
	 * @param city is the city in which the player has built last time
	 */
	public void AssignColorPoints(Player player, City city){
		
		if(player.hasBuilt(city)){
			
			for(City c: game.getCities()){
					
				if(c.getColor()==city.getColor()){
					
						if(!player.hasBuilt(c)){
							
							this.flag = true;
								break;
						}						
					}	
			}
			
			if(flag==false){
				
				if(!colorType.contains(city.getColor())){
					
					colorType.add(city.getColor());
					constructionOrder.add(player);
					map.put(player, city.getColor());
					
				}
				
			}
			
		}
	}
	
	/**
	 * AssignTypePoints is the method that assign at the end of the game Points if a player has built in whole cities of a specific region of the map
	 * @param player is the player to be controlled
	 * @param city is the city in wich the player has built last time
	 */
	public void AssignTypePoints(Player player, City city){
		
			if(player.hasBuilt(city)){
						
						for(City c: game.getCities()){
								
							if(c.getType()==city.getType()){
								
									if(!player.hasBuilt(c)){
										
										this.flag = true;
											break;
									}						
								}	
						}
						
						if(flag==false){
							
							if(!colorType.contains(city.getType())){
								
								colorType.add(city.getType());
								constructionOrder.add(player);
								map.put(player, city.getType());
								
							}
							
						}
						
					}
					
				}

	/**
	 * 
	 * @return the game which is set
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * 
	 * @param game the game that has to be set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * 
	 * @return the flag
	 */
	public boolean isFlag() {
		return flag;
	}

	/**
	 * 
	 * @param flag the flag to set
	 */
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	/**
	 * 
	 * @return the array list of object
	 */
	public ArrayList<Object> getColorType() {
		return colorType;
	}

	/**
	 * 
	 * @param colorType the array list to be set
	 */
	public void setColorType(ArrayList<Object> colorType) {
		this.colorType = colorType;
	}

	/**
	 * 
	 * @return the array list of player
	 */
	public ArrayList<Player> getConstructionOrder() {
		return constructionOrder;
	}

	/**
	 * 
	 * @param constructionOrder the array list of player
	 */
	public void setConstructionOrder(ArrayList<Player> constructionOrder) {
		this.constructionOrder = constructionOrder;
	}

	/**
	 * 
	 * @return the hash map where are set the player and the object used
	 */
	public Map<Player, Object> getMap() {
		return map;
	}

	/**
	 * 
	 * @param map the hash map to set
	 */
	public void setMap(Map<Player, Object> map) {
		this.map = map;
	}
	
	
}
