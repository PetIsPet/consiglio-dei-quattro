package bonuses;

import gamemanagement.Player;

public class GivePoints extends Bonus{
	
	private int numberOfPoints;
	private String name="Points Bonus";

	/**
	 * 
	 * @param n is the number of givenpoints
	 */
	public GivePoints(int n){
		
		this.numberOfPoints=n;
	}
	
	/**
	 * Method that set the Points to the player
	 * @param the player to give the points to
	 */
	@Override
	public void act (Player player){
		
		player.setPoints(player.getPoints()+this.numberOfPoints);
	}
	
	/**
	 * 
	 * @return the number of points to give to the player
	 */

	public int getNumberOfPoints() {
		return numberOfPoints;
	}
	
	/**
	 * 
	 * @param numberOfPoints to set
	 */

	public void setNumberOfPoints(int numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}
	
	/**
	 * 
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}