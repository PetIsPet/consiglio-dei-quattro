package bonuses;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import actions.PrimaryAction;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import mappaeoggetti.City;
import mappaeoggetti.PermissionCard;
import mappaeoggetti.PermissionDeck;
import servercompleto.ServerView;

public class GiveNobilityPoints extends Bonus{

	private int numberOfPoints;
	private String name="Nobility Points Bonus";
	private Map<Integer, ArrayList<Bonus>> nobilityScale;
	private ArrayList<PermissionDeck> decks=new ArrayList<>();
	private ServerView client;
	private TurnManager controller;
	
	/**
	 * 
	 * @param n is the number of given nobility points
	 * @param nobilityScale2 is the nobility "scale"
	 */
	public GiveNobilityPoints(int n, Map<Integer, ArrayList<Bonus>> nobilityScale2){
		
		this.numberOfPoints=n;
		this.nobilityScale=nobilityScale2;
	}
	/**
	 * Method that give the Nobility Points to the player, refreshing the nobility scale
	 * @param player the player that uses the nobility points bonus
	 */
	@Override
	public void act (Player player){
		
		
		int pointsBefore=player.getNobilityPoints();
		int pointsAfter=pointsBefore+this.numberOfPoints;
		if(this.getNobilityBonus(nobilityScale, pointsAfter)!=null){
			for(Bonus b:this.getNobilityBonus(nobilityScale, pointsAfter)){
				if(b.getClass()==DrawPermissionCard.class){
					
					try {
						int deckId=this.client.getChosenDeck();
						int cardId=this.client.getPermissionCard(this.discoveredCardsToString(this.decks.get(deckId)));
						b.setDeck(this.decks.get(deckId));
						b.setChoice(cardId);
						b.act(player);
						this.decks.get(deckId).getDiscoveredCards()[cardId]=this.decks.get(deckId).replaceCard();
					} catch (RemoteException e) {
						
						
					}
				}
				 if(b.getClass()==DoAnotherPrimaryActionBonus.class){
					   try {
							PrimaryAction actionBonus=controller.parsePrimaryAction(client.askPrimaryAction(), player, client);
							
							b.setAction(actionBonus);
						} catch (RemoteException e) {
							
						System.out.println("Connection lost");
				    }
				   }
				 if(b.getClass()==ChooseCityBonus.class){
					 ArrayList<String> cities=new ArrayList<>();
					 for(City c:controller.getGame().getCities()){
						 if(player.hasBuilt(c)){
							 cities.add(c.getName());
						 }
					 }
					 try {
						String s1=client.chooseCity(cities);
						
						for(City n:controller.getGame().getCities()){
							if(s1.equals(n.getName())) {
								ArrayList<City> cityTemp=new ArrayList<>();
								cityTemp.add(n);
								b.setCities(cityTemp);
							}
						}
					} catch (RemoteException e) {
						System.out.println("Connection lost");
					}
				 }
				 if(b.getClass()==DrawPermissionCard.class){
					try {
						b.setDeck(controller.getGame().getDecks().get(client.getChosenBalcony()));
					} catch (RemoteException e) {
						System.out.println("Connection lost");
					}
				 }
				 if(b.getClass()==GetUsedBonus.class){
				try {
					int card=client.askYourPermissionCard(controller.permitsToString(controller.getGame().getUsedPermits()));
					b.setPermit(controller.getGame().getUsedPermits().get(card));
					
				} catch (RemoteException e) {
					System.out.println("Connection lost");
				}
				 }
				b.act(player);
			}
		
		}
		player.setNobilityPoints(pointsAfter);
	}
	
	
	/**
	 * a method to transform the discovered cards of the decks into strings
	 */
	public ArrayList<String> discoveredCardsToString(PermissionDeck deck){
		
		ArrayList<String> discoveredCards=new ArrayList<>();
		for(int i=0; i<2; i++){
			String card="";
			card="PERMITS: ";
			for(City n:deck.getDiscoveredCards()[i].getBuildPermissions()){
				card=card+n.getName()+" ";
			}
			card=card+"BONUS: ";
			for(Bonus m:deck.getDiscoveredCards()[i].getBonus()){
				card=card+m.getName()+" ";
			}
			card=card+"ID: "+i;
			card=card+"#"+deck.getDiscoveredCards()[i].getId();
			discoveredCards.add(card);
		}
		return discoveredCards;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Method that sets a deck and the Server view that are used for some NobilityScale bonus
	 */
	@Override
	public void setNobilityStuff(ArrayList<PermissionDeck> decks, ServerView s,TurnManager t){
		this.decks=decks;
		this.client=s;
		this.controller=t;
	}
	
	/**
	 * 
	 * @param nobilityScale2 is the nobility "scale"
	 * @param finalPoint the point where the scale is called to be positioned
	 * @return the position on the nobility scale
	 */
	public List<Bonus> getNobilityBonus(Map<Integer, ArrayList<Bonus>> nobilityScale2,Integer finalPoint){
		
		return nobilityScale2.get(finalPoint);
	}

	/**
	 * 
	 * @return the number of the points to give
	 */
	public int getNumberOfPoints() {
		return numberOfPoints;
	}

	/**
	 * 
	 * @param numberOfPoints numberOfPoints to set
	 */
	public void setNumberOfPoints(int numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}
	
	/**
	 * 
	 * @return the decks setted
	 */
	public ArrayList<PermissionDeck> getDecks() {
		return decks;
	}
	
	/**
	 * 
	 * @param decks the decks that has to be sets
	 */
	public void setDecks(ArrayList<PermissionDeck> decks) {
		this.decks = decks;
	}
}