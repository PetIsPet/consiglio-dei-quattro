package bonuses;


import java.rmi.RemoteException;

import actions.PrimaryAction;
import exceptions.InvalidInputException;
import exceptions.NotEnoughResourcesException;
import gamemanagement.Player;
import gamemanagement.TurnManager;
import servercompleto.ServerView;

public class DoAnotherPrimaryActionBonus extends Bonus {

	private PrimaryAction action;
	
	
	/**
	 * Method that allow the player to do another Primary action thanks to a NobilityScale's bonus
	 */
	@Override
	public void act(Player p) {

		
			try {
				action.doPrimaryAction();
			} catch (NotEnoughResourcesException | InvalidInputException e) {
				
				
			}
		
			
		
		
	}

	/**
	 * 
	 * @return the primary action choose
	 */
	public PrimaryAction getAction() {
		return action;
	}

	/**
	 * 
	 * @param action the primary action that must be set
	 */
	@Override
	public void setAction(PrimaryAction action) {
		this.action = action;
	}

}
