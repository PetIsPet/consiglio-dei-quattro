To launch the game you must:

-Launch the Broker as Java application

-After the launch you must choose if you want to play using CLI or GUI, typing it on the console, then you have to
	choose a name for the new game and typr a map configuration (there are 8 possible configuration).

-Then launch the Subscribers as Java application: you can choose between SubscriberRMI and SubscriberSocket,
	and choose the Game you want to play. 
	The game will automatically start 20 seconds after the registration of 2 players.

-Enjoy the game!